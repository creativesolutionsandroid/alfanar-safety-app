package com.cs.alfanar.utils

import android.content.Context
import android.content.pm.PackageInfo


fun Context.appVersion() : String {
    val pInfo: PackageInfo = getPackageManager().getPackageInfo(getPackageName(), 0)
    val version = pInfo.versionName
    return version
}

fun Context.appVersionCode() : Int {
    val pInfo: PackageInfo = getPackageManager().getPackageInfo(getPackageName(), 0)
    val versionCode = pInfo.versionCode
    return versionCode
}