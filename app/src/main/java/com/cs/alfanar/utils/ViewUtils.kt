package com.cs.alfanar.utils

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ScrollView
import android.widget.Toast
import android.transition.ChangeBounds
import android.transition.Transition
import java.text.DecimalFormat


val distanceFormat = DecimalFormat("##,##,##0.0")

fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun scrollTo(view: View, scrollView: ScrollView) {
    val scrollTo: Int = (view.getParent()
        .getParent() as View).top + view.getTop()
    scrollView.smoothScrollTo(0, scrollTo)
    view.requestFocus()
}

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

 fun enterTransition(): Transition? {
    val bounds = ChangeBounds()
    bounds.setDuration(1000)
    return bounds
}

fun returnTransition(): Transition? {
    val bounds = ChangeBounds()
    bounds.setInterpolator(DecelerateInterpolator())
    bounds.setDuration(1000)
    return bounds
}

