package com.cs.alfanar.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.cs.alfanar.R
import com.gmail.samehadar.iosdialog.CamomileSpinner
import com.gmail.samehadar.iosdialog.utils.DialogUtils

var customDialog: AlertDialog? = null

fun Context.showLoadingDialog(context: Activity) {
    val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
    // ...Irrelevant code for customizing the buttons and title
    val inflater = context.layoutInflater
    val layout: Int = R.layout.dialog_loading
    val dialogView = inflater.inflate(layout, null)
    dialogBuilder.setView(dialogView)
    dialogBuilder.setCancelable(false)
    val spinner1 =
        dialogView.findViewById<View>(R.id.spinner1) as CamomileSpinner
    spinner1.start()
    spinner1.recreateWithParams(
        context,
        DialogUtils.getColor(context, R.color.black),
        120,
        true
    )
    customDialog = dialogBuilder.create()
    customDialog?.show()
    val lp = WindowManager.LayoutParams()
    val window: Window? = customDialog?.getWindow()
    window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    lp.copyFrom(window?.attributes)
    //This makes the progressDialog take up the full width
    val display = context.windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    val screenWidth: Int = size.x
    val d = screenWidth * 0.45
    lp.width = d.toInt()
    lp.height = WindowManager.LayoutParams.WRAP_CONTENT
    window?.attributes = lp
}

fun Context.closeLoadingDialog() {
    customDialog?.dismiss()
}
