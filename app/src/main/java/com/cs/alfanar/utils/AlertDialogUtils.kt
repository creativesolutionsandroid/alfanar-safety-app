package com.cs.alfanar.utils

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import com.cs.alfanar.R


fun showOneButtonAlertDialog(
    descriptionStr: String?,
    buttonStr: String?,
    context: Activity,
    intent: Intent?
) {
    var customDialog: AlertDialog? = null
    val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
    // ...Irrelevant code for customizing the buttons and title
    val inflater = context.layoutInflater
    val layout: Int = R.layout.dialog_message
    val dialogView: View = inflater.inflate(layout, null)
    dialogBuilder.setView(dialogView)
    dialogBuilder.setCancelable(false)

    val desc = dialogView.findViewById(R.id.desc) as TextView
    val yes = dialogView.findViewById(R.id.pos_btn) as TextView
    val no = dialogView.findViewById(R.id.ngt_btn) as TextView
    val line: View = dialogView.findViewById(R.id.vert_line) as View

    no.visibility = View.GONE
    line.setVisibility(View.GONE)

    yes.text = buttonStr
    desc.text = descriptionStr

    customDialog = dialogBuilder.create()
    customDialog.show()
    val finalCustomDialog: AlertDialog? = customDialog

    yes.setOnClickListener( View.OnClickListener {
        if (intent != null) {
            context.startActivity(intent)
            context.finish()
        }
        finalCustomDialog?.dismiss()
    })

    val lp = WindowManager.LayoutParams()
    val window: Window? = customDialog?.getWindow()
    window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    lp.copyFrom(window?.getAttributes())
    //This makes the dialog take up the full width
    val display = context.windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    val screenWidth: Int = size.x
    val d = screenWidth * 0.85
    lp.width = d.toInt()
    lp.height = WindowManager.LayoutParams.WRAP_CONTENT
    window?.setAttributes(lp)
}

fun showTwoButtonsDialog(context: Activity, descriptionStr: String, positiveButtonStr: String, neagtiveButtonStr: String, intent: Intent?) {
    var customDialog: AlertDialog? = null
    val dialogBuilder =
        AlertDialog.Builder(context)

    val inflater = context.layoutInflater
    val layout: Int = R.layout.dialog_message
    val dialogView = inflater.inflate(layout, null)
    dialogBuilder.setView(dialogView)
    dialogBuilder.setCancelable(false)

    val title = dialogView.findViewById<View>(R.id.title) as TextView
    val desc = dialogView.findViewById<View>(R.id.desc) as TextView
    val yes = dialogView.findViewById<View>(R.id.pos_btn) as TextView
    val no = dialogView.findViewById<View>(R.id.ngt_btn) as TextView

    desc.text = descriptionStr
    yes.text = positiveButtonStr
    no.text = neagtiveButtonStr
    customDialog = dialogBuilder.create()
    customDialog.show()
    val finalCustomDialog = customDialog

    yes.setOnClickListener {
        finalCustomDialog!!.dismiss()
        context.startActivity(intent!!)
    }

    no.setOnClickListener {
        finalCustomDialog!!.dismiss()
    }

    val lp = WindowManager.LayoutParams()
    val window = customDialog.window
    window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    lp.copyFrom(window.attributes)
    //This makes the dialog take up the full width
    val display = context.windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    val screenWidth = size.x
    val d = screenWidth * 0.85
    lp.width = d.toInt()
    lp.height = WindowManager.LayoutParams.WRAP_CONTENT
    window.attributes = lp
}