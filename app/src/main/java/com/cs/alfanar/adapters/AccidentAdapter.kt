package com.cs.alfanar.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import com.cs.alfanar.R
import com.cs.alfanar.model.accidentIncident.ChildItem

class AccidentAdapter (context: Context, @LayoutRes private val layoutResource: Int, var allRoles: List<ChildItem>) :
    ArrayAdapter<ChildItem>(context, layoutResource, allRoles) {

    private var mRoles: List<ChildItem> = allRoles

    override fun getCount(): Int {
        return mRoles.size
    }

    override fun getItem(p0: Int): ChildItem? {
        return mRoles.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        // Or just return p0
        return mRoles.get(p0).Id.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(layoutResource, parent, false) as TextView
        view.setTextSize(context.resources.getDimension(R.dimen.dropdown_textsize))
        view.text = mRoles[position].ActionName
        return view
    }
}