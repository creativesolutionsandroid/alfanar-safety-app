package com.cs.alfanar.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import com.cs.alfanar.R
import com.cs.alfanar.model.dailyReport.Data
import com.cs.alfanar.model.masterData.Project

class DailyReportProjectAdapter (context: Context, @LayoutRes private val layoutResource: Int, var allProjects: List<Data>) :
    ArrayAdapter<Data>(context, layoutResource, allProjects) {

    private var mProjects: List<Data> = allProjects

    override fun getCount(): Int {
        return mProjects.size
    }

    override fun getItem(p0: Int): Data? {
        return mProjects.get(p0)
    }

    override fun getItemId(p0: Int): Long {
         return p0.toLong()
//        return mProjects.get(p0).ProjectId.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(layoutResource, parent, false) as TextView
        view.setTextSize(context.resources.getDimension(R.dimen.dropdown_textsize))
        view.text = mProjects[position].ProjectNameEn
        return view
    }

}