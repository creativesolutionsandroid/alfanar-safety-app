package com.cs.alfanar.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import com.cs.alfanar.R
import com.cs.alfanar.model.masterData.Nationality

class NationalityAdapter (context: Context, @LayoutRes private val layoutResource: Int, var allNationalitys: List<Nationality>) :
    ArrayAdapter<Nationality>(context, layoutResource, allNationalitys) {

    private var mNationalitys: List<Nationality> = allNationalitys

    override fun getCount(): Int {
        return mNationalitys.size
    }

    override fun getItem(p0: Int): Nationality? {
        return mNationalitys.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        // Or just return p0
        return mNationalitys.get(p0).Id.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(layoutResource, parent, false) as TextView
        view.setTextSize(context.resources.getDimension(R.dimen.dropdown_textsize))
        view.text = mNationalitys[position].NameEn
        return view
    }

//    override fun getFilter(): Filter {
//        return object : Filter() {
//            override fun publishResults(charSequence: CharSequence?, filterResults: Filter.FilterResults) {
//                mNationalitys = filterResults.values as List<Nationality>
//                notifyDataSetChanged()
//            }
//
//            override fun performFiltering(charSequence: CharSequence?): Filter.FilterResults {
//                val queryString = charSequence?.toString()?.toLowerCase()
//
//                val filterResults = Filter.FilterResults()
//                filterResults.values = if (queryString==null || queryString.isEmpty())
//                    allNationalitys
//                else
//                    allNationalitys.filter {
//                        it.NameEn.toLowerCase().contains(queryString)
//                    }
//                return filterResults
//            }
//        }
//    }
}