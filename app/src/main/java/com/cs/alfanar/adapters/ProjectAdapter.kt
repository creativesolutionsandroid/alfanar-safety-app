package com.cs.alfanar.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import com.cs.alfanar.R
import com.cs.alfanar.model.masterData.Project

class ProjectAdapter (context: Context, @LayoutRes private val layoutResource: Int, var allProjects: List<Project>) :
    ArrayAdapter<Project>(context, layoutResource, allProjects) {

    private var mProjects: List<Project> = allProjects

    override fun getCount(): Int {
        return mProjects.size
    }

    override fun getItem(p0: Int): Project? {
        return mProjects.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        // Or just return p0
        return mProjects.get(p0).Id.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(layoutResource, parent, false) as TextView
        view.setTextSize(context.resources.getDimension(R.dimen.dropdown_textsize))
        view.text = mProjects[position].ProjectNameEn
        return view
    }

}