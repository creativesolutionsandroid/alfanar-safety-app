package com.cs.alfanar.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import com.cs.alfanar.R
import com.cs.alfanar.model.weeklyReport.Checklist

class WeeklyReportChecklistAdapter (context: Context, @LayoutRes private val layoutResource: Int, var allProjects: List<Checklist>) :
    ArrayAdapter<Checklist>(context, layoutResource, allProjects) {

    private var mProjects: List<Checklist> = allProjects

    override fun getCount(): Int {
        return mProjects.size
    }

    override fun getItem(p0: Int): Checklist? {
        return mProjects.get(p0)
    }

    override fun getItemId(p0: Int): Long {
         return p0.toLong()
//        return mProjects.get(p0).ProjectId.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(layoutResource, parent, false) as TextView
        view.setTextSize(context.resources.getDimension(R.dimen.dropdown_textsize))
        view.text = mProjects[position].CheckListNameEn
        return view
    }

}