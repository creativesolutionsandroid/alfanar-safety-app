package com.cs.alfanar.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cs.alfanar.R;


public class SideMenuAdapter extends ArrayAdapter<String> {

    Context context;
    String[] menuItems;
    Integer[] menuImages;
    int layoutResourceId;
    int selectedPosition;
    private Typeface regular;

    public SideMenuAdapter(Context context, int layoutResourceId, String[] menuItems, Integer[] menuImages, int selectedPosition){
        super(context, layoutResourceId, menuItems);
        this.context = context;
        this.menuItems = menuItems;
        this.menuImages = menuImages;
        this.layoutResourceId = layoutResourceId;
        this.selectedPosition = selectedPosition;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        if (row == null) {
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();

            holder.textTitle = (TextView) row.findViewById(R.id.menu_title);
            holder.menuImage = (ImageView) row.findViewById(R.id.menu_image);
            holder.menuItemLayout = (LinearLayout) row.findViewById(R.id.menu_item_layout);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        holder.textTitle.setText(menuItems[position]);
        holder.menuImage.setImageDrawable(context.getResources().getDrawable(menuImages[position]));

        return row;
    }

    static class ViewHolder {
        TextView textTitle;
        ImageView menuImage;
        LinearLayout menuItemLayout;
    }
}
