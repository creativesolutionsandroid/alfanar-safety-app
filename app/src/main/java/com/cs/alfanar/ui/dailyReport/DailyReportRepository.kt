package com.cs.alfanar.ui.dailyReport

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.dailyReport.GetProjectsData
import com.cs.alfanar.model.emergencyNumbers.EmergencyNumbersResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class DailyReportRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getProjectDataForDailyReport(inputJson : String): GetProjectsData {
        return apiRequest { api.getProjectDataForDailyReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun insertDailyReport(inputJson : String): BasicResponse {
        return apiRequest { api.insertDailyReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

}