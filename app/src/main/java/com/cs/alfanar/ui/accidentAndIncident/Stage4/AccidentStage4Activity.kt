package com.cs.alfanar.ui.accidentAndIncident.Stage4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.adapters.AccidentAdapter
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentInvestigationEntity
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.accidentIncident.ChildItem
import com.cs.alfanar.model.accidentIncident.Data
import com.cs.alfanar.model.accidentIncident.Master
import com.cs.alfanar.ui.accidentAndIncident.Stage3.AccidentStage3ViewModel
import com.cs.alfanar.ui.accidentAndIncident.Stage3.AccidentStage3ViewModelFactory
import com.cs.alfanar.ui.accidentAndIncident.Summary.AccidentIncidentSummaryActivity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_accident_stage1.*
import kotlinx.android.synthetic.main.activity_accident_stage2.*
import kotlinx.android.synthetic.main.activity_accident_stage3.*
import kotlinx.android.synthetic.main.activity_accident_stage3.date
import kotlinx.android.synthetic.main.activity_accident_stage4.*
import kotlinx.android.synthetic.main.activity_weekly_report_stage3.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*

class AccidentStage4Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: AccidentStage4ViewModelFactory by instance()

    private lateinit var viewModel: AccidentStage4ViewModel

//    private val TAG : String = this::class.java.simpleName
    val KEY_DATA: String = "DATA_KEY"
    private lateinit var masterData: Data

    private var injuryTypeList: MutableList<ChildItem> = mutableListOf()
    private var contactTypeList: MutableList<ChildItem> = mutableListOf()
    private var unsafeActList: MutableList<ChildItem> = mutableListOf()
    private var trafficAccidentsList: MutableList<ChildItem> = mutableListOf()
    private var rootCauseOfUnsafeActList: MutableList<ChildItem> = mutableListOf()
    private var unsafeConditionsList: MutableList<ChildItem> = mutableListOf()
    private var rootCauseOfUnsafeConditionsList: MutableList<ChildItem> = mutableListOf()
    private var suggestedControlMeasuresList: MutableList<ChildItem> = mutableListOf()
    private var projectData: List<AccidentInvestigationEntity> = mutableListOf()
    private var safetyMeasureOtherData: List<AccidentIncidentEntity> = mutableListOf()
    val KEY_BACK_ACTION : String = "KEY_BACK_ACTION"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accident_stage4)

        viewModel = ViewModelProvider(this, factory).get(AccidentStage4ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Data
        masterData = Data!!

        filterData(masterData.MasterList)
        fetchDataFromDb()
        addTextWatcher()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onSubmitClicked(view: View) {
        lifecycleScope.launch {
            projectData = viewModel.getData()
            safetyMeasureOtherData = viewModel.getSafetyMeasureOther()

            if (projectData.size > 0) {
                var idsList: MutableList<Int> = mutableListOf()
                for (i in 0 until projectData.size) {
                    idsList.add(projectData[i].typeId)
                }

                if (!idsList.contains(INJURY_TYPE))
                    showOneButtonAlertDialog(getString(R.string.please_select_injury_type), getString(R.string.Ok),
                        this@AccidentStage4Activity, null)
                else if (!idsList.contains(CONTACT_TYPE))
                    showOneButtonAlertDialog(getString(R.string.please_select_contact_type), getString(R.string.Ok),
                        this@AccidentStage4Activity, null)
                else if (!idsList.contains(UNSAFE_ACT))
                    showOneButtonAlertDialog(getString(R.string.please_select_unsafe_act), getString(R.string.Ok),
                        this@AccidentStage4Activity, null)
                else if (!idsList.contains(TRAFFIC_ACCIDENTS))
                    showOneButtonAlertDialog(getString(R.string.please_select_traffic_accident), getString(R.string.Ok),
                        this@AccidentStage4Activity, null)
                else if (!idsList.contains(ROOT_CAUSE_OF_UNSAFE_ACT))
                    showOneButtonAlertDialog(getString(R.string.please_select_root_cause_of_unsafe_act), getString(R.string.Ok),
                        this@AccidentStage4Activity, null)
                else if (!idsList.contains(UNSAFE_CONDITIONS))
                    showOneButtonAlertDialog(getString(R.string.please_select_unsafe_condition), getString(R.string.Ok),
                        this@AccidentStage4Activity, null)
                else if (!idsList.contains(ROOT_CAUSE_OF_UNSAFE_CONDITIONS))
                    showOneButtonAlertDialog(getString(R.string.please_select_root_cause_of_unsafe_condition), getString(R.string.Ok),
                        this@AccidentStage4Activity, null)
                else if (!idsList.contains(SUGGESTED_CONTROL_MEASURES)) {
                    showOneButtonAlertDialog(getString(R.string.please_select_suggested_control_measures), getString(R.string.Ok),
                        this@AccidentStage4Activity, null)
                }
                else if (idsList.contains(SUGGESTED_CONTROL_MEASURES_OTHER) &&
                    safetyMeasureOtherData[0].SafetyMeasureOther.equals("")) {
                    showOneButtonAlertDialog(getString(R.string.please_enter_other_suggested_control_measures), getString(R.string.Ok),
                        this@AccidentStage4Activity, null)
                }
                else {
                    insertAccidentDataApi()
//                    prepareInputJsonToGetMasterData()
                }
            }
            else {
                showOneButtonAlertDialog(getString(R.string.please_select_injury_type), getString(R.string.Ok),
                    this@AccidentStage4Activity, null)
            }
        }
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            safetyMeasureOtherData = viewModel.getSafetyMeasureOther()
            if (projectData.size > 0) {
                for (i in 0 until projectData.size) {
                    if (projectData[i].typeId == INJURY_TYPE)
                        spinner_injury_type.setText(projectData[i].selectName)
                    else if (projectData[i].typeId == CONTACT_TYPE)
                        spinner_contact_type.setText(projectData[i].selectName)
                    else if (projectData[i].typeId == UNSAFE_ACT)
                        spinner_unsafe_act.setText(projectData[i].selectName)
                    else if (projectData[i].typeId == TRAFFIC_ACCIDENTS)
                        spinner_traffic_accident.setText(projectData[i].selectName)
                    else if (projectData[i].typeId == ROOT_CAUSE_OF_UNSAFE_ACT)
                        spinner_root_cause.setText(projectData[i].selectName)
                    else if (projectData[i].typeId == UNSAFE_CONDITIONS)
                        spinner_unsafe_conditions.setText(projectData[i].selectName)
                    else if (projectData[i].typeId == ROOT_CAUSE_OF_UNSAFE_CONDITIONS)
                        spinner_root_cause_unsafe_conditions.setText(projectData[i].selectName)
                    else if (projectData[i].typeId == SUGGESTED_CONTROL_MEASURES) {
                        spinner_control_measures.setText(projectData[i].selectName)
                        if (projectData[i].selectId == SUGGESTED_CONTROL_MEASURES_OTHER) {
                            suggested_other.visibility = View.VISIBLE
                            suggested_other.setText(safetyMeasureOtherData[0].SafetyMeasureOther)
                        }
                    }
                }
            }
        }
    }

    fun filterData(data: List<Master>) {
        for (i in 0 until data.size) {
            if (data[i].Id == INJURY_TYPE) {
                for (j in 0 until data[i].ChildItem.size) {
                    injuryTypeList.add(data[i].ChildItem[j])
                }
            }
            else if (data[i].Id == CONTACT_TYPE) {
                for (j in 0 until data[i].ChildItem.size) {
                    contactTypeList.add(data[i].ChildItem[j])
                }
            }
            else if (data[i].Id == UNSAFE_ACT) {
                for (j in 0 until data[i].ChildItem.size) {
                    unsafeActList.add(data[i].ChildItem[j])
                }
            }
            else if (data[i].Id == TRAFFIC_ACCIDENTS) {
                for (j in 0 until data[i].ChildItem.size) {
                    trafficAccidentsList.add(data[i].ChildItem[j])
                }
            }
            else if (data[i].Id == ROOT_CAUSE_OF_UNSAFE_ACT) {
                for (j in 0 until data[i].ChildItem.size) {
                    rootCauseOfUnsafeActList.add(data[i].ChildItem[j])
                }
            }
            else if (data[i].Id == UNSAFE_CONDITIONS) {
                for (j in 0 until data[i].ChildItem.size) {
                    unsafeConditionsList.add(data[i].ChildItem[j])
                }
            }
            else if (data[i].Id == ROOT_CAUSE_OF_UNSAFE_CONDITIONS) {
                for (j in 0 until data[i].ChildItem.size) {
                    rootCauseOfUnsafeConditionsList.add(data[i].ChildItem[j])
                }
            }
            else if (data[i].Id == SUGGESTED_CONTROL_MEASURES) {
                for (j in 0 until data[i].ChildItem.size) {
                    suggestedControlMeasuresList.add(data[i].ChildItem[j])
                }
            }
        }

        setInjuryTypeSpinnerData()
        setContactTypeSpinnerData()
        setUnsafeActSpinnerData()
        setTrafficAccidentSpinnerData()
        setRootCauseForUnsafeActSpinnerData()
        setUnsafeConditionsSpinnerData()
        setRootCauseOfUnsafeConditionsSpinnerData()
        setSuggestedControlMeasuresSpinnerData()
    }

    fun setInjuryTypeSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                injuryTypeList
            )
        spinner_injury_type.setAdapter(accidentAdapter)

        spinner_injury_type.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = injuryTypeList.get(position).ActionName
                spinner_injury_type.setText(selectedItem)
                updateDataToDB("", injuryTypeList.get(position).ParentId, injuryTypeList.get(position).ActionName,
                    injuryTypeList.get(position).Id)
            }

        // Disabling keyboard for spinners
        spinner_injury_type.setInputType(InputType.TYPE_NULL)
    }

    fun setContactTypeSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                contactTypeList
            )
        spinner_contact_type.setAdapter(accidentAdapter)

        spinner_contact_type.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = contactTypeList.get(position).ActionName
                spinner_contact_type.setText(selectedItem)
                updateDataToDB("", contactTypeList.get(position).ParentId, contactTypeList.get(position).ActionName,
                    contactTypeList.get(position).Id)
            }

        // Disabling keyboard for spinners
        spinner_contact_type.setInputType(InputType.TYPE_NULL)
    }

    fun setUnsafeActSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                unsafeActList
            )
        spinner_unsafe_act.setAdapter(accidentAdapter)

        spinner_unsafe_act.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = unsafeActList.get(position).ActionName
                spinner_unsafe_act.setText(selectedItem)
                updateDataToDB("", unsafeActList.get(position).ParentId, unsafeActList.get(position).ActionName,
                    unsafeActList.get(position).Id)
            }

        // Disabling keyboard for spinners
        spinner_unsafe_act.setInputType(InputType.TYPE_NULL)
    }

    fun setTrafficAccidentSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                trafficAccidentsList
            )
        spinner_traffic_accident.setAdapter(accidentAdapter)

        spinner_traffic_accident.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = trafficAccidentsList.get(position).ActionName
                spinner_traffic_accident.setText(selectedItem)
                updateDataToDB("", trafficAccidentsList.get(position).ParentId, trafficAccidentsList.get(position).ActionName,
                    trafficAccidentsList.get(position).Id)
            }

        // Disabling keyboard for spinners
        spinner_traffic_accident.setInputType(InputType.TYPE_NULL)
    }

    fun setRootCauseForUnsafeActSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                rootCauseOfUnsafeActList
            )
        spinner_root_cause.setAdapter(accidentAdapter)

        spinner_root_cause.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = rootCauseOfUnsafeActList.get(position).ActionName
                spinner_root_cause.setText(selectedItem)
                updateDataToDB("", rootCauseOfUnsafeActList.get(position).ParentId, rootCauseOfUnsafeActList.get(position).ActionName,
                    rootCauseOfUnsafeActList.get(position).Id)
            }

        // Disabling keyboard for spinners
        spinner_root_cause.setInputType(InputType.TYPE_NULL)
    }

    fun setUnsafeConditionsSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                unsafeConditionsList
            )
        spinner_unsafe_conditions.setAdapter(accidentAdapter)

        spinner_unsafe_conditions.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = unsafeConditionsList.get(position).ActionName
                spinner_unsafe_conditions.setText(selectedItem)
                updateDataToDB("", unsafeConditionsList.get(position).ParentId, unsafeConditionsList.get(position).ActionName,
                    unsafeConditionsList.get(position).Id)
            }

        // Disabling keyboard for spinners
        spinner_unsafe_conditions.setInputType(InputType.TYPE_NULL)
    }

    fun setRootCauseOfUnsafeConditionsSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                rootCauseOfUnsafeConditionsList
            )
        spinner_root_cause_unsafe_conditions.setAdapter(accidentAdapter)

        spinner_root_cause_unsafe_conditions.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = rootCauseOfUnsafeConditionsList.get(position).ActionName
                spinner_root_cause_unsafe_conditions.setText(selectedItem)
                updateDataToDB("", rootCauseOfUnsafeConditionsList.get(position).ParentId, rootCauseOfUnsafeConditionsList.get(position).ActionName,
                    rootCauseOfUnsafeConditionsList.get(position).Id)
            }

        // Disabling keyboard for spinners
        spinner_root_cause_unsafe_conditions.setInputType(InputType.TYPE_NULL)
    }

    fun setSuggestedControlMeasuresSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                suggestedControlMeasuresList
            )
        spinner_control_measures.setAdapter(accidentAdapter)

        spinner_control_measures.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = suggestedControlMeasuresList.get(position).ActionName
                spinner_control_measures.setText(selectedItem)
                updateDataToDB("", suggestedControlMeasuresList.get(position).ParentId, suggestedControlMeasuresList.get(position).ActionName,
                    suggestedControlMeasuresList.get(position).Id)
                if (suggestedControlMeasuresList.get(position).Id == SUGGESTED_CONTROL_MEASURES_OTHER) {
                    suggested_other.visibility = View.VISIBLE
                    suggested_other.setText(safetyMeasureOtherData[0].SafetyMeasureOther)
                }
                else {
                    suggested_other.visibility = View.GONE
                    updateSafetyMeasureOther("")
                }
            }

        // Disabling keyboard for spinners
        spinner_control_measures.setInputType(InputType.TYPE_NULL)
    }

    fun addTextWatcher() {
        suggested_other.afterTextChanged { updateSafetyMeasureOther(suggested_other.text.toString().trim()) }

    }

    fun updateDataToDB(typeName: String, typeId: Int, selectName: String, selectId: Int) {
        lifecycleScope.launch {
            val dbSize = viewModel.getData()
            val listFromDB = viewModel.getDataBasedOnTypeId(typeId)
            if (listFromDB.size > 0) {
                viewModel.updateData(selectId, selectName, typeId)
            }
            else {
                val accidentInvestigationEntity = AccidentInvestigationEntity((dbSize.size + 1), typeName, typeId, selectName, selectId)
                viewModel.insert(accidentInvestigationEntity)
            }
        }
    }

    fun updateSafetyMeasureOther(name: String) {
        lifecycleScope.launch {
            viewModel.updateSafetyMeasureOther(name)
        }
    }

    fun prepareInputJsonToGetMasterData(): String {
        val parentObj = JSONObject()
        parentObj.put("ProjectId", safetyMeasureOtherData[0].projectId)
        parentObj.put("ReportDate", getFormattedDate(safetyMeasureOtherData[0].Date + " "+ safetyMeasureOtherData[0].Time))
        parentObj.put("SubmittedBy", UserPreferences(this).userId())

        val accidentDetailObj = JSONObject()
        accidentDetailObj.put("AccidentType", safetyMeasureOtherData[0].AccidentTypeId)
        accidentDetailObj.put("AccidentClass", safetyMeasureOtherData[0].AccidentClassId)
        accidentDetailObj.put("InjuredEmp", safetyMeasureOtherData[0].InjuredEmployeeName)
        accidentDetailObj.put("Company", safetyMeasureOtherData[0].CompanyContractor)
        accidentDetailObj.put("PartInjured", safetyMeasureOtherData[0].BodyPartInjuredId)
        accidentDetailObj.put("PartInjuredOther", safetyMeasureOtherData[0].BodyPartInjuredOther)
        accidentDetailObj.put("Task", safetyMeasureOtherData[0].Task)
        accidentDetailObj.put("FileIqamaNo", safetyMeasureOtherData[0].Iqama)
        accidentDetailObj.put("Age", safetyMeasureOtherData[0].Age)
        accidentDetailObj.put("ShiftTime", safetyMeasureOtherData[0].ShiftTimeId)
        accidentDetailObj.put("Nationality", safetyMeasureOtherData[0].NationalityId)
        accidentDetailObj.put("IncidentLocation", safetyMeasureOtherData[0].IncidentLocation)
        accidentDetailObj.put("IncidentTime", safetyMeasureOtherData[0].IncidentTime)
        accidentDetailObj.put("Cause", safetyMeasureOtherData[0].CauseOfIncidentId)
        accidentDetailObj.put("FirstAid", safetyMeasureOtherData[0].FirstAidtreatment)
        accidentDetailObj.put("Description", safetyMeasureOtherData[0].AccidentDescription)
        parentObj.put("AccidentDetail", accidentDetailObj)

        if (safetyMeasureOtherData[0].vehicleApplicable) {
            val accidentVehicleInfoObj = JSONObject()
            accidentVehicleInfoObj.put("Model", safetyMeasureOtherData[0].Model)
            accidentVehicleInfoObj.put("Plate", safetyMeasureOtherData[0].Plate)
            accidentVehicleInfoObj.put("VehicleId", safetyMeasureOtherData[0].VehicleID)
            accidentVehicleInfoObj.put("NoVehicle", safetyMeasureOtherData[0].NoOfVehicleInvolved)
            accidentVehicleInfoObj.put("Cause", safetyMeasureOtherData[0].CauseOfIncidentId)
            accidentVehicleInfoObj.put("Overtime", safetyMeasureOtherData[0].OverTime)
            val dateSpilt = safetyMeasureOtherData[0].ReportingDate.split("-")
            accidentVehicleInfoObj.put("ReportingDate", dateSpilt[2] + "-" + dateSpilt[1] + "-" + dateSpilt[0])
            if (safetyMeasureOtherData[0].OverTime) {
                accidentVehicleInfoObj.put("OvertimeHours", getFormattedTime(safetyMeasureOtherData[0].OverTimeHours))
            }
            else {
                accidentVehicleInfoObj.put("OvertimeHours", 0)
            }
            accidentVehicleInfoObj.put("ReportingTime", getFormattedTime(safetyMeasureOtherData[0].ReportingTime))
            accidentVehicleInfoObj.put("Hospital", safetyMeasureOtherData[0].Hospital)
            accidentVehicleInfoObj.put("Police", safetyMeasureOtherData[0].Police)
            accidentVehicleInfoObj.put("CivilDefence", safetyMeasureOtherData[0].CivilDefence)
            parentObj.put("AccidentVehicleInfo", accidentVehicleInfoObj)
        }

        val accidentAppendixObj = JSONObject()
        for (i in 0 until projectData.size) {
            if (projectData[i].typeId == INJURY_TYPE)
                accidentAppendixObj.put("InjuryType", projectData[i].selectId)
            else if (projectData[i].typeId == CONTACT_TYPE)
                accidentAppendixObj.put("ContactType", projectData[i].selectId)
            else if (projectData[i].typeId == UNSAFE_ACT)
                accidentAppendixObj.put("UnsafeActs", projectData[i].selectId)
            else if (projectData[i].typeId == TRAFFIC_ACCIDENTS)
                accidentAppendixObj.put("TrafficAccident", projectData[i].selectId)
            else if (projectData[i].typeId == ROOT_CAUSE_OF_UNSAFE_ACT)
                accidentAppendixObj.put("CauseUnsafeAct", projectData[i].selectId)
            else if (projectData[i].typeId == UNSAFE_CONDITIONS)
                accidentAppendixObj.put("UnsafeCondition", projectData[i].selectId)
            else if (projectData[i].typeId == ROOT_CAUSE_OF_UNSAFE_CONDITIONS)
                accidentAppendixObj.put("CauseUnsafeCondition", projectData[i].selectId)
            else if (projectData[i].typeId == SUGGESTED_CONTROL_MEASURES)
                accidentAppendixObj.put("SuggestedControl", projectData[i].selectId)
        }
        accidentAppendixObj.put("SuggestedControlOther", safetyMeasureOtherData[0].SafetyMeasureOther)
        parentObj.put("AccidentAppendix", accidentAppendixObj)


//        Log.d(TAG, "prepareInputJsonToGetMasterData: " + parentObj.toString())
        return parentObj.toString()
    }

    fun getFormattedDate(date: String): String {
        val calendar = Calendar.getInstance()
        calendar.time = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US).parse(date)
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US).format(calendar.time)
    }

    fun getFormattedTime(time: String): Int {
        val splitTime = time.split(":")
        val splitminutes = splitTime[1].split(" ")
        val minutes = splitTime[0].toInt() * 60 + splitminutes[0].toInt()
        return minutes
    }

    private fun insertAccidentDataApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.InsertAccidentIncidentReport(prepareInputJsonToGetMasterData())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()

                    lifecycleScope.launch {
                        viewModel.deleteAccidentIncidentData()
                        viewModel.deleteAccidentInvestigationData()
                    }

                    val intent = Intent(this@AccidentStage4Activity, AccidentIncidentSummaryActivity::class.java)
                    intent.putExtra(KEY_BACK_ACTION, "home_screen")
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@AccidentStage4Activity,
                        intent
                    )
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@AccidentStage4Activity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@AccidentStage4Activity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@AccidentStage4Activity,
                    null
                )
            }
        }
    }
}