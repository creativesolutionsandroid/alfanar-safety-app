package com.cs.alfanar.ui.dailyReportDetails

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.dailyReportSummary.Data
import com.cs.alfanar.ui.dailyReportSummary.DailyReportSummaryAdapter
import com.cs.alfanar.ui.dailyReportSummary.DailyReportSummaryViewModel
import com.cs.alfanar.ui.dailyReportSummary.DailyReportSummaryViewModelFactory
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_daily_report_details.*
import kotlinx.android.synthetic.main.activity_daily_report_summary.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class DailyReportDetailsActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: DailyReportDetailsViewModelFactory by instance()

    private lateinit var viewModel: DailyReportDetailsViewModel

    private val KEY_DATA: String = "DATA_KEY"
    private lateinit var adapter: DailyReportsRemarksAdapter

//    private val TAG : String = this::class.java.simpleName
    private var ReportId: Int = 0
    private var ReportStatus: Int = 0
    private var isStatusUpdated: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daily_report_details)

        viewModel = ViewModelProvider(this, factory).get(DailyReportDetailsViewModel::class.java)

        val selectedData = intent.getParcelableExtra(KEY_DATA) as? Data
        initRecyclerView()
        initView(selectedData)
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        if (isStatusUpdated) {
            setResult(Activity.RESULT_OK)
        }
        else {
            setResult(Activity.RESULT_CANCELED)
        }
        finish()
    }

    override fun onBackPressed() {
        if (isStatusUpdated) {
            setResult(Activity.RESULT_OK)
        }
        else {
            setResult(Activity.RESULT_CANCELED)
        }
        finish()
    }

    fun updateStaus(view: View) {
        if (ReportStatus == DAILY_REPORT_STATUS_OPEN)
            updateStatusApi()
    }

    private fun initRecyclerView(){
        list_remarks.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_remarks.addItemDecoration(itemDecoration)

        adapter = DailyReportsRemarksAdapter()
        list_remarks.adapter = adapter
    }

    private fun initView(data: Data?) {
        ReportId = data!!.Id
        ReportStatus = data.ReportStatus
        item_number.text = data.ReportId
        report_date.text = data.ReportDate
        project_id.text = data.ProjectCode
        man_power.text = data.TotalManPower.toString()
        location.text = data.Location
        activity.text = data.Activity
        unsafe_act.text = data.UnSafeAct
        corrective_action.text = data.CorrectionAction
        action_taken_by_hse_officer.text = data.HSEOfficerAction
        area_activity_incharge.text = data.ActivityIncharge
        status.text = data.ReportStatusEn

        adapter.setList(data.Remarks, this@DailyReportDetailsActivity)
        adapter.notifyDataSetChanged()

        if (data.ReportStatus == DAILY_REPORT_STATUS_OPEN) { // open
            switch_status.setImageDrawable(resources.getDrawable(R.drawable.switch_on))
        }
        else { // close
            switch_status.setImageDrawable(resources.getDrawable(R.drawable.switch_off))
        }
    }

    fun prepareInputJsonForUpdateStatus(): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())
        parentObj.put("ReportId", ReportId)

//        Log.d(TAG, "prepareInputJsonForUpdateStatus: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun updateStatusApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.UpdateDailyReportStatus(prepareInputJsonForUpdateStatus())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    toast(response.MessageEn)
                    status.text = resources.getString(R.string.label_close)
                    switch_status.setImageDrawable(resources.getDrawable(R.drawable.switch_off))
                    isStatusUpdated = true
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@DailyReportDetailsActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@DailyReportDetailsActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@DailyReportDetailsActivity,
                    null
                )
            }
        }
    }
}