package com.cs.alfanar.ui.MonthlyReport.MRStage4

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage4ViewModelFactory (
    private val repository: MonthlyReportStage4Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage4ViewModel(repository) as T
    }
}