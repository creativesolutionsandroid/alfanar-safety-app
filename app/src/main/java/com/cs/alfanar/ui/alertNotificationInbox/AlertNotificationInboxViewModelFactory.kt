package com.cs.alfanar.ui.alertNotificationInbox

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AlertNotificationInboxViewModelFactory (
    private val repository: AlertNotificationInboxRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AlertNotificationInboxViewModel(repository) as T
    }
}