package com.cs.alfanar.ui.MonthlyReport.MRStage6

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage6Entity
import com.cs.alfanar.data.db.entities.monthlyReport.MRstage6EventsEntity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class MonthlyReportStage6Repository  (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun saveEventData(data: MRstage6EventsEntity): Long = db.getMRStage6EventDao().insert(data)
    suspend fun getEventData(): List<MRstage6EventsEntity> = db.getMRStage6EventDao().getData()
    suspend fun updateEventData(name: String, id: Int) = db.getMRStage6EventDao().updateData(name, id)

    // database requests
    suspend fun saveData(projectData: MRStage6Entity) = db.getMRStage6Dao().insert(projectData)
    suspend fun updatePositiveAlf(PositiveAlf: Int) = db.getMRStage6Dao().updatePositiveAlf(PositiveAlf)
    suspend fun updatePositiveSub(PositiveSub: Int) = db.getMRStage6Dao().updatePositiveSub(PositiveSub)
    suspend fun updateNegativeAlf(NegativeAlf: Int) = db.getMRStage6Dao().updateNegativeAlf(NegativeAlf)
    suspend fun updateNegativeSub(NegativeSub: Int) = db.getMRStage6Dao().updateNegativeSub(NegativeSub)
    suspend fun updatePendingAlf(PendingAlf: Int) = db.getMRStage6Dao().updatePendingAlf(PendingAlf)
    suspend fun updatePendingSub(PendingSub: Int) = db.getMRStage6Dao().updatePendingSub(PendingSub)
    suspend fun updateClosedAlf(ClosedAlf: Int) = db.getMRStage6Dao().updateClosedAlf(ClosedAlf)
    suspend fun updateClosedSub(ClosedSub: Int) = db.getMRStage6Dao().updateClosedSub(ClosedSub)
    suspend fun getData() : List<MRStage6Entity> { return db.getMRStage6Dao().getData() }
    suspend fun updateStage4(Stage4: Boolean) = db.getMRDashboardDao().updateStage4(Stage4)
}