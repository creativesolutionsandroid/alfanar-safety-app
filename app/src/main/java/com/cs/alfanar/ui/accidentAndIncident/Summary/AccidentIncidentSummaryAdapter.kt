package com.cs.alfanar.ui.accidentAndIncident.Summary

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ItemAccidentIncidentSummaryBinding
import com.cs.alfanar.model.accidentIncidentSummary.Data
import com.cs.alfanar.utils.DAILY_REPORT_STATUS_OPEN


class AccidentIncidentSummaryAdapter(
    private val clickListener: (Data, Int) -> Unit,
    private val statusListener: (Data, Int) -> Unit
)
    : RecyclerView.Adapter<MyViewHolder>()
{
    private val subscribersList = ArrayList<Data>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : ItemAccidentIncidentSummaryBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_accident_incident_summary,
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position], clickListener, statusListener, appContext)
    }

    fun setList(subscribers: List<Data>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemAccidentIncidentSummaryBinding): RecyclerView.ViewHolder(binding.root){

    fun bind(
        data: Data,
        clickListener: (Data, Int) -> Unit,
        statusListener: (Data, Int) -> Unit,
        context: Context
    ){

        binding.itemNumber.text = data.ReportId
        binding.reportDate.text = data.ReportDate
        binding.location.text = data.ProjectCode
        binding.status.text = data.StatusName

        if (data.ReportStatus == DAILY_REPORT_STATUS_OPEN) { // open
            binding.switchStatus.setImageDrawable(context.resources.getDrawable(R.drawable.switch_on))
        }
        else { // close
            binding.switchStatus.setImageDrawable(context.resources.getDrawable(R.drawable.switch_off))
        }

        binding.switchStatus.setOnClickListener{
            if (data.ReportStatus == DAILY_REPORT_STATUS_OPEN)
                statusListener(data, adapterPosition)
        }

        binding.itemLayout.setOnClickListener{
            clickListener(data, adapterPosition)
        }
    }

}