package com.cs.alfanar.ui.nearestHospitalDirections

import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.model.nearestHospitalDirections.Route
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import kotlinx.android.synthetic.main.activity_nearest_hospital.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*
import kotlin.collections.ArrayList


class NearestHospitalDirectionsActivity : AppCompatActivity(), KodeinAware, OnMapReadyCallback {

    override val kodein by kodein()
    private val factory: NearestHospitalDirectionsViewModelFactory by instance()

    private lateinit var viewModel: NearestHospitalDirectionsViewModel

//    private val TAG : String = this::class.java.simpleName

    private var currentLocation: String? = ""
    private var hospitalLocation: String? = ""

    private val KEY_CURRENT_LOCATION: String = "KEY_CURRENT_LOCATION"
    private val KEY_HOSPITAL_LOCATION: String = "KEY_HOSPITAL_LOCATION"

    private var googleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nearest_hospital_directions)

        viewModel = ViewModelProvider(this, factory).get(NearestHospitalDirectionsViewModel::class.java)

        currentLocation = intent.getStringExtra(KEY_CURRENT_LOCATION)
        hospitalLocation = intent.getStringExtra(KEY_HOSPITAL_LOCATION)

        getHospitalListApi()

        val mapFragment = supportFragmentManager.findFragmentById(R.id.fragment_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onGetDirectionsClicked(view: View) {
        val url = "http://maps.google.com/maps?daddr="+hospitalLocation
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(intent)
    }

    private fun getHospitalListApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getHospitalDirection(currentLocation, hospitalLocation, GOOGLE_MAPS_API_KEY)
                if (response.status.equals("OK")) {
                    // status true from api
                    closeLoadingDialog()
                    drawDirectionsOnMap(response.routes)
                } else {
                    // status false from api
                    closeLoadingDialog()
                    list_nearest_hospitals.visibility = View.GONE
                    empty_list_message.visibility = View.VISIBLE
                    showOneButtonAlertDialog(
                        getString(R.string.str_cannot_reach_server),
                        getString(R.string.Ok),
                        this@NearestHospitalDirectionsActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                list_nearest_hospitals.visibility = View.GONE
                empty_list_message.visibility = View.VISIBLE
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@NearestHospitalDirectionsActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                list_nearest_hospitals.visibility = View.GONE
                empty_list_message.visibility = View.VISIBLE
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@NearestHospitalDirectionsActivity,
                    null
                )
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap
    }

    fun drawDirectionsOnMap(route: List<Route>) {
        val PATTERN_DASH_LENGTH_PX = 20
        val PATTERN_GAP_LENGTH_PX = 10
        val DASH: PatternItem = Dash(PATTERN_DASH_LENGTH_PX.toFloat())
        val GAP: PatternItem = Gap(PATTERN_GAP_LENGTH_PX.toFloat())
        val PATTERN_POLYGON_ALPHA: List<PatternItem> = Arrays.asList(GAP, DASH)

        // code to capture zoom between start and end location
        val builder = LatLngBounds.Builder()
        val origin = LatLng(route.get(0).legs.get(0).start_location.lat,
            route.get(0).legs.get(0).start_location.lng)
        val destination = LatLng(route.get(0).legs.get(0).end_location.lat,
            route.get(0).legs.get(0).end_location.lng)
        builder.include(origin)
        builder.include(destination)
        val bounds = builder.build()
        val padding = 100 // offset from edges of the map in pixels

        val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)

        if (googleMap != null) {

            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                val success = googleMap!!.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.maps_styles_json
                    )
                )
                if (!success) {
//                    Log.e(TAG, "Style parsing failed.")
                }
            } catch (e: Resources.NotFoundException) {
//                Log.e(TAG, "Can't find style. Error: ", e)
            }

            this.googleMap!!.addMarker(MarkerOptions().position(origin).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_origin)))
            this.googleMap!!.addMarker(MarkerOptions().position(destination).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker_destination)))
            this.googleMap!!.animateCamera(cu)

            val polyOptions = PolylineOptions()
            polyOptions.pattern(PATTERN_POLYGON_ALPHA)
            polyOptions.width(7f)

            for (i in 0 until route.get(0).legs.get(0).steps.size) {
                polyOptions.color(ContextCompat.getColor(this, R.color.map_direction_color))
                val path: MutableList<List<LatLng>> = ArrayList()
                val points = route.get(0).legs.get(0).steps.get(i).polyline.points
                for (j in 0 until PolyUtil.decode(points).size) {
                    polyOptions.add(LatLng(PolyUtil.decode(points).get(j).latitude,
                        PolyUtil.decode(points).get(j).longitude))
                }
                val polyline = googleMap!!.addPolyline(polyOptions)
            }
        }
    }
}