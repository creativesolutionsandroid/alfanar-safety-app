package com.cs.alfanar.ui.weeklyReport.weeklyReportSummary

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.weeklyReportSummary.WeeklyReportSummaryResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class WeeklyReportSummaryRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getWeeklyReportSummary(inputJson : String): WeeklyReportSummaryResponse {
        return apiRequest { api.getWeeklyReportSummary(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun UpdateReportStatus(inputJson : String): BasicResponse {
        return apiRequest { api.updateWeeklyReportStatus(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}