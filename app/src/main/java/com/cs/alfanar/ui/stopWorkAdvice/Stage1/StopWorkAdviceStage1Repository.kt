package com.cs.alfanar.ui.stopWorkAdvice.Stage1

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.nearMissIncident.NearMissIncidentEntity
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkAdviceEntity
import com.cs.alfanar.model.accidentIncident.AccidentMasterDataResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class StopWorkAdviceStage1Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun GetAccidentIncidentReportMasterData(inputJson : String): AccidentMasterDataResponse {
        return apiRequest { api.GetAccidentIncidentReportMasterData(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // database requests
    suspend fun saveData(projectData: StopWorkAdviceEntity) = db.getStopWorkAdviceDao().insert(projectData)

    suspend fun getData(): List<StopWorkAdviceEntity> = db.getStopWorkAdviceDao().getData()

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) =
        db.getStopWorkAdviceDao().updateProject(projectId, projectName, projectCode)

    suspend fun updateDate(date: String) = db.getStopWorkAdviceDao().updateDate(date)

    suspend fun updateTime(Time: String) = db.getStopWorkAdviceDao().updateTime(Time)

    suspend fun updateHseRepresentative(HseRepresentative: String) =
        db.getStopWorkAdviceDao().updateHseRepresentative(HseRepresentative)

    suspend fun updateNameOfSupervisor(NameOfSupervisor: String) =
        db.getStopWorkAdviceDao().updateNameOfSupervisor(NameOfSupervisor)

    suspend fun updateDesignation(Designation: String) = db.getStopWorkAdviceDao().updateDesignation(Designation)

    suspend fun updateLocation(Location: String) = db.getStopWorkAdviceDao().updateLocation(Location)
}