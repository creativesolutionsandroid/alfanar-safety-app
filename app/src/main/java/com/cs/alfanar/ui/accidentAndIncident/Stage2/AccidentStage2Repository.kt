package com.cs.alfanar.ui.accidentAndIncident.Stage2

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.model.masterData.MasterDataResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class AccidentStage2Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    // Api requests

    suspend fun getMasterData(): MasterDataResponse {
        return apiRequest { api.getMasterData() }
    }

    // DB requests
    suspend fun getData() : List<AccidentIncidentEntity> {
        return db.getAccidentIncidentDao().getData()
    }

    suspend fun updateTask(Task: String)  =
        db.getAccidentIncidentDao().updateTask(Task)

    suspend fun updateIqama(Iqama: String)  =
        db.getAccidentIncidentDao().updateIqama(Iqama)

    suspend fun updateAge(Age: String)  =
        db.getAccidentIncidentDao().updateAge(Age)

    suspend fun updateShiftTime(ShiftTime: String, ShiftTimeId: Int)  =
        db.getAccidentIncidentDao().updateShiftTime(ShiftTime, ShiftTimeId)

    suspend fun updateNationality(Nationality: String, NationalityId: Int)  =
        db.getAccidentIncidentDao().updateNationality(Nationality, NationalityId)

    suspend fun updateIncidentLocation(IncidentLocation: String)  =
        db.getAccidentIncidentDao().updateIncidentLocation(IncidentLocation)

    suspend fun updateIncidentTime(IncidentTime: String)  =
        db.getAccidentIncidentDao().updateIncidentTime(IncidentTime)

    suspend fun updateCauseOfIncident(CauseOfIncident: String, CauseOfIncidentId: Int)  =
        db.getAccidentIncidentDao().updateCauseOfIncident(CauseOfIncident, CauseOfIncidentId)

    suspend fun updateFirstAidtreatment(FirstAidtreatment: String) =
        db.getAccidentIncidentDao().updateFirstAidtreatment(FirstAidtreatment)

    suspend fun updateAccidentDescription(AccidentDescription: String) =
        db.getAccidentIncidentDao().updateAccidentDescription(AccidentDescription)

}