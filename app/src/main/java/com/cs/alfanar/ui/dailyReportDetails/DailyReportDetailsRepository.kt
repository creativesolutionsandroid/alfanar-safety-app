package com.cs.alfanar.ui.dailyReportDetails

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.dailyReportSummary.DailyReportSummaryResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class DailyReportDetailsRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun UpdateDailyReportStatus(inputJson : String): BasicResponse {
        return apiRequest { api.UpdateDailyReportStatus(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

}