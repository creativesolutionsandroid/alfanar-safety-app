package com.cs.alfanar.ui.alertNotificationInsert

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.emergencyNumbers.EmergencyNumbersResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class AlertNotificationInsertRepository  (private val api: ApiInterface) : SafeApiRequest(){

    // get emergency numbers api
    suspend fun getEmergencyNumbers(inputJson : String): EmergencyNumbersResponse {
        return apiRequest { api.getEmergencyNumbers(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // insert alert notification api
    suspend fun insertAlertNotifications(inputJson : String): BasicResponse {
        return apiRequest { api.insertAlertNotifications(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}