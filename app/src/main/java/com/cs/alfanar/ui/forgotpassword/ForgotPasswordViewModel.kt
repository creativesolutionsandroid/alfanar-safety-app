package com.cs.alfanar.ui.forgotpassword

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.alfanar.R
import com.cs.alfanar.ui.login.LoginFormState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ForgotPasswordViewModel(private val forgotPasswordRepository: ForgotPasswordRepository): ViewModel() {

    private val _forgotPasswordForm = MutableLiveData<ForgotPasswordFormState>()
    val forgotPasswordFormState: LiveData<ForgotPasswordFormState> = _forgotPasswordForm

    // forgot password api calling
    suspend fun forgotPassword(
        inputJson: String
    ) = withContext(Dispatchers.IO) { forgotPasswordRepository.forgotPassword(inputJson) }


    // data validation
    fun forgotPasswordDataChanged(username: String) {
        if (username.isBlank()) {
            _forgotPasswordForm.value = ForgotPasswordFormState(emailError = R.string.validation_error_enter_email)
        }
        else if (!isUserNameValid(username)) {
            _forgotPasswordForm.value = ForgotPasswordFormState(emailError  = R.string.validation_error_invalid_email)
        }
        else {
            _forgotPasswordForm.value = ForgotPasswordFormState(isDataValid = true)
        }
    }

    // Email id validation check
    fun isUserNameValid(username: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(username).matches()
    }
}