package com.cs.alfanar.ui.MonthlyReport.MRSummary

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MonthlyReportSummaryViewModel (private val repository: MonthlyReportSummaryRepository) : ViewModel() {

    suspend fun getMonthlyReportSummary(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getMonthlyReportSummary(inputJson)
    }

    suspend fun updateMonthlyReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.updateMonthlyReportStatus(inputJson)
    }

}