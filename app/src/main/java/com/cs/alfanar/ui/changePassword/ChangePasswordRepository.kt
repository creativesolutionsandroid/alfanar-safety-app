package com.cs.alfanar.ui.changePassword

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.resetPasswordData.ResetPasswordResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class ChangePasswordRepository (private val api: ApiInterface): SafeApiRequest() {

    suspend fun changePassword(inputJson: String): BasicResponse {
        return apiRequest { api.changePassword(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}