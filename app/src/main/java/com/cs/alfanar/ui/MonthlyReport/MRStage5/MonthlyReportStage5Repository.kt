package com.cs.alfanar.ui.MonthlyReport.MRStage5

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage5Entity
import com.cs.alfanar.data.db.entities.monthlyReport.MRstage5EventsEntity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class MonthlyReportStage5Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {
    
    suspend fun saveEventData(data: MRstage5EventsEntity): Long = db.getMRStage5EventDao().insert(data)
    suspend fun getEventData(): List<MRstage5EventsEntity> = db.getMRStage5EventDao().getData()
    suspend fun updateEventData(name: String, id: Int) = db.getMRStage5EventDao().updateData(name, id)

    // database requests
    suspend fun saveData(projectData: MRStage5Entity) = db.getMRStage5Dao().insert(projectData)
    suspend fun updateSiteManagement(SiteManagement: Int) = db.getMRStage5Dao().updateSiteManagement(SiteManagement)
    suspend fun updateSafetyAudits(SafetyAudits: Int) = db.getMRStage5Dao().updateSafetyAudits(SafetyAudits)
    suspend fun updateHighManagement(HighManagement: Int) = db.getMRStage5Dao().updateHighManagement(HighManagement)
    suspend fun updateAdministrative(Administrative: Int) = db.getMRStage5Dao().updateAdministrative(Administrative)
    suspend fun getData() : List<MRStage5Entity> { return db.getMRStage5Dao().getData() }
    suspend fun updateStage3(Stage3: Boolean) = db.getMRDashboardDao().updateStage3(Stage3)
}