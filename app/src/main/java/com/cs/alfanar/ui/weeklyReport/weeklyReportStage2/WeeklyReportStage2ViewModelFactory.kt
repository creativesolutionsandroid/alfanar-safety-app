package com.cs.alfanar.ui.weeklyReport.weeklyReportStage2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WeeklyReportStage2ViewModelFactory (
    private val repository: WeeklyReportStage2Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeeklyReportStage2ViewModel(repository) as T
    }
}