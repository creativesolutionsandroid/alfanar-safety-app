package com.cs.alfanar.ui.MonthlyReport.MRDashboard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.monthlyReport.MRDashboardEntity
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage3Entity
import com.cs.alfanar.model.accidentIncident.Data
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.ui.MonthlyReport.MRProjectSelection.MonthlyReportProjectSelectionViewModel
import com.cs.alfanar.ui.MonthlyReport.MRProjectSelection.MonthlyReportProjectSelectionViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage1.MonthlyReportStage1Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage2.MonthlyReportStage2Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage4.MonthlyReportStage4Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage5.MonthlyReportStage5Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage6.MonthlyReportStage6Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage7.MonthlyReportStage7Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage8.MonthlyReportStage8Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage9.MonthlyReportStage9Activity
import com.cs.alfanar.ui.mainActivity.MainActivity
import kotlinx.android.synthetic.main.activity_monthly_report_dashboard.*
import kotlinx.android.synthetic.main.activity_monthly_report_stage3.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MonthlyReportDashboardActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: MonthlyReportDashboardViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportDashboardViewModel

//    private val TAG : String = this::class.java.simpleName

    private lateinit var projectData: Project
    val KEY_DATA: String = "KEY_DATA"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_dashboard)

        viewModel = ViewModelProvider(this, factory).get(MonthlyReportDashboardViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Project
        projectData = Data!!
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    override fun onResume() {
        super.onResume()
        fetchDataFromDb()
    }

    fun onStage1Clicked(view: View) {
        val intent = Intent(this, MonthlyReportStage1Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun onStage2Clicked(view: View) {
        val intent = Intent(this, MonthlyReportStage4Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun onStage3Clicked(view: View) {
        val intent = Intent(this, MonthlyReportStage5Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun onStage4Clicked(view: View) {
        val intent = Intent(this, MonthlyReportStage5Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun onStage5Clicked(view: View) {
        val intent = Intent(this, MonthlyReportStage6Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun onStage6Clicked(view: View) {
        val intent = Intent(this, MonthlyReportStage6Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun onStage7Clicked(view: View) {
        val intent = Intent(this, MonthlyReportStage7Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun onStage8Clicked(view: View) {
        val intent = Intent(this, MonthlyReportStage8Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun onStage9Clicked(view: View) {
        val intent = Intent(this, MonthlyReportStage9Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                if (projectData[0].Stage1) {
                    tick1.visibility = View.VISIBLE
                }
                if (projectData[0].Stage2) {
                    tick2.visibility = View.VISIBLE
                }
                if (projectData[0].Stage3) {
                    tick3.visibility = View.VISIBLE
                }
                if (projectData[0].Stage3) {
                    tick4.visibility = View.VISIBLE
                }
                if (projectData[0].Stage4) {
                    tick5.visibility = View.VISIBLE
                }
                if (projectData[0].Stage4) {
                    tick6.visibility = View.VISIBLE
                }
                if (projectData[0].Stage7) {
                    tick7.visibility = View.VISIBLE
                }
                if (projectData[0].Stage8) {
                    tick8.visibility = View.VISIBLE
                }
                if (projectData[0].Stage9) {
                    tick9.visibility = View.VISIBLE
                }
            }
            else {
                val projectData = MRDashboardEntity(1, false, false, false, false, false
                    , false, false, false, false)
                viewModel.saveData(projectData)
            }
        }
    }
}