package com.cs.alfanar.ui.documentCenterList

import com.cs.alfanar.model.documentCenterListData.DocumentCenterListResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class DocumentCenterListRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getDocumentCenterList(inputJson : String): DocumentCenterListResponse {
        return apiRequest { api.getDocumentCenterList(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

}