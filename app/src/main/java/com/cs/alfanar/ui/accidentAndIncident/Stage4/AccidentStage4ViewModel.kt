package com.cs.alfanar.ui.accidentAndIncident.Stage4

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentInvestigationEntity
import com.cs.alfanar.ui.accidentAndIncident.Stage3.AccidentStage3Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccidentStage4ViewModel (private val repository: AccidentStage4Repository) : ViewModel() {

    suspend fun InsertAccidentIncidentReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.InsertAccidentIncidentReport(inputJson)
    }

    suspend fun getData() : List<AccidentInvestigationEntity> {
        return repository.getData()
    }

    suspend fun insert(projectData: AccidentInvestigationEntity) = repository.insert(projectData)

    suspend fun getDataBasedOnTypeId(TypeId: Int): List<AccidentInvestigationEntity> {
        return repository.getDataBasedOnTypeId(TypeId)
    }

    suspend fun updateData(SelectId: Int, SelectName: String, TypeId: Int) =
        repository.updateData(SelectId, SelectName, TypeId)

    suspend fun updateSafetyMeasureOther(SafetyMeasureOther: String) =
        repository.updateSafetyMeasureOther(SafetyMeasureOther)

    suspend fun getSafetyMeasureOther() : List<AccidentIncidentEntity> {
        return repository.getSafetyMeasureOther()
    }

    suspend fun deleteAccidentIncidentData() = repository.deleteAccidentIncidentData()

    suspend fun deleteAccidentInvestigationData() = repository.deleteAccidentInvestigationData()
}