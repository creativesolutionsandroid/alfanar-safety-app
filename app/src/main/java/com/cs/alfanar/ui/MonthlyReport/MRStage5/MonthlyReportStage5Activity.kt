package com.cs.alfanar.ui.MonthlyReport.MRStage5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage3Entity
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage5Entity
import com.cs.alfanar.data.db.entities.monthlyReport.MRstage5EventsEntity
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4OtherHealthEntity
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.ui.MonthlyReport.MRStage4.MonthlyReportStage4Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage4.MonthlyReportStage4ViewModel
import com.cs.alfanar.ui.MonthlyReport.MRStage4.MonthlyReportStage4ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage6.MonthlyReportStage6Activity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage4.*
import com.cs.alfanar.utils.afterTextChanged
import com.cs.alfanar.utils.showOneButtonAlertDialog
import kotlinx.android.synthetic.main.activity_monthly_report_stage3.*
import kotlinx.android.synthetic.main.activity_monthly_report_stage5.*
import kotlinx.android.synthetic.main.activity_weekly_report_stage4.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MonthlyReportStage5Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val ehs_manager_subtory: MonthlyReportStage5ViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportStage5ViewModel

//    private val TAG : String = this::class.java.simpleName

    private lateinit var projectData: Project
    val KEY_DATA: String = "KEY_DATA"

    private lateinit var eventAdapter: MRStage5EventAdapter
    var eventData: MutableList<MRstage5EventsEntity> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_stage5)

        viewModel = ViewModelProvider(this, ehs_manager_subtory).get(MonthlyReportStage5ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Project
        projectData = Data!!

        initRecyclerViews()
        fetchDataFromDb()
        fetchEventDataFromDb()
        addTextWatcher()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextClicked(view: View) {
        lifecycleScope.launch {
            val dataList = viewModel.getEventData()
            if (dataList.size == 1 && dataList[0].Name.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.error_narration), getString(R.string.Ok),
                    this@MonthlyReportStage5Activity, null
                )
            } else {
                viewModel.updateStage3(true)
                val intent = Intent(this@MonthlyReportStage5Activity, MonthlyReportStage6Activity::class.java)
                intent.putExtra(KEY_DATA, projectData)
                startActivity(intent)
            }
        }
    }

    fun OtherEventPlusClicked(view: View) {
        lifecycleScope.launch {
            val dataList = viewModel.getEventData()
            eventData.clear()
            eventData.addAll(dataList)
            if (eventData.size > 0 && !eventData[(eventData.size - 1)].Name.equals("")) {
                val data = MRstage5EventsEntity(eventData.size + 1, "")
                viewModel.saveEventData(data)
                fetchEventDataFromDb()
            }
        }
    }

    fun fetchEventDataFromDb() {
        lifecycleScope.launch {
            val dataList = viewModel.getEventData()
            eventData.clear()
            eventData.addAll(dataList)
            if (eventData.size > 0) {
                eventAdapter.setList(eventData, this@MonthlyReportStage5Activity)
                eventAdapter.notifyDataSetChanged()
            }
            else {
                // Adding empty data
                val data = MRstage5EventsEntity(1, "")
                viewModel.saveEventData(data)
                fetchEventDataFromDb()
            }
        }
    }
    
    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                site_management_this_month.setText(projectData[0].SiteManagement.toString())
                safety_audits_this_month.setText(projectData[0].SafetyAudits.toString())
                high_management_this_month.setText(projectData[0].HighManagement.toString())
                administrative_this_month.setText(projectData[0].Administrative.toString())
                updateCumulativeHours()
            }
            else {
                val projectData = MRStage5Entity(1, 0, 0, 0)
                viewModel.saveData(projectData)
                updateCumulativeHours()
            }
            site_management_this_month.requestFocus()
            site_management_this_month.setSelection(site_management_this_month.text.length)
            addFocusListener()
        }
    }

    private fun initRecyclerViews(){
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))

        list_events.layoutManager = LinearLayoutManager(this)
        list_events.addItemDecoration(itemDecoration)
        eventAdapter = MRStage5EventAdapter({name: String, position: Int ->onEventUpdated(name,position)})
        list_events.adapter = eventAdapter
    }

    private fun onEventUpdated(name: String, position: Int){
        lifecycleScope.launch {
            viewModel.updateEventData(name, eventData.get(position).id)
            if (position == 0 && name.equals("")) {
                viewModel.updateStage3(false)
            }
            else {
                viewModel.updateStage3(true)
            }
//            fetchEventDataFromDb()
        }
    }

    fun updateCumulativeHours() {
        lifecycleScope.launch {
            val dbData = viewModel.getData()

//            val site_management_this_month: Int = dbData[0].SiteManagement
            val site_management_this_month: Int = 0
            val site_management_this_monthCum: Int = projectData.MonthlyEHSAct[0].SiteMgmntSafetyWalk
            site_management_sub.text = ""+ (site_management_this_month + site_management_this_monthCum)

//            val safety_audits_this_month: Int = dbData[0].SafetyAudits
            val safety_audits_this_month: Int = 0
            val safety_audits_this_monthCum: Int = projectData.MonthlyEHSAct[0].SafetyAudits
            safety_audits_sub.text = ""+ (safety_audits_this_month + safety_audits_this_monthCum)

//            val high_management_this_month: Int = dbData[0].HighManagement
            val high_management_this_month: Int = 0
            val high_management_this_monthCum: Int = projectData.MonthlyEHSAct[0].HighMgmntWalk
            high_management_sub.text = ""+ (high_management_this_month + high_management_this_monthCum)

//            val administrative_this_month: Int = dbData[0].Administrative
            val administrative_this_month: Int = 0
            val administrative_this_month_cum: Int = projectData.MonthlyEHSAct[0].AdminFieldSafetyAudit
            administrative_sub.text = ""+ (administrative_this_month + administrative_this_month_cum)
        }
    }

    fun addFocusListener() {
        site_management_this_month.setOnFocusChangeListener { view, b ->
            if (site_management_this_month.text.toString().length == 0) {
                site_management_this_month.setText("0")
                site_management_this_month.setSelection(site_management_this_month.text.toString().length)
            }
        }
        safety_audits_this_month.setOnFocusChangeListener { view, b ->
            if (safety_audits_this_month.text.toString().length == 0) {
                safety_audits_this_month.setText("0")
                safety_audits_this_month.setSelection(safety_audits_this_month.text.toString().length)
            }
        }
        high_management_this_month.setOnFocusChangeListener { view, b ->
            if (high_management_this_month.text.toString().length == 0) {
                high_management_this_month.setText("0")
                high_management_this_month.setSelection(high_management_this_month.text.toString().length)
            }
        }
        administrative_this_month.setOnFocusChangeListener { view, b ->
            if (administrative_this_month.text.toString().length == 0) {
                administrative_this_month.setText("0")
                administrative_this_month.setSelection(administrative_this_month.text.toString().length)
            }
        }
    }

    fun addTextWatcher() {
        site_management_this_month.afterTextChanged {
            if (site_management_this_month.text.toString().length > 0) {
                updatesite_management_this_month(site_management_this_month.text.toString())
            }
            else {
                updatesite_management_this_month("0")
            }
        }
        safety_audits_this_month.afterTextChanged {
            if (safety_audits_this_month.text.toString().length > 0) {
                updatesafety_audits_this_month(safety_audits_this_month.text.toString())
            }
            else {
                updatesafety_audits_this_month("0")
            }
        }
        high_management_this_month.afterTextChanged {
            if (high_management_this_month.text.toString().length > 0) {
                updatePdAlfanar(high_management_this_month.text.toString())
            }
            else {
                updatePdAlfanar("0")
            }
        }
        administrative_this_month.afterTextChanged {
            if (administrative_this_month.text.toString().length > 0) {
                updatePdSC(administrative_this_month.text.toString())
            }
            else {
                updatePdSC("0")
            }
        }
    }

    fun updatesite_management_this_month(site_management_this_month: String) {
        lifecycleScope.launch {
            viewModel.updateSiteManagement(site_management_this_month.toInt())
        }
        updateCumulativeHours()
    }

    fun updatesafety_audits_this_month(safety_audits_this_month: String) {
        lifecycleScope.launch {
            viewModel.updateSafetyAudits(safety_audits_this_month.toInt())
        }
        updateCumulativeHours()
    }

    fun updatePdAlfanar(PdAlfanar: String) {
        lifecycleScope.launch {
            viewModel.updateHighManagement(PdAlfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updatePdSC(PdSC: String) {
        lifecycleScope.launch {
            viewModel.updateAdministrative(PdSC.toInt())
        }
        updateCumulativeHours()
    }
}