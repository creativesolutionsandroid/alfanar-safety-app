package com.cs.alfanar.ui.weeklyReport.weeklyReportStage2

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage2Entity
import com.cs.alfanar.databinding.ItemDailyReportRemarksBinding
import com.cs.alfanar.databinding.ItemWeeklyReportStage2Binding
import com.cs.alfanar.model.dailyReportSummary.Remark

class WeeklyReportStage2Adapter : RecyclerView.Adapter<MyViewHolder>() {
    private val subscribersList = ArrayList<WRstage2Entity>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemWeeklyReportStage2Binding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_weekly_report_stage2,
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position], appContext)
    }

    fun setList(subscribers: List<WRstage2Entity>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemWeeklyReportStage2Binding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: WRstage2Entity, context: Context) {

        binding.serialNumber.text = data.id.toString() + "."
        binding.topic.text = data.Topic
        binding.duration.text = data.Duration
        binding.date.text = data.Date
        binding.attendeeCount.text = String.format(context.resources.getString(R.string.text_attendee_count, data.Attendee.toString()))
    }
}