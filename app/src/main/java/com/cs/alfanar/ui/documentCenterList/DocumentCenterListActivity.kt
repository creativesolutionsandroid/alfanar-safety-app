package com.cs.alfanar.ui.documentCenterList

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.model.documentCenterListData.Data
import com.cs.alfanar.ui.documentCenterItem.DocumentCenterItemActivity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_document_center_list.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class DocumentCenterListActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: DocumentCenterListViewModelFactory by instance()

    private lateinit var viewModel: DocumentCenterListViewModel
    private lateinit var adapter: DocumentCenterListAdapter

//    private val TAG : String = this::class.java.simpleName
    private val DATA_KEY: String = "DATA_KEY"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document_center_list)

        viewModel = ViewModelProvider(this, factory).get(DocumentCenterListViewModel::class.java)

        initRecyclerView()
        getDocumentListApi()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun prepareInputJsonForDocumentList(): String {
        val parentObj = JSONObject()

//        Log.d(TAG, "prepareInputJsonForDocumentList: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getDocumentListApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getDocumentCenterList(prepareInputJsonForDocumentList())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    adapter.setList(response.Data, this@DocumentCenterListActivity)
                    adapter.notifyDataSetChanged()
                    list_document_center.scheduleLayoutAnimation()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@DocumentCenterListActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@DocumentCenterListActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@DocumentCenterListActivity,
                    null
                )
            }
        }
    }

    private fun initRecyclerView(){
        list_document_center.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_document_center.addItemDecoration(itemDecoration)

        adapter = DocumentCenterListAdapter({ selectedItem:Data->listItemClicked(selectedItem)})
        list_document_center.adapter = adapter
    }

    private fun listItemClicked(data: Data){
        intent = Intent(this, DocumentCenterItemActivity::class.java)
        intent.putExtra(DATA_KEY, data)
        startActivity(intent)
    }
}