package com.cs.alfanar.ui.MonthlyReport.MRStage3

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage3Entity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class MonthlyReportStage3Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    // database requests
    suspend fun saveData(projectData: MRStage3Entity) = db.getMRStage3Dao().insert(projectData)

    suspend fun updateMonthlyEHSAward(MonthlyEHSAward: Int) =
        db.getMRStage3Dao().updateMonthlyEHSAward(MonthlyEHSAward)

    suspend fun updateGSLC(GSLC: Int) =
        db.getMRStage3Dao().updateGSLC(GSLC)

    suspend fun updateViolations(Violations: Int) =
        db.getMRStage3Dao().updateViolations(Violations)

    suspend fun updateManagement(Management: Int) =
        db.getMRStage3Dao().updateManagement(Management)

    suspend fun updateClients(Clients: Int) =
        db.getMRStage3Dao().updateClients(Clients)

    suspend fun updateFICAlfanar(FICAlfanar: Int) =
        db.getMRStage3Dao().updateFICAlfanar(FICAlfanar)

    suspend fun updateFICSub(FICSub: Int) =
        db.getMRStage3Dao().updateFICSub(FICSub)

    suspend fun updateSMD(SMD: Int) =
        db.getMRStage3Dao().updateSMD(SMD)

    suspend fun getData() : List<MRStage3Entity> {
        return db.getMRStage3Dao().getData()
    }

    suspend fun updateStage1(Stage1: Boolean) =
        db.getMRDashboardDao().updateStage1(Stage1)
}