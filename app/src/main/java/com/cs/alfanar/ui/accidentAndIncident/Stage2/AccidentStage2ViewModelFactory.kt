package com.cs.alfanar.ui.accidentAndIncident.Stage2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AccidentStage2ViewModelFactory (
    private val repository: AccidentStage2Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AccidentStage2ViewModel(repository) as T
    }
}