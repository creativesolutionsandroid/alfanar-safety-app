package com.cs.alfanar.ui.weeklyReport.weeklyReportDashboard

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.weeklyReport.*
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class WeeklyReportDashboardRepository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun getStage1Data(): List<WRstage1Entity> = db.getWRstage1Dao().getData()
    suspend fun getStage2Data(): List<WRstage2Entity> = db.getWRstage2Dao().getData()
    suspend fun getStage3Data(): List<WRstage3Entity> = db.getWRstage3Dao().getData()
    suspend fun getCheckListData(): List<WRstage4ChecklistEntity> = db.getWRstage4ChecklistDao().getData()
    suspend fun getDrillListData(): List<WRstage4EmergencyDrillEntity> = db.getWRstage4DrillDao().getData()
    suspend fun getOtherHealthData(): List<WRstage4OtherHealthEntity> = db.getWRstage4OtherHealthDao().getData()
    suspend fun getHSEData(): List<WRstage4HSEEntity> = db.getWRstage4HSEDao().getData()
    suspend fun getVulnerableData(): List<WRstage4VulnerableEntity> = db.getWRstage4VulnerableDao().getData()

}