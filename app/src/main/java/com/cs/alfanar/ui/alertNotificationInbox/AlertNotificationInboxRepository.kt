package com.cs.alfanar.ui.alertNotificationInbox

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.alertNotification.AlertNotificationTypeResponse
import com.cs.alfanar.model.alertNotificationInbox.AlertNotificationInboxResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class AlertNotificationInboxRepository (private val api: ApiInterface) : SafeApiRequest() {

    suspend fun getAlertNotifications(inputJson: String): AlertNotificationInboxResponse {
        return apiRequest { api.getAlertNotificationsInbox(
                RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun acknowledgeAlertNotification(inputJson: String): BasicResponse {
        return apiRequest { api.acknowledgeAlertNotification(
                RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}