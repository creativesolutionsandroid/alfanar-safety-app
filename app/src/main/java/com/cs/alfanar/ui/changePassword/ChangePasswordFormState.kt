package com.cs.alfanar.ui.changePassword

data class ChangePasswordFormState(val oldPasswordError: Int? = null,
                                   val newPasswordError: Int? = null,
                                  val confirmPasswordError: Int? = null,
                                  val isDataValid: Boolean = false)