package com.cs.alfanar.ui.MonthlyReport.MRStage3

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage3Entity

class MonthlyReportStage3ViewModel (private val repository: MonthlyReportStage3Repository) : ViewModel() {

    // database requests
    suspend fun saveData(projectData: MRStage3Entity) = repository.saveData(projectData)

    suspend fun updateMonthlyEHSAward(MonthlyEHSAward: Int) =
        repository.updateMonthlyEHSAward(MonthlyEHSAward)

    suspend fun updateGSLC(GSLC: Int) =
        repository.updateGSLC(GSLC)

    suspend fun updateViolations(Violations: Int) =
        repository.updateViolations(Violations)

    suspend fun updateManagement(Management: Int) =
        repository.updateManagement(Management)

    suspend fun updateClients(Clients: Int) =
        repository.updateClients(Clients)

    suspend fun updateFICAlfanar(FICAlfanar: Int) =
        repository.updateFICAlfanar(FICAlfanar)

    suspend fun updateFICSub(FICSub: Int) =
        repository.updateFICSub(FICSub)

    suspend fun updateSMD(SMD: Int) =
        repository.updateSMD(SMD)

    suspend fun getData() : List<MRStage3Entity> {
        return repository.getData()
    }

    suspend fun updateStage1(Stage1: Boolean) =
        repository.updateStage1(Stage1)
}