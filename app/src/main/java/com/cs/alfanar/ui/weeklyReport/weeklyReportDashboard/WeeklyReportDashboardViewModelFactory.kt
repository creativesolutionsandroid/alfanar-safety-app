package com.cs.alfanar.ui.weeklyReport.weeklyReportDashboard

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WeeklyReportDashboardViewModelFactory (
    private val repository: WeeklyReportDashboardRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeeklyReportDashboardViewModel(repository) as T
    }
}