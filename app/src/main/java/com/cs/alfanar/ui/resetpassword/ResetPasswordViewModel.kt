package com.cs.alfanar.ui.resetpassword

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.alfanar.R
import com.cs.alfanar.ui.forgotpassword.ForgotPasswordFormState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.regex.Pattern

class ResetPasswordViewModel(private val resetPasswordRespository: ResetPasswordRespository): ViewModel() {

    private val _resetPasswordForm = MutableLiveData<ResetPasswordFormState>()
    val resetPasswordFormState: LiveData<ResetPasswordFormState> = _resetPasswordForm

    // resetPassword api calling
    suspend fun resetPassword(
        inputJson: String
    ) = withContext(Dispatchers.IO) { resetPasswordRespository.resetPassword(inputJson) }

    // data validation
    fun resetPasswordDataChanged(newPassword: String, confirmPassword: String) {
        if (newPassword.isBlank()) {
            _resetPasswordForm.value = ResetPasswordFormState(newPasswordError = R.string.validation_error_enter_new_password)
        }
        else if (!isValidPassword(newPassword)) {
            _resetPasswordForm.value = ResetPasswordFormState(newPasswordError  = R.string.validation_error_invalid_password)
        }
        else if (!newPassword.equals(confirmPassword)) {
            _resetPasswordForm.value = ResetPasswordFormState(newPasswordError = R.string.validation_error_invalid_confirm_password)
        }
        else {
            _resetPasswordForm.value = ResetPasswordFormState(isDataValid = true)
        }
    }


    /**
     * Checks if the password is valid as per the following password policy.
     * Password should be minimum minimum 8 characters long.
     * Password should contain at least one number.
     * Password should contain at least one capital letter.
     * Password should contain at least one small letter.
     * Password should contain at least one special character.
     * Allowed special characters: "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
     *
     * @param data - String
     * @param updateUI - if true and if data is EditText, the function sets error to the EditText or its TextInputLayout
     * @return - true if the password is valid as per the password policy.
     */
    fun isValidPassword(data: String): Boolean {
        val str : String = data
        var valid = true

        // Password policy check
        // Password should be minimum minimum 8 characters long
        if (str.length < 8) {
            valid = false
        }
        // Password should contain at least one number
        var exp = ".*[0-9].*"
        var pattern = Pattern.compile(exp, Pattern.CASE_INSENSITIVE)
        var matcher = pattern.matcher(str)
        if (!matcher.matches()) {
            valid = false
        }

        // Password should contain at least one capital letter
        exp = ".*[A-Z].*"
        pattern = Pattern.compile(exp)
        matcher = pattern.matcher(str)
        if (!matcher.matches()) {
            valid = false
        }

        // Password should contain at least one small letter
        exp = ".*[a-z].*"
        pattern = Pattern.compile(exp)
        matcher = pattern.matcher(str)
        if (!matcher.matches()) {
            valid = false
        }

        // commented code - validtion to check special character
//        // Password should contain at least one special character
//        // Allowed special characters : "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
//        exp = ".*[~!@#\$%\\^&*()\\-_=+\\|\\[{\\]};:'\",<.>/?].*"
//        pattern = Pattern.compile(exp)
//        matcher = pattern.matcher(str)
//        if (!matcher.matches()) {
//            valid = false
//        }

        return valid
    }
}
