package com.cs.alfanar.ui.resetpassword

import com.cs.alfanar.model.resetPasswordData.ResetPasswordResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class ResetPasswordRespository(private val api: ApiInterface): SafeApiRequest() {

    suspend fun resetPassword(inputJson: String): ResetPasswordResponse {
        return apiRequest { api.resetPassword(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

}