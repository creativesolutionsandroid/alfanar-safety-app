package com.cs.alfanar.ui.accidentAndIncident.Stage1

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AccidentStage1ViewModelFactory (
    private val repository: AccidentStage1Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AccidentStage1ViewModel(repository) as T
    }
}