package com.cs.alfanar.ui.safetySummaryReport

import com.cs.alfanar.model.MonthlyReportDetails.MonthlyReportDetails
import com.cs.alfanar.model.SafetySummaryReport.SafetySummaryReponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class SafetySummaryReportRepository(private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getSafetySummaryReponse(inputJson : String): SafetySummaryReponse {
        return apiRequest { api.getSafetySummaryReponse(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

}