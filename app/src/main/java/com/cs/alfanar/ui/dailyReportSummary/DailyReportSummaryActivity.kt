package com.cs.alfanar.ui.dailyReportSummary

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.dailyReportSummary.Data
import com.cs.alfanar.ui.dailyReportDetails.DailyReportDetailsActivity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_daily_report_details.*
import kotlinx.android.synthetic.main.activity_daily_report_summary.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*


class DailyReportSummaryActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: DailyReportSummaryViewModelFactory by instance()

    private lateinit var viewModel: DailyReportSummaryViewModel
    private lateinit var adapter: DailyReportSummaryAdapter
    private lateinit var dataList: List<Data>

//    private val TAG : String = this::class.java.simpleName

    var fromCalendar = Calendar.getInstance()
    var toCalendar = Calendar.getInstance()

    var fromDay: Int = 0
    var fromMonth: Int = 0
    var fromYear: Int = 0

    var toDay: Int = 0
    var toMonth: Int = 0
    var toYear: Int = 0

    val KEY_DATA: String = "DATA_KEY"
    val INTENT_FOR_STATUS: Int = 1
    var selectedPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daily_report_summary)

        viewModel = ViewModelProvider(this, factory).get(DailyReportSummaryViewModel::class.java)

        initRecyclerView()
        getLast7DaysSummary()

//        fromCalendar.set(Calendar.DATE, -30)
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onFromDateClicked(view: View) {
        showFromDatePicker()
    }

    fun onToDateClicked(view: View) {
        showToDatePicker()
    }

    private fun initRecyclerView(){
        list_daily_reports.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_daily_reports.addItemDecoration(itemDecoration)

        adapter = DailyReportSummaryAdapter({ selectedItem: Data, selectedPosition: Int ->listItemClicked(selectedItem, selectedPosition)},
            { selectedItem: Data, selectedPosition: Int ->updateStatusApi(selectedItem, selectedPosition)})
    }

    private fun listItemClicked(data: Data, position: Int){
        selectedPosition = position
        intent = Intent(this, DailyReportDetailsActivity::class.java)
        intent.putExtra(KEY_DATA, data)
        startActivityForResult(intent, INTENT_FOR_STATUS)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == INTENT_FOR_STATUS && resultCode == Activity.RESULT_OK) {
            dataList.get(selectedPosition).ReportStatus = DAILY_REPORT_STATUS_CLOSE
            dataList.get(selectedPosition).ReportStatusEn = resources.getString(R.string.label_close)
            adapter.notifyItemChanged(selectedPosition)
        }
    }

    fun showFromDatePicker() {
        fromYear = fromCalendar.get(Calendar.YEAR)
        fromMonth = fromCalendar.get(Calendar.MONTH)
        fromDay = fromCalendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            fromCalendar.set(Calendar.YEAR, year)
            fromCalendar.set(Calendar.MONTH, monthOfYear)
            fromCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            label_date_from.text = SimpleDateFormat("dd-MM-yyyy", Locale.US).format(fromCalendar.time)
            label_date_to.text = ""
        }, fromYear, fromMonth, fromDay)

        val c = Calendar.getInstance()
        c.add(Calendar.DATE, -30) // show previous 30 days only

        dpd.setTitle(resources.getString(R.string.label_from))
        dpd.datePicker.minDate = c.timeInMillis
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }

    fun showToDatePicker() {
        toYear = toCalendar.get(Calendar.YEAR)
        toMonth = toCalendar.get(Calendar.MONTH)
        toDay = toCalendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            toCalendar.set(Calendar.YEAR, year)
            toCalendar.set(Calendar.MONTH, monthOfYear)
            toCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            label_date_to.text = SimpleDateFormat("dd-MM-yyyy", Locale.US).format(toCalendar.time)
            list_daily_reports.adapter = null
            getDailyReportSummaryApi()
        }, toYear, toMonth, toDay)

        val title = TextView(this)
        title.text = resources.getString(R.string.labelto)
        dpd.setCustomTitle(title)
        dpd.datePicker.minDate = fromCalendar.timeInMillis
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }

    fun getLast7DaysSummary() {
        val todayCalendar = Calendar.getInstance()

        toYear = todayCalendar.get(Calendar.YEAR)
        toMonth = todayCalendar.get(Calendar.MONTH)
        toDay = todayCalendar.get(Calendar.DAY_OF_MONTH)

        toCalendar.set(Calendar.YEAR, toYear)
        toCalendar.set(Calendar.MONTH, toMonth)
        toCalendar.set(Calendar.DAY_OF_MONTH, toDay)
        label_date_to.text = SimpleDateFormat("dd-MM-yyyy", Locale.US).format(toCalendar.time)

        todayCalendar.add(Calendar.DATE, -7)

        fromYear = todayCalendar.get(Calendar.YEAR)
        fromMonth = todayCalendar.get(Calendar.MONTH)
        fromDay = todayCalendar.get(Calendar.DAY_OF_MONTH)

        fromCalendar.set(Calendar.YEAR, fromYear)
        fromCalendar.set(Calendar.MONTH, fromMonth)
        fromCalendar.set(Calendar.DAY_OF_MONTH, fromDay)
        label_date_from.text = SimpleDateFormat("dd-MM-yyyy", Locale.US).format(fromCalendar.time)

        getDailyReportSummaryApi()
    }

    fun prepareInputJsonForGetDailyReportSummary(): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())
        parentObj.put("FromDate", "" + fromYear + "-" + (fromMonth + 1) + "-" + fromDay)
        parentObj.put("ToDate", "" + toYear + "-" + (toMonth + 1) + "-" + toDay)

//        Log.d(TAG, "prepareInputJsonForGetDailyReportSummary: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getDailyReportSummaryApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getDailyReportSummary(prepareInputJsonForGetDailyReportSummary())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    list_daily_reports.adapter = adapter
                    dataList = response.Data
                    adapter.setList(response.Data, this@DailyReportSummaryActivity)
                    adapter.notifyDataSetChanged()
                    list_daily_reports.scheduleLayoutAnimation()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@DailyReportSummaryActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@DailyReportSummaryActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@DailyReportSummaryActivity,
                    null
                )
            }
        }
    }

    fun prepareInputJsonForUpdateStatus(ReportId: Int): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())
        parentObj.put("ReportId", ReportId)

//        Log.d(TAG, "prepareInputJsonForUpdateStatus: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun updateStatusApi(data: Data, position: Int) {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.UpdateDailyReportStatus(prepareInputJsonForUpdateStatus(data.Id))
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    toast(response.MessageEn)
                    getDailyReportSummaryApi()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@DailyReportSummaryActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@DailyReportSummaryActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@DailyReportSummaryActivity,
                    null
                )
            }
        }
    }
}