package com.cs.alfanar.ui.MonthlyReport.MRStage12

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage12ViewModelFactory(
    private val repository: MonthlyReportStage12Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage12ViewModel(repository) as T
    }
}