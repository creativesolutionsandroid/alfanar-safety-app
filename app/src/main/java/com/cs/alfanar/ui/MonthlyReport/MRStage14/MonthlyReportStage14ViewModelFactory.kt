package com.cs.alfanar.ui.MonthlyReport.MRStage14

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage14ViewModelFactory (
    private val repository: MonthlyReportStage14Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage14ViewModel(repository) as T
    }
}