package com.cs.alfanar.ui.documentCenterItem

import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.network.ApiInterface
import com.cs.alfanar.utils.DOCUMENT_URL
import com.cs.alfanar.utils.closeLoadingDialog
import com.cs.alfanar.utils.showLoadingDialog
import com.cs.alfanar.utils.toast
import com.github.barteksc.pdfviewer.listener.OnErrorListener
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener
import com.github.barteksc.pdfviewer.util.Constants
import com.github.barteksc.pdfviewer.util.FitPolicy
import kotlinx.android.synthetic.main.activity_document_pdf_viewer.*
import java.io.BufferedInputStream
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class DocumentPdfViewerActivity : AppCompatActivity(), OnLoadCompleteListener, OnErrorListener {

    private val TAG : String = this::class.java.simpleName
    private val DATA_KEY: String = "DATA_KEY"

    private lateinit var pdfUrlString: String
    private var pdfName: String? = null

    private val SUCCESS: Int = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document_pdf_viewer)

        val listData = intent.getParcelableExtra(DATA_KEY)  as?  com.cs.alfanar.model.documentCenterItemData.Data
        title_document.text = listData?.FileNameEn

        pdfName = listData?.DocumentCopy
        pdfUrlString = ApiInterface.BASE_URL +DOCUMENT_URL + pdfName

        Constants.Pinch.MINIMUM_ZOOM = 1f;
        Constants.Pinch.MAXIMUM_ZOOM = 1f;

        showLoadingDialog(this)
        RetrivePDFStream().execute(pdfUrlString)
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    inner class RetrivePDFStream :
        AsyncTask<String?, Void?, InputStream?>() {

        override fun onPostExecute(inputStream: InputStream?) {
            pdfView.fromStream(inputStream)
                .enableSwipe(true) // allows to block changing pages using swipe
                .swipeHorizontal(false)
                .enableDoubletap(false)
                .defaultPage(0)
                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                .password(null)
                .scrollHandle(null)
                .onLoad(this@DocumentPdfViewerActivity)
                .onError(this@DocumentPdfViewerActivity)
                .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                // spacing between pages in dp. To define spacing color, set view background
                .spacing(0)
                .autoSpacing(false) // add dynamic spacing to fit each page on its own on the screen
                .pageFitPolicy(FitPolicy.BOTH) // mode to fit pages in the view
                .fitEachPage(false) // fit each page to the view, else smaller pages are scaled relative to largest page.
                .pageSnap(false) // snap pages to screen boundaries
                .pageFling(false) // make a fling change only a single page like ViewPager
                .nightMode(false) // toggle night mode
                .load()
        }

        override fun doInBackground(vararg p0: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val uri = URL(p0[0])
                val urlConnection: HttpURLConnection = uri.openConnection() as HttpURLConnection
                if (urlConnection.getResponseCode() === SUCCESS) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                return null
            }
            return inputStream
        }
    }

    override fun loadComplete(nbPages: Int) {
        closeLoadingDialog()
    }

    override fun onError(t: Throwable?) {
        closeLoadingDialog()
        toast(resources.getString(R.string.error_pdf_download))
    }
}