package com.cs.alfanar.ui.alertNotification

import com.cs.alfanar.model.alertNotification.AlertNotificationTypeResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class AlertNotificationRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getAlertNotifications(inputJson : String): AlertNotificationTypeResponse {
        return apiRequest { api.getAlertNotifications(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}