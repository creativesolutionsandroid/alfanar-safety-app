package com.cs.alfanar.ui.forgotpassword

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ActivityForgotPasswordBinding
import com.cs.alfanar.ui.emailverification.EmailVerificationActivity
import com.cs.alfanar.utils.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ForgotPasswordActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: ForgotPasswordViewModelFactory by instance()

    private lateinit var viewModel: ForgotPasswordViewModel
    private lateinit var binding: ActivityForgotPasswordBinding

//    private val TAG : String = this::class.java.simpleName
    private var isTextWatchersEnabled: Boolean = false

    private val OTP_KEY: String = "OTP_KEY"
    private val EMAIL_KEY: String = "EMAIL_KEY"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)

        viewModel = ViewModelProvider(this, factory).get(ForgotPasswordViewModel::class.java)

        viewModel.forgotPasswordFormState.observe(this, Observer {
            val forgotPasswordFormState: ForgotPasswordFormState = it ?: return@Observer
            if (forgotPasswordFormState.emailError != null) {
                binding.etEmail.error = getString(forgotPasswordFormState.emailError)
            }
            if (forgotPasswordFormState.isDataValid) {
                forgotPasswordApi()
            }
        })
    }

    fun onBackButtonClicked(view: View) {
        supportFinishAfterTransition()
    }

    fun onSendOtpClicked(view: View) {
        // calling data validation method in viewModel
        doValidateAndSubmit()
    }

    fun addTextWatchers() {
        isTextWatchersEnabled = true

        binding.etEmail.afterTextChanged {
            if (viewModel.isUserNameValid(binding.etEmail.text.toString().trim()))
                binding.inputEmailLayout.isErrorEnabled = false
        }
    }

    fun doValidateAndSubmit() {
        // Checking and enabling text watchers
//        if (!isTextWatchersEnabled)
//            addTextWatchers()

        // calling validations method in view model
        viewModel.forgotPasswordDataChanged(
            binding.etEmail.text.toString().trim()
        )
    }

    fun prepareInputJsonForForgotPassword(): String {
        val parentObj = JSONObject()

        val email: String = binding.etEmail.text.toString().trim()

        parentObj.put("Email", email)

//        Log.d(TAG, "prepareInputJsonForForgotPassword: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun forgotPasswordApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.forgotPassword(prepareInputJsonForForgotPassword())
                if (response.Status && response.Data.OTP.length == 4) {
                    // status true from api
                    closeLoadingDialog()
//                    Log.d(TAG, "otp: "+response.Data.OTP)
                    intent = Intent(this@ForgotPasswordActivity, EmailVerificationActivity::class.java)
                    intent.putExtra(OTP_KEY, response.Data.OTP)
                    intent.putExtra(EMAIL_KEY, binding.etEmail.text.toString().trim())
                    startActivity(intent)
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@ForgotPasswordActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@ForgotPasswordActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@ForgotPasswordActivity,
                    null
                )
            }
        }
    }
}