package com.cs.alfanar.ui.stopWorkAdvice.SummaryDetails

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class StopWorkAdviceDetailsViewModel (private val repository: StopWorkAdviceDetailsRepository) : ViewModel() {

    suspend fun getWeeklyReportDetails(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getAccidentIncidentSummary(inputJson)
    }

    suspend fun UpdateReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.UpdateReportStatus(inputJson)
    }

}