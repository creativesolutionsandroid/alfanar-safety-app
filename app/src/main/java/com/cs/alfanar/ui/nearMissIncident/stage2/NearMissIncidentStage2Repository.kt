package com.cs.alfanar.ui.nearMissIncident.stage2

import androidx.room.Query
import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.nearMissIncident.NearMissIncidentEntity
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage1Entity
import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.accidentIncident.AccidentMasterDataResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class NearMissIncidentStage2Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun InsertAccidentIncidentReport(inputJson : String): BasicResponse {
        return apiRequest { api.InsertNearMissReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // database requests
    suspend fun getData(): List<NearMissIncidentEntity> = db.getNearMissIncidentDao().getData()

    suspend fun updateDescription(Description: String) = db.getNearMissIncidentDao().updateDescription(Description)

    suspend fun updateWitness(Witness: String) = db.getNearMissIncidentDao().updateWitness(Witness)

    suspend fun updateImprovements(Improvements: String) = db.getNearMissIncidentDao().updateImprovements(Improvements)

    suspend fun updateImage(Image: String, ImageName: String) = db.getNearMissIncidentDao().updateImage(Image, ImageName)

    suspend fun deleteData() = db.getNearMissIncidentDao().deleteData()

}