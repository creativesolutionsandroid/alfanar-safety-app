package com.cs.alfanar.ui.alertNotificationInbox

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AlertNotificationInboxViewModel (private val repository: AlertNotificationInboxRepository) : ViewModel() {

    // get Alert notifications list api calling
    suspend fun getAlertNotifications(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getAlertNotifications(inputJson)
    }

    suspend fun acknowledgeAlertNotification(inputJson : String) = withContext(Dispatchers.IO) {
        repository.acknowledgeAlertNotification(inputJson)
    }
}