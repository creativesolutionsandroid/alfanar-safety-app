package com.cs.alfanar.ui.mainActivity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ListView
import android.widget.RelativeLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.cs.alfanar.R
import com.cs.alfanar.adapters.SideMenuAdapter
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.ui.MonthlyReport.MRProjectSelection.MonthlyReportProjectSelectionActivity
import com.cs.alfanar.ui.accidentAndIncident.Stage1.AccidentStage1Activity
import com.cs.alfanar.ui.alertNotification.AlertNotificationActivity
import com.cs.alfanar.ui.dailyReport.DailyReportActivity
import com.cs.alfanar.ui.dashboard.DashboardFragment
import com.cs.alfanar.ui.documentCenterList.DocumentCenterListActivity
import com.cs.alfanar.ui.emergencyNumbers.EmergencyNumbersActivity
import com.cs.alfanar.ui.nearMissIncident.stage1.NearMissIncidentStage1Activity
import com.cs.alfanar.ui.nearestHospital.NearestHospitalActivity
import com.cs.alfanar.ui.safetySummaryReport.SafetySummaryReportActivity
import com.cs.alfanar.ui.settings.SettingsActivity
import com.cs.alfanar.ui.stopWorkAdvice.Stage1.StopWorkAdviceStage1Activity
import com.cs.alfanar.ui.trainingAttendanceRecord.Stage1.TrainingAttendanceRecordActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportProjectSelection.WeeklyReportProjectSelectionActivity
import kotlinx.android.synthetic.main.nav_header_main.*


class MainActivity : AppCompatActivity() {

    var mDrawerLinear: RelativeLayout? = null
    private lateinit var sideMenuItems: Array<String>
    private lateinit var sideMenuImages: Array<Int>
    var sideMenuListView: ListView? = null
    var mSideMenuAdapter: SideMenuAdapter? = null
    var itemSelectedPostion = 0

    companion object {
        var drawer: DrawerLayout? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        drawer = findViewById(R.id.drawer_layout) as DrawerLayout

        initDrawerLayoutAndToolbar()
        initSideMenu()
        prepareSideMenuData()
        setSideMenuAdapter()
        setFragment(DashboardFragment())

        sideMenuListView!!.setOnItemClickListener { adapterView, view, position, id ->
            selectItem(position)
//            itemSelectedPostion = position
            setSideMenuAdapter()
        }
    }

    fun initDrawerLayoutAndToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        val toggle = ActionBarDrawerToggle(
            this,
            drawer,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer?.addDrawerListener(toggle)
        toggle.syncState()
    }

    fun initSideMenu() {
        mDrawerLinear = findViewById(R.id.left_drawer) as RelativeLayout
        sideMenuListView = findViewById(R.id.side_menu_list_view) as ListView
    }

    fun prepareSideMenuData() {
        sideMenuItems = arrayOf(
            resources.getString(R.string.menu_dashboard),
            resources.getString(R.string.menu_emergency_numbers),
            resources.getString(R.string.menu_emergency_code_and_alert),
            resources.getString(R.string.menu_nearest_hospitals),
//            resources.getString(R.string.menu_ehs_cordinator),
//            resources.getString(R.string.menu_my_account),
//            resources.getString(R.string.menu_faq),
            resources.getString(R.string.menu_settings),
            resources.getString(R.string.menu_logout)
        )

        sideMenuImages = arrayOf(
            R.drawable.menu_ic_dashboard,
            R.drawable.menu_ic_emergency_numbers,
            R.drawable.menu_ic_emergencycodenalert,
            R.drawable.menu_ic_nearest_hospital,
//            R.drawable.menu_ic_ehs,
//            R.drawable.menu_ic_my_account,
//            R.drawable.menu_ic_faq,
            R.drawable.menu_ic_settings,
            R.drawable.menu_ic_logout
        )

        menu_employee_username.text = UserPreferences(
            this
        ).employeeName()?.split(' ')?.joinToString(" ") { it.capitalize() }
        menu_employee_position.text = UserPreferences(
            this
        ).employeeRole()?.split(' ')?.joinToString(" ") { it.capitalize() }
    }

    fun setSideMenuAdapter() {
        mSideMenuAdapter = SideMenuAdapter(
            this@MainActivity,
            R.layout.list_side_menu,
            sideMenuItems,
            sideMenuImages,
            itemSelectedPostion
        )
        sideMenuListView!!.adapter = mSideMenuAdapter
        mSideMenuAdapter!!.notifyDataSetChanged()
    }


    override fun onBackPressed() {
        val drawer = findViewById(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
//            super.onBackPressed()
            finish()
        }
    }

    fun setFragment(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.nav_host_fragment, fragment)
        fragmentTransaction.commit()
    }

    fun setActivity(activity: Activity) {
        startActivity(Intent(this@MainActivity, activity::class.java))
    }

    fun selectItem(position: Int) {
        when (position) {
            0 -> {
                drawer?.closeDrawer(GravityCompat.START)
                setFragment(DashboardFragment())
            }
            1 -> {
                drawer?.closeDrawer(GravityCompat.START)
                setActivity(EmergencyNumbersActivity())
            }
            2 -> {
                drawer?.closeDrawer(GravityCompat.START)
                setActivity(AlertNotificationActivity())
            }
            3 -> {
                drawer?.closeDrawer(GravityCompat.START)
                setActivity(NearestHospitalActivity())
            }
            4 -> {
                drawer?.closeDrawer(GravityCompat.START)
                setActivity(SettingsActivity())
            }
            5 -> {
                logOutPopUp(this)
            }
        }
    }

    fun onLogoutClicked(view: View) {
        logOutPopUp(this)
    }

   // onClick methods for fragments
   // Document center
   fun onDocumentCenterClicked(view: View) {
       setActivity(DocumentCenterListActivity())
   }

    // Emergency numbers
    fun onEmergencyNumbersClicked(view: View) {
        setActivity(EmergencyNumbersActivity())
    }

    // Alert Notification
    fun onAlertNotificationsClicked(view: View) {
        setActivity(AlertNotificationActivity())
    }

    // Nearest Hospital
    fun onNearestHospitalClicked(view: View) {
        setActivity(NearestHospitalActivity())
    }

    // Daily report
    fun onDailyReportClicked(view: View) {
        setActivity(DailyReportActivity())
    }

    // Weekly report
    fun onWeeklyReportClicked(view: View) {
        setActivity(WeeklyReportProjectSelectionActivity())
    }

    // Monthly report
    fun onMonthlyReportClicked(view: View) {
        setActivity(MonthlyReportProjectSelectionActivity())
    }

    // Safety summary report
    fun onSafetySummaryReportClicked(view: View) {
        setActivity(SafetySummaryReportActivity())
    }

    // Accident and Incident
    fun onAccidentIncidentClicked(view: View) {
        setActivity(AccidentStage1Activity())
    }

    // Near miss Incident
    fun onNearMissIncidentClicked(view: View) {
        setActivity(NearMissIncidentStage1Activity())
    }

    // Stop work advice
    fun onStopWorkAdviceClicked(view: View) {
        setActivity(StopWorkAdviceStage1Activity())
    }

    // Stop work advice
    fun onTrainingAttendanceClicked(view: View) {
        setActivity(TrainingAttendanceRecordActivity())
    }

}