package com.cs.alfanar.ui.documentCenterItem

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cs.alfanar.ui.documentCenterList.DocumentCenterListRepository
import com.cs.alfanar.ui.documentCenterList.DocumentCenterListViewModel

class DocumentCenterItemViewModelFactory (
    private val repository: DocumentCenterItemRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DocumentCenterItemViewModel(repository) as T
    }
}