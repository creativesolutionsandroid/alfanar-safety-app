package com.cs.alfanar.ui.stopWorkAdvice.Stage2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class StopWorkAdviceStage2ViewModelFactory (
    private val repository: StopWorkAdviceStage2Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return StopWorkAdviceStage2ViewModel(repository) as T
    }
}