package com.cs.alfanar.ui.documentCenterItem

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.MyApplication
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.databinding.ItemDocumentCenterVideosBinding
import com.cs.alfanar.model.documentCenterItemData.Data
import com.cs.alfanar.network.ApiInterface
import com.cs.alfanar.utils.DOCUMENT_URL
import com.danikula.videocache.HttpProxyCacheServer
import com.master.exoplayer.ExoPlayerHelper


class DocumentCenterVideoAdapter (private val clickListener:(Data, Int)->Unit)
    : RecyclerView.Adapter<com.cs.alfanar.ui.documentCenterItem.VideoViewHolder>()
{
    private val subscribersList = ArrayList<Data>()
    private val thumbnails = ArrayList<Bitmap?>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): com.cs.alfanar.ui.documentCenterItem.VideoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : ItemDocumentCenterVideosBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_document_center_videos,parent,false)
        return VideoViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: com.cs.alfanar.ui.documentCenterItem.VideoViewHolder, position: Int) {
        if((position+1) == subscribersList.size) {
            holder.bind(subscribersList[position], thumbnails[position], clickListener, appContext, true)
        }
        else {
            holder.bind(subscribersList[position], thumbnails[position], clickListener, appContext, false)
        }
    }

    fun setList(subscribers: List<Data>, thumbnailsList : List<Bitmap?>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        thumbnails.clear()
        thumbnails.addAll(thumbnailsList)
        appContext = context
    }

}

class VideoViewHolder(val binding: ItemDocumentCenterVideosBinding): RecyclerView.ViewHolder(binding.root){

    fun bind(data: Data, thumbnail: Bitmap?, clickListener:(Data, Int)->Unit, context: Context, isLastPosition: Boolean){

        binding.videoTitle.text = data.FileNameEn
        binding.thumbnail.setImageBitmap(thumbnail!!)

        // getting video cache data and setting video to player
        val proxy: HttpProxyCacheServer? = MyApplication.getProxy(context)
        val proxyUrl: String? = proxy?.getProxyUrl(
            ApiInterface.BASE_URL
                +DOCUMENT_URL + data.DocumentCopy)
        binding.videoView.url = proxyUrl

        // adding extra margin for last position, for video playback to work.
        if (isLastPosition) {
            val params = ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(0, 0, 0, 250)
            binding.itemLayout.setLayoutParams(params)
        }

        // video mute/unmute
        binding.ivVolume.setOnClickListener {
            if (binding.videoView.isMute) {
                binding.ivVolume.setImageResource(R.drawable.ic_volume_off)
                binding.videoView.isMute = false
            }
            else  {
                binding.ivVolume.setImageResource(R.drawable.ic_volume_on)
                binding.videoView.isMute = true
            }
        }

        binding.videoView.listener = object : ExoPlayerHelper.Listener {

            //Listen for buffering listener
            override fun onBuffering(isBuffering: Boolean) {
                super.onBuffering(isBuffering)
                if (isBuffering) {
                    binding.videoLoadingSpinner.visibility = View.VISIBLE
                    binding.videoLoadingSpinner.start()
                }
                else {
                    binding.videoLoadingSpinner.visibility = View.GONE
                    binding.videoLoadingSpinner.stop()
                }
            }

            override fun onPlayerReady() {
                super.onPlayerReady()

                // making controller visible always
                binding.videoView.playerView?.showController()
                binding.videoView.playerView?.controllerShowTimeoutMs = 0
                binding.videoView.playerView?.controllerHideOnTouch  = false

                // mute/ unmute image disaplying
                binding.ivVolume.visibility = View.VISIBLE
                if (binding.videoView.isMute) {
                    binding.ivVolume.setImageResource(R.drawable.ic_volume_off)
                } else {
                    binding.ivVolume.setImageResource(R.drawable.ic_volume_on)
                }
            }

            override fun onStop() {
                super.onStop()
                binding.videoLoadingSpinner.visibility = View.GONE
//                binding.ivVolume.visibility = View.GONE
            }
        }

        binding.itemLayout.setOnClickListener{
            clickListener(data, adapterPosition)
        }
    }


}