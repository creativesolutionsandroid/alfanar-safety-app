package com.cs.alfanar.ui.trainingAttendanceRecord.Stage1

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEmployeeEntity
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEntity
import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.accidentIncident.AccidentMasterDataResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class TrainingAttendanceRecordRepository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun GetProjectData(inputJson : String): AccidentMasterDataResponse {
        return apiRequest { api.GetAccidentIncidentReportMasterData(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun InsertTrainingAttendanceReport(inputJson : String): BasicResponse {
        return apiRequest { api.InsertTrainingAttendanceReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // database requests
    suspend fun saveData(projectData: TrainingAttendanceRecordEntity)
            = db.getTrainingAttendanceRecordDao().insert(projectData)

    suspend fun getData(): List<TrainingAttendanceRecordEntity>
            = db.getTrainingAttendanceRecordDao().getData()

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) =
        db.getTrainingAttendanceRecordDao().updateProject(projectId, projectName, projectCode)

    suspend fun updateDate(date: String) = db.getTrainingAttendanceRecordDao().updateDate(date)

    suspend fun updateTime(Time: String) = db.getTrainingAttendanceRecordDao().updateTime(Time)

    suspend fun updateVenue(Venue: String) = db.getTrainingAttendanceRecordDao().updateVenue(Venue)

    suspend fun updateInstructorName(InstructorName: String)
            = db.getTrainingAttendanceRecordDao().updateInstructorName(InstructorName)

    suspend fun updateTopicOfTraining(TopicOfTraining: String)
            = db.getTrainingAttendanceRecordDao().updateTopicOfTraining(TopicOfTraining)

    suspend fun saveEmployeeData(data: TrainingAttendanceRecordEmployeeEntity): Long
            = db.getTrainingAttendanceRecordEmployeeDao().insert(data)

    suspend fun getEmployeeData(): List<TrainingAttendanceRecordEmployeeEntity>
            = db.getTrainingAttendanceRecordEmployeeDao().getData()

    suspend fun deleteEmployeeData() = db.getTrainingAttendanceRecordEmployeeDao().deleteData()

    suspend fun deleteData() = db.getTrainingAttendanceRecordDao().deleteData()
}