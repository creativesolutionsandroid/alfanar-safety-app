package com.cs.alfanar.ui.trainingAttendanceRecord.Summary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TrainingAttendanceRecordSummaryViewModelFactory (
    private val repository: TrainingAttendanceRecordSummaryRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TrainingAttendanceRecordSummaryViewModel(repository) as T
    }
}