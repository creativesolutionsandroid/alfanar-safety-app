package com.cs.alfanar.ui.safetySummaryReport

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class SafetySummaryReportViewModelFactory (
    private val repository: SafetySummaryReportRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SafetySummaryReportViewModel(repository) as T
    }
}