package com.cs.alfanar.ui.MonthlyReport.MRStage1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.monthlyReport.MRProjectionSelectionEntity
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage1Entity
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.ui.MonthlyReport.MRProjectSelection.MonthlyReportProjectSelectionViewModel
import com.cs.alfanar.ui.MonthlyReport.MRProjectSelection.MonthlyReportProjectSelectionViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage2.MonthlyReportStage2Activity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.afterTextChanged
import kotlinx.android.synthetic.main.activity_daily_report.*
import kotlinx.android.synthetic.main.activity_monthly_report_stage1.*
import kotlinx.android.synthetic.main.activity_monthly_report_stage2.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MonthlyReportStage1Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: MonthlyReportStage1ViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportStage1ViewModel

//    private val TAG : String = this::class.java.simpleName

    private lateinit var projectData: Project
    val KEY_DATA: String = "KEY_DATA"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_stage1)

        viewModel = ViewModelProvider(this, factory).get(MonthlyReportStage1ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Project
        projectData = Data!!

        fetchDataFromDb()
        addTextWatcher()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextClicked(view: View) {
        val intent = Intent(this, MonthlyReportStage2Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                safe_man_hours.setText(projectData[0].SafeManHoursWorked.toString())
                total_man_hours.setText(projectData[0].TotalManHours.toString())
                direct_workers_alfanar.setText(projectData[0].DirectAlfanar.toString())
                direct_workers_sub.setText(projectData[0].DirectSC.toString())
                indirect_workers_alfanar.setText(projectData[0].InDirectAlfanar.toString())
                indirect_workers_sub.setText(projectData[0].InDirectSC.toString())
                fatal_case.setText(projectData[0].Fatal.toString())
                lost_work_case.setText(projectData[0].LWC.toString())
            }
            else {
                val projectData = MRStage1Entity(1, 0, 0, 0
                    , 0, 0, 0, 0, 0)
                viewModel.saveData(projectData)
            }
            safe_man_hours.requestFocus()
            safe_man_hours.setSelection(safe_man_hours.text.length)
            updateCumulativeHours()
            addFocusListener()
        }
    }

    fun updateCumulativeHours() {
        lifecycleScope.launch {
            val dbData = viewModel.getData()

            //Safe man hours
//            val safeManHours: Int = dbData[0].SafeManHoursWorked
            val safeManHours: Int = 0
            val safeManCumHours: Int = projectData.MonthlyCummulative[0].SafeManHoursWorked
            safe_man_hours_cumulative.text = ""+ (safeManHours + safeManCumHours)

            //Total man hours
//            val totalManHours: Int = dbData[0].TotalManHours
            val totalManHours: Int = 0
            val totalManCumHours: Int = projectData.MonthlyCummulative[0].TotalManHours
            total_man_hours_cumulative.text = ""+ (totalManHours + totalManCumHours)

            // Direct workers alfanar
            val directWorkersAlfanar: Int = dbData[0].DirectAlfanar
            val directWorkersAlfanarCum: Int = projectData.MonthlyCummulative[0].ManPowerAvgDWAlfanar
//            direct_workers_alfanar_cumulative.text = ""+ (directWorkersAlfanar + directWorkersAlfanarCum)
            direct_workers_alfanar_cumulative.text = ""+ (directWorkersAlfanarCum)

            // Direct workers sub
            val directWorkersSub: Int = dbData[0].DirectSC
            val directWorkersSubCum: Int = projectData.MonthlyCummulative[0].ManPowerAvgDWSubCont
//            direct_workers_sub_cumulative.text = ""+ (directWorkersSub + directWorkersSubCum)
            direct_workers_sub_cumulative.text = ""+ (directWorkersSubCum)

            // Indirect Workers alfanar
            val inDirectWorkersAlfanar: Int = dbData[0].InDirectAlfanar
            val inDirectWorkersAlfanarCum: Int = projectData.MonthlyCummulative[0].ManPowerAvgIDOAlfanar
//            indirect_workers_alfanar_cumulative.text = ""+ (inDirectWorkersAlfanar + inDirectWorkersAlfanarCum)
            indirect_workers_alfanar_cumulative.text = ""+ (inDirectWorkersAlfanarCum)

            // Indirect Workers sub
            val inDirectWorkersSub: Int = dbData[0].InDirectSC
            val inDirectWorkersSubCum: Int = projectData.MonthlyCummulative[0].ManPowerAvgIDOSubCont
//            indirect_workers_sub_cumulative.text = ""+ (inDirectWorkersSub + inDirectWorkersSubCum)
            indirect_workers_sub_cumulative.text = ""+ (inDirectWorkersSubCum)

            total_workers_alfanar.text = ""+ (directWorkersAlfanar + inDirectWorkersAlfanar)
//            total_workers_alfanar_cumulative.text = ""+ (directWorkersAlfanar + directWorkersAlfanarCum +
//                    inDirectWorkersAlfanar + inDirectWorkersAlfanarCum)
            total_workers_alfanar_cumulative.text = ""+ (directWorkersAlfanarCum + inDirectWorkersAlfanarCum)

            total_workers_sub.text = ""+ (directWorkersSub + inDirectWorkersSub)
//            total_workers_sub_cumulative.text = ""+ (directWorkersSub + directWorkersSubCum +
//                    inDirectWorkersSub + inDirectWorkersSubCum)
            total_workers_sub_cumulative.text = ""+ (directWorkersSubCum + inDirectWorkersSubCum)

            // Fatal case
//            val fatal: Int = dbData[0].Fatal
            val fatal: Int = 0
            val fatalCum: Int = projectData.MonthlyCummulative[0].FatalCase
            fatal_case_cumulative.text = ""+ (fatal + fatalCum)

            // Fatal case
//            val lostWorkCase: Int = dbData[0].LWC
            val lostWorkCase: Int = 0
            val lostWorkCaseCum: Int = projectData.MonthlyCummulative[0].FatalCase
            lost_work_case_cumulative.text = ""+ (lostWorkCase + lostWorkCaseCum)

        }
    }

    fun addFocusListener() {
        safe_man_hours.setOnFocusChangeListener { view, b ->
            if (safe_man_hours.text.toString().length == 0) {
                safe_man_hours.setText("0")
                safe_man_hours.setSelection(safe_man_hours.text.toString().length)
            }
        }
        total_man_hours.setOnFocusChangeListener { view, b ->
            if (total_man_hours.text.toString().length == 0) {
                total_man_hours.setText("0")
                total_man_hours.setSelection(total_man_hours.text.toString().length)
            }
        }
        direct_workers_alfanar.setOnFocusChangeListener { view, b ->
            if (direct_workers_alfanar.text.toString().length == 0) {
                direct_workers_alfanar.setText("0")
                direct_workers_alfanar.setSelection(direct_workers_alfanar.text.toString().length)
            }
        }
        direct_workers_sub.setOnFocusChangeListener { view, b ->
            if (direct_workers_sub.text.toString().length == 0) {
                direct_workers_sub.setText("0")
                direct_workers_sub.setSelection(direct_workers_sub.text.toString().length)
            }
        }
        indirect_workers_alfanar.setOnFocusChangeListener { view, b ->
            if (indirect_workers_alfanar.text.toString().length == 0) {
                indirect_workers_alfanar.setText("0")
                indirect_workers_alfanar.setSelection(indirect_workers_alfanar.text.toString().length)
            }
        }
        indirect_workers_sub.setOnFocusChangeListener { view, b ->
            if (indirect_workers_sub.text.toString().length == 0) {
                indirect_workers_sub.setText("0")
                indirect_workers_sub.setSelection(indirect_workers_sub.text.toString().length)
            }
        }
        fatal_case.setOnFocusChangeListener { view, b ->
            if (fatal_case.text.toString().length == 0) {
                fatal_case.setText("0")
                fatal_case.setSelection(fatal_case.text.toString().length)
            }
        }
        lost_work_case.setOnFocusChangeListener { view, b ->
            if (lost_work_case.text.toString().length == 0) {
                lost_work_case.setText("0")
                lost_work_case.setSelection(lost_work_case.text.toString().length)
            }
        }
    }

    fun addTextWatcher() {
        safe_man_hours.afterTextChanged {
            if (safe_man_hours.text.toString().length > 0) {
                updateSafeManHoursWorked(safe_man_hours.text.toString())
            }
            else {
                updateSafeManHoursWorked("0")
            }
        }
        total_man_hours.afterTextChanged {
            if (total_man_hours.text.toString().length > 0) {
                updateTotalManHours(total_man_hours.text.toString())
            }
            else {
                updateTotalManHours("0")
            }
        }
        direct_workers_alfanar.afterTextChanged {
            if (direct_workers_alfanar.text.toString().length > 0) {
                updateDirectAlfanar(direct_workers_alfanar.text.toString())
            }
            else {
                updateDirectAlfanar("0")
            }
        }
        direct_workers_sub.afterTextChanged {
            if (direct_workers_sub.text.toString().length > 0) {
                updateDirectSC(direct_workers_sub.text.toString())
            }
            else {
                updateDirectSC("0")
            }
        }
        indirect_workers_alfanar.afterTextChanged {
            if (indirect_workers_alfanar.text.toString().length > 0) {
                updateInDirectAlfanar(indirect_workers_alfanar.text.toString())
            }
            else {
                updateInDirectAlfanar("0")
            }
        }
        indirect_workers_sub.afterTextChanged {
            if (indirect_workers_sub.text.toString().length > 0) {
                updateInDirectSC(indirect_workers_sub.text.toString())
            }
            else {
                updateInDirectSC("0")
            }
        }
        fatal_case.afterTextChanged {
            if (fatal_case.text.toString().length > 0) {
                updateFatal(fatal_case.text.toString())
            }
            else {
                updateFatal("0")
            }
        }
        lost_work_case.afterTextChanged {
            if (lost_work_case.text.toString().length > 0) {
                updateLWC(lost_work_case.text.toString())
            }
            else {
                updateLWC("0")
            }
        }
    }

    fun updateSafeManHoursWorked(SafeManHoursWorked: String) {
        lifecycleScope.launch {
            viewModel.updateSafeManHoursWorked(SafeManHoursWorked.toInt())
        }
        updateCumulativeHours()
    }

    fun updateTotalManHours(TotalManHours: String) {
        lifecycleScope.launch {
            viewModel.updateTotalManHours(TotalManHours.toInt())
        }
        updateCumulativeHours()
    }

    fun updateDirectAlfanar(DirectAlfanar: String) {
        lifecycleScope.launch {
            viewModel.updateDirectAlfanar(DirectAlfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updateDirectSC(DirectSC: String) {
        lifecycleScope.launch {
            viewModel.updateDirectSC(DirectSC.toInt())
        }
        updateCumulativeHours()
    }

    fun updateInDirectAlfanar(InDirectAlfanar: String) {
        lifecycleScope.launch {
            viewModel.updateInDirectAlfanar(InDirectAlfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updateInDirectSC(InDirectSC: String) {
        lifecycleScope.launch {
            viewModel.updateInDirectSC(InDirectSC.toInt())
        }
        updateCumulativeHours()
    }

    fun updateFatal(Fatal: String) {
            lifecycleScope.launch {
            viewModel.updateFatal(Fatal.toInt())
        }
        updateCumulativeHours()
    }

    fun updateLWC(LWC: String) {
        lifecycleScope.launch {
            viewModel.updateLWC(LWC.toInt())
        }
        updateCumulativeHours()
    }
}