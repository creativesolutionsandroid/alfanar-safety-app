package com.cs.alfanar.ui.accidentAndIncident.Stage3

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.ui.accidentAndIncident.Stage2.AccidentStage2Repository

class AccidentStage3ViewModel (private val repository: AccidentStage3Repository) : ViewModel() {

    suspend fun updatevehicleApplicable(vehicleApplicable: Boolean) =
        repository.updatevehicleApplicable(vehicleApplicable)

    suspend fun updateModel(Model: String) =
        repository.updateModel(Model)

    suspend fun updatePlate(Plate: String) =
        repository.updatePlate(Plate)

    suspend fun updateVehicleID(VehicleID: String) =
        repository.updateVehicleID(VehicleID)

    suspend fun updateNoOfVehicleInvolved(NoOfVehicleInvolved: String) =
        repository.updateNoOfVehicleInvolved(NoOfVehicleInvolved)

    suspend fun updateOverTime(OverTime: Boolean) =
        repository.updateOverTime(OverTime)

    suspend fun updateReportingDate(ReportingDate: String) =
        repository.updateReportingDate(ReportingDate)

    suspend fun updateReportingTime(ReportingTime: String) =
        repository.updateReportingTime(ReportingTime)

    suspend fun updateOverTimeHours(OverTimeHours: String) =
        repository.updateOverTimeHours(OverTimeHours)

    suspend fun updateHospital(Hospital: Boolean) =
        repository.updateHospital(Hospital)

    suspend fun updatePolice(Police: Boolean) =
        repository.updatePolice(Police)

    suspend fun updateCivilDefence(CivilDefence: Boolean) =
        repository.updateCivilDefence(CivilDefence)

    suspend fun getData() : List<AccidentIncidentEntity> {
        return repository.getData()
    }
}