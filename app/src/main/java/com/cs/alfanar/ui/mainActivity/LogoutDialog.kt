package com.cs.alfanar.ui.mainActivity

import android.app.Activity
import android.app.AlarmManager
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.system.Os.close
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import com.cs.alfanar.R
import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.ui.splashscreen.SplashScreenActivity
import com.cs.alfanar.ui.welcomeScreen.WelcomeScreenActivity
import org.kodein.di.bindings.ScopeCloseable
import java.io.File


fun logOutPopUp(context: Activity) {
    var customDialog: AlertDialog? = null
    val dialogBuilder =
        AlertDialog.Builder(context)

    val inflater = context.layoutInflater
    val layout: Int = R.layout.dialog_message
    val dialogView = inflater.inflate(layout, null)
    dialogBuilder.setView(dialogView)
    dialogBuilder.setCancelable(false)

    val title = dialogView.findViewById<View>(R.id.title) as TextView
    val desc = dialogView.findViewById<View>(R.id.desc) as TextView
    val yes = dialogView.findViewById<View>(R.id.pos_btn) as TextView
    val no = dialogView.findViewById<View>(R.id.ngt_btn) as TextView

    desc.text = context.resources.getString(R.string.logout_message)
    customDialog = dialogBuilder.create()
    customDialog.show()
    val finalCustomDialog = customDialog

    yes.setOnClickListener {
        finalCustomDialog!!.dismiss()
        UserPreferences(context).clearData()

        try {
            val databasesDir = File(context.applicationInfo.dataDir.toString() + "/databases")
            File(databasesDir, "AlfanarDatabase.db").delete()
            AppDatabase.destroy()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val intent = Intent(context, WelcomeScreenActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
        context.finish()

//        val mStartActivity = Intent(context, SplashScreenActivity::class.java)
//        val mPendingIntentId = 123456
//        val mPendingIntent = PendingIntent.getActivity(
//            context,
//            mPendingIntentId,
//            mStartActivity,
//            PendingIntent.FLAG_CANCEL_CURRENT
//        )
//        val mgr = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
//        mgr[AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 100] = mPendingIntent
//        System.exit(0)
    }

    no.setOnClickListener {
        finalCustomDialog!!.dismiss()
    }

    val lp = WindowManager.LayoutParams()
    val window = customDialog.window
    window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    lp.copyFrom(window.attributes)
    //This makes the dialog take up the full width
    val display = context.windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    val screenWidth = size.x
    val d = screenWidth * 0.85
    lp.width = d.toInt()
    lp.height = WindowManager.LayoutParams.WRAP_CONTENT
    window.attributes = lp
}