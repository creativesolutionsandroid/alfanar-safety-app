package com.cs.alfanar.ui.weeklyReport.weeklyReportDashboard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportProjectSelection.WeeklyReportProjectSelectionViewModel
import com.cs.alfanar.ui.weeklyReport.weeklyReportProjectSelection.WeeklyReportProjectSelectionViewModelFactory
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage1.WeeklyReportStage1Activity
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage2.WeeklyReportStage2Activity
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage3.WeeklyReportStage3Activity
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage4.WeeklyReportStage4Activity
import kotlinx.android.synthetic.main.activity_weekly_report_dashboard.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class WeeklyReportDashboardActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: WeeklyReportDashboardViewModelFactory by instance()

    private lateinit var viewModel: WeeklyReportDashboardViewModel

//    private val TAG : String = this::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weekly_report_dashboard)
        viewModel = ViewModelProvider(this, factory).get(WeeklyReportDashboardViewModel::class.java)

    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onStage1Clicked(view: View) {
        startActivity(Intent(this, WeeklyReportStage1Activity::class.java))
    }

    fun onStage2Clicked(view: View) {
        startActivity(Intent(this, WeeklyReportStage2Activity::class.java))
    }

    fun onStage3Clicked(view: View) {
        startActivity(Intent(this, WeeklyReportStage3Activity::class.java))
    }

    fun onStage4Clicked(view: View) {
        startActivity(Intent(this, WeeklyReportStage4Activity::class.java))
    }

    fun fetchDataFromDB() {
        lifecycleScope.launch {
            val stage1Data = viewModel.getStage1Data()
            val stage2Data = viewModel.getStage2Data()
            val stage3Data = viewModel.getStage3Data()
            val checklistData = viewModel.getCheckListData()
            val drillListData = viewModel.getDrillListData()
            val otherHealthData = viewModel.getOtherHealthData()
            val hseData = viewModel.getHSEData()
            val vulnerableData = viewModel.getVulnerableData()

            if (stage1Data.size == 2) icon_tick_hse_report.visibility = View.VISIBLE
            if (stage2Data.size > 0) icon_tick_daily_toolbox.visibility = View.VISIBLE
            if (stage3Data.size > 0) icon_tick_weekly_health_safety.visibility = View.VISIBLE
            if (checklistData.size > 0 && drillListData.size > 0 &&
                (otherHealthData.size == 1 && !otherHealthData[0].Name.equals(""))
                && (hseData.size == 1 && !hseData[0].Name.equals(""))
                && (vulnerableData.size == 1 && !vulnerableData[0].Name.equals(""))) {
                icon_tick_checklist_details.visibility = View.VISIBLE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        fetchDataFromDB()
    }
}