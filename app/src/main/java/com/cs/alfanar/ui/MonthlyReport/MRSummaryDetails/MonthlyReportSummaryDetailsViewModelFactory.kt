package com.cs.alfanar.ui.MonthlyReport.MRSummaryDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportSummaryDetailsViewModelFactory (
    private val repository: MonthlyReportSummaryDetailsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportSummaryDetailsViewModel(repository) as T
    }
}