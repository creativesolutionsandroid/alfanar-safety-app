package com.cs.alfanar.ui.alertNotification

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.alertNotification.Data
import com.cs.alfanar.ui.alertNotificationInbox.AlertNotificationInboxActivity
import com.cs.alfanar.ui.alertNotificationInsert.AlertNotificationInsertActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_alert_notification.*
import kotlinx.android.synthetic.main.activity_daily_report_summary.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class AlertNotificationActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: AlertNotificationViewModelFactory by instance()

    private lateinit var viewModel: AlertNotificationViewModel
    private lateinit var adapter: AlertNotificationAdapter

//    private val TAG : String = this::class.java.simpleName
    private val DATA_KEY: String = "DATA_KEY"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alert_notification)

        viewModel = ViewModelProvider(this, factory).get(AlertNotificationViewModel::class.java)

        if (UserPreferences(this).roleId() == ROLE_REGULAR_USER) {
            inbox.visibility = View.INVISIBLE
        }

        initRecyclerView()
        getDocumentListApi()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onInboxClicked(view: View) {
        startActivity(Intent(this, AlertNotificationInboxActivity::class.java))
    }

    fun prepareInputJsonForAlertNotification(): String {
        val parentObj = JSONObject()

//        Log.d(TAG, "prepareInputJsonForAlertNotification: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getDocumentListApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getAlertNotifications(prepareInputJsonForAlertNotification())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    adapter.setList(response.Data, this@AlertNotificationActivity)
                    adapter.notifyDataSetChanged()
                    list_alert_notification.scheduleLayoutAnimation()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@AlertNotificationActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@AlertNotificationActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@AlertNotificationActivity,
                    null
                )
            }
        }
    }

    private fun initRecyclerView(){
        list_alert_notification.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_alert_notification.addItemDecoration(itemDecoration)

        adapter = AlertNotificationAdapter({ selectedItem: Data ->listItemClicked(selectedItem)})
        list_alert_notification.adapter = adapter
    }

    private fun listItemClicked(data: Data){
        intent = Intent(this, AlertNotificationInsertActivity::class.java)
        intent.putExtra(DATA_KEY, data.Id)
        startActivity(intent)
    }
}