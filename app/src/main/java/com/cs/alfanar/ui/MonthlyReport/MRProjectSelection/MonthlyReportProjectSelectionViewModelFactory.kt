package com.cs.alfanar.ui.MonthlyReport.MRProjectSelection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportProjectSelectionViewModelFactory (
    private val repository: MonthlyReportProjectSelectionRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportProjectSelectionViewModel(repository) as T
    }
}