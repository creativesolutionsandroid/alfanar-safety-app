package com.cs.alfanar.ui.trainingAttendanceRecord.Stage1

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEmployeeEntity
import com.cs.alfanar.databinding.ItemTrainingAttendanceListBinding

class TrainingAttendanceRecordAdapter : RecyclerView.Adapter<MyViewHolder>() {
    private val subscribersList = ArrayList<TrainingAttendanceRecordEmployeeEntity>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemTrainingAttendanceListBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_training_attendance_list,
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position], position)
    }

    fun setList(subscribers: List<TrainingAttendanceRecordEmployeeEntity>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemTrainingAttendanceListBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: TrainingAttendanceRecordEmployeeEntity, position: Int) {

        binding.serialNumber.text = ""+(position + 1) + "."
        binding.topic.text = data.Name
        binding.tiltleDesignation.text = data.Designation
        binding.titleFileNumber.text = data.FileNumber
        binding.titleMode.text = data.Department
    }
}