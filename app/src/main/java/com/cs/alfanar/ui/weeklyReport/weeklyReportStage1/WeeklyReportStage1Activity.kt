package com.cs.alfanar.ui.weeklyReport.weeklyReportStage1

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage1Entity
import com.cs.alfanar.model.weeklyReport.Stage1Data
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage2.WeeklyReportStage2Activity
import com.cs.alfanar.utils.CONTRACTOR_ALFANAR
import com.cs.alfanar.utils.CONTRACTOR_SUB
import kotlinx.android.synthetic.main.activity_weekly_report_stage1.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class WeeklyReportStage1Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: WeeklyReportStage1ViewModelFactory by instance()

    private lateinit var viewModel: WeeklyReportStage1ViewModel

//    private val TAG : String = this::class.java.simpleName

    var stage1DataList: MutableList<Stage1Data> = mutableListOf()
    var entityDataList: List<WRstage1Entity> = mutableListOf()

    var reportType: Int = CONTRACTOR_ALFANAR
    val counts = IntArray(14)

    private lateinit var adapter: WeeklyReportStage1Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weekly_report_stage1)
        viewModel = ViewModelProvider(this, factory).get(WeeklyReportStage1ViewModel::class.java)

        initRecyclerView()
        fetchDataFromDb()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val data = viewModel.getDataByType(reportType)
//            Log.d(TAG, "fetchDataFromDb: "+data)
            if (data.size > 0) {
                counts[0] = data[0].ManpowerAtSite
                counts[1] = data[0].ManHour
                counts[2] = data[0].SafeManHours
                counts[3] = data[0].CumulativeManHour
                counts[4] = data[0].LostTimeInjuries
                counts[5] = data[0].DaysLost
                counts[6] = data[0].Accidents
                counts[7] = data[0].AccidentInvestigation
                counts[8] = data[0].MedicalTCase
                counts[9] = data[0].FirstAidCases
                counts[10] = data[0].NearMiss
                counts[11] = data[0].SickLeave
                counts[12] = data[0].AlfanarSafety
                counts[13] = data[0].Attendee
                prepareData()
            }
            else {
                val stage1Data = WRstage1Entity(reportType, reportType, 0,
                    0, 0, 0, 0,
                    0,0,1,0,
                    0,0,0,0,0,false)
                viewModel.saveData(stage1Data)
                fetchDataFromDb()
            }
        }
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextClicked(view: View) {
        lifecycleScope.launch {
            val dataList = viewModel.getDataByType(CONTRACTOR_SUB)
            if (dataList.size == 0) {
                layout_subcontractor.performClick()
            } else {
                startActivity(Intent(this@WeeklyReportStage1Activity, WeeklyReportStage2Activity::class.java))
            }
        }
    }

    fun onAlfanarClicked(view: View) {
        layout_alfanar.setTextColor(resources.getColor(R.color.white))
        layout_alfanar.setBackgroundDrawable(resources.getDrawable(R.drawable.shape_weekly_report_type_selected))

        layout_subcontractor.setTextColor(resources.getColor(R.color.weekly_type_unselected_text))
        layout_subcontractor.setBackgroundDrawable(resources.getDrawable(R.drawable.shape_weekly_report_type))
        reportType = CONTRACTOR_ALFANAR
        fetchDataFromDb()
    }

    fun onSubcontractorClicked(view: View) {
        layout_subcontractor.setTextColor(resources.getColor(R.color.white))
        layout_subcontractor.setBackgroundDrawable(resources.getDrawable(R.drawable.shape_weekly_report_type_selected))

        layout_alfanar.setTextColor(resources.getColor(R.color.weekly_type_unselected_text))
        layout_alfanar.setBackgroundDrawable(resources.getDrawable(R.drawable.shape_weekly_report_type))

        reportType = CONTRACTOR_SUB
        fetchDataFromDb()
    }

    fun prepareData() {
        stage1DataList.clear()
        for (i in 1..14) {
            val stringId = resources.getIdentifier("weekly_sub_title_" + i, "string", packageName)
            val stage1Data = Stage1Data(i, resources.getString(stringId),
                if (i == 8) false else true)
            stage1DataList.add(stage1Data)
        }
        setData()
    }

    private fun setData() {
        adapter.setList(stage1DataList, counts,this@WeeklyReportStage1Activity)
        adapter.notifyDataSetChanged()
        list_weekly_subtitles.scheduleLayoutAnimation()
    }

    private fun initRecyclerView(){
        list_weekly_subtitles.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_weekly_subtitles.addItemDecoration(itemDecoration)

        adapter = WeeklyReportStage1Adapter({ count: Int, position: Int ->onCountChanged(count,position)})
        list_weekly_subtitles.adapter = adapter
    }

    private fun onCountChanged(count: Int, position: Int){
        lifecycleScope.launch {
            counts[position] = count
            if (position == 0) {
                viewModel.updateManpowerAtSite(count, reportType)
            }
            else if (position == 1) {
                viewModel.updateManHour(count, reportType)
            }
            else if (position == 2) {
                viewModel.updateSafeManHours(count, reportType)
            }
            else if (position == 3) {
                viewModel.updateCumulativeManHour(count, reportType)
            }
            else if (position == 4) {
                viewModel.updateLostTimeInjuries(count, reportType)
            }
            else if (position == 5) {
                viewModel.updateDaysLost(count, reportType)
            }
            else if (position == 6) {
                viewModel.updateAccidents(count, reportType)
            }
            else if (position == 7) {
                viewModel.updateAccidentInvestigation(count, reportType)
            }
            else if (position == 8) {
                viewModel.updateMedicalTCase(count, reportType)
            }
            else if (position == 9) {
                viewModel.updateFirstAidCases(count, reportType)
            }
            else if (position == 10) {
                viewModel.updateNearMiss(count, reportType)
            }
            else if (position == 11) {
                viewModel.updateSickLeave(count, reportType)
            }
            else if (position == 12) {
                viewModel.updateAlfanarSafety(count, reportType)
            }
            else if (position == 13) {
                viewModel.updateAttendee(count, reportType)
            }
        }
    }
}