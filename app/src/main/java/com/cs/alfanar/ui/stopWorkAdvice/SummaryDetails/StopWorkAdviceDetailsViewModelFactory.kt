package com.cs.alfanar.ui.stopWorkAdvice.SummaryDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class StopWorkAdviceDetailsViewModelFactory (
    private val repository: StopWorkAdviceDetailsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return StopWorkAdviceDetailsViewModel(repository) as T
    }
}