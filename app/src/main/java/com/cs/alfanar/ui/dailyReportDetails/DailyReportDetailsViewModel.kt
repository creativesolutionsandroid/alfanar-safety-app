package com.cs.alfanar.ui.dailyReportDetails

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.dailyReportSummary.DailyReportSummaryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DailyReportDetailsViewModel (private val repository: DailyReportDetailsRepository) : ViewModel() {

    // UpdateDailyReportStatus
    suspend fun UpdateDailyReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.UpdateDailyReportStatus(inputJson)
    }

}