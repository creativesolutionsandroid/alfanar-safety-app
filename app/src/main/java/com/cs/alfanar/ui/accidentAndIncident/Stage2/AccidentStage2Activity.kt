package com.cs.alfanar.ui.accidentAndIncident.Stage2

import android.app.TimePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.Button
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.adapters.AccidentAdapter
import com.cs.alfanar.adapters.NationalityAdapter
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.model.accidentIncident.ChildItem
import com.cs.alfanar.model.accidentIncident.Data
import com.cs.alfanar.model.accidentIncident.Master
import com.cs.alfanar.model.masterData.Nationality
import com.cs.alfanar.ui.accidentAndIncident.Stage1.AccidentStage1ViewModel
import com.cs.alfanar.ui.accidentAndIncident.Stage1.AccidentStage1ViewModelFactory
import com.cs.alfanar.ui.accidentAndIncident.Stage3.AccidentStage3Activity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_accident_stage1.*
import kotlinx.android.synthetic.main.activity_accident_stage1.project_id
import kotlinx.android.synthetic.main.activity_accident_stage1.spinner_project_name
import kotlinx.android.synthetic.main.activity_accident_stage2.*
import kotlinx.android.synthetic.main.activity_daily_report.*
import kotlinx.android.synthetic.main.activity_weekly_report_stage2.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class AccidentStage2Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: AccidentStage2ViewModelFactory by instance()

    private lateinit var viewModel: AccidentStage2ViewModel

//    private val TAG : String = this::class.java.simpleName
    val KEY_DATA: String = "DATA_KEY"

    private var shiftTimeList: MutableList<ChildItem> = mutableListOf()
    private var causesList: MutableList<ChildItem> = mutableListOf()
    private var nationalityList: List<Nationality> = mutableListOf()
    private lateinit var masterData: Data
    var selectedHour : Int = 0
    var selectedMin : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accident_stage2)

        viewModel = ViewModelProvider(this, factory).get(AccidentStage2ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Data
        masterData = Data!!
        filterData(masterData.MasterList)
        fetchDataFromDb()
        getNationalityApi()
        addTextWatcher()

        task.setImeOptions(EditorInfo.IME_ACTION_NEXT)
        task.setRawInputType(InputType.TYPE_CLASS_TEXT)
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextButtonClicked(view: View) {
        validationCheck()
    }

    fun ontimeClicked(view: View) {
        showCustomTimePicker()
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                task.setText(projectData[0].Task)
                iqama_number.setText(projectData[0].Iqama)
                age.setText(projectData[0].Age)
                spinner_nationality.setText(projectData[0].Nationality)
                spinner_shift_time.setText(projectData[0].ShiftTime)
                spinner_nationality.setText(projectData[0].Nationality)
                incident_location.setText(projectData[0].IncidentLocation)
                incident_time.setText(projectData[0].IncidentTime)
                spinner_cause.setText(projectData[0].CauseOfIncident)
                first_aid_treatment.setText(projectData[0].FirstAidtreatment)
                incident_description.setText(projectData[0].AccidentDescription)
            }
        }
    }

    fun addTextWatcher() {
        task.afterTextChanged { updateTask(task.text.toString().trim()) }
        iqama_number.afterTextChanged { updateIqama(iqama_number.text.toString().trim()) }
        age.afterTextChanged { updateAge(age.text.toString().trim()) }
        incident_location.afterTextChanged { updateIncidentLocation(incident_location.text.toString().trim()) }
        first_aid_treatment.afterTextChanged { updateFirstAidtreatment(first_aid_treatment.text.toString().trim()) }
        incident_description.afterTextChanged { updateAccidentDescription(incident_description.text.toString().trim()) }
    }

    fun filterData(data: List<Master>) {
        for (i in 0 until data.size) {
            if (data[i].Id == 25) { // Shift Time
                for (j in 0 until data[i].ChildItem.size) {
                    shiftTimeList.add(data[i].ChildItem[j])
                }
            }
            else if (data[i].Id == 28) { // Cause of Incident/ Accident
                for (j in 0 until data[i].ChildItem.size) {
                    causesList.add(data[i].ChildItem[j])
                }
            }
        }

        setShiftTimeSpinnerData()
        setCausesSpinnerData()
    }

    fun setShiftTimeSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                shiftTimeList
            )
        spinner_shift_time.setAdapter(accidentAdapter)


        spinner_shift_time.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = shiftTimeList.get(position).ActionName
                spinner_shift_time.setText(selectedItem)
                updateShiftTime(position)
            }

        // Disabling keyboard for spinners
        spinner_shift_time.setInputType(InputType.TYPE_NULL)
    }

    fun setCausesSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                causesList
            )
        spinner_cause.setAdapter(accidentAdapter)

        spinner_cause.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = causesList.get(position).ActionName
                spinner_cause.setText(selectedItem)
                updateCauseOfIncident(position)
            }

        // Disabling keyboard for spinners
        spinner_cause.setInputType(InputType.TYPE_NULL)
    }

    fun setNationalitySpinnerData() {
        val accidentAdapter: NationalityAdapter =
            NationalityAdapter(
                this,
                android.R.layout.simple_list_item_1,
                nationalityList
            )
        spinner_nationality.setAdapter(accidentAdapter)

        spinner_nationality.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = nationalityList.get(position).NameEn
                spinner_nationality.setText(selectedItem)
                updateNationality(position)
            }

        // Disabling keyboard for spinners
        spinner_nationality.setInputType(InputType.TYPE_NULL)
    }

    fun updateNationality(position: Int) {
        lifecycleScope.launch {
            viewModel.updateNationality(nationalityList[position].NameEn, nationalityList[position].Id)
        }
    }

    fun updateShiftTime(position: Int) {
        lifecycleScope.launch {
            viewModel.updateShiftTime(shiftTimeList[position].ActionName, shiftTimeList[position].Id)
        }
    }

    fun updateCauseOfIncident(position: Int) {
        lifecycleScope.launch {
            viewModel.updateCauseOfIncident(causesList[position].ActionName, causesList[position].Id)
        }
    }

    fun updateTask(Name: String) {
        lifecycleScope.launch {
            viewModel.updateTask(Name)
        }
    }

    fun updateIqama(Name: String) {
        lifecycleScope.launch {
            viewModel.updateIqama(Name)
        }
    }

    fun updateAge(Name: String) {
        lifecycleScope.launch {
            viewModel.updateAge(Name)
        }
    }

    fun updateFirstAidtreatment(Name: String) {
        lifecycleScope.launch {
            viewModel.updateFirstAidtreatment(Name)
        }
    }

    fun updateIncidentLocation(Name: String) {
        lifecycleScope.launch {
            viewModel.updateIncidentLocation(Name)
        }
    }

    fun updateAccidentDescription(Name: String) {
        lifecycleScope.launch {
            viewModel.updateAccidentDescription(Name)
        }
    }

    fun updateTime() {
        val time = getDuration()
        lifecycleScope.launch {
            viewModel.updateIncidentTime(time)
        }
    }

    fun getNationalityApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getMasterData()
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    nationalityList = response.Data.Nationalities
                    setNationalitySpinnerData()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(response.MessageEn, getString(R.string.Ok), this@AccidentStage2Activity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(getString(R.string.str_cannot_reach_server), getString(R.string.Ok), this@AccidentStage2Activity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(getString(R.string.str_connection_error), getString(R.string.Ok), this@AccidentStage2Activity,
                    null
                )
            }
        }
    }

    fun showCustomTimePicker() {
        val myTimeListener =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                if (view.isShown) {
                    selectedHour = hourOfDay
                    selectedMin = minute
                    incident_time.text = getDuration()
                    updateTime()
                }
            }
        val timePickerDialog = CustomTimePickerDialogFor1Minute(
            this,
            myTimeListener,
            selectedHour,
            selectedMin,
            true
        )
        timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        timePickerDialog.show()
    }

    fun getDuration(): String {
        var duration: String = ""
        val hour: String
        val min: String
        var filteredHour: Int

        if (selectedHour < 10) {
            hour = "0" +selectedHour.toString()
        }
        else {
            hour = selectedHour.toString()
        }

        if (selectedMin < 10) {
            min = "0" + selectedMin.toString()
        }
        else  {
            min = selectedMin.toString()
        }
        duration = hour + ":" + min
        return duration
    }

    fun validationCheck() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()

            if (projectData[0].Task.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_task), getString(R.string.Ok),
                    this@AccidentStage2Activity, null
                )
            } else if (projectData[0].Iqama.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_file_iqama), getString(R.string.Ok),
                    this@AccidentStage2Activity, null
                )
            } else if (projectData[0].Age.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_age), getString(R.string.Ok),
                    this@AccidentStage2Activity, null
                )
            } else if (projectData[0].ShiftTime.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_shift_time), getString(R.string.Ok),
                    this@AccidentStage2Activity, null
                )
            } else if (projectData[0].Nationality.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_nationality),
                    getString(R.string.Ok),
                    this@AccidentStage2Activity,
                    null
                )
            } else if (projectData[0].IncidentLocation.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_incident_location), getString(R.string.Ok),
                    this@AccidentStage2Activity, null
                )
            } else if (projectData[0].IncidentTime.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_incident_time), getString(R.string.Ok),
                    this@AccidentStage2Activity, null
                )
            } else if (projectData[0].CauseOfIncident.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_cause_of_accident), getString(R.string.Ok),
                    this@AccidentStage2Activity, null
                )
            }  else if (projectData[0].FirstAidtreatment.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_first_aid_treatment), getString(R.string.Ok),
                    this@AccidentStage2Activity, null
                )
            }  else if (projectData[0].AccidentDescription.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_incident_description), getString(R.string.Ok),
                    this@AccidentStage2Activity, null
                )
            } else {
                val intent = Intent(this@AccidentStage2Activity, AccidentStage3Activity::class.java)
                intent.putExtra(KEY_DATA, masterData)
                startActivity(intent)
            }
        }
    }
}