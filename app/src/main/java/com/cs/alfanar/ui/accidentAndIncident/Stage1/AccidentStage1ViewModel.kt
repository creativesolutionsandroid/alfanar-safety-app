package com.cs.alfanar.ui.accidentAndIncident.Stage1

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccidentStage1ViewModel (private val repository: AccidentStage1Repository) : ViewModel() {

    suspend fun GetAccidentIncidentReportMasterData(inputJson : String) = withContext(Dispatchers.IO) {
        repository.GetAccidentIncidentReportMasterData(inputJson)
    }

    suspend fun insert(data: AccidentIncidentEntity) = repository.insert(data)

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) =
        repository.updateProject(projectId, projectName, projectCode)

    suspend fun updateDate(date: String)  =
        repository.updateDate(date)

    suspend fun updateAccidentType(AccidentType: String, AccidentId: Int)  =
        repository.updateAccidentType(AccidentType, AccidentId)

    suspend fun updateTime(time: String)  =
        repository.updateTime(time)

    suspend fun updateInjuredEmployeeName(InjuredEmployeeName: String)  =
        repository.updateInjuredEmployeeName(InjuredEmployeeName)

    suspend fun updateCompanyContractor(CompanyContractor: String)  =
        repository.updateCompanyContractor(CompanyContractor)

    suspend fun updateAccidentClass(AccidentClass: String, AccidentClassId: Int)  =
        repository.updateAccidentClass(AccidentClass, AccidentClassId)

    suspend fun updateBodyPartInjured(BodyPartInjured: String, BodyPartInjuredId: String)  =
        repository.updateBodyPartInjured(BodyPartInjured, BodyPartInjuredId)

    suspend fun updateBodyPartInjuredOther(BodyPartInjuredOther: String)  =
        repository.updateBodyPartInjuredOther(BodyPartInjuredOther)

    suspend fun getData() : List<AccidentIncidentEntity> {
        return repository.getData()
    }

    suspend fun deleteData() = repository.deleteData()
}