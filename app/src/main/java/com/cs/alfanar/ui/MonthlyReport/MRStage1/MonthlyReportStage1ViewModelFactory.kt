package com.cs.alfanar.ui.MonthlyReport.MRStage1

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage1ViewModelFactory (
    private val repository: MonthlyReportStage1Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage1ViewModel(repository) as T
    }
}