package com.cs.alfanar.ui.weeklyReport.weeklyReportStage1

import android.content.Context
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ItemWeeklyReportStage1Binding
import com.cs.alfanar.model.weeklyReport.Stage1Data
import com.cs.alfanar.utils.afterTextChanged


class WeeklyReportStage1Adapter(
    private val onCountChanged: (Int, Int) -> Unit
) : RecyclerView.Adapter<MyViewHolder>() {

    private val subscribersList = ArrayList<Stage1Data>()
    private var counts = IntArray(14)
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemWeeklyReportStage1Binding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_weekly_report_stage1,
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position], counts[position], position, onCountChanged, appContext)
    }

    fun setList(subscribers: List<Stage1Data>, count: IntArray, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        counts = IntArray(14)
        counts = count
        appContext = context
    }

}

class MyViewHolder(val binding: ItemWeeklyReportStage1Binding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        data: Stage1Data,
        count: Int,
        position: Int,
        onCountChanged: (Int, Int) -> Unit,
        context: Context
    ) {

        binding.title.text = data.name
        binding.count.setText(count.toString())
        if (!data.isTextField) {
            binding.layoutCount.visibility = View.INVISIBLE
            binding.switchStatus.visibility = View.VISIBLE
        }
        else {
            binding.layoutCount.visibility = View.VISIBLE
            binding.switchStatus.visibility = View.INVISIBLE
        }

        if (position == 1 || position == 2) {
            binding.count.setFilters(arrayOf<InputFilter>(LengthFilter(8)))
        }
        else {
            binding.count.setFilters(arrayOf<InputFilter>(LengthFilter(6)))
        }

        if (count == 0) {
            binding.switchStatus.setImageDrawable(context.resources.getDrawable(R.drawable.switch_off))
        }
        else {
            binding.switchStatus.setImageDrawable(context.resources.getDrawable(R.drawable.switch_on))
        }

        binding.switchStatus.setOnClickListener {
            val presentCount = binding.count.text.toString().toInt()
            if (presentCount % 2 == 0) {
                binding.count.setText((presentCount + 1).toString())
                binding.switchStatus.setImageDrawable(context.resources.getDrawable(R.drawable.switch_on))
                onCountChanged(1, adapterPosition)
            }
            else {
                binding.count.setText((presentCount + 1).toString())
                binding.switchStatus.setImageDrawable(context.resources.getDrawable(R.drawable.switch_off))
                onCountChanged(0, adapterPosition)
            }
        }

        binding.plus.setOnClickListener {
            val presentCount = binding.count.text.toString().toInt()
            binding.count.setText((presentCount + 1).toString())
            onCountChanged(presentCount + 1, adapterPosition)
        }
        binding.minus.setOnClickListener {
            val presentCount = binding.count.text.toString().toInt()
            if (presentCount > 0) {
                binding.count.setText((presentCount - 1).toString())
                onCountChanged(presentCount - 1, adapterPosition)
            }
        }

        binding.count.afterTextChanged {
            if (binding.count.text.length > 0)
                onCountChanged(binding.count.text.toString().toInt(), adapterPosition)
        }

//        binding.count.setOnFocusChangeListener(OnFocusChangeListener { view, hasFocus ->
//            if (hasFocus) {
////                binding.count.setError("")
//            } else {
//                if (binding.count.text.length == 0) {
//                    binding.count.setError(context.getString(R.string.validation_error_empty_field))
//                }
//            }
//        })
    }
}