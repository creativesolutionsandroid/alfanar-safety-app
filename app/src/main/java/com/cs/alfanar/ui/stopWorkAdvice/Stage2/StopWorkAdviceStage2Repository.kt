package com.cs.alfanar.ui.stopWorkAdvice.Stage2

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkAdviceEntity
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkCommunicationEntity
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage2Entity
import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class StopWorkAdviceStage2Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun InsertStopWorkAdviceReport(inputJson : String): BasicResponse {
        return apiRequest { api.InsertStopWorkAdviceReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // database requests
    suspend fun getData(): List<StopWorkAdviceEntity> = db.getStopWorkAdviceDao().getData()

    suspend fun updateDescription(Description: String) = db.getStopWorkAdviceDao().updateDescription(Description)

    suspend fun saveData(data: StopWorkCommunicationEntity): Long = db.getStopWorkCommunicationDao().insert(data)

    suspend fun getCommunicationData(): List<StopWorkCommunicationEntity> = db.getStopWorkCommunicationDao().getCommunicationData()

    suspend fun getInformationData(): List<StopWorkCommunicationEntity> = db.getStopWorkCommunicationDao().getInformationData()

    suspend fun deleteCommunicationData() = db.getStopWorkCommunicationDao().deleteCommunicationData()

    suspend fun deleteStopWorkData() = db.getStopWorkCommunicationDao().deleteStopWorkData()

    suspend fun updateImage(Image: String, ImageName: String) = db.getStopWorkAdviceDao().updateImage(Image, ImageName)

}