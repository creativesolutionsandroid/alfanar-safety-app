package com.cs.alfanar.ui.MonthlyReport.MRSummaryDetails

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.MonthlyReportDetails.MonthlyReportDetails
import com.cs.alfanar.model.weeklyReportDetails.WeeklyReportDetailsResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class MonthlyReportSummaryDetailsRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getMonthlyReportDetails(inputJson : String): MonthlyReportDetails {
        return apiRequest { api.getMonthlyReportDetails(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun updateMonthlyReportStatus(inputJson : String): BasicResponse {
        return apiRequest { api.updateMonthlyReportStatus(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}