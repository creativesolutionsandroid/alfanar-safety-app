package com.cs.alfanar.ui.nearMissIncident.SummaryDetails

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.nearMissIncident.Summary.NearMissIncidentSummaryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NearMissIncidentDetailsViewModel (private val repository: NearMissIncidentDetailsRepository) : ViewModel() {

    suspend fun getWeeklyReportSummary(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getWeeklyReportSummary(inputJson)
    }

    suspend fun updateNearMissStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.updateNearMissStatus(inputJson)
    }
}