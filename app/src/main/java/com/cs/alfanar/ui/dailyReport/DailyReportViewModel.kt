package com.cs.alfanar.ui.dailyReport

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.alfanar.R
import com.cs.alfanar.ui.login.LoginFormState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DailyReportViewModel (private val repository: DailyReportRepository) : ViewModel() {

    private val _form = MutableLiveData<DailyReportFormState>()
    val formState: LiveData<DailyReportFormState> = _form

    // get project data api calling
    suspend fun getProjectDataForDailyReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getProjectDataForDailyReport(inputJson)
    }

    // insert daily report api calling
    suspend fun insertDailyReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.insertDailyReport(inputJson)
    }

    // data validation
    fun dataValidation(manPower: String, location: String, activity: String, unsafeAct: String, correctiveAction: String, actionTaken: String, areaActivityIncharge: String) {
        if (manPower.isBlank()) {
            _form.value = DailyReportFormState(manPowerError = R.string.validation_error_empty_field)
        }
        else if (location.isBlank()) {
            _form.value = DailyReportFormState(locationError = R.string.validation_error_empty_field)
        }
        else if (activity.isBlank()) {
            _form.value = DailyReportFormState(activityError = R.string.validation_error_empty_field)
        }
        else if (unsafeAct.isBlank()) {
            _form.value = DailyReportFormState(unsafeActError = R.string.validation_error_empty_field)
        }
        else if (correctiveAction.isBlank()) {
            _form.value = DailyReportFormState(correctiveError = R.string.validation_error_empty_field)
        }
        else if (actionTaken.isBlank()) {
            _form.value = DailyReportFormState(actionTakenError = R.string.validation_error_empty_field)
        }
        else if (areaActivityIncharge.isBlank()) {
            _form.value = DailyReportFormState(areaError = R.string.validation_error_empty_field)
        }
        else {
            _form.value = DailyReportFormState(isDataValid = true)
        }
    }
}