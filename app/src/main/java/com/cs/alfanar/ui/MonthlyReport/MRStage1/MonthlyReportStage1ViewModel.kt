package com.cs.alfanar.ui.MonthlyReport.MRStage1

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.monthlyReport.MRProjectionSelectionEntity
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage1Entity

class MonthlyReportStage1ViewModel  (private val repository: MonthlyReportStage1Repository) : ViewModel() {

    // database request
    suspend fun getData() : List<MRStage1Entity> {
        return repository.getData()
    }

    suspend fun saveData(projectData: MRStage1Entity) = repository.saveData(projectData)

    suspend fun updateSafeManHoursWorked(SafeManHoursWorked: Int) = repository.updateSafeManHoursWorked(SafeManHoursWorked)

    suspend fun updateTotalManHours(TotalManHours: Int) = repository.updateTotalManHours(TotalManHours)

    suspend fun updateDirectAlfanar(DirectAlfanar: Int) = repository.updateDirectAlfanar(DirectAlfanar)

    suspend fun updateDirectSC(DirectSC: Int) = repository.updateDirectSC(DirectSC)

    suspend fun updateInDirectAlfanar(InDirectAlfanar: Int) = repository.updateInDirectAlfanar(InDirectAlfanar)

    suspend fun updateInDirectSC(InDirectSC: Int) = repository.updateInDirectSC(InDirectSC)

    suspend fun updateFatal(Fatal: Int) = repository.updateFatal(Fatal)

    suspend fun updateLWC(LWC: Int) = repository.updateLWC(LWC)

}