package com.cs.alfanar.ui.accidentAndIncident.Stage2

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccidentStage2ViewModel (private val repository: AccidentStage2Repository) : ViewModel() {

    // Master data api calling
    suspend fun getMasterData() = withContext(Dispatchers.IO) { repository.getMasterData() }

    // DB changes
    suspend fun updateTask(Task: String)  =
        repository.updateTask(Task)

    suspend fun updateIqama(Iqama: String)  =
        repository.updateIqama(Iqama)

    suspend fun updateAge(Age: String)  =
        repository.updateAge(Age)

    suspend fun updateShiftTime(ShiftTime: String, ShiftTimeId: Int)  =
        repository.updateShiftTime(ShiftTime, ShiftTimeId)

    suspend fun updateNationality(Nationality: String, NationalityId: Int)  =
        repository.updateNationality(Nationality, NationalityId)

    suspend fun updateIncidentLocation(IncidentLocation: String)  =
        repository.updateIncidentLocation(IncidentLocation)

    suspend fun updateIncidentTime(IncidentTime: String)  =
        repository.updateIncidentTime(IncidentTime)

    suspend fun updateCauseOfIncident(CauseOfIncident: String, CauseOfIncidentId: Int)  =
        repository.updateCauseOfIncident(CauseOfIncident, CauseOfIncidentId)

    suspend fun updateAccidentDescription(AccidentDescription: String) =
        repository.updateAccidentDescription(AccidentDescription)

    suspend fun updateFirstAidtreatment(FirstAidtreatment: String) =
        repository.updateFirstAidtreatment(FirstAidtreatment)

    suspend fun getData() : List<AccidentIncidentEntity> {
        return repository.getData()
    }

}