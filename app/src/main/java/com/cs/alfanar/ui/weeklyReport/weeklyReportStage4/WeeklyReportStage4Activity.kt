package com.cs.alfanar.ui.weeklyReport.weeklyReportStage4

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.adapters.WeeklyReportChecklistAdapter
import com.cs.alfanar.data.db.entities.weeklyReport.*
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.weeklyReport.Checklist
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_weekly_report_stage4.*
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*

class WeeklyReportStage4Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: WeeklyReportStage4ViewModelFactory by instance()

    private lateinit var viewModel: WeeklyReportStage4ViewModel

    private lateinit var checkListAdapter: CheckListAdapter
    private lateinit var observationAdapter: ObservationAdapter
    private lateinit var otherHealthAdapter: OtherHealthAdapter
    private lateinit var hseAdapter: HSEAdapter
    private lateinit var vulnerableAdapter: VulnerableAdapter
    private lateinit var emergencyDrillAdapter: EmergencyDrillAdapter

//    private val TAG : String = this::class.java.simpleName
    var checkListData: MutableList<WRstage4ChecklistEntity> = mutableListOf()
    var otherHealthData: MutableList<WRstage4OtherHealthEntity> = mutableListOf()
    var hseData: MutableList<WRstage4HSEEntity> = mutableListOf()
    var vulnerableData: MutableList<WRstage4VulnerableEntity> = mutableListOf()
    var emergencyDrillData: MutableList<WRstage4EmergencyDrillEntity> = mutableListOf()
    var projectsList: List<Checklist> = mutableListOf()

    var checkListName: String = ""
    var checkListId: Int = 0
    val KEY_BACK_ACTION : String = "KEY_BACK_ACTION"

    var todayCalendar = Calendar.getInstance()
    var selectedDate: Int = 0
    var selectedMonth: Int = 0
    var selectedYear: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weekly_report_stage4)

        viewModel = ViewModelProvider(this, factory).get(WeeklyReportStage4ViewModel::class.java)

        getProjectDataForWeeklyReportApi()
        initRecyclerViews()
        fetchCheckListDataFromDb()
        fetchDrilltDataFromDb()
        fetchOtherHealthDataFromDb()
        fetchHSEDataFromDb()
        fetchVulnerableDataFromDb()
        setIMEOptions()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun setIMEOptions() {
        et_topic.setImeOptions(EditorInfo.IME_ACTION_DONE)
        et_topic.setRawInputType(InputType.TYPE_CLASS_TEXT)
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onDrillSubmitClicked(view: View) {
        if (et_topic.text.trim().length == 0) {
            et_topic.error = getString(R.string.validation_error_empty_field)
        }
        else if (selectedDate == 0) {
            showOneButtonAlertDialog(getString(R.string.error_date), getString(R.string.Ok), this, null)
        }
        else if (attendee.text.trim().length == 0) {
            attendee.error = getString(R.string.validation_error_empty_field)
        }
        else {
            saveDrillListDataToDB()
        }
    }

    fun onCheckListSubmitClicked(view: View) {
        saveCheckListDataToDB()
    }

    fun onDateClicked(view: View) {
        showDatePicker()
    }

    fun onEquipmentPlusClicked(view: View) {
        var equipment = equipments.text.toString().trim().toInt()
        equipment = equipment + 1
        equipments.setText(equipment.toString())
    }

    fun onEquipmentMinusClicked(view: View) {
        var equipment = equipments.text.toString().trim().toInt()
        if (equipment > 1) {
            equipment = equipment - 1
            equipments.setText(equipment.toString())
        }
    }

    fun onChecklistPlusClicked(view: View) {
        var checklists = checklist.text.toString().trim().toInt()
        checklists = checklists + 1
        checklist.setText(checklists.toString())
    }

    fun onChecklistMinusClicked(view: View) {
        var checklists = checklist.text.toString().trim().toInt()
        if (checklists > 0) {
            checklists = checklists - 1
            checklist.setText(checklists.toString())
        }
    }

    fun OtherHealthPlusClicked(view: View) {
        lifecycleScope.launch {
            val dataList = viewModel.getOtherHealthData()
            otherHealthData.clear()
            otherHealthData.addAll(dataList)
            if (otherHealthData.size > 0 && !otherHealthData[(otherHealthData.size - 1)].Name.equals("")) {
                val data = WRstage4OtherHealthEntity(otherHealthData.size + 1, "")
                viewModel.saveOtherHealthData(data)
                fetchOtherHealthDataFromDb()
            }
        }
    }

    fun HsePlusClicked(view: View) {
        lifecycleScope.launch {
            val dataList = viewModel.getHSEData()
            hseData.clear()
            hseData.addAll(dataList)
            if (hseData.size > 0 && !hseData[(hseData.size - 1)].Name.equals("")) {
                val data = WRstage4HSEEntity(hseData.size + 1, "")
                viewModel.saveHSEData(data)
                fetchHSEDataFromDb()
            }
        }
    }

    fun VulnerablePlusClicked(view: View) {
        lifecycleScope.launch {
            val dataList = viewModel.getVulnerableData()
            vulnerableData.clear()
            vulnerableData.addAll(dataList)
            if (vulnerableData.size > 0 && !vulnerableData[(vulnerableData.size - 1)].Name.equals("")) {
                val data = WRstage4VulnerableEntity(vulnerableData.size + 1, "")
                viewModel.saveVulnerableData(data)
                fetchVulnerableDataFromDb()
            }
        }
    }

    fun fetchCheckListDataFromDb() {
        lifecycleScope.launch {
            val dataList = viewModel.getCheckListData()
            checkListData.clear()
            checkListData.addAll(dataList)
            checkListAdapter.setList(checkListData, this@WeeklyReportStage4Activity)
            checkListAdapter.notifyDataSetChanged()
        }
    }

    fun fetchDrilltDataFromDb() {
        lifecycleScope.launch {
            val dataList = viewModel.getDrillData()
            emergencyDrillData.clear()
            emergencyDrillData.addAll(dataList)
            emergencyDrillAdapter.setList(emergencyDrillData, this@WeeklyReportStage4Activity)
            emergencyDrillAdapter.notifyDataSetChanged()
        }
    }

    fun fetchOtherHealthDataFromDb() {
        lifecycleScope.launch {
            val dataList = viewModel.getOtherHealthData()
            otherHealthData.clear()
            otherHealthData.addAll(dataList)
            if (otherHealthData.size > 0) {
                otherHealthAdapter.setList(otherHealthData, this@WeeklyReportStage4Activity)
                otherHealthAdapter.notifyDataSetChanged()
            }
            else {
                // Adding empty data
                val data = WRstage4OtherHealthEntity(1, "")
                viewModel.saveOtherHealthData(data)
                fetchOtherHealthDataFromDb()
            }
        }
    }

    fun fetchHSEDataFromDb() {
        lifecycleScope.launch {
            val dataList = viewModel.getHSEData()
            hseData.clear()
            hseData.addAll(dataList)
            if (hseData.size > 0) {
                hseAdapter.setList(hseData, this@WeeklyReportStage4Activity)
                hseAdapter.notifyDataSetChanged()
            }
            else {
                // Adding empty data
                val data = WRstage4HSEEntity(1, "")
                viewModel.saveHSEData(data)
                fetchHSEDataFromDb()
            }
        }
    }

    fun fetchVulnerableDataFromDb() {
        lifecycleScope.launch {
            val dataList = viewModel.getVulnerableData()
            vulnerableData.clear()
            vulnerableData.addAll(dataList)
            if (vulnerableData.size > 0) {
                vulnerableAdapter.setList(vulnerableData, this@WeeklyReportStage4Activity)
                vulnerableAdapter.notifyDataSetChanged()
            }
            else {
                // Adding empty data
                val data = WRstage4VulnerableEntity(1, "")
                viewModel.saveVulnerableData(data)
                fetchVulnerableDataFromDb()
            }
        }
    }

    fun saveCheckListDataToDB() {
        val equipments = equipments.text.toString().trim().toInt()
        val checklists = checklist.text.toString().trim().toInt()
        val data = WRstage4ChecklistEntity((checkListData.size + 1), checkListName, checkListId, equipments, checklists)
        lifecycleScope.launch {
            val id = viewModel.saveCheckListData(data)
            if (id > 0) {
                clearAllFields()
                fetchCheckListDataFromDb()
            }
        }
    }

    fun clearAllFields() {
        equipments.setText("1")
        checklist.setText("1")
    }

    fun saveDrillListDataToDB() {
        val topic = et_topic.text.toString().trim()
        val attendees = attendee.text.toString().trim().toInt()
        val date = "" + selectedDate + "-" + getMonth() + "-" + selectedYear
        val data = WRstage4EmergencyDrillEntity((emergencyDrillData.size + 1), topic, date, attendees)
        lifecycleScope.launch {
            val id = viewModel.updateDrillData(data)
            if (id > 0) {
                clearDrillFields()
                fetchDrilltDataFromDb()
            }
        }
    }

    fun clearDrillFields() {
        et_topic.setText("")
        attendee.setText("")
        date_drill.setText("")

        selectedDate = 0
        selectedMonth = 0
        selectedYear = 0
    }

    fun getMonth(): String {
        var month: String = ""
        if (selectedMonth < 9) {
            month = "0" + (selectedMonth + 1)
        }
        else {
            month = "" + (selectedMonth + 1)
        }
        return month
    }

    private fun initRecyclerViews(){
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))

        list_checklist.layoutManager = LinearLayoutManager(this)
        list_checklist.addItemDecoration(itemDecoration)
        checkListAdapter = CheckListAdapter()
        list_checklist.adapter = checkListAdapter

        list_observations.layoutManager = LinearLayoutManager(this)
        list_observations.addItemDecoration(itemDecoration)
        observationAdapter = ObservationAdapter()
        list_observations.adapter = observationAdapter

        list_other_health_safety.layoutManager = LinearLayoutManager(this)
        list_other_health_safety.addItemDecoration(itemDecoration)
        otherHealthAdapter = OtherHealthAdapter({name: String, position: Int ->onOtherHealthUpdated(name,position)})
        list_other_health_safety.adapter = otherHealthAdapter

        list_hse_good_practices.layoutManager = LinearLayoutManager(this)
        list_hse_good_practices.addItemDecoration(itemDecoration)
        hseAdapter = HSEAdapter({name: String, position: Int ->onHseUpdated(name,position)})
        list_hse_good_practices.adapter = hseAdapter

        list_vulnerable_area.layoutManager = LinearLayoutManager(this)
        list_vulnerable_area.addItemDecoration(itemDecoration)
        vulnerableAdapter = VulnerableAdapter({name: String, position: Int ->onVulnerableUpdated(name,position)})
        list_vulnerable_area.adapter = vulnerableAdapter

        list_drills.layoutManager = LinearLayoutManager(this)
        list_drills.addItemDecoration(itemDecoration)
        emergencyDrillAdapter = EmergencyDrillAdapter()
        list_drills.adapter = emergencyDrillAdapter
    }

    private fun onOtherHealthUpdated(name: String, position: Int){
        lifecycleScope.launch {
            viewModel.updateOtherHealthData(name, otherHealthData.get(position).id)
        }
    }

    private fun onHseUpdated(name: String, position: Int){
        lifecycleScope.launch {
            viewModel.updateHSEData(name, hseData.get(position).id)
        }
    }

    private fun onVulnerableUpdated(name: String, position: Int){
        lifecycleScope.launch {
            viewModel.updateVulnerableData(name, vulnerableData.get(position).id)
        }
    }


    fun prepareInputJsonForgetProjectData(): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())

//        Log.d(TAG, "prepareInputJsonForgetProjectData: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getProjectDataForWeeklyReportApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getProjectDataForWeeklyReport(prepareInputJsonForgetProjectData())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    projectsList = response.Data.Checklist
                    observationAdapter.setList(response.Data.Observation, this@WeeklyReportStage4Activity)
                    observationAdapter.notifyDataSetChanged()
                    if (projectsList.size > 0) {
                        setChecklistSpinnerData()
                    }
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@WeeklyReportStage4Activity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@WeeklyReportStage4Activity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@WeeklyReportStage4Activity,
                    null
                )
            }
        }
    }

    fun setChecklistSpinnerData() {
        val projectAdapter: WeeklyReportChecklistAdapter =
            WeeklyReportChecklistAdapter(
                this,
                android.R.layout.simple_list_item_1,
                projectsList
            )
        spinner_checklist.setAdapter(projectAdapter)

        spinner_checklist.setText(projectsList.get(0).CheckListNameEn) // by default first project will be selected
        checkListName = projectsList.get(0).CheckListNameEn
        checkListId = projectsList.get(0).Id

        spinner_checklist.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                spinner_checklist.setText(projectsList.get(position).CheckListNameEn) // by default first project will be selected
                checkListName = projectsList.get(position).CheckListNameEn
                checkListId = projectsList.get(position).Id
            }

        // Disabling keyboard for spinners
        spinner_checklist.setInputType(InputType.TYPE_NULL)
    }

    fun onReportSubmitClicked(view: View) {
        lifecycleScope.launch {
            val projectData = viewModel.getProjectData()
            val stage1Data = viewModel.getStage1Data()
            val stage2Data = viewModel.getStage2Data()
            val stage3Data = viewModel.getStage3Data()
            val checklistData = viewModel.getCheckListData()
            val drillListData = viewModel.getDrillData()
            val otherHealthData = viewModel.getOtherHealthData()
            val hseData = viewModel.getHSEData()
            val vulnerableData = viewModel.getVulnerableData()

            if (stage1Data.size < 2) {
                showOneButtonAlertDialog(getString(R.string.error_weekly_hse_report), getString(R.string.Ok),
                this@WeeklyReportStage4Activity, null)
            }
            else if (stage2Data.size == 0) {
                showOneButtonAlertDialog(getString(R.string.error_daily_toolbox_talk), getString(R.string.Ok),
                    this@WeeklyReportStage4Activity, null)
            }
            else if (stage3Data.size == 0) {
                showOneButtonAlertDialog(getString(R.string.error_weekly_health_safety), getString(R.string.Ok),
                    this@WeeklyReportStage4Activity, null)
            }
            else if (checklistData.size == 0) {
                showOneButtonAlertDialog(getString(R.string.error_checklist_details), getString(R.string.Ok),
                    this@WeeklyReportStage4Activity, null)
            }
            else if (drillListData.size == 0) {
                showOneButtonAlertDialog(getString(R.string.error_emergency_drill), getString(R.string.Ok),
                    this@WeeklyReportStage4Activity, null)
            }
            else if (otherHealthData.size == 1 && otherHealthData[0].Name.equals("")) {
                showOneButtonAlertDialog(getString(R.string.error_other_health_safety), getString(R.string.Ok),
                    this@WeeklyReportStage4Activity, null)
            }
            else if (hseData.size == 1&& hseData[0].Name.equals("")) {
                showOneButtonAlertDialog(getString(R.string.error_hse_good_practices), getString(R.string.Ok),
                    this@WeeklyReportStage4Activity, null)
            }
            else if (vulnerableData.size == 1&& vulnerableData[0].Name.equals("")) {
                showOneButtonAlertDialog(getString(R.string.error_vulnerable_area), getString(R.string.Ok),
                    this@WeeklyReportStage4Activity, null)
            }
            else {
                val parentObj = JSONObject()
                parentObj.put("ProjectId", projectData[0].projectId)
                parentObj.put("Location", projectData[0].location)
                parentObj.put("SubmittedBy", UserPreferences(this@WeeklyReportStage4Activity).userId())

                for (i in 0 until stage1Data.size) {
                    val stage1Obj = JSONObject()
                    stage1Obj.put("ManPower", stage1Data[i].ManpowerAtSite)
                    stage1Obj.put("TotManHour", stage1Data[i].ManHour)
                    stage1Obj.put("SafeManHour", stage1Data[i].SafeManHours)
                    stage1Obj.put("CumManHour", stage1Data[i].CumulativeManHour)
                    stage1Obj.put("NoLostTimeInjury", stage1Data[i].LostTimeInjuries)
                    stage1Obj.put("NoLostDays", stage1Data[i].DaysLost)
                    stage1Obj.put("NoAccidents", stage1Data[i].Accidents)
                    stage1Obj.put("IsInvestigation",
                        if (stage1Data[i].AccidentInvestigation == 0) false else true
                    )
                    stage1Obj.put("MedicalTCase", stage1Data[i].MedicalTCase)
                    stage1Obj.put("NoFirstAid", stage1Data[i].FirstAidCases)
                    stage1Obj.put("NoNearMisses", stage1Data[i].NearMiss)
                    stage1Obj.put("NoSickLeave", stage1Data[i].SickLeave)
                    stage1Obj.put("NoSafetyInduction", stage1Data[i].AlfanarSafety)
                    stage1Obj.put("NoSafetyAttendee", stage1Data[i].Attendee)
                    stage1Obj.put("CoType", stage1Data[i].ReportType)
                    if (stage1Data[i].ReportType == CONTRACTOR_ALFANAR) {
                        parentObj.put("WeeklyAlfanarDetail", stage1Obj)
                    }
                    else {
                        parentObj.put("WeeklySubContractorDetail", stage1Obj)
                    }
                }

                val weeklyTalkTrainingArray = JSONArray()
                for (i in 0 until stage2Data.size) {
                    val stage2Obj = JSONObject()
                    stage2Obj.put("TopicName", stage2Data[i].Topic)
                    stage2Obj.put("Duration", getFormattedTime(stage2Data[i].Duration))
                    stage2Obj.put("TrainingDate", getFormattedDate(stage2Data[i].Date))
                    stage2Obj.put("NoAttendee", stage2Data[i].Attendee)
                    stage2Obj.put("TalkType", 1)
                    stage2Obj.put("ConductedBy", stage2Data[i].ConductedBy)
                    weeklyTalkTrainingArray.put(stage2Obj)
                }
                for (i in 0 until stage3Data.size) {
                    val stage3Obj = JSONObject()
                    stage3Obj.put("TopicName", stage3Data[i].Topic)
                    stage3Obj.put("Duration", getFormattedTime(stage3Data[i].Duration))
                    stage3Obj.put("TrainingDate", getFormattedDate(stage3Data[i].Date))
                    stage3Obj.put("NoAttendee", stage3Data[i].Attendee)
                    stage3Obj.put("TalkType", 2)
                    stage3Obj.put("ConductedBy", stage3Data[i].ConductedBy)
                    weeklyTalkTrainingArray.put(stage3Obj)
                }
                parentObj.put("LstWeeklyTalkTraning", weeklyTalkTrainingArray)

                val checklistArray = JSONArray()
                for (i in 0 until checklistData.size) {
                    val checkListObj = JSONObject()
                    checkListObj.put("ChecklistId", checklistData[i].ChecklistId)
                    checkListObj.put("NoEquipment", checklistData[i].NoOfEquipments)
                    checkListObj.put("NoChecklist", checklistData[i].NoOfChecklist)
                    checklistArray.put(checkListObj)
                }
                parentObj.put("LstWeeklyCheckList", checklistArray)

                val drillListArray = JSONArray()
                for (i in 0 until drillListData.size) {
                    val stage3Obj = JSONObject()
                    stage3Obj.put("DrillName", drillListData[i].TopicName)
                    stage3Obj.put("DrillDate", getFormattedDate(drillListData[i].Date))
                    stage3Obj.put("NoParticipants", drillListData[i].NoOfChecklist)
                    drillListArray.put(stage3Obj)
                }
                parentObj.put("LstWeeklyEmergencyDrill", drillListArray)

                val weeklyHseArray = JSONArray()
                for (i in 0 until otherHealthData.size) {
                    if (!otherHealthData[i].Name.equals("")) {
                        val localObj = JSONObject()
                        localObj.put("Description", otherHealthData[i].Name)
                        localObj.put("HSEType", 1)
                        weeklyHseArray.put(localObj)
                    }
                }
                for (i in 0 until hseData.size) {
                    if (!hseData[i].Name.equals("")) {
                        val localObj = JSONObject()
                        localObj.put("Description", hseData[i].Name)
                        localObj.put("HSEType", 2)
                        weeklyHseArray.put(localObj)
                    }
                }
                for (i in 0 until vulnerableData.size) {
                    if (!vulnerableData[i].Name.equals("")) {
                        val localObj = JSONObject()
                        localObj.put("Description", vulnerableData[i].Name)
                        localObj.put("HSEType", 3)
                        weeklyHseArray.put(localObj)
                    }
                }
                parentObj.put("LstWeeklyHSEActivities", weeklyHseArray)

//                Log.d(TAG, "onReportSubmitClicked: "+parentObj.toString())

                submitWeeklyReportApi(parentObj.toString())
            }
        }
    }

    fun getFormattedDate(date: String): String {
        val calendar = Calendar.getInstance()
        calendar.time = SimpleDateFormat("dd-MM-yyyy", Locale.US).parse(date)
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US).format(calendar.time)
    }

    fun getFormattedTime(time: String): Int {
        val splitTime = time.split(":")
        val minutes = splitTime[0].toInt() * 60 + splitTime[1].toInt()
        return minutes
    }

    private fun submitWeeklyReportApi(inputJson: String) {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.insertWeeklyReport(inputJson)
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    lifecycleScope.launch {
                        viewModel.clearStage1Data()
                        viewModel.clearStage2Data()
                        viewModel.clearStage3Data()
                        viewModel.clearProjectData()
                        viewModel.clearChecklistData()
                        viewModel.clearOtherHealthData()
                        viewModel.clearHSEData()
                        viewModel.clearVulnerbaleData()
                        viewModel.clearDrillData()
                    }
                    val intent = Intent(this@WeeklyReportStage4Activity, WeeklyReportSummaryActivity::class.java)
                    intent.putExtra(KEY_BACK_ACTION, "home_screen")
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@WeeklyReportStage4Activity,
                    intent
                    )
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@WeeklyReportStage4Activity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@WeeklyReportStage4Activity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@WeeklyReportStage4Activity,
                    null
                )
            }
        }
    }

    fun showDatePicker() {
        selectedYear = todayCalendar.get(Calendar.YEAR)
        selectedMonth = todayCalendar.get(Calendar.MONTH)
        selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            todayCalendar.set(Calendar.YEAR, year)
            todayCalendar.set(Calendar.MONTH, monthOfYear)
            todayCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            selectedYear = todayCalendar.get(Calendar.YEAR)
            selectedMonth = todayCalendar.get(Calendar.MONTH)
            selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

            date_drill.setText(SimpleDateFormat("MMM dd", Locale.US).format(todayCalendar.time))
//            date.text = SimpleDateFormat("MMM dd", Locale.US).format(todayCalendar.time)
        }, selectedYear, selectedMonth, selectedDate)

        val c = Calendar.getInstance()
        c.add(Calendar.DATE, -7) // show previous 7 days only

        dpd.datePicker.minDate = c.timeInMillis
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }
}