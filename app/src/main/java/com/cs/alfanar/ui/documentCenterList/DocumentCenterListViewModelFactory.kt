package com.cs.alfanar.ui.documentCenterList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DocumentCenterListViewModelFactory (
    private val repository: DocumentCenterListRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DocumentCenterListViewModel(repository) as T
    }
}