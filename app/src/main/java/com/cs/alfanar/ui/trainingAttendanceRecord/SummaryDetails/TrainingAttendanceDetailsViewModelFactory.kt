package com.cs.alfanar.ui.trainingAttendanceRecord.SummaryDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TrainingAttendanceDetailsViewModelFactory (
    private val repository: TrainingAttendanceDetailsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TrainingAttendanceDetailsViewModel(repository) as T
    }
}