package com.cs.alfanar.ui.nearestHospital

import androidx.lifecycle.ViewModel
import com.cs.alfanar.model.nearestHospital.NearestHospitalResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NearestHospitalViewModel (private val repository: NearestHospitalRepository) : ViewModel()  {

    // get nearest hospital list api calling
    suspend fun getHospitalsList(location : String, api_key: String) = withContext(Dispatchers.IO) {
        repository.getHospitalsList(location, api_key)
    }

}