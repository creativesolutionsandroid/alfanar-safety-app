package com.cs.alfanar.ui.dailyReport

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ItemCalendarBinding
import com.cs.alfanar.model.dailyReport.CalendarData
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DailyReportCalendarAdapter (private val clickListener:(CalendarData)->Unit)
    : RecyclerView.Adapter<MyViewHolder>()
{
    private val subscribersList = ArrayList<CalendarData>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : ItemCalendarBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_calendar,parent,false)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position],clickListener, appContext)
    }

    fun setList(subscribers: List<CalendarData>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemCalendarBinding): RecyclerView.ViewHolder(binding.root){

    fun bind(data: CalendarData, clickListener:(CalendarData)->Unit, context: Context){
        val inputFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val outputFormatter = SimpleDateFormat("dd", Locale.ENGLISH)

        binding.date.text = outputFormatter.format(inputFormatter.parse(data.date))
        binding.weekOfDay.text = data.week


        if (!data.isFutureDate) {
            if (data.isReported) {
                binding.date.setBackgroundResource(R.drawable.shape_calendar_grey)
                binding.date.setTextColor(context.resources.getColor(R.color.calendar_date_color))
            } else {
                binding.date.setBackgroundResource(R.drawable.shape_calendar_red)
                binding.date.setTextColor(context.resources.getColor(R.color.white))
            }
        }

        if (data.isToday && !data.isReported) {
            binding.date.setBackgroundResource(R.drawable.shape_calendar_blue)
            binding.date.setTextColor(context.resources.getColor(R.color.white))
        }

        binding.itemLayout.setOnClickListener{
            clickListener(data)
        }
    }
}