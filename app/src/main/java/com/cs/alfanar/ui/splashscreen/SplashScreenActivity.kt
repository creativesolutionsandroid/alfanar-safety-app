package com.cs.alfanar.ui.splashscreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.core.app.ActivityOptionsCompat
import com.cs.alfanar.MyApplication
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.ui.welcomeScreen.WelcomeScreenActivity
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity() : AppCompatActivity() {

    private val SPLASH_TIME: Long = 1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        MyApplication.setContext(this)

        Handler(Looper.getMainLooper()).postDelayed({

            if (UserPreferences(context = this).checkLoginStatus()) {
//                startActivity(Intent(this, MainActivity::class.java))
                val intent = Intent(this, MainActivity::class.java)
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, alfanar_logo, "transition_image")
                startActivity(intent, options.toBundle())
            }
            else {
                val intent = Intent(this, WelcomeScreenActivity::class.java)
                val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                    alfanar_logo, "transition_image")
                startActivity(intent, options.toBundle())
//                startActivity(Intent(this, WelcomeScreenActivity::class.java))
            }

            Handler(Looper.getMainLooper()).postDelayed({
                finish()
            }, 3000)

        }, SPLASH_TIME)
    }
}