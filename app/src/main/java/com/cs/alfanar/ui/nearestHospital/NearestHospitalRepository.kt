package com.cs.alfanar.ui.nearestHospital

import com.cs.alfanar.model.nearestHospital.NearestHospitalResponse
import com.cs.alfanar.network.GoogleApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class NearestHospitalRepository (private val api: GoogleApiInterface) : SafeApiRequest(){

    suspend fun getHospitalsList(location : String, api_key: String): NearestHospitalResponse {
        return apiRequest { api.getHospitalsList(location, api_key) }
    }
}