package com.cs.alfanar.ui.stopWorkAdvice.Stage1

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class StopWorkAdviceStage1ViewModelFactory (
    private val repository: StopWorkAdviceStage1Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return StopWorkAdviceStage1ViewModel(repository) as T
    }
}