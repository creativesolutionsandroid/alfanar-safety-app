package com.cs.alfanar.ui.alertNotificationInsert

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.alfanar.R
import com.cs.alfanar.ui.emailverification.EmailVerificationFormState
import com.cs.alfanar.ui.emergencyNumbers.EmergencyNumbersRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AlertNotificationInsertViewModel (private val repository: AlertNotificationInsertRepository) : ViewModel() {

    private val _DescriptionForm = MutableLiveData<AlertNotificationInsertFormState>()
    val DescriptionFormState: LiveData<AlertNotificationInsertFormState> = _DescriptionForm

    // get emergency numbers api calling
    suspend fun getEmergencyNumbers(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getEmergencyNumbers(inputJson)
    }

    // insert alert notifications api calling
    suspend fun insertAlertNotifications(inputJson : String) = withContext(Dispatchers.IO) {
        repository.insertAlertNotifications(inputJson)
    }

    fun descriptionFieldValidator(description: String) {
        if (description.length == 0) {
            _DescriptionForm.value = AlertNotificationInsertFormState(descriptionError = R.string.validation_error_enter_alert_notification)
        }
        else {
            _DescriptionForm.value = AlertNotificationInsertFormState(isDataValid = true)
        }
    }
}