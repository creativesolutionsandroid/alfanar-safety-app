package com.cs.alfanar.ui.weeklyReport.weeklyReportProjectSelection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WeeklyReportProjectSelectionViewModelFactory (
    private val repository: WeeklyReportProjectSelectionRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeeklyReportProjectSelectionViewModel(repository) as T
    }
}