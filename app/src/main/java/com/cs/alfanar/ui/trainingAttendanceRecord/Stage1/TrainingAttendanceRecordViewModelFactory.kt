package com.cs.alfanar.ui.trainingAttendanceRecord.Stage1

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TrainingAttendanceRecordViewModelFactory(
    private val repository: TrainingAttendanceRecordRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TrainingAttendanceRecordViewModel(repository) as T
    }
}