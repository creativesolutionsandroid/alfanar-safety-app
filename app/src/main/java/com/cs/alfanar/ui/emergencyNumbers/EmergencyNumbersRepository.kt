package com.cs.alfanar.ui.emergencyNumbers

import com.cs.alfanar.model.documentCenterListData.DocumentCenterListResponse
import com.cs.alfanar.model.emergencyNumbers.EmergencyNumbersResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class EmergencyNumbersRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getEmergencyNumbers(inputJson : String): EmergencyNumbersResponse {
        return apiRequest { api.getEmergencyNumbers(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}