package com.cs.alfanar.ui.stopWorkAdvice.Stage1

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkAdviceEntity
import com.cs.alfanar.ui.nearMissIncident.stage1.NearMissIncidentStage1Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class StopWorkAdviceStage1ViewModel (private val repository: StopWorkAdviceStage1Repository): ViewModel() {

    suspend fun GetAccidentIncidentReportMasterData(inputJson : String) = withContext(Dispatchers.IO) {
        repository.GetAccidentIncidentReportMasterData(inputJson)
    }

    suspend fun saveData(projectData: StopWorkAdviceEntity) = repository.saveData(projectData)

    suspend fun getData(): List<StopWorkAdviceEntity> = repository.getData()

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) =
        repository.updateProject(projectId, projectName, projectCode)

    suspend fun updateDate(date: String) = repository.updateDate(date)

    suspend fun updateTime(Time: String) = repository.updateTime(Time)

    suspend fun updateHseRepresentative(HseRepresentative: String) =
        repository.updateHseRepresentative(HseRepresentative)

    suspend fun updateNameOfSupervisor(NameOfSupervisor: String) =
        repository.updateNameOfSupervisor(NameOfSupervisor)

    suspend fun updateDesignation(Designation: String) = repository.updateDesignation(Designation)

    suspend fun updateLocation(Location: String) = repository.updateLocation(Location)
}