package com.cs.alfanar.ui.alertNotificationInsert

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AlertNotificationInsertViewModelFactory (
    private val repository: AlertNotificationInsertRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AlertNotificationInsertViewModel(repository) as T
    }
}