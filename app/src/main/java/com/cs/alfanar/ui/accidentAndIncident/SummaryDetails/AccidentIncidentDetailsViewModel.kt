package com.cs.alfanar.ui.accidentAndIncident.SummaryDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccidentIncidentDetailsViewModel (private val repository: AccidentIncidentDetailsRepository) : ViewModel() {

    suspend fun getWeeklyReportDetails(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getAccidentIncidentSummary(inputJson)
    }

    suspend fun UpdateReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.UpdateReportStatus(inputJson)
    }

}