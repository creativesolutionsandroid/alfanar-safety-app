package com.cs.alfanar.ui.stopWorkAdvice.Stage2

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkAdviceEntity
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkCommunicationEntity
import com.cs.alfanar.ui.stopWorkAdvice.Stage1.StopWorkAdviceStage1Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class StopWorkAdviceStage2ViewModel (private val repository: StopWorkAdviceStage2Repository): ViewModel() {

    suspend fun InsertStopWorkAdviceReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.InsertStopWorkAdviceReport(inputJson)
    }

    // database requests
    suspend fun getData(): List<StopWorkAdviceEntity> = repository.getData()

    suspend fun updateDescription(Description: String) = repository.updateDescription(Description)

    suspend fun saveData(data: StopWorkCommunicationEntity): Long = repository.saveData(data)

    suspend fun getCommunicationData(): List<StopWorkCommunicationEntity> = repository.getCommunicationData()

    suspend fun getInformationData(): List<StopWorkCommunicationEntity> = repository.getInformationData()

    suspend fun deleteCommunicationData() = repository.deleteCommunicationData()

    suspend fun deleteStopWorkData() = repository.deleteStopWorkData()

    suspend fun updateImage(Image: String, ImageName: String) = repository.updateImage(Image, ImageName)

}