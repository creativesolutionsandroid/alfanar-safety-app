package com.cs.alfanar.ui.documentCenterList


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ItemDocumentCenterListBinding
import com.cs.alfanar.model.documentCenterListData.Data
import com.cs.alfanar.utils.DOCUMENT_CENTER_IMAGE_URL

class DocumentCenterListAdapter(private val clickListener: (Data) -> Unit) :
    RecyclerView.Adapter<MyViewHolder>() {
    private val subscribersList = ArrayList<Data>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemDocumentCenterListBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_document_center_list,
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position], clickListener, appContext)
    }

    fun setList(subscribers: List<Data>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemDocumentCenterListBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: Data, clickListener: (Data) -> Unit, context: Context) {
        binding.title.text = data.TypeName
        binding.itemCount.text = data.Counts.toString()

        if (data.Id == 1) {
            binding.icon.setImageDrawable(context.resources.getDrawable(R.drawable.ic_doc_safety_policy))
        } else if (data.Id == 2) {
            binding.icon.setImageDrawable(context.resources.getDrawable(R.drawable.ic_doc_safety_procedure))
        } else if (data.Id == 3) {
            binding.icon.setImageDrawable(context.resources.getDrawable(R.drawable.ic_doc_safety_video))
        } else if (data.Id == 4) {
            binding.icon.setImageDrawable(context.resources.getDrawable(R.drawable.ic_doc_safety_document))
        } else if (data.Id == 5) {
            binding.icon.setImageDrawable(context.resources.getDrawable(R.drawable.ic_doc_safety_tips))
        }

        binding.itemLayout.setOnClickListener {
            clickListener(data)
        }
    }
}