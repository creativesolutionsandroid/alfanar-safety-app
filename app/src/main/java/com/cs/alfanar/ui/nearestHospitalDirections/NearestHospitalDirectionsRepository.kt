package com.cs.alfanar.ui.nearestHospitalDirections

import com.cs.alfanar.model.nearestHospitalDirections.NearestHospitalDirectionsResponse
import com.cs.alfanar.network.GoogleApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class NearestHospitalDirectionsRepository (private val api: GoogleApiInterface) : SafeApiRequest(){

    suspend fun getHospitalDirection(origin : String?, destination : String?, api_key: String): NearestHospitalDirectionsResponse {
        return apiRequest { api.getHospitalDirection(origin, destination, api_key) }
    }
}