package com.cs.alfanar.ui.trainingAttendanceRecord.SummaryDetails

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.trainingAttendanceSummary.TrainingAttendanceSummaryResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class TrainingAttendanceDetailsRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getAccidentIncidentSummary(inputJson : String): TrainingAttendanceSummaryResponse {
        return apiRequest { api.GetTrainingAttendanceReportList(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun UpdateReportStatus(inputJson : String): BasicResponse {
        return apiRequest { api.updateAccidentIncidentStatus(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}