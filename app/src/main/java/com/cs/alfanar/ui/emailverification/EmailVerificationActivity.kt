package com.cs.alfanar.ui.emailverification

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ActivityEmailVerificationBinding
import com.cs.alfanar.databinding.ActivityForgotPasswordBinding
import com.cs.alfanar.ui.forgotpassword.ForgotPasswordViewModel
import com.cs.alfanar.ui.forgotpassword.ForgotPasswordViewModelFactory
import com.cs.alfanar.ui.resetpassword.ResetPasswordActivity
import com.cs.alfanar.utils.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class EmailVerificationActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: EmailVerificationViewModelFactory by instance()

    private lateinit var viewModel: EmailVerificationViewModel
    private lateinit var binding: ActivityEmailVerificationBinding

//    private val TAG : String = this::class.java.simpleName

    private var serverOTP: String? = ""
    private var emailId: String? = ""

    private val OTP_KEY: String = "OTP_KEY"
    private val EMAIL_KEY: String = "EMAIL_KEY"

    private lateinit var timer: CountDownTimer
    private var isResendOtpAvailable: Boolean = false

    private val RESEND_AVAILABLE_IN: Long = 1 * 60 * 1000 // 1 min

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_email_verification)

        viewModel = ViewModelProvider(this, factory).get(EmailVerificationViewModel::class.java)

        serverOTP = intent.getStringExtra(OTP_KEY)
        emailId = intent.getStringExtra(EMAIL_KEY)

        startTimerForResendOTP()

        viewModel.EmailVerificationFormState.observe(this, Observer {
            val emailVerificationFormState: EmailVerificationFormState = it ?: return@Observer

            if (emailVerificationFormState.otpError != null) {
                showOneButtonAlertDialog(getString(emailVerificationFormState.otpError), getString(R.string.Ok), this, null)
            }

            if (emailVerificationFormState.isDataValid) {
                intent = Intent(this, ResetPasswordActivity::class.java)
                intent.putExtra(OTP_KEY, serverOTP)
                intent.putExtra(EMAIL_KEY, emailId)
                startActivity(intent)
            }
        })
    }

    fun onSubmitClicked(view: View) {
        viewModel.forgotPasswordDataChanged(binding.otpView.otp, serverOTP)
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }
    
    fun onResendOTPClicked(view: View) {
        if (isResendOtpAvailable)
            forgotPasswordApi()
    }

    fun startTimerForResendOTP() {
        timer = object: CountDownTimer(RESEND_AVAILABLE_IN, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                    val secondsLeft = millisUntilFinished/1000
                binding.emailVerificationResendOtp.text = String.format(getString(R.string.label_resend_in_seconds), secondsLeft)
                UpdateOtpView(false, R.color.light_grey)
            }

            override fun onFinish() {
                binding.emailVerificationResendOtp.text = getString(R.string.label_resend)
                timer.cancel()
                UpdateOtpView(true, R.color.login_title)
            }
        }
        timer.start()
    }

    fun UpdateOtpView(availability: Boolean, colorId: Int){
        isResendOtpAvailable = availability
        binding.emailVerificationResendOtp.isClickable = availability
        binding.emailVerificationResendOtp.setTextColor(resources.getColor(colorId))
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
    }

    fun prepareInputJsonForForgotPassword(): String {
        val parentObj = JSONObject()

        parentObj.put("Email", emailId)

//        Log.d(TAG, "prepareInputJsonForForgotPassword: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun forgotPasswordApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.forgotPassword(prepareInputJsonForForgotPassword())
                if (response.Data.OTP.length == 4) {
                    // status true from api
                    closeLoadingDialog()
//                    Log.d(TAG, "otp: "+response.Data.OTP)
                    serverOTP = response.Data.OTP
                    isResendOtpAvailable = false
                    startTimerForResendOTP()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@EmailVerificationActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@EmailVerificationActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@EmailVerificationActivity,
                    null
                )
            }
        }
    }
}