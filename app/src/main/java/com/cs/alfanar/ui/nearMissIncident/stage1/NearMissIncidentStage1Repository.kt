package com.cs.alfanar.ui.nearMissIncident.stage1

import androidx.room.Query
import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.nearMissIncident.NearMissIncidentEntity
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage1Entity
import com.cs.alfanar.model.accidentIncident.AccidentMasterDataResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class NearMissIncidentStage1Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun GetAccidentIncidentReportMasterData(inputJson : String): AccidentMasterDataResponse {
        return apiRequest { api.GetAccidentIncidentReportMasterData(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // database requests
    suspend fun saveData(projectData: NearMissIncidentEntity) = db.getNearMissIncidentDao().insert(projectData)

    suspend fun getData(): List<NearMissIncidentEntity> = db.getNearMissIncidentDao().getData()

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) =
        db.getNearMissIncidentDao().updateProject(projectId, projectName, projectCode)

    suspend fun updateDate(date: String) = db.getNearMissIncidentDao().updateDate(date)

    suspend fun updateTime(Time: String) = db.getNearMissIncidentDao().updateTime(Time)

    suspend fun updateLocation(Location: String) = db.getNearMissIncidentDao().updateLocation(Location)

    suspend fun updateReporterBy(ReporterBy: String) = db.getNearMissIncidentDao().updateReporterBy(ReporterBy)

    suspend fun updateDesignation(Designation: String) = db.getNearMissIncidentDao().updateDesignation(Designation)

    suspend fun updateIqama(Iqama: String) = db.getNearMissIncidentDao().updateIqama(Iqama)

    suspend fun updateContactNo(ContactNo: String) = db.getNearMissIncidentDao().updateContactNo(ContactNo)

}