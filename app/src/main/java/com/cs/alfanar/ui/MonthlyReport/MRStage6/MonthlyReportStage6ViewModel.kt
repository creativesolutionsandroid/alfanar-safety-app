package com.cs.alfanar.ui.MonthlyReport.MRStage6

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage6Entity
import com.cs.alfanar.data.db.entities.monthlyReport.MRstage6EventsEntity
import com.cs.alfanar.ui.MonthlyReport.MRStage5.MonthlyReportStage5Repository

class MonthlyReportStage6ViewModel (private val repository: MonthlyReportStage6Repository) : ViewModel() {

    suspend fun saveEventData(data: MRstage6EventsEntity): Long = repository.saveEventData(data)
    suspend fun getEventData(): List<MRstage6EventsEntity> = repository.getEventData()
    suspend fun updateEventData(name: String, id: Int) = repository.updateEventData(name, id)

    // database requests
    suspend fun saveData(projectData: MRStage6Entity) = repository.saveData(projectData)
    suspend fun updatePositiveAlf(PositiveAlf: Int) = repository.updatePositiveAlf(PositiveAlf)
    suspend fun updatePositiveSub(PositiveSub: Int) = repository.updatePositiveSub(PositiveSub)
    suspend fun updateNegativeAlf(NegativeAlf: Int) = repository.updateNegativeAlf(NegativeAlf)
    suspend fun updateNegativeSub(NegativeSub: Int) = repository.updateNegativeSub(NegativeSub)
    suspend fun updatePendingAlf(PendingAlf: Int) = repository.updatePendingAlf(PendingAlf)
    suspend fun updatePendingSub(PendingSub: Int) = repository.updatePendingSub(PendingSub)
    suspend fun updateClosedAlf(ClosedAlf: Int) = repository.updateClosedAlf(ClosedAlf)
    suspend fun updateClosedSub(ClosedSub: Int) = repository.updateClosedSub(ClosedSub)
    suspend fun getData() : List<MRStage6Entity> { return repository.getData() }
    suspend fun updateStage4(Stage4: Boolean) = repository.updateStage4(Stage4)
}