package com.cs.alfanar.ui.weeklyReport.weeklyReportStage2

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage2Entity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class WeeklyReportStage2Repository  (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {


    // database requests
    suspend fun saveData(data: WRstage2Entity): Long = db.getWRstage2Dao().insert(data)

    suspend fun getData(): List<WRstage2Entity> = db.getWRstage2Dao().getData()

}