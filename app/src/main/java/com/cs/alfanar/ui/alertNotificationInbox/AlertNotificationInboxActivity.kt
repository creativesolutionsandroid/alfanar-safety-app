package com.cs.alfanar.ui.alertNotificationInbox

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.alertNotificationInbox.Data
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_alert_notification.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class AlertNotificationInboxActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: AlertNotificationInboxViewModelFactory by instance()

    private lateinit var viewModel: AlertNotificationInboxViewModel
    private lateinit var adapter: AlertNotificationInboxAdapter

    private val TAG : String = this::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alert_notification_inbox)

        viewModel = ViewModelProvider(this, factory).get(AlertNotificationInboxViewModel::class.java)

        initRecyclerView()
        getDocumentListApi()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun prepareInputJsonForAlertNotification(): String {
        val parentObj = JSONObject()
        parentObj.put("FromDate", null)
        parentObj.put("ToDate", null)
        parentObj.put("ProjectId", 0)
        parentObj.put("ActionBy", UserPreferences(this).userId())
        parentObj.put("PageNumber", 1)
        parentObj.put("PageSize", 10)
        parentObj.put("Id", 0)

        Log.d(TAG, "prepareInputJsonForAlertNotification: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getDocumentListApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getAlertNotifications(prepareInputJsonForAlertNotification())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
//                    Log.d(TAG, "getDocumentListApi: "+response.Data)
                    if (response.Data.size > 0) {
                        adapter.setList(response.Data, this@AlertNotificationInboxActivity)
                        adapter.notifyDataSetChanged()
                        list_alert_notification.scheduleLayoutAnimation()
                    }
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@AlertNotificationInboxActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@AlertNotificationInboxActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@AlertNotificationInboxActivity,
                    null
                )
            }
        }
    }

    private fun initRecyclerView(){
        list_alert_notification.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_alert_notification.addItemDecoration(itemDecoration)

        adapter = AlertNotificationInboxAdapter ({ selectedItem: Data ->listItemClicked(selectedItem)})
        list_alert_notification.adapter = adapter
    }

    private fun listItemClicked(data: Data){
        acknowledgeAlertNotificationApi(data)
    }

    fun prepareInputJsonForAcknowledgeAlertNotificationApi(data: Data): String {
        val parentObj = JSONObject()
        parentObj.put("NotificationId", data.Id)
        parentObj.put("UserId", UserPreferences(this).userId())
        parentObj.put("IsAcknowledged", 1)

//        Log.d(TAG, "prepareInputJsonForAcknowledgeAlertNotificationApi: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun acknowledgeAlertNotificationApi(data: Data) {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.acknowledgeAlertNotification(prepareInputJsonForAcknowledgeAlertNotificationApi(data))
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                   toast(response.MessageEn)
                    getDocumentListApi()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@AlertNotificationInboxActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@AlertNotificationInboxActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@AlertNotificationInboxActivity,
                    null
                )
            }
        }
    }
}