package com.cs.alfanar.ui.trainingAttendanceRecord.Summary

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.stopWorkAdvice.Summary.StopWorkAdviceSummaryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TrainingAttendanceRecordSummaryViewModel (private val repository: TrainingAttendanceRecordSummaryRepository) : ViewModel() {

    suspend fun getAccidentIncidentSummary(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getAccidentIncidentSummary(inputJson)
    }

    suspend fun UpdateReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.UpdateReportStatus(inputJson)
    }

}