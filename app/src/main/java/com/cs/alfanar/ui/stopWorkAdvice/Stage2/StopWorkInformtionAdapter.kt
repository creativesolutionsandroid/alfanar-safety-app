package com.cs.alfanar.ui.stopWorkAdvice.Stage2

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkCommunicationEntity
import com.cs.alfanar.databinding.ItemStopWorkCommunicationListBinding

class StopWorkInformtionAdapter : RecyclerView.Adapter<StopWorkViewHolder>() {
    private val subscribersList = ArrayList<StopWorkCommunicationEntity>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StopWorkViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemStopWorkCommunicationListBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_stop_work_communication_list,
                parent,
                false
            )
        return StopWorkViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: StopWorkViewHolder, position: Int) {
        holder.bind(subscribersList[position], position)
    }

    fun setList(subscribers: List<StopWorkCommunicationEntity>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class StopWorkViewHolder(val binding: ItemStopWorkCommunicationListBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: StopWorkCommunicationEntity, position: Int) {

        binding.serialNumber.text = ""+(position + 1) + "."
        binding.topic.text = data.Name
        binding.tiltleDesignation.text = data.Designation
        binding.titleMode.text = data.Mode
    }
}