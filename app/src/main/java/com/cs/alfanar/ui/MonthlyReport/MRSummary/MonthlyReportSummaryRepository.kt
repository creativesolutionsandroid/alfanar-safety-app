package com.cs.alfanar.ui.MonthlyReport.MRSummary

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.MonthlyReportSummary.MonthlyReportSummary
import com.cs.alfanar.model.weeklyReportSummary.WeeklyReportSummaryResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class MonthlyReportSummaryRepository  (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getMonthlyReportSummary(inputJson : String): MonthlyReportSummary {
        return apiRequest { api.getMonthlyReportSummary(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun updateMonthlyReportStatus(inputJson : String): BasicResponse {
        return apiRequest { api.updateMonthlyReportStatus(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}