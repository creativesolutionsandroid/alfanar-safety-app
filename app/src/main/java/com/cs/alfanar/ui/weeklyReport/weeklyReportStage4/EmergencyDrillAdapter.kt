package com.cs.alfanar.ui.weeklyReport.weeklyReportStage4

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4EmergencyDrillEntity
import com.cs.alfanar.databinding.ItemWeeklyReportDrillBinding

class EmergencyDrillAdapter : RecyclerView.Adapter<MyDrillViewHolder>() {
    private val subscribersList = ArrayList<WRstage4EmergencyDrillEntity>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyDrillViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemWeeklyReportDrillBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_weekly_report_drill,
                parent,
                false
            )
        return MyDrillViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyDrillViewHolder, position: Int) {
        holder.bind(subscribersList[position], appContext)
    }

    fun setList(subscribers: List<WRstage4EmergencyDrillEntity>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyDrillViewHolder(val binding: ItemWeeklyReportDrillBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: WRstage4EmergencyDrillEntity, context: Context) {

        binding.serialNumber.text = data.id.toString() + "."
        binding.topic.text = data.TopicName
        binding.date.text = data.Date.toString()

        if (data.NoOfChecklist < 10) {
//            binding.attendeeCount.text = String.format(context.resources.getString(R.string.text_checklist, "0"+data.NoOfChecklist.toString()))
            binding.attendeeCount.text =  "0"+data.NoOfChecklist.toString()
        }
        else {
//            binding.attendeeCount.text = String.format(context.resources.getString(R.string.text_checklist, data.NoOfChecklist.toString()))
            binding.attendeeCount.text = data.NoOfChecklist.toString()
        }
    }
}