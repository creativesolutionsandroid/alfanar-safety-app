package com.cs.alfanar.ui.accidentAndIncident.Stage3

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AccidentStage3ViewModelFactory (
    private val repository: AccidentStage3Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AccidentStage3ViewModel(repository) as T
    }
}