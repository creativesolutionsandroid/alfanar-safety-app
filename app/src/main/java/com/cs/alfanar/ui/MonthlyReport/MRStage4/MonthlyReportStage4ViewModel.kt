package com.cs.alfanar.ui.MonthlyReport.MRStage4

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage4Entity
import com.cs.alfanar.ui.MonthlyReport.MRStage3.MonthlyReportStage3Repository

class MonthlyReportStage4ViewModel (private val repository: MonthlyReportStage4Repository) : ViewModel() {

    // database requests
    suspend fun saveData(projectData: MRStage4Entity) = repository.saveData(projectData)

    suspend fun updateManagerAlf(ManagerAlf: Int) =
        repository.updateManagerAlf(ManagerAlf)

    suspend fun updateManagerSub(ManagerSub: Int) =
        repository.updateManagerSub(ManagerSub)

    suspend fun updateEngineerAlf(EngineerAlf: Int) =
        repository.updateEngineerAlf(EngineerAlf)

    suspend fun updateEngineerSub(EngineerSub: Int) =
        repository.updateEngineerSub(EngineerSub)

    suspend fun updateSupervisorAlf(SupervisorAlf: Int) =
        repository.updateSupervisorAlf(SupervisorAlf)

    suspend fun updateSupervisorSub(SupervisorSub: Int) =
        repository.updateSupervisorSub(SupervisorSub)

    suspend fun updateOfficerAlf(OfficerAlf: Int) =
        repository.updateOfficerAlf(OfficerAlf)

    suspend fun updateOfficerSub(OfficerSub: Int) =
        repository.updateOfficerSub(OfficerSub)

    suspend fun updateNurseAlf(NurseAlf: Int) =
        repository.updateNurseAlf(NurseAlf)

    suspend fun updateNurseSub(NurseSub: Int) =
        repository.updateNurseSub(NurseSub)

    suspend fun updateFirstAidAlf(FirstAidAlf: Int) =
        repository.updateFirstAidAlf(FirstAidAlf)

    suspend fun updateFirstAidSub(FirstAidSub: Int) =
        repository.updateFirstAidSub(FirstAidSub)

    suspend fun updateNote(Note: String) =
        repository.updateNote(Note)

    suspend fun getData() : List<MRStage4Entity> {
        return repository.getData()
    }

    suspend fun updateStage2(Stage2: Boolean) =
        repository.updateStage2(Stage2)
}