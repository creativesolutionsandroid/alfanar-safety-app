package com.cs.alfanar.ui.accidentAndIncident.Stage1

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.adapters.AccidentAdapter
import com.cs.alfanar.adapters.AccidentIncidentProjectAdapter
import com.cs.alfanar.adapters.DailyReportProjectAdapter
import com.cs.alfanar.adapters.WeeklyReportProjectAdapter
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.data.db.entities.weeklyReport.WRprojectSelectionEntity
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.accidentIncident.ChildItem
import com.cs.alfanar.model.accidentIncident.Data
import com.cs.alfanar.model.accidentIncident.Master
import com.cs.alfanar.model.accidentIncident.Project
import com.cs.alfanar.ui.accidentAndIncident.Stage2.AccidentStage2Activity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_accident_stage1.*
import kotlinx.android.synthetic.main.activity_accident_stage1.date
import kotlinx.android.synthetic.main.activity_accident_stage1.project_id
import kotlinx.android.synthetic.main.activity_accident_stage1.spinner_project_name
import kotlinx.android.synthetic.main.activity_daily_report.*
import kotlinx.android.synthetic.main.activity_weekly_report_stage2.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*

class AccidentStage1Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: AccidentStage1ViewModelFactory by instance()

    private lateinit var viewModel: AccidentStage1ViewModel

//    private val TAG : String = this::class.java.simpleName
    private var accidentTypesList: MutableList<ChildItem> = mutableListOf()
    private var accidentClassList: MutableList<ChildItem> = mutableListOf()
    private var bodyPartList: MutableList<ChildItem> = mutableListOf()
    private var projectsList: List<Project> = mutableListOf()
    private var projectData: List<AccidentIncidentEntity> = mutableListOf()
    private lateinit var masterData: Data

    private var selectedProjectPos: Int = 0
    private var projectName: String = ""
    private var accidentTypeName: String = ""
    private var accidentClassName: String = ""
    private var bodyPartName: String = ""
    private var bodyPartsSelected: String = ""
    private var bodyPartsSelectedIds: String = ""

    var todayCalendar = Calendar.getInstance()
    var selectedDate: Int = 0
    var selectedMonth: Int = 0
    var selectedYear: Int = 0
    var selectedHour : Int = 0
    var selectedMin : Int = 0
    var isDurationSet: Boolean = false
    var isDateSet: Boolean = false
    val KEY_DATA: String = "DATA_KEY"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accident_stage1)

        viewModel = ViewModelProvider(this, factory).get(AccidentStage1ViewModel::class.java)

        fetchDataFromDb()
        addTextWatcher()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun ontimeClicked(view: View) {
        showCustomTimePicker()
    }

    fun onDateClicked(view: View) {
        showDatePicker()
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            projectData = viewModel.getData()
            if (projectData.size > 0) {
                projectName = projectData[0].projectName
                spinner_project_name.setText(projectData[0].projectName)
                project_id.text = projectData[0].projectCode
                date.text = projectData[0].Date
                time.text = projectData[0].Time
                spinner_accident_type.setText(projectData[0].AccidentType)
                accidentTypeName = projectData[0].AccidentType
                spinner_accident_class.setText(projectData[0].AccidentClass)
                accidentClassName = projectData[0].AccidentClass
                employee_name.setText(projectData[0].InjuredEmployeeName)
                company_name.setText(projectData[0].CompanyContractor)
                spinner_body_part_injured.setText(projectData[0].BodyPartInjured)
                bodyPartName = projectData[0].BodyPartInjured
                bodyPartsSelected = projectData[0].BodyPartInjured
                bodyPartsSelectedIds = projectData[0].BodyPartInjuredId
                date.text = projectData[0].Date

                if (projectData[0].BodyPartInjuredId.equals(BODY_PART_OTHER_TYPE.toString())) {
                    body_part_injured_other.visibility = View.VISIBLE
                    body_part_injured_other.setText(projectData[0].BodyPartInjuredOther)
                }

                getMasterDataApi()
            }
            else {
                val EmptyData = AccidentIncidentEntity(1, "", 0, "", "", ""
                , "", 0, "", 0, "", "",
                "", "","", "", "", "", "", 0, "",
                0, "", "", "", 0, "", "",
                false, "", "", "", "", false, "", "","",
                    false, false, false)
                viewModel.insert(EmptyData)
                projectData = viewModel.getData()
                getMasterDataApi()
            }
        }
    }

    fun onNextButtonClicked(view: View) {
        validationCheck()
    }

    fun prepareInputJsonToGetMasterData(): String {
        val parentObj = JSONObject()
        parentObj.put("ActionBy", UserPreferences(this).userId())

//        Log.d(TAG, "prepareInputJsonToGetMasterData: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getMasterDataApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.GetAccidentIncidentReportMasterData(prepareInputJsonToGetMasterData())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    filterData(response.Data.MasterList)
                    masterData = response.Data
                    projectsList = response.Data.ProjectList
                    if (projectsList.size > 0) {
                        for (i in 1 until projectsList.size) {
                            if (projectsList[i].ProjectNameEn.equals(projectName)) {
                                selectedProjectPos = i
                                break
                            }
                        }
                        setProjectSpinnerData()
                    }
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@AccidentStage1Activity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@AccidentStage1Activity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@AccidentStage1Activity,
                    null
                )
            }
        }
    }

    fun filterData(data: List<Master>) {
        for (i in 0 until data.size) {
            if (data[i].Id == ACCIDENT_TYPE) {
                for (j in 0 until data[i].ChildItem.size) {
                    accidentTypesList.add(data[i].ChildItem[j])
                }
            }
            else if (data[i].Id == ACCIDENT_CLASS) {
                for (j in 0 until data[i].ChildItem.size) {
                    accidentClassList.add(data[i].ChildItem[j])
                }
            }
            else if (data[i].Id == BODY_PART_INJURED) {
                for (j in 0 until data[i].ChildItem.size) {
                    bodyPartList.add(data[i].ChildItem[j])
                }
            }
        }

        setAccidentTypeSpinnerData()
        setAccidentClassSpinnerData()
        setBodyPartInjuredSpinnerData()
    }

    fun setAccidentTypeSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                accidentTypesList
            )
        spinner_accident_type.setAdapter(accidentAdapter)

        spinner_accident_type.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = accidentTypesList.get(position).ActionName
                spinner_accident_type.setText(selectedItem)
                updateAccidentType(position)
            }

        // Disabling keyboard for spinners
        spinner_accident_type.setInputType(InputType.TYPE_NULL)
    }

    fun setAccidentClassSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                accidentClassList
            )
        spinner_accident_class.setAdapter(accidentAdapter)

        spinner_accident_class.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = accidentClassList.get(position).ActionName
                spinner_accident_class.setText(selectedItem)
                updateAccidentClass(position)
            }

        // Disabling keyboard for spinners
        spinner_accident_class.setInputType(InputType.TYPE_NULL)
    }

    fun setBodyPartInjuredSpinnerData() {
        val accidentAdapter: AccidentAdapter =
            AccidentAdapter(
                this,
                android.R.layout.simple_list_item_1,
                bodyPartList
            )
        spinner_body_part_injured.setAdapter(accidentAdapter)

        spinner_body_part_injured.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                lifecycleScope.launch {
                    projectData = viewModel.getData()

                    if (projectData[0].BodyPartInjuredId.equals(BODY_PART_OTHER_TYPE.toString()) ||
                        bodyPartList.get(position).Id == BODY_PART_OTHER_TYPE) {
                        updateBodyPart("", "")
                        projectData = viewModel.getData()
                    }

                    if (bodyPartList.get(position).Id == BODY_PART_OTHER_TYPE) {
                        body_part_injured_other.visibility = View.VISIBLE
                        body_part_injured_other.setText("")
                    }
                    else {
                        body_part_injured_other.visibility = View.GONE
                        updateBodyPartOther("")
                    }
                    var bodyParts = projectData[0].BodyPartInjured
                    var bodyPartIds = projectData[0].BodyPartInjuredId

                    val parts = projectData[0].BodyPartInjured.split(",")
                    var isPartAlreadySelected: Boolean = false

                    if (parts.size > 0) {
                        for (i in 0 until parts.size) {
                            if (parts[i].equals(bodyPartList.get(position).ActionName)) {
                                isPartAlreadySelected = true
                                if (i == 0 && parts.size == 1) {
                                    bodyParts = ""
                                    bodyPartIds = ""
                                }
                                else if (i == 0 && parts.size >  1) {
                                    bodyParts = bodyParts.replace((bodyPartList.get(position).ActionName+","), "")
                                    bodyPartIds = bodyPartIds.replace((""+bodyPartList.get(position).Id+","), "")
                                }
                                else {
                                    bodyParts = bodyParts.replace((","+bodyPartList.get(position).ActionName), "")
                                    bodyPartIds = bodyPartIds.replace((","+bodyPartList.get(position).Id), "")
                                }
                            }
                        }

                        if (!isPartAlreadySelected) {
                            if (projectData[0].BodyPartInjured.equals("")) {
                                bodyParts = bodyPartList.get(position).ActionName
                                bodyPartIds = bodyPartList.get(position).Id.toString()
                            }
                            else {
                                bodyParts = bodyParts + "," + bodyPartList.get(position).ActionName
                                bodyPartIds = bodyPartIds + "," + bodyPartList.get(position).Id.toString()
                            }
                        }

                        updateBodyPart(bodyParts, bodyPartIds)
                        spinner_body_part_injured.setText(bodyParts)
                    } else {
                        updateBodyPart(
                            bodyPartList.get(position).ActionName,
                            bodyPartList.get(position).Id.toString()
                        )
                        spinner_body_part_injured.setText(bodyPartList.get(position).ActionName)
                    }
                }
            }

        // Disabling keyboard for spinners
        spinner_body_part_injured.setInputType(InputType.TYPE_NULL)
    }

    fun setProjectSpinnerData() {
        val projectAdapter: AccidentIncidentProjectAdapter =
            AccidentIncidentProjectAdapter(
                this,
                android.R.layout.simple_list_item_1,
                projectsList
            )
        spinner_project_name.setAdapter(projectAdapter)

        spinner_project_name.setText(projectsList.get(selectedProjectPos).ProjectNameEn) // by default first project will be selected
        project_id.text = projectsList.get(selectedProjectPos).ProjectCode
        projectName = projectsList[selectedProjectPos].ProjectNameEn
        updateProjectDetails(selectedProjectPos)

        spinner_project_name.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = projectsList.get(position).ProjectNameEn
                project_id.text = projectsList.get(position).ProjectCode
                spinner_project_name.setText(selectedItem)
                updateProjectDetails(position)
            }

        // Disabling keyboard for spinners
        spinner_project_name.setInputType(InputType.TYPE_NULL)
    }

    fun updateProjectDetails(position: Int) {
        lifecycleScope.launch {
            viewModel.updateProject(projectsList[position].ProjectId, projectsList[position].ProjectNameEn, projectsList[position].ProjectCode)
        }
    }

    fun updateAccidentType(position: Int) {
        lifecycleScope.launch {
            viewModel.updateAccidentType(accidentTypesList[position].ActionName, accidentTypesList[position].Id)
        }
    }

    fun updateAccidentClass(position: Int) {
        lifecycleScope.launch {
            viewModel.updateAccidentClass(accidentClassList[position].ActionName, accidentClassList[position].Id)
        }
    }

    fun updateBodyPart(selectedPart: String, selectedId: String) {
        lifecycleScope.launch {
            viewModel.updateBodyPartInjured(selectedPart, selectedId)
        }
    }

    fun updateBodyPartOther(name: String) {
        lifecycleScope.launch {
            viewModel.updateBodyPartInjuredOther(name)
        }
    }

    fun addTextWatcher() {
        employee_name.afterTextChanged { updateEmployeeName(employee_name.text.toString().trim()) }
        company_name.afterTextChanged { updateCompanyContractor(company_name.text.toString().trim()) }
        body_part_injured_other.afterTextChanged { updateBodyPartOther((body_part_injured_other.text.toString().trim())) }
    }

    fun updateEmployeeName(Name: String) {
        lifecycleScope.launch {
            viewModel.updateInjuredEmployeeName(Name)
        }
    }

    fun updateCompanyContractor(Name: String) {
        lifecycleScope.launch {
            viewModel.updateCompanyContractor(Name)
        }
    }

    fun updateDate() {
        val date = "" + getDate() + "-" + getMonth() + "-" + selectedYear
        lifecycleScope.launch {
            viewModel.updateDate(date)
        }
    }

    fun updateTime() {
        val time = getDuration()
        lifecycleScope.launch {
            viewModel.updateTime(time)
        }
    }

    fun getMonth(): String {
        var month: String = ""
        if (selectedMonth < 9) {
            month = "0" + (selectedMonth + 1)
        }
        else {
            month = "" + (selectedMonth + 1)
        }
        return month
    }

    fun getDate(): String {
        var month: String = ""
        if (selectedDate < 9) {
            month = "0" + selectedDate
        }
        else {
            month = "" + selectedDate
        }
        return month
    }

    fun showDatePicker() {
        selectedYear = todayCalendar.get(Calendar.YEAR)
        selectedMonth = todayCalendar.get(Calendar.MONTH)
        selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            todayCalendar.set(Calendar.YEAR, year)
            todayCalendar.set(Calendar.MONTH, monthOfYear)
            todayCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            selectedYear = todayCalendar.get(Calendar.YEAR)
            selectedMonth = todayCalendar.get(Calendar.MONTH)
            selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

            val dateSelected = "" + getDate() + "-" + getMonth() + "-" + selectedYear
            date.text = dateSelected
            isDateSet = true
            updateDate()
        }, selectedYear, selectedMonth, selectedDate)

        val c = Calendar.getInstance()
        c.add(Calendar.DATE, -30) // show previous 7 days only

        dpd.datePicker.minDate = c.timeInMillis
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }

    fun showCustomTimePicker() {
        val myTimeListener =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                if (view.isShown) {
                    isDurationSet = true
                    selectedHour = hourOfDay
                    selectedMin = minute
                    time.text = getDuration()
                    updateTime()
                }
            }
        val timePickerDialog = CustomTimePickerDialog(
            this,
            myTimeListener,
            selectedHour,
            selectedMin,
            true
        )
        timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        timePickerDialog.show()
    }

    fun getDuration(): String {
        var duration: String = ""
        val hour: String
        val min: String
        var filteredHour: Int

        if (selectedHour < 10) {
            hour = "0" +selectedHour.toString()
        }
        else {
            hour = selectedHour.toString()
        }

        if (selectedMin < 10) {
            min = "0" + selectedMin.toString()
        }
        else  {
            min = selectedMin.toString()
        }
        duration = hour + ":" + min
        return duration
    }

    fun validationCheck() {
        lifecycleScope.launch {
            projectData = viewModel.getData()

            if (projectData[0].Date.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_date), getString(R.string.Ok),
                    this@AccidentStage1Activity, null
                )
            } else if (projectData[0].Time.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_time), getString(R.string.Ok),
                    this@AccidentStage1Activity, null
                )
            } else if (projectData[0].AccidentType.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_accident_type), getString(R.string.Ok),
                    this@AccidentStage1Activity, null
                )
            } else if (projectData[0].AccidentClass.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_accident_class), getString(R.string.Ok),
                    this@AccidentStage1Activity, null
                )
            } else if (projectData[0].InjuredEmployeeName.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_name_of_injured_employee),
                    getString(R.string.Ok),
                    this@AccidentStage1Activity,
                    null
                )
            } else if (projectData[0].CompanyContractor.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_company_subcontractor), getString(R.string.Ok),
                    this@AccidentStage1Activity, null
                )
            } else if (projectData[0].BodyPartInjured.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_body_part_injured), getString(R.string.Ok),
                    this@AccidentStage1Activity, null
                )
            } else if (projectData[0].BodyPartInjuredId.equals(BODY_PART_OTHER_TYPE) &&
                    projectData[0].BodyPartInjuredOther.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_other_body_part_injured),
                    getString(R.string.Ok),
                    this@AccidentStage1Activity,
                    null
                )
            } else {
                val intent = Intent(this@AccidentStage1Activity, AccidentStage2Activity::class.java)
                intent.putExtra(KEY_DATA, masterData)
                startActivity(intent)
            }
        }
    }
 }