package com.cs.alfanar.ui.alertNotification

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.databinding.ItemAlertNotificationBinding
import com.cs.alfanar.model.alertNotification.Data
import com.cs.alfanar.network.ApiInterface
import com.cs.alfanar.utils.ALERT_TYPE_IMAGE_URL

class AlertNotificationAdapter (private val clickListener:(Data)->Unit)
    : RecyclerView.Adapter<com.cs.alfanar.ui.alertNotification.MyViewHolder>()
{
    private val subscribersList = ArrayList<Data>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): com.cs.alfanar.ui.alertNotification.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : ItemAlertNotificationBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_alert_notification,parent,false)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: com.cs.alfanar.ui.alertNotification.MyViewHolder, position: Int) {
        holder.bind(subscribersList[position],clickListener, appContext)
    }

    fun setList(subscribers: List<Data>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemAlertNotificationBinding): RecyclerView.ViewHolder(binding.root){

    fun bind(data: Data, clickListener:(Data)->Unit, context: Context){
        binding.title.text = data.TypeNameEn

        Glide.with(context)
            .load(
                ApiInterface.BASE_URL +ALERT_TYPE_IMAGE_URL + data.Icon)
            .into(binding.icon)

        binding.itemLayout.setOnClickListener{
            clickListener(data)
        }
    }
}