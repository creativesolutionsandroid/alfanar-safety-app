package com.cs.alfanar.ui.weeklyReport.weeklyReportProjectSelection

import android.location.Location
import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.weeklyReport.WRprojectSelectionEntity
import com.cs.alfanar.model.dailyReport.GetProjectsData
import com.cs.alfanar.model.weeklyReport.WeeklyProjectsData
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class WeeklyReportProjectSelectionRepository(
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    //api requests
    suspend fun getProjectDataForWeeklyReport(inputJson : String): WeeklyProjectsData {
        return apiRequest { api.getProjectDataForWeeklyReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // database requests
    suspend fun saveData(projectData: WRprojectSelectionEntity) = db.getWRprojectSelectionDao().insert(projectData)

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) =
        db.getWRprojectSelectionDao().updateProject(projectId, projectName, projectCode)

    suspend fun updateLocation(location: String) = db.getWRprojectSelectionDao().updateLocation(location)

    suspend fun getData() : List<WRprojectSelectionEntity> {
        return db.getWRprojectSelectionDao().getData()
    }

    suspend fun deleteData() = db.getWRprojectSelectionDao().deleteData()
}