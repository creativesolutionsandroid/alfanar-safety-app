package com.cs.alfanar.ui.nearMissIncident.stage2

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.nearMissIncident.NearMissIncidentEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NearMissIncidentStage2ViewModel (private val repository: NearMissIncidentStage2Repository): ViewModel() {

    suspend fun InsertAccidentIncidentReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.InsertAccidentIncidentReport(inputJson)
    }

    suspend fun getData(): List<NearMissIncidentEntity> = repository.getData()

    suspend fun updateDescription(Description: String) = repository.updateDescription(Description)

    suspend fun updateWitness(Witness: String) = repository.updateWitness(Witness)

    suspend fun updateImprovements(Improvements: String) = repository.updateImprovements(Improvements)

    suspend fun updateImage(Image: String, ImageName: String) = repository.updateImage(Image, ImageName)

    suspend fun deleteData() = repository.deleteData()

}