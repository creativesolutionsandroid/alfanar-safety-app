package com.cs.alfanar.ui.nearestHospital

import android.content.Context
import android.location.Location
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ItemNearestHospitalBinding
import com.cs.alfanar.model.nearestHospital.Result
import com.cs.alfanar.utils.distanceFormat

class NearestHospitalAdapter (private val clickListener:(Result)->Unit)
    : RecyclerView.Adapter<com.cs.alfanar.ui.nearestHospital.MyViewHolder>()
{
    private val subscribersList = ArrayList<Result>()
    private lateinit var location: Location
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): com.cs.alfanar.ui.nearestHospital.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : ItemNearestHospitalBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_nearest_hospital,parent,false)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: com.cs.alfanar.ui.nearestHospital.MyViewHolder, position: Int) {
        holder.bind(subscribersList[position],clickListener, appContext, location)
    }

    fun setList(subscribers: List<Result>, context: Context, currentLocation: Location) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        location = currentLocation
        appContext = context
    }
}

class MyViewHolder(val binding: ItemNearestHospitalBinding): RecyclerView.ViewHolder(binding.root){

    fun bind(data: Result, clickListener:(Result)->Unit, context: Context, location: Location){
        binding.title.text = data.name
        binding.address.text = data.vicinity

        val loc1 = Location("")

        loc1.setLatitude(location.latitude)
        loc1.setLongitude(location.longitude)

        val loc2 = Location("")
        loc2.setLatitude(data.geometry.location.lat)
        loc2.setLongitude(data.geometry.location.lng)

        val distanceInKiloMeters: Float = (loc1.distanceTo(loc2)) / 1000

        binding.distance.text = String.format(context.resources.getString(R.string.kilometers), distanceFormat.format(distanceInKiloMeters))

        binding.itemLayout.setOnClickListener{
            clickListener(data)
        }
    }
}