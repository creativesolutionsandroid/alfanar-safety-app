package com.cs.alfanar.ui.MonthlyReport.MRSummary

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.MonthlyReportSummary.Data
import com.cs.alfanar.ui.MonthlyReport.MRSummaryDetails.MonthlyReportSummaryDetailsActivity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportDetails.WeeklyReportDetailsActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryAdapter
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryViewModel
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryViewModelFactory
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_daily_report_summary.*
import kotlinx.android.synthetic.main.activity_weekly_report_summary.*
import kotlinx.android.synthetic.main.activity_weekly_report_summary.label_date_from
import kotlinx.android.synthetic.main.activity_weekly_report_summary.label_date_to
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*

class MonthlyReportSummaryActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: MonthlyReportSummaryViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportSummaryViewModel
    private lateinit var adapter: MonthlyReportSummaryAdapter

    var fromCalendar = Calendar.getInstance()
    var toCalendar = Calendar.getInstance()

    var fromDay: Int = 0
    var fromMonth: Int = 0
    var fromYear: Int = 0

    var toDay: Int = 0
    var toMonth: Int = 0
    var toYear: Int = 0

    private var dataList: List<Data> = mutableListOf()
//    private val TAG : String = this::class.java.simpleName

    var pageSize = 20
    var pageNumber = 1
    val KEY_REPORT_ID: String = "KEY_REPORT_ID"
    val KEY_BACK_ACTION : String = "KEY_BACK_ACTION"
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_summary)

        viewModel = ViewModelProvider(this, factory).get(MonthlyReportSummaryViewModel::class.java)

        initRecyclerView()
        getLast30DaysSummary()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        backfunctionality()
    }

    fun onBackButtonClicked(view: View) {
        backfunctionality()
    }

    fun backfunctionality() {
        if ((intent.getStringExtra(KEY_BACK_ACTION)).equals("home_screen")) {
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        }
        finish()
    }

    fun onFromDateClicked(view: View) {
        showFromDatePicker()
    }

    fun onToDateClicked(view: View) {
        showToDatePicker()
    }

    private fun initRecyclerView(){
        list_weekly_reports.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_weekly_reports.addItemDecoration(itemDecoration)
        adapter = MonthlyReportSummaryAdapter({ selectedItem: Data, selectedPosition: Int, options: ActivityOptionsCompat ->listItemClicked(selectedItem, selectedPosition, options)},
            { selectedItem: Data, selectedPosition: Int ->updateStatusApi(selectedItem, selectedPosition)})
    }

    private fun listItemClicked(data: Data, position: Int, options: ActivityOptionsCompat){
        val intent = Intent(this, MonthlyReportSummaryDetailsActivity::class.java)
        intent.putExtra(KEY_REPORT_ID, data.Id)
//        startActivity(intent, options.toBundle())
        startActivity(intent)
    }

    fun getLast30DaysSummary() {
        val todayCalendar = Calendar.getInstance()

        toYear = todayCalendar.get(Calendar.YEAR)
        toMonth = todayCalendar.get(Calendar.MONTH)
        toDay = todayCalendar.get(Calendar.DAY_OF_MONTH)

        toCalendar.set(Calendar.YEAR, toYear)
        toCalendar.set(Calendar.MONTH, toMonth)
        toCalendar.set(Calendar.DAY_OF_MONTH, toDay)
        label_date_to.text = SimpleDateFormat("dd-MM-yyyy", Locale.US).format(toCalendar.time)

        todayCalendar.add(Calendar.DATE, -30)

        fromYear = todayCalendar.get(Calendar.YEAR)
        fromMonth = todayCalendar.get(Calendar.MONTH)
        fromDay = todayCalendar.get(Calendar.DAY_OF_MONTH)

        fromCalendar.set(Calendar.YEAR, fromYear)
        fromCalendar.set(Calendar.MONTH, fromMonth)
        fromCalendar.set(Calendar.DAY_OF_MONTH, fromDay)
        label_date_from.text = SimpleDateFormat("dd-MM-yyyy", Locale.US).format(fromCalendar.time)
        getWeeklyReportSummaryApi()
    }

    fun showFromDatePicker() {
        fromYear = fromCalendar.get(Calendar.YEAR)
        fromMonth = fromCalendar.get(Calendar.MONTH)
        fromDay = fromCalendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            fromCalendar.set(Calendar.YEAR, year)
            fromCalendar.set(Calendar.MONTH, monthOfYear)
            fromCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            label_date_from.text = SimpleDateFormat("dd-MM-yyyy", Locale.US).format(fromCalendar.time)
            label_date_to.text = ""
        }, fromYear, fromMonth, fromDay)

        val c = Calendar.getInstance()
        c.add(Calendar.DATE, -30) // show previous 30 days only

        dpd.setTitle(resources.getString(R.string.label_from))
        dpd.datePicker.minDate = c.timeInMillis
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }

    fun showToDatePicker() {
        toYear = toCalendar.get(Calendar.YEAR)
        toMonth = toCalendar.get(Calendar.MONTH)
        toDay = toCalendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            toCalendar.set(Calendar.YEAR, year)
            toCalendar.set(Calendar.MONTH, monthOfYear)
            toCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            label_date_to.text = SimpleDateFormat("dd-MM-yyyy", Locale.US).format(toCalendar.time)
            list_weekly_reports.adapter = null
            getWeeklyReportSummaryApi()
        }, toYear, toMonth, toDay)

        val title = TextView(this)
        title.text = resources.getString(R.string.labelto)
        dpd.setCustomTitle(title)
        dpd.datePicker.minDate = fromCalendar.timeInMillis
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }

    fun prepareInputJsonToGetWeeklyReportSummary(): String {
        val parentObj = JSONObject()

        parentObj.put("Id", 0) // Id = 0 to get list of reports
        parentObj.put("pageSize", pageSize)
        parentObj.put("pageNumber", pageNumber)
        parentObj.put("SubmittedBy", 0)
        parentObj.put("ReportStatus", 0)
        parentObj.put("Fromdate", "" + fromYear + "-" + (fromMonth + 1) + "-" + fromDay)
        parentObj.put("Todate", "" + toYear + "-" + (toMonth + 1) + "-" + toDay)
        parentObj.put("FlagId", 1) // FlagId = 1 to get list and FlagId = 2 to get details
        parentObj.put("ActionBy", UserPreferences(this).userId())

//        Log.d(TAG, "prepareInputJsonForGetDailyReportSummary: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getWeeklyReportSummaryApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getMonthlyReportSummary(prepareInputJsonToGetWeeklyReportSummary())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    list_weekly_reports.adapter = adapter
                    dataList = response.Data
                    adapter.setList(response.Data, this@MonthlyReportSummaryActivity)
                    adapter.notifyDataSetChanged()
                    list_weekly_reports.scheduleLayoutAnimation()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@MonthlyReportSummaryActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@MonthlyReportSummaryActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@MonthlyReportSummaryActivity,
                    null
                )
            }
        }
    }

    fun prepareInputJsonForUpdateStatus(ReportId: Int): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())
        parentObj.put("ReportId", ReportId)

//        Log.d(TAG, "prepareInputJsonForUpdateStatus: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun updateStatusApi(data: Data, position: Int) {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.updateMonthlyReportStatus(prepareInputJsonForUpdateStatus(data.Id))
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    toast(response.MessageEn)
                    getWeeklyReportSummaryApi()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@MonthlyReportSummaryActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@MonthlyReportSummaryActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@MonthlyReportSummaryActivity,
                    null
                )
            }
        }
    }
}