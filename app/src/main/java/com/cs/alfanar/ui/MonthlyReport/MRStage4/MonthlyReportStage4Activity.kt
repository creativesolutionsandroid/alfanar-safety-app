package com.cs.alfanar.ui.MonthlyReport.MRStage4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage3Entity
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage4Entity
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.ui.MonthlyReport.MRStage3.MonthlyReportStage3ViewModel
import com.cs.alfanar.ui.MonthlyReport.MRStage3.MonthlyReportStage3ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage5.MonthlyReportStage5Activity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.afterTextChanged
import kotlinx.android.synthetic.main.activity_monthly_report_stage3.*
import kotlinx.android.synthetic.main.activity_monthly_report_stage4.*
import kotlinx.android.synthetic.main.activity_monthly_report_stage9.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MonthlyReportStage4Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val ehs_manager_subtory: MonthlyReportStage4ViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportStage4ViewModel

//    private val TAG : String = this::class.java.simpleName

    private lateinit var projectData: Project
    val KEY_DATA: String = "KEY_DATA"
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_stage4)

        viewModel = ViewModelProvider(this, ehs_manager_subtory).get(MonthlyReportStage4ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Project
        projectData = Data!!

        fetchDataFromDb()
        addTextWatcher()
        et_note.setImeOptions(EditorInfo.IME_ACTION_DONE)
        et_note.setRawInputType(InputType.TYPE_CLASS_TEXT)
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextClicked(view: View) {
//        if (et_note.text.length == 0) { /*Commented on 06-01-2020 after client request*/
//            et_note.error = getString(R.string.validation_error_empty_field)
//            et_note.requestFocus()
//        }
//        else {
            val intent = Intent(this, MonthlyReportStage5Activity::class.java)
            intent.putExtra(KEY_DATA, projectData)
            startActivity(intent)
//        }
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                ehs_manager_alfanar.setText(projectData[0].ManagerAlf.toString())
                ehs_manager_sub.setText(projectData[0].ManagerSub.toString())
                ehs_engineer_alfanar.setText(projectData[0].EngineerAlf.toString())
                ehs_engineer_sub.setText(projectData[0].EngineerSub.toString())
                ehs_supervisor_alfanar.setText(projectData[0].SupervisorAlf.toString())
                ehs_supervisor_sub.setText(projectData[0].SupervisorSub.toString())
                ehs_officer_alfanar.setText(projectData[0].OfficerAlf.toString())
                ehs_officer_sub.setText(projectData[0].OfficerSub.toString())
                nurse_alfanar.setText(projectData[0].NurseAlf.toString())
                nurse_sub.setText(projectData[0].NurseSub.toString())
                first_aider_alfanar.setText(projectData[0].FirstAidAlf.toString())
                first_aider_sub.setText(projectData[0].FirstAidSub.toString())
                et_note.setText(projectData[0].Note)
                updateCumulativeHours()
            }
            else {
                val projectData = MRStage4Entity(1, 0, 0, 0,0,0
                    , 0, 0, 0, 0, 0,0,0,"")
                viewModel.saveData(projectData)
                updateCumulativeHours()
            }
            ehs_manager_alfanar.requestFocus()
            ehs_manager_alfanar.setSelection(ehs_manager_alfanar.text.length)
            addFocusListener()
            lifecycleScope.launch {
                viewModel.updateStage2(true)
            }
        }
    }

    fun updateCumulativeHours() {
        lifecycleScope.launch {
            val dbData = viewModel.getData()

            total_alfanar.text = "" + (dbData[0].ManagerAlf +
                    dbData[0].EngineerAlf +
                    dbData[0].SupervisorAlf +
                    dbData[0].OfficerAlf +
                    dbData[0].NurseAlf +
                    dbData[0].FirstAidAlf)


             total_sub.text = "" + (dbData[0].ManagerSub +
                     dbData[0].EngineerSub +
                     dbData[0].SupervisorSub +
                     dbData[0].OfficerSub +
                     dbData[0].NurseSub +
                     dbData[0].FirstAidSub)

        }
    }

    fun addFocusListener() {
        ehs_manager_alfanar.setOnFocusChangeListener { view, b ->
            if (ehs_manager_alfanar.text.toString().length == 0) {
                ehs_manager_alfanar.setText("0")
                ehs_manager_alfanar.setSelection(ehs_manager_alfanar.text.toString().length)
            }
        }
        ehs_manager_sub.setOnFocusChangeListener { view, b ->
            if (ehs_manager_sub.text.toString().length == 0) {
                ehs_manager_sub.setText("0")
                ehs_manager_sub.setSelection(ehs_manager_sub.text.toString().length)
            }
        }
        ehs_engineer_alfanar.setOnFocusChangeListener { view, b ->
            if (ehs_engineer_alfanar.text.toString().length == 0) {
                ehs_engineer_alfanar.setText("0")
                ehs_engineer_alfanar.setSelection(ehs_engineer_alfanar.text.toString().length)
            }
        }
        ehs_engineer_sub.setOnFocusChangeListener { view, b ->
            if (ehs_engineer_sub.text.toString().length == 0) {
                ehs_engineer_sub.setText("0")
                ehs_engineer_sub.setSelection(ehs_engineer_sub.text.toString().length)
            }
        }
        ehs_supervisor_alfanar.setOnFocusChangeListener { view, b ->
            if (ehs_supervisor_alfanar.text.toString().length == 0) {
                ehs_supervisor_alfanar.setText("0")
                ehs_supervisor_alfanar.setSelection(ehs_supervisor_alfanar.text.toString().length)
            }
        }
        ehs_supervisor_sub.setOnFocusChangeListener { view, b ->
            if (ehs_supervisor_sub.text.toString().length == 0) {
                ehs_supervisor_sub.setText("0")
                ehs_supervisor_sub.setSelection(ehs_supervisor_sub.text.toString().length)
            }
        }
        ehs_officer_alfanar.setOnFocusChangeListener { view, b ->
            if (ehs_officer_alfanar.text.toString().length == 0) {
                ehs_officer_alfanar.setText("0")
                ehs_officer_alfanar.setSelection(ehs_officer_alfanar.text.toString().length)
            }
        }
        ehs_officer_sub.setOnFocusChangeListener { view, b ->
            if (ehs_officer_sub.text.toString().length == 0) {
                ehs_officer_sub.setText("0")
                ehs_officer_sub.setSelection(ehs_officer_sub.text.toString().length)
            }
        }
        nurse_alfanar.setOnFocusChangeListener { view, b ->
            if (nurse_alfanar.text.toString().length == 0) {
                nurse_alfanar.setText("0")
                nurse_alfanar.setSelection(nurse_alfanar.text.toString().length)
            }
        }
        nurse_sub.setOnFocusChangeListener { view, b ->
            if (nurse_sub.text.toString().length == 0) {
                nurse_sub.setText("0")
                nurse_sub.setSelection(nurse_sub.text.toString().length)
            }
        }
        first_aider_alfanar.setOnFocusChangeListener { view, b ->
            if (first_aider_alfanar.text.toString().length == 0) {
                first_aider_alfanar.setText("0")
                first_aider_alfanar.setSelection(first_aider_alfanar.text.toString().length)
            }
        }
        first_aider_sub.setOnFocusChangeListener { view, b ->
            if (first_aider_sub.text.toString().length == 0) {
                first_aider_sub.setText("0")
                first_aider_sub.setSelection(first_aider_sub.text.toString().length)
            }
        }
    }

    fun addTextWatcher() {
        ehs_manager_alfanar.afterTextChanged {
            if (ehs_manager_alfanar.text.toString().length > 0) {
                updateehs_manager_alfanar(ehs_manager_alfanar.text.toString())
            }
            else {
                updateehs_manager_alfanar("0")
            }
        }
        ehs_manager_sub.afterTextChanged {
            if (ehs_manager_sub.text.toString().length > 0) {
                updateehs_manager_sub(ehs_manager_sub.text.toString())
            }
            else {
                updateehs_manager_sub("0")
            }
        }
        ehs_engineer_alfanar.afterTextChanged {
            if (ehs_engineer_alfanar.text.toString().length > 0) {
                updatePdAlfanar(ehs_engineer_alfanar.text.toString())
            }
            else {
                updatePdAlfanar("0")
            }
        }
        ehs_engineer_sub.afterTextChanged {
            if (ehs_engineer_sub.text.toString().length > 0) {
                updatePdSC(ehs_engineer_sub.text.toString())
            }
            else {
                updatePdSC("0")
            }
        }
        ehs_supervisor_alfanar.afterTextChanged {
            if (ehs_supervisor_alfanar.text.toString().length > 0) {
                updateNmAlfanar(ehs_supervisor_alfanar.text.toString())
            }
            else {
                updateNmAlfanar("0")
            }
        }
        ehs_supervisor_sub.afterTextChanged {
            if (ehs_supervisor_sub.text.toString().length > 0) {
                updateNmSC(ehs_supervisor_sub.text.toString())
            }
            else {
                updateNmSC("0")
            }
        }
        ehs_officer_alfanar.afterTextChanged {
            if (ehs_officer_alfanar.text.toString().length > 0) {
                updateehs_officer_alfanar(ehs_officer_alfanar.text.toString())
            }
            else {
                updateehs_officer_alfanar("0")
            }
        }
        ehs_officer_sub.afterTextChanged {
            if (ehs_officer_sub.text.toString().length > 0) {
                updateCampInspection(ehs_officer_sub.text.toString())
            }
            else {
                updateCampInspection("0")
            }
        }
        nurse_alfanar.afterTextChanged {
            if (nurse_alfanar.text.toString().length > 0) {
                updatenurse_alfanar(nurse_alfanar.text.toString())
            }
            else {
                updatenurse_alfanar("0")
            }
        }
        nurse_sub.afterTextChanged {
            if (nurse_sub.text.toString().length > 0) {
                updatenurse_sub(nurse_sub.text.toString())
            }
            else {
                updatenurse_sub("0")
            }
        }
        first_aider_alfanar.afterTextChanged {
            if (first_aider_alfanar.text.toString().length > 0) {
                updatefirst_aider_alfanar(first_aider_alfanar.text.toString())
            }
            else {
                updatefirst_aider_alfanar("0")
            }
        }
        first_aider_sub.afterTextChanged {
            if (first_aider_sub.text.toString().length > 0) {
                updatefirst_aider_sub(first_aider_sub.text.toString())
            }
            else {
                updatefirst_aider_sub("0")
            }
        }

        et_note.afterTextChanged {
            if (et_note.text.toString().length > 0) {
                updateNote(et_note.text.toString())
            }
            else {
                updateNote("")
            }
        }
    }

    fun updateehs_manager_alfanar(ehs_manager_alfanar: String) {
        lifecycleScope.launch {
            viewModel.updateManagerAlf(ehs_manager_alfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updateehs_manager_sub(ehs_manager_sub: String) {
        lifecycleScope.launch {
            viewModel.updateManagerSub(ehs_manager_sub.toInt())
        }
        updateCumulativeHours()
    }

    fun updatePdAlfanar(PdAlfanar: String) {
        lifecycleScope.launch {
            viewModel.updateEngineerAlf(PdAlfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updatePdSC(PdSC: String) {
        lifecycleScope.launch {
            viewModel.updateEngineerSub(PdSC.toInt())
        }
        updateCumulativeHours()
    }

    fun updateNmAlfanar(NmAlfanar: String) {
        lifecycleScope.launch {
            viewModel.updateSupervisorAlf(NmAlfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updateNmSC(NmSC: String) {
        lifecycleScope.launch {
            viewModel.updateSupervisorSub(NmSC.toInt())
        }
        updateCumulativeHours()
    }

    fun updateehs_officer_alfanar(ehs_officer_alfanar: String) {
        lifecycleScope.launch {
            viewModel.updateOfficerAlf(ehs_officer_alfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updateCampInspection(CampInspection: String) {
        lifecycleScope.launch {
            viewModel.updateOfficerSub(CampInspection.toInt())
        }
        updateCumulativeHours()
    }

    fun updatenurse_alfanar(CampInspection: String) {
        lifecycleScope.launch {
            viewModel.updateNurseAlf(CampInspection.toInt())
        }
        updateCumulativeHours()
    }

    fun updatenurse_sub(CampInspection: String) {
        lifecycleScope.launch {
            viewModel.updateNurseSub(CampInspection.toInt())
        }
        updateCumulativeHours()
    }

    fun updatefirst_aider_alfanar(CampInspection: String) {
        lifecycleScope.launch {
            viewModel.updateFirstAidAlf(CampInspection.toInt())
        }
        updateCumulativeHours()
    }

    fun updatefirst_aider_sub(CampInspection: String) {
        lifecycleScope.launch {
            viewModel.updateFirstAidSub(CampInspection.toInt())
        }
        updateCumulativeHours()
    }

    fun updateNote(Note: String) {
        lifecycleScope.launch {
            viewModel.updateNote(Note)
        }
        updateCumulativeHours()
    }
}