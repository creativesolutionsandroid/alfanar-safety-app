package com.cs.alfanar.ui.MonthlyReport.MRStage10

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage10ViewModelFactory (
    private val repository: MonthlyReportStage10Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage10ViewModel(repository) as T
    }
}