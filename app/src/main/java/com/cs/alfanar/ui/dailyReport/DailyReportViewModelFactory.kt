package com.cs.alfanar.ui.dailyReport

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DailyReportViewModelFactory (
    private val repository: DailyReportRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DailyReportViewModel(repository) as T
    }
}