package com.cs.alfanar.ui.forgotpassword

import com.cs.alfanar.model.forgotPasswordData.ForgotPasswordResponse
import com.cs.alfanar.model.signupData.SignUpResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class ForgotPasswordRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun forgotPassword(inputJson : String): ForgotPasswordResponse {
        return apiRequest { api.forgotPassword(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

}