package com.cs.alfanar.ui.stopWorkAdvice.Summary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class StopWorkAdviceSummaryViewModelFactory (
    private val repository: StopWorkAdviceSummaryRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return StopWorkAdviceSummaryViewModel(repository) as T
    }
}