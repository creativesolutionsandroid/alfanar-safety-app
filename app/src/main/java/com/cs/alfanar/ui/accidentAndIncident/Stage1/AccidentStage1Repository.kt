package com.cs.alfanar.ui.accidentAndIncident.Stage1

import androidx.room.Query
import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.data.db.entities.weeklyReport.WRprojectSelectionEntity
import com.cs.alfanar.model.accidentIncident.AccidentMasterDataResponse
import com.cs.alfanar.model.alertNotification.AlertNotificationTypeResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class AccidentStage1Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun GetAccidentIncidentReportMasterData(inputJson : String): AccidentMasterDataResponse {
        return apiRequest { api.GetAccidentIncidentReportMasterData(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun insert(data: AccidentIncidentEntity) = db.getAccidentIncidentDao().insert(data)

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) =
        db.getAccidentIncidentDao().updateProject(projectId, projectName, projectCode)

    suspend fun updateDate(date: String)  =
        db.getAccidentIncidentDao().updateDate(date)

    suspend fun updateAccidentType(AccidentType: String, AccidentId: Int)  =
        db.getAccidentIncidentDao().updateAccidentType(AccidentType, AccidentId)

    suspend fun updateTime(time: String)  =
        db.getAccidentIncidentDao().updateTime(time)

    suspend fun updateInjuredEmployeeName(InjuredEmployeeName: String)  =
        db.getAccidentIncidentDao().updateInjuredEmployeeName(InjuredEmployeeName)

    suspend fun updateCompanyContractor(CompanyContractor: String)  =
        db.getAccidentIncidentDao().updateCompanyContractor(CompanyContractor)

    suspend fun updateAccidentClass(AccidentClass: String, AccidentClassId: Int)  =
        db.getAccidentIncidentDao().updateAccidentClass(AccidentClass, AccidentClassId)

    suspend fun updateBodyPartInjured(BodyPartInjured: String, BodyPartInjuredId: String)  =
        db.getAccidentIncidentDao().updateBodyPartInjured(BodyPartInjured, BodyPartInjuredId)

    suspend fun updateBodyPartInjuredOther(BodyPartInjuredOther: String)  =
        db.getAccidentIncidentDao().updateBodyPartInjuredOther(BodyPartInjuredOther)

    suspend fun getData() : List<AccidentIncidentEntity> {
        return db.getAccidentIncidentDao().getData()
    }

    suspend fun deleteData() = db.getAccidentIncidentDao().deleteData()
}