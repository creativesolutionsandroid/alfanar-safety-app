package com.cs.alfanar.ui.stopWorkAdvice.SummaryDetails

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.accidentIncidentSummary.Data
import com.cs.alfanar.ui.accidentAndIncident.SummaryDetails.AccidentIncidentDetailsViewModel
import com.cs.alfanar.ui.accidentAndIncident.SummaryDetails.AccidentIncidentDetailsViewModelFactory
import com.cs.alfanar.ui.accidentAndIncident.SummaryDetails.AccidentIncidentRemarksAdapter
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_accident_incident_details.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class StopWorkAdviceDetailsActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: StopWorkAdviceDetailsViewModelFactory by instance()

    private lateinit var viewModel: StopWorkAdviceDetailsViewModel

    val KEY_REPORT_ID: String = "KEY_REPORT_ID"
    private lateinit var adapter: StopWorkDetailsRemarksAdapter

//    private val TAG : String = this::class.java.simpleName

    var pageSize = 20
    var pageNumber = 1
    private var ReportId: Int = 0
    private var ReportStatus: Int = 0
    private var isStatusUpdated: Boolean = false
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stop_work_advice_details)

        viewModel = ViewModelProvider(this, factory).get(StopWorkAdviceDetailsViewModel::class.java)

        ReportId = intent.getIntExtra(KEY_REPORT_ID, 0)
        initRecyclerView()
        getReportDetails()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        if (isStatusUpdated) {
            setResult(Activity.RESULT_OK)
        }
        else {
            setResult(Activity.RESULT_CANCELED)
        }
        supportFinishAfterTransition()
    }

    override fun onBackPressed() {
        if (isStatusUpdated) {
            setResult(Activity.RESULT_OK)
        }
        else {
            setResult(Activity.RESULT_CANCELED)
        }
        supportFinishAfterTransition()
    }

    fun updateStaus(view: View) {
        if (ReportStatus == DAILY_REPORT_STATUS_OPEN)
            updateStatusApi()
    }

    private fun initRecyclerView(){
        list_remarks.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_remarks.addItemDecoration(itemDecoration)

        adapter = StopWorkDetailsRemarksAdapter()
        list_remarks.adapter = adapter
    }

    private fun initView(data: Data?) {
        ReportId = data!!.Id
        ReportStatus = data.ReportStatus
        item_number.text = data.ReportId
        report_date.text = data.ReportDate
        project_id.text = data.ProjectCode
//        location.text = data.EmployeeName
        status.text = if (data.ReportStatus == DAILY_REPORT_STATUS_OPEN)
            getString(R.string.label_open)
        else
            getString(R.string.label_close)

        if (data.ReportStatus == DAILY_REPORT_STATUS_OPEN) { // open
            switch_status.setImageDrawable(resources.getDrawable(R.drawable.switch_on))
        }
        else { // close
            switch_status.setImageDrawable(resources.getDrawable(R.drawable.switch_off))
        }

        adapter.setList(data.JRemarksJson, this@StopWorkAdviceDetailsActivity)
        adapter.notifyDataSetChanged()
    }

    fun prepareInputJsontoGetDetails(): String {
        val parentObj = JSONObject()

        parentObj.put("Id", ReportId) // Id = 0 to get list of reports
        parentObj.put("pageSize", pageSize)
        parentObj.put("pageNumber", pageNumber)
        parentObj.put("SubmittedBy", 0)
        parentObj.put("ReportStatus", 0)
        parentObj.put("Fromdate", null)
        parentObj.put("Todate", null)
        parentObj.put("FlagId", 2) // FlagId = 1 to get list and FlagId = 2 to get details
        parentObj.put("ActionBy", UserPreferences(this).userId())
//        parentObj.put("ActionBy", 1)

//        Log.d(TAG, "prepareInputJsontoGetDetails: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getReportDetails() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getWeeklyReportDetails(prepareInputJsontoGetDetails())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    initView(response.Data[0])
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@StopWorkAdviceDetailsActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@StopWorkAdviceDetailsActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@StopWorkAdviceDetailsActivity,
                    null
                )
            }
        }
    }

    fun prepareInputJsonForUpdateStatus(): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())
        parentObj.put("ReportId", ReportId)

//        Log.d(TAG, "prepareInputJsonForUpdateStatus: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun updateStatusApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.UpdateReportStatus(prepareInputJsonForUpdateStatus())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    toast(response.MessageEn)
                    status.text = resources.getString(R.string.label_close)
                    switch_status.setImageDrawable(resources.getDrawable(R.drawable.switch_off))
                    isStatusUpdated = true
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@StopWorkAdviceDetailsActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@StopWorkAdviceDetailsActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@StopWorkAdviceDetailsActivity,
                    null
                )
            }
        }
    }
}