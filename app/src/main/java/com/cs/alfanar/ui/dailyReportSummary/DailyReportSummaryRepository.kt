package com.cs.alfanar.ui.dailyReportSummary

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.dailyReportSummary.DailyReportSummaryResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class DailyReportSummaryRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getDailyReportSummary(inputJson : String): DailyReportSummaryResponse {
        return apiRequest { api.getDailyReportSummary(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun UpdateDailyReportStatus(inputJson : String): BasicResponse {
        return apiRequest { api.UpdateDailyReportStatus(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}