package com.cs.alfanar.ui.weeklyReport.weeklyReportDetails

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.weeklyReportDetails.WeeklyReportDetailsResponse
import com.cs.alfanar.model.weeklyReportSummary.WeeklyReportSummaryResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class WeeklyReportDetailsRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getWeeklyReportDetails(inputJson : String): WeeklyReportDetailsResponse {
        return apiRequest { api.getWeeklyReportDetails(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun UpdateReportStatus(inputJson : String): BasicResponse {
        return apiRequest { api.updateWeeklyReportStatus(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}