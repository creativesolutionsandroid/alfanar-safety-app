package com.cs.alfanar.ui.accidentAndIncident.Stage3

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.model.accidentIncident.Data
import com.cs.alfanar.model.accidentIncident.Master
import com.cs.alfanar.ui.accidentAndIncident.Stage2.AccidentStage2Activity
import com.cs.alfanar.ui.accidentAndIncident.Stage2.AccidentStage2ViewModel
import com.cs.alfanar.ui.accidentAndIncident.Stage2.AccidentStage2ViewModelFactory
import com.cs.alfanar.ui.accidentAndIncident.Stage4.AccidentStage4Activity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.CustomTimePickerDialog
import com.cs.alfanar.utils.afterTextChanged
import com.cs.alfanar.utils.showOneButtonAlertDialog
import kotlinx.android.synthetic.main.activity_accident_stage1.*
import kotlinx.android.synthetic.main.activity_accident_stage2.*
import kotlinx.android.synthetic.main.activity_accident_stage2.incident_time
import kotlinx.android.synthetic.main.activity_accident_stage3.*
import kotlinx.android.synthetic.main.activity_weekly_report_stage2.*
import kotlinx.android.synthetic.main.activity_weekly_report_stage3.*
import kotlinx.android.synthetic.main.activity_weekly_report_stage3.btn_next
import kotlinx.android.synthetic.main.activity_weekly_report_stage3.date
import kotlinx.android.synthetic.main.item_weekly_report_stage3_edittext.view.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class AccidentStage3Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: AccidentStage3ViewModelFactory by instance()

    private lateinit var viewModel: AccidentStage3ViewModel

//    private val TAG : String = this::class.java.simpleName
    val KEY_DATA: String = "DATA_KEY"

    var isVehicleApplicable: Boolean = false
    var overtimeStatus: Boolean = false
    var hospitalStatus: Boolean = false
    var policeStatus: Boolean = false
    var civilStatus: Boolean = false
    private lateinit var masterData: Data

    var todayCalendar = Calendar.getInstance()
    var selectedDate: Int = 0
    var selectedMonth: Int = 0
    var selectedYear: Int = 0
    var isDateSet: Boolean = false

    var selectedOverTimeHour : Int = 0
    var selectedOverTimeMin : Int = 0
    var selectedReportingHour : Int = 0
    var selectedReportingMin : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accident_stage3)

        viewModel = ViewModelProvider(this, factory).get(AccidentStage3ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Data
        masterData = Data!!
        fetchDataFromDb()
        addTextWatcher()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onDateClicked(view: View) {
        showDatePicker()
    }

    fun onOvertimeHoursClicked(view: View) {
        if (overtimeStatus) {
            showOvertimeHoursTimePicker()
        }
    }

    fun onReportingTimeClicked(view: View) {
        showReportingTimePicker()
    }

    fun onNextButtonClicked(view: View) {
        validationCheck()
    }

    fun onVehicleSwitchClicked(view: View) {
        if (isVehicleApplicable) {
            isVehicleApplicable = false
            overtimeStatus = false
            hospitalStatus = false
            policeStatus = false
            civilStatus = false
            VehicleFields(false)
            vehicle_applicable_status.setImageDrawable(getDrawable(R.drawable.switch_off))
            overtime_status.setImageDrawable(getDrawable(R.drawable.switch_off))
            hospital_status.setImageDrawable(getDrawable(R.drawable.switch_off))
            police_status.setImageDrawable(getDrawable(R.drawable.switch_off))
            civil_defence_status.setImageDrawable(getDrawable(R.drawable.switch_off))
        }
        else {
            isVehicleApplicable = true
            VehicleFields(true)
            vehicle_applicable_status.setImageDrawable(getDrawable(R.drawable.switch_on))
        }
        updateVehicleApplicable()
    }

    fun VehicleFields(isEditable: Boolean) {
        model.isEnabled = isEditable
        plate_number.isEnabled = isEditable
        vehicle_id.isEnabled = isEditable
        vehicle_involved.isEnabled = isEditable
        overtime_status.isEnabled = isEditable
        overtime_hours.isEnabled = isEditable
        date.isEnabled = isEditable
        reporting_time.isEnabled = isEditable
        hospital_status.isEnabled = isEditable
        police_status.isEnabled = isEditable
        civil_defence_status.isEnabled = isEditable

        if (!isEditable) {
            model.setText("")
            plate_number.setText("")
            vehicle_id.setText("")
            vehicle_involved.setText("")
            overtime_hours.setText("")
            date.setText("")
            reporting_time.setText("")

            updateVehicleApplicable()
            updateModel("")
            updatePlate("")
            updateVehicleID("")
            updateNoOfVehicleInvolved("")
            updateOverTime()
            updatePolice()
            updateHospital()
            updateCivilDefence()
        }
    }

    fun onOvertimeClicked(view: View) {
        if (overtimeStatus) {
            overtimeStatus = false
            overtime_status.setImageDrawable(getDrawable(R.drawable.switch_off))
            overtime_status_text.text = getString(R.string.no)
            selectedOverTimeHour = 0
            selectedOverTimeMin = 0
            updateOverTimeHours()
            overtime_hours.text = ""
        }
        else {
            overtimeStatus = true
            overtime_status.setImageDrawable(getDrawable(R.drawable.switch_on))
            overtime_status_text.text = getString(R.string.yes)
        }
        updateOverTime()
    }

    fun onHospitalClicked(view: View) {
        if (hospitalStatus) {
            hospitalStatus = false
            hospital_status.setImageDrawable(getDrawable(R.drawable.switch_off))
            hospital_status_text.text = getString(R.string.no)
        }
        else {
            hospitalStatus = true
            hospital_status.setImageDrawable(getDrawable(R.drawable.switch_on))
            hospital_status_text.text = getString(R.string.yes)
        }
        updateHospital()
    }

    fun onPoliceClicked(view: View) {
        if (policeStatus) {
            policeStatus = false
            police_status.setImageDrawable(getDrawable(R.drawable.switch_off))
            police_status_text.text = getString(R.string.no)
        }
        else {
            policeStatus = true
            police_status.setImageDrawable(getDrawable(R.drawable.switch_on))
            police_status_text.text = getString(R.string.yes)
        }
        updatePolice()
    }

    fun onCivilClicked(view: View) {
        if (civilStatus) {
            civilStatus = false
            civil_defence_status.setImageDrawable(getDrawable(R.drawable.switch_off))
            civil_defence_status_text.text = getString(R.string.no)
        }
        else {
            civilStatus = true
            civil_defence_status.setImageDrawable(getDrawable(R.drawable.switch_on))
            civil_defence_status_text.text = getString(R.string.yes)
        }
        updateCivilDefence()
    }

    fun addTextWatcher() {
        model.afterTextChanged { updateModel(model.text.toString().trim()) }
        plate_number.afterTextChanged { updatePlate(plate_number.text.toString().trim()) }
        vehicle_id.afterTextChanged { updateVehicleID(vehicle_id.text.toString().trim()) }
        vehicle_involved.afterTextChanged { updateNoOfVehicleInvolved(vehicle_involved.text.toString().trim()) }
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                if (isVehicleApplicable) {
                    VehicleFields(true)
                    model.setText(projectData[0].Model)
                    plate_number.setText(projectData[0].Plate)
                    vehicle_id.setText(projectData[0].VehicleID)
                    vehicle_involved.setText(projectData[0].NoOfVehicleInvolved)
                    date.setText(projectData[0].ReportingDate)
                    reporting_time.setText(projectData[0].ReportingTime)
                    isVehicleApplicable = projectData[0].vehicleApplicable
                    overtimeStatus = projectData[0].OverTime
                    hospitalStatus = projectData[0].Hospital
                    policeStatus = projectData[0].Police
                    civilStatus = projectData[0].CivilDefence

                    if (isVehicleApplicable) {
                        VehicleFields(true)
                        vehicle_applicable_status.setImageDrawable(getDrawable(R.drawable.switch_on))
                    } else {
                        VehicleFields(false)
                        vehicle_applicable_status.setImageDrawable(getDrawable(R.drawable.switch_off))
                    }

                    if (overtimeStatus) {
                        overtime_status.setImageDrawable(getDrawable(R.drawable.switch_on))
                        overtime_hours.setText(projectData[0].OverTimeHours)
                        overtime_status_text.text = getString(R.string.yes)
                    } else {
                        overtime_status.setImageDrawable(getDrawable(R.drawable.switch_off))
                        overtime_status_text.text = getString(R.string.no)
                    }

                    if (hospitalStatus) {
                        hospital_status.setImageDrawable(getDrawable(R.drawable.switch_on))
                        hospital_status_text.text = getString(R.string.yes)
                    } else {
                        hospital_status.setImageDrawable(getDrawable(R.drawable.switch_off))
                        hospital_status_text.text = getString(R.string.no)
                    }

                    if (policeStatus) {
                        police_status.setImageDrawable(getDrawable(R.drawable.switch_on))
                        police_status_text.text = getString(R.string.yes)
                    } else {
                        police_status.setImageDrawable(getDrawable(R.drawable.switch_off))
                        police_status_text.text = getString(R.string.no)
                    }

                    if (civilStatus) {
                        civil_defence_status.setImageDrawable(getDrawable(R.drawable.switch_on))
                        civil_defence_status_text.text = getString(R.string.yes)
                    } else {
                        civil_defence_status.setImageDrawable(getDrawable(R.drawable.switch_off))
                        civil_defence_status_text.text = getString(R.string.no)
                    }
                }
                else {
                    VehicleFields(false)
                }
            }
        }
    }

    fun updateModel(Name: String) {
        lifecycleScope.launch {
            viewModel.updateModel(Name)
        }
    }

    fun updatePlate(Name: String) {
        lifecycleScope.launch {
            viewModel.updatePlate(Name)
        }
    }

    fun updateVehicleID(Name: String) {
        lifecycleScope.launch {
            viewModel.updateVehicleID(Name)
        }
    }

    fun updateNoOfVehicleInvolved(Name: String) {
        lifecycleScope.launch {
            viewModel.updateNoOfVehicleInvolved(Name)
        }
    }

    fun updateVehicleApplicable() {
        lifecycleScope.launch {
            viewModel.updatevehicleApplicable(isVehicleApplicable)
        }
    }

    fun updateOverTime() {
        lifecycleScope.launch {
            viewModel.updateOverTime(overtimeStatus)
        }
    }

    fun updateHospital() {
        lifecycleScope.launch {
            viewModel.updateHospital(hospitalStatus)
        }
    }

    fun updatePolice() {
        lifecycleScope.launch {
            viewModel.updatePolice(policeStatus)
        }
    }

    fun updateCivilDefence() {
        lifecycleScope.launch {
            viewModel.updateCivilDefence(civilStatus)
        }
    }

    fun getMonth(): String {
        var month: String = ""
        if (selectedMonth < 9) {
            month = "0" + (selectedMonth + 1)
        }
        else {
            month = "" + (selectedMonth + 1)
        }
        return month
    }

    fun getDate(): String {
        var month: String = ""
        if (selectedDate < 9) {
            month = "0" + selectedDate
        }
        else {
            month = "" + selectedDate
        }
        return month
    }

    fun showDatePicker() {
        selectedYear = todayCalendar.get(Calendar.YEAR)
        selectedMonth = todayCalendar.get(Calendar.MONTH)
        selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            todayCalendar.set(Calendar.YEAR, year)
            todayCalendar.set(Calendar.MONTH, monthOfYear)
            todayCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            selectedYear = todayCalendar.get(Calendar.YEAR)
            selectedMonth = todayCalendar.get(Calendar.MONTH)
            selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

            val dateSelected = "" + getDate() + "-" + getMonth() + "-" + selectedYear
            date.text = dateSelected
            isDateSet = true
            updateDate()
        }, selectedYear, selectedMonth, selectedDate)

        val c = Calendar.getInstance()
        c.add(Calendar.DATE, -30) // show previous 7 days only

        dpd.datePicker.minDate = c.timeInMillis
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }

    fun updateOverTimeHours() {
        val time = getOverTimeDuration()
        lifecycleScope.launch {
            viewModel.updateOverTimeHours(time)
        }
    }

    fun updateReportingTime() {
        val time = getReportingTimeDuration()
        lifecycleScope.launch {
            viewModel.updateReportingTime(time)
        }
    }

    fun updateDate() {
        val date = "" + getDate() + "-" + getMonth() + "-" + selectedYear
        lifecycleScope.launch {
            viewModel.updateReportingDate(date)
        }
    }

    fun getOverTimeDuration(): String {
        var duration: String = ""
        val hour: String
        val min: String

        if (selectedOverTimeHour < 10) {
            hour = "0" +selectedOverTimeHour.toString()
        }
        else {
            hour = selectedOverTimeHour.toString()
        }

        if (selectedOverTimeMin < 10) {
            min = "0" + selectedOverTimeMin.toString()
        }
        else  {
            min = selectedOverTimeMin.toString()
        }
        duration = hour + ":" + min
        return duration
    }

    fun getReportingTimeDuration(): String {
        var duration: String = ""
        val hour: String
        val min: String

        if (selectedReportingHour < 10) {
            hour = "0" +selectedReportingHour.toString()
        }
        else {
            hour = selectedReportingHour.toString()
        }

        if (selectedReportingMin < 10) {
            min = "0" + selectedReportingMin.toString()
        }
        else  {
            min = selectedReportingMin.toString()
        }
        duration = hour + ":" + min
        return duration
    }

    fun showOvertimeHoursTimePicker() {
        val myTimeListener =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                if (view.isShown) {
                    selectedOverTimeHour = hourOfDay
                    selectedOverTimeMin = minute
                    overtime_hours.text = getOverTimeDuration()
                    updateOverTimeHours()
                }
            }
        val timePickerDialog = CustomTimePickerDialog(
            this,
            myTimeListener,
            selectedOverTimeHour,
            selectedOverTimeMin,
            true
        )
        timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        timePickerDialog.show()
    }

    fun showReportingTimePicker() {
        val myTimeListener =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                if (view.isShown) {
                    selectedReportingHour = hourOfDay
                    selectedReportingMin = minute
                    reporting_time.text = getReportingTimeDuration()
                    updateReportingTime()
                }
            }
        val timePickerDialog = CustomTimePickerDialog(
            this,
            myTimeListener,
            selectedReportingHour,
            selectedReportingMin,
            true
        )
        timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        timePickerDialog.show()
    }

    fun validationCheck() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()

            if (isVehicleApplicable && projectData[0].Model.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_vehicle_model), getString(R.string.Ok),
                    this@AccidentStage3Activity, null
                )
            } else if (isVehicleApplicable && projectData[0].Plate.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_plate), getString(R.string.Ok),
                    this@AccidentStage3Activity, null
                )
            } else if (isVehicleApplicable && projectData[0].VehicleID.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_vehicle_id), getString(R.string.Ok),
                    this@AccidentStage3Activity, null
                )
            } else if (isVehicleApplicable && projectData[0].NoOfVehicleInvolved.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_no_of_vehicle), getString(R.string.Ok),
                    this@AccidentStage3Activity, null
                )
            } else if (isVehicleApplicable && projectData[0].ReportingDate.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_reporting_date),
                    getString(R.string.Ok),
                    this@AccidentStage3Activity,
                    null
                )
            } else if (isVehicleApplicable && projectData[0].ReportingTime.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_reporting_time), getString(R.string.Ok),
                    this@AccidentStage3Activity, null
                )
            } else if (overtimeStatus && projectData[0].OverTimeHours.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_overtime_hours), getString(R.string.Ok),
                    this@AccidentStage3Activity, null
                )
            } else {
                val intent = Intent(this@AccidentStage3Activity, AccidentStage4Activity::class.java)
                intent.putExtra(KEY_DATA, masterData)
                startActivity(intent)
            }
        }
    }
}