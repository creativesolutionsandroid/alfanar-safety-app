package com.cs.alfanar.ui.nearMissIncident.stage1

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.nearMissIncident.NearMissIncidentEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NearMissIncidentStage1ViewModel (private val repository: NearMissIncidentStage1Repository): ViewModel() {

    suspend fun GetAccidentIncidentReportMasterData(inputJson : String) = withContext(Dispatchers.IO) {
        repository.GetAccidentIncidentReportMasterData(inputJson)
    }


    suspend fun saveData(projectData: NearMissIncidentEntity) = repository.saveData(projectData)

    suspend fun getData(): List<NearMissIncidentEntity> = repository.getData()

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) =
        repository.updateProject(projectId, projectName, projectCode)

    suspend fun updateDate(date: String) = repository.updateDate(date)

    suspend fun updateTime(Time: String) = repository.updateTime(Time)

    suspend fun updateLocation(Location: String) = repository.updateLocation(Location)

    suspend fun updateReporterBy(ReporterBy: String) = repository.updateReporterBy(ReporterBy)

    suspend fun updateDesignation(Designation: String) = repository.updateDesignation(Designation)

    suspend fun updateIqama(Iqama: String) = repository.updateIqama(Iqama)

    suspend fun updateContactNo(ContactNo: String) = repository.updateContactNo(ContactNo)


}