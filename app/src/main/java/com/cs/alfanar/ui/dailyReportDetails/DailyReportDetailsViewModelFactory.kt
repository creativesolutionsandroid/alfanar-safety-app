package com.cs.alfanar.ui.dailyReportDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DailyReportDetailsViewModelFactory (
    private val repository: DailyReportDetailsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DailyReportDetailsViewModel(repository) as T
    }
}