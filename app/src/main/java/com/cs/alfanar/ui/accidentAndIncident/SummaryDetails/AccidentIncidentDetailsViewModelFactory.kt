package com.cs.alfanar.ui.accidentAndIncident.SummaryDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AccidentIncidentDetailsViewModelFactory (
    private val repository: AccidentIncidentDetailsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AccidentIncidentDetailsViewModel(repository) as T
    }
}