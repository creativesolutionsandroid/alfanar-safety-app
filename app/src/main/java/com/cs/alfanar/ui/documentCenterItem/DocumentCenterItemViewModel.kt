package com.cs.alfanar.ui.documentCenterItem

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.documentCenterList.DocumentCenterListRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DocumentCenterItemViewModel (private val respository: DocumentCenterItemRepository) : ViewModel() {

    // get documents list api calling
    suspend fun getDocumentsByTypeId(inputJson : String) = withContext(Dispatchers.IO) {
        respository.getDocumentsByTypeId(inputJson)
    }

}