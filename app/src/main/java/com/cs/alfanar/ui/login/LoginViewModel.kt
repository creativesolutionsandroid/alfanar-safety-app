package com.cs.alfanar.ui.login

import android.content.Context
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.loginData.Data
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.regex.Pattern

class LoginViewModel(private val repository: LoginRepository) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    // login api calling
    suspend fun LoginUser(
        inputJson: String
    ) = withContext(Dispatchers.IO) { repository.LoginUser(inputJson) }

    // saving user data in shared preference
    fun saveLoggedInUser(context: Context, data: Data) {
        UserPreferences(context)
            .saveUserData(true, data.Id, data.EmployeeNo.toString(),
            data.EmployeeName.toString(), data.Email.toString(), data.Mobile.toString(), data.ProjectId.toString(), data.ProjectName.toString(),
            data.RoleId, data.RoleEn.toString(), data.NationalityId, data.Nationality.toString(), data.CountryId, data.CountryNameEn)
    }

    // data validation
    fun loginDataChanged(username: String, password: String) {
        if (username.isBlank()) {
            _loginForm.value = LoginFormState(usernameError = R.string.validation_error_enter_username)
        }
//        else if (!isUserNameValid(username)) {
//            _loginForm.value = LoginFormState(usernameError = R.string.validation_error_invalid_email)
//        }
        else if (password.isBlank()) {
            _loginForm.value = LoginFormState(passwordError = R.string.validation_error_enter_password)
        }
        else if (!isValidPassword(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.validation_error_invalid_password_text)
        }
        else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    fun isUserNameValid(username: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(username).matches()
    }

    /**
     * Checks if the password is valid as per the following password policy.
     * Password should be minimum minimum 8 characters long.
     * Password should contain at least one number.
     * Password should contain at least one capital letter.
     * Password should contain at least one small letter.
     * Password should contain at least one special character.
     * Allowed special characters: "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
     *
     * @param data - String
     * @param updateUI - if true and if data is EditText, the function sets error to the EditText or its TextInputLayout
     * @return - true if the password is valid as per the password policy.
     */
    fun isValidPassword(data: String): Boolean {
        val str : String = data
        var valid = true

        // Password policy check
        // Password should be minimum minimum 8 characters long
        if (str.length < 8) {
            valid = false
        }
        // Password should contain at least one number
        var exp = ".*[0-9].*"
        var pattern = Pattern.compile(exp, Pattern.CASE_INSENSITIVE)
        var matcher = pattern.matcher(str)
        if (!matcher.matches()) {
            valid = false
        }

        // Password should contain at least one capital letter
        exp = ".*[A-Z].*"
        pattern = Pattern.compile(exp)
        matcher = pattern.matcher(str)
        if (!matcher.matches()) {
            valid = false
        }

        // Password should contain at least one small letter
        exp = ".*[a-z].*"
        pattern = Pattern.compile(exp)
        matcher = pattern.matcher(str)
        if (!matcher.matches()) {
            valid = false
        }

        // commented code - validtion to check special character
//        // Password should contain at least one special character
//        // Allowed special characters : "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
//        exp = ".*[~!@#\$%\\^&*()\\-_=+\\|\\[{\\]};:'\",<.>/?].*"
//        pattern = Pattern.compile(exp)
//        matcher = pattern.matcher(str)
//        if (!matcher.matches()) {
//            valid = false
//        }

        return valid
    }

    suspend fun clearStage1Data() = repository.clearStage1Data()
    suspend fun clearStage2Data() = repository.clearStage2Data()
    suspend fun clearStage3Data() = repository.clearStage3Data()
    suspend fun clearProjectData() = repository.clearProjectData()
    suspend fun clearChecklistData() = repository.clearChecklistData()
    suspend fun clearOtherHealthData() = repository.clearOtherHealthData()
    suspend fun clearHSEData() = repository.clearHSEData()
    suspend fun clearVulnerbaleData() = repository.clearVulnerbaleData()
    suspend fun clearDrillData() = repository.clearDrillData()

    suspend fun deleteAccidentIncidentData() = repository.deleteAccidentIncidentData()
    suspend fun deleteAccidentInvestigationData() = repository.deleteAccidentInvestigationData()
    suspend fun deleteNearMissData() = repository.deleteNearMissData()
    suspend fun deleteCommunicationData() = repository.deleteCommunicationData()
    suspend fun deleteStopWorkData() = repository.deleteStopWorkData()
    suspend fun deleteEmployeeData() = repository.deleteEmployeeData()
    suspend fun deleteTrainingAttendanceData() = repository.deleteTrainingAttendanceData()

    suspend fun deleteDashboardData() = repository.deleteDashboardData()
    suspend fun deleteProjectData() = repository.deleteProjectData()
    suspend fun deleteStage1Data() = repository.deleteStage1Data()
    suspend fun deleteStage2Data() = repository.deleteStage2Data()
    suspend fun deleteStage3Data() = repository.deleteStage3Data()
    suspend fun deleteStage4Data() = repository.deleteStage4Data()
    suspend fun deleteStage5Data() = repository.deleteStage5Data()
    suspend fun deleteMRStage5EventData() = repository.deleteMRStage5EventData()
    suspend fun deleteStage6Data() = repository.deleteStage6Data()
    suspend fun deleteMRStage6EventData() = repository.deleteMRStage6EventData()
    suspend fun deleteStage9Data() = repository.deleteStage9Data()
}