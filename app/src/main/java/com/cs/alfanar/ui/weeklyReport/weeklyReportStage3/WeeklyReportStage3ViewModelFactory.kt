package com.cs.alfanar.ui.weeklyReport.weeklyReportStage3

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WeeklyReportStage3ViewModelFactory (
    private val repository: WeeklyReportStage3Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeeklyReportStage3ViewModel(repository) as T
    }
}