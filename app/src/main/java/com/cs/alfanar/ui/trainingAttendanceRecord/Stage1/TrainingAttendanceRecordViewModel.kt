package com.cs.alfanar.ui.trainingAttendanceRecord.Stage1

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEmployeeEntity
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TrainingAttendanceRecordViewModel (private val repository: TrainingAttendanceRecordRepository) : ViewModel() {

    suspend fun GetProjectData(inputJson : String) = withContext(Dispatchers.IO) {
        repository.GetProjectData(inputJson)
    }

    suspend fun InsertTrainingAttendanceReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.InsertTrainingAttendanceReport(inputJson)
    }

    // database requests
    suspend fun saveData(projectData: TrainingAttendanceRecordEntity)
            = repository.saveData(projectData)

    suspend fun getData(): List<TrainingAttendanceRecordEntity>
            = repository.getData()

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) =
        repository.updateProject(projectId, projectName, projectCode)

    suspend fun updateDate(date: String) = repository.updateDate(date)

    suspend fun updateTime(Time: String) = repository.updateTime(Time)

    suspend fun updateVenue(Venue: String) = repository.updateVenue(Venue)

    suspend fun updateInstructorName(InstructorName: String)
            = repository.updateInstructorName(InstructorName)

    suspend fun updateTopicOfTraining(TopicOfTraining: String)
            = repository.updateTopicOfTraining(TopicOfTraining)

    suspend fun saveEmployeeData(data: TrainingAttendanceRecordEmployeeEntity): Long
            = repository.saveEmployeeData(data)

    suspend fun getEmployeeData(): List<TrainingAttendanceRecordEmployeeEntity>
            = repository.getEmployeeData()

    suspend fun deleteEmployeeData() = repository.deleteEmployeeData()

    suspend fun deleteData() = repository.deleteData()
}