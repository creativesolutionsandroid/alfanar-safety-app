package com.cs.alfanar.ui.weeklyReport.weeklyReportStage2

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage2Entity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage3.WeeklyReportStage3Activity
import com.cs.alfanar.utils.CustomTimePickerDialog
import com.cs.alfanar.utils.showOneButtonAlertDialog
import kotlinx.android.synthetic.main.activity_weekly_report_stage2.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*


class WeeklyReportStage2Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: WeeklyReportStage2ViewModelFactory by instance()

    private lateinit var viewModel: WeeklyReportStage2ViewModel
    private lateinit var adapter: WeeklyReportStage2Adapter
    var stage2DataList: MutableList<WRstage2Entity> = mutableListOf()

//    private val TAG : String = this::class.java.simpleName

    var todayCalendar = Calendar.getInstance()
    var selectedDate: Int = 0
    var selectedMonth: Int = 0
    var selectedYear: Int = 0
    var selectedHour : Int = 0
    var selectedMin : Int = 0
    var isDurationSet: Boolean = false
    var isDateSet: Boolean = false
    var canScroll: Boolean = false //should scroll after adding item


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weekly_report_stage2)
        viewModel = ViewModelProvider(this, factory).get(WeeklyReportStage2ViewModel::class.java)

        initRecyclerView()
        fetchDataFromDb()
        setObserver()
        setIMEOptions()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun setIMEOptions() {
        et_topic.setImeOptions(EditorInfo.IME_ACTION_DONE)
        et_topic.setRawInputType(InputType.TYPE_CLASS_TEXT)

        et_conducted_by.setImeOptions(EditorInfo.IME_ACTION_DONE)
        et_conducted_by.setRawInputType(InputType.TYPE_CLASS_TEXT)
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val dataList = viewModel.getData()
            stage2DataList.clear()
            stage2DataList.addAll(dataList)
            adapter.setList(stage2DataList, this@WeeklyReportStage2Activity)
            adapter.notifyDataSetChanged()

            if (canScroll) scroll_view.post(Runnable { scroll_view.fullScroll(View.FOCUS_DOWN) })
            canScroll = true
            enableSubmitButton()
        }
    }

    private fun initRecyclerView(){
        list_topics.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_topics.addItemDecoration(itemDecoration)

        adapter = WeeklyReportStage2Adapter()
        list_topics.adapter = adapter
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onSubmitClicked(view: View) {
        viewModel.dataValidation(et_topic.text.toString().trim(), isDurationSet, isDateSet,
            attendee.text.toString().trim(), et_conducted_by.text.toString().trim())
    }

    fun onDurationClicked(view: View) {
        showCustomTimePicker()
    }

    fun onDateClicked(view: View) {
        showDatePicker()
    }

    fun onNextButtonClicked(view: View) {
        startActivity(Intent(this, WeeklyReportStage3Activity::class.java))
    }

    fun disableSubmitButton() {
        btn_submit.isClickable = false
        cardView_submit.alpha = 0.4f
    }

    fun enableSubmitButton() {
        btn_submit.isClickable = true
        cardView_submit.alpha = 1.0f
    }

    fun setObserver() {
        viewModel.formState.observe(this, androidx.lifecycle.Observer {
            val state = it ?: return@Observer
            if (state.topicError != null) {
                et_topic.error = getString(state.topicError)
            }
            if (state.durationError != null) {
                showOneButtonAlertDialog(getString(state.durationError), getString(R.string.Ok), this, null)
            }
            if (state.dateError != null) {
                showOneButtonAlertDialog(getString(state.dateError), getString(R.string.Ok), this, null)
            }
            if (state.attendeeError != null) {
                attendee.error = getString(state.attendeeError)
            }
            if (state.conductedByError != null) {
                et_conducted_by.error = getString(state.conductedByError)
            }
            if (state.isDataValid) {
                disableSubmitButton()
                saveDataToDB()
            }
        })
    }

    fun saveDataToDB() {
        val topic = et_topic.text.toString().trim()
        val duration = getDuration()
        val date = "" + selectedDate + "-" + getMonth() + "-" + selectedYear
        val attendees = attendee.text.toString().trim().toInt()
        val conductedBy = et_conducted_by.text.toString().trim()
        val data = WRstage2Entity((stage2DataList.size + 1), topic, duration, date, attendees, conductedBy)
        lifecycleScope.launch {
            val id = viewModel.saveData(data)
            if (id > 0) {
                clearAllFields()
                fetchDataFromDb()
            }
        }
    }

    fun getMonth(): String {
        var month: String = ""
        if (selectedMonth < 9) {
            month = "0" + (selectedMonth + 1)
        }
        else {
            month = "" + (selectedMonth + 1)
        }
        return month
    }

    fun clearAllFields() {
        et_topic.setText("")
        attendee.setText("")
        et_conducted_by.setText("")
        duration.setText("")
        date.setText("")

        isDurationSet = false
        isDateSet = false
        selectedDate = 0
        selectedMonth = 0
        selectedYear = 0
        selectedHour = 0
        selectedMin = 0
    }

    fun getDuration(): String {
        var duration: String = ""
        val hour: String
        val min: String
        if (selectedHour < 10) {
            hour = "0" +selectedHour.toString()
        }
        else {
            hour = selectedHour.toString()
        }

        if (selectedMin < 10) {
            min = "0" + selectedMin.toString()
        }
        else  {
            min = selectedMin.toString()
        }
        duration = hour + ":" + min
        return duration
    }

    fun showDatePicker() {
        selectedYear = todayCalendar.get(Calendar.YEAR)
        selectedMonth = todayCalendar.get(Calendar.MONTH)
        selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            todayCalendar.set(Calendar.YEAR, year)
            todayCalendar.set(Calendar.MONTH, monthOfYear)
            todayCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            selectedYear = todayCalendar.get(Calendar.YEAR)
            selectedMonth = todayCalendar.get(Calendar.MONTH)
            selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

            date.text = SimpleDateFormat("MMM dd", Locale.US).format(todayCalendar.time)
            isDateSet = true
        }, selectedYear, selectedMonth, selectedDate)

        val c = Calendar.getInstance()
        c.add(Calendar.DATE, -7) // show previous 7 days only

        dpd.datePicker.minDate = c.timeInMillis
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }

    fun showCustomTimePicker() {
        val myTimeListener =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                if (view.isShown) {
                    isDurationSet = true
                    selectedHour = hourOfDay
                    selectedMin = minute
                    duration.text = getDuration()
                }
            }
        val timePickerDialog = CustomTimePickerDialog(
            this,
            myTimeListener,
            selectedHour,
            selectedMin,
            true
        )
        timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        timePickerDialog.show()
    }
}