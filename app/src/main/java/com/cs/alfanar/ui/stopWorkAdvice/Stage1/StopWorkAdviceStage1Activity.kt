package com.cs.alfanar.ui.stopWorkAdvice.Stage1

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.adapters.AccidentIncidentProjectAdapter
import com.cs.alfanar.data.db.entities.nearMissIncident.NearMissIncidentEntity
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkAdviceEntity
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.accidentIncident.Project
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.nearMissIncident.stage2.NearMissIncidentStage2Activity
import com.cs.alfanar.ui.stopWorkAdvice.Stage2.StopWorkAdviceStage2Activity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_near_miss_report_stage1.*
import kotlinx.android.synthetic.main.activity_stop_work_advice_step1.*
import kotlinx.android.synthetic.main.activity_stop_work_advice_step1.date
import kotlinx.android.synthetic.main.activity_stop_work_advice_step1.project_id
import kotlinx.android.synthetic.main.activity_stop_work_advice_step1.spinner_project_name
import kotlinx.android.synthetic.main.activity_stop_work_advice_step1.time
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class StopWorkAdviceStage1Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: StopWorkAdviceStage1ViewModelFactory by instance()

    private lateinit var viewModel: StopWorkAdviceStage1ViewModel

//    private val TAG : String = this::class.java.simpleName

    private var projectsList: List<Project> = mutableListOf()
    private var projectData: List<StopWorkAdviceEntity> = mutableListOf()

    private var selectedProjectPos: Int = 0
    private var projectName: String = ""

    var todayCalendar = Calendar.getInstance()
    var selectedDate: Int = 0
    var selectedMonth: Int = 0
    var selectedYear: Int = 0
    var selectedHour : Int = 0
    var selectedMin : Int = 0
    var isDurationSet: Boolean = false
    var isDateSet: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stop_work_advice_step1)

        viewModel = ViewModelProvider(this, factory).get(StopWorkAdviceStage1ViewModel::class.java)

        fetchDataFromDb()
        addTextWatcher()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun ontimeClicked(view: View) {
        showCustomTimePicker()
    }

    fun onDateClicked(view: View) {
        showDatePicker()
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            projectData = viewModel.getData()
            if (projectData.size > 0) {
                projectName = projectData[0].projectName
                spinner_project_name.setText(projectData[0].projectName)
                project_id.text = projectData[0].projectCode
                date.text = projectData[0].Date
                time.text = projectData[0].Time
                employee_location.setText(projectData[0].Location)
                employee_hse.setText(projectData[0].HseRepresentative)
                employee_nameof_activity.setText(projectData[0].NameOfSupervisor)
                employee_designation.setText(projectData[0].Designation)
                getMasterDataApi()
            }
            else {
                val EmptyData = StopWorkAdviceEntity(1, "", 0, "", "", ""
                    , "",  "",  "", "",
                    "", "")
                viewModel.saveData(EmptyData)
                projectData = viewModel.getData()
                getMasterDataApi()
            }
        }
    }

    fun onNextButtonClicked(view: View) {
        validationCheck()
    }

    fun prepareInputJsonToGetMasterData(): String {
        val parentObj = JSONObject()
        parentObj.put("ActionBy", UserPreferences(this).userId())

//        Log.d(TAG, "prepareInputJsonToGetMasterData: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getMasterDataApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.GetAccidentIncidentReportMasterData(prepareInputJsonToGetMasterData())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    projectsList = response.Data.ProjectList
                    if (projectsList.size > 0) {
                        for (i in 1 until projectsList.size) {
                            if (projectsList[i].ProjectNameEn.equals(projectName)) {
                                selectedProjectPos = i
                                break
                            }
                        }
                        setProjectSpinnerData()
                    }
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@StopWorkAdviceStage1Activity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@StopWorkAdviceStage1Activity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@StopWorkAdviceStage1Activity,
                    null
                )
            }
        }
    }

    fun setProjectSpinnerData() {
        val projectAdapter: AccidentIncidentProjectAdapter =
            AccidentIncidentProjectAdapter(
                this,
                android.R.layout.simple_list_item_1,
                projectsList
            )
        spinner_project_name.setAdapter(projectAdapter)

        spinner_project_name.setText(projectsList.get(selectedProjectPos).ProjectNameEn) // by default first project will be selected
        project_id.text = projectsList.get(selectedProjectPos).ProjectCode
        projectName = projectsList[selectedProjectPos].ProjectNameEn
        updateProjectDetails(selectedProjectPos)

        spinner_project_name.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = projectsList.get(position).ProjectNameEn
                project_id.text = projectsList.get(position).ProjectCode
                spinner_project_name.setText(selectedItem)
                updateProjectDetails(position)
            }

        // Disabling keyboard for spinners
        spinner_project_name.setInputType(InputType.TYPE_NULL)
    }

    fun addTextWatcher() {
        employee_location.afterTextChanged { updateLocation(employee_location.text.toString().trim()) }
        employee_hse.afterTextChanged { updateHseRepresentative(employee_hse.text.toString().trim()) }
        employee_nameof_activity.afterTextChanged { updateNameOfSupervisor((employee_nameof_activity.text.toString().trim())) }
        employee_designation.afterTextChanged { updateDesignation((employee_designation.text.toString().trim())) }
    }

    fun updateLocation(Name: String) {
        lifecycleScope.launch {
            viewModel.updateLocation(Name)
        }
    }

    fun updateHseRepresentative(Name: String) {
        lifecycleScope.launch {
            viewModel.updateHseRepresentative(Name)
        }
    }

    fun updateNameOfSupervisor(Name: String) {
        lifecycleScope.launch {
            viewModel.updateNameOfSupervisor(Name)
        }
    }

    fun updateDesignation(Name: String) {
        lifecycleScope.launch {
            viewModel.updateDesignation(Name)
        }
    }

    fun updateProjectDetails(position: Int) {
        lifecycleScope.launch {
            viewModel.updateProject(projectsList[position].ProjectId, projectsList[position].ProjectNameEn, projectsList[position].ProjectCode)
        }
    }

    fun updateDate() {
        val date = "" + getDate() + "-" + getMonth() + "-" + selectedYear
        lifecycleScope.launch {
            viewModel.updateDate(date)
        }
    }

    fun updateTime() {
        val time = getDuration()
        lifecycleScope.launch {
            viewModel.updateTime(time)
        }
    }

    fun getMonth(): String {
        var month: String = ""
        if (selectedMonth < 9) {
            month = "0" + (selectedMonth + 1)
        }
        else {
            month = "" + (selectedMonth + 1)
        }
        return month
    }

    fun getDate(): String {
        var month: String = ""
        if (selectedDate < 9) {
            month = "0" + selectedDate
        }
        else {
            month = "" + selectedDate
        }
        return month
    }

    fun showDatePicker() {
        selectedYear = todayCalendar.get(Calendar.YEAR)
        selectedMonth = todayCalendar.get(Calendar.MONTH)
        selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            todayCalendar.set(Calendar.YEAR, year)
            todayCalendar.set(Calendar.MONTH, monthOfYear)
            todayCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            selectedYear = todayCalendar.get(Calendar.YEAR)
            selectedMonth = todayCalendar.get(Calendar.MONTH)
            selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

            val dateSelected = "" + getDate() + "-" + getMonth() + "-" + selectedYear
            date.text = dateSelected
            isDateSet = true
            updateDate()
        }, selectedYear, selectedMonth, selectedDate)

        val c = Calendar.getInstance()
        c.add(Calendar.DATE, -30) // show previous 7 days only

        dpd.datePicker.minDate = c.timeInMillis
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }

    fun showCustomTimePicker() {
        val myTimeListener =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                if (view.isShown) {
                    isDurationSet = true
                    selectedHour = hourOfDay
                    selectedMin = minute
                    time.text = getDuration()
                    updateTime()
                }
            }
        val timePickerDialog = CustomTimePickerDialog(
            this,
            myTimeListener,
            selectedHour,
            selectedMin,
            true
        )
        timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        timePickerDialog.show()
    }

    fun getDuration(): String {
        var duration: String = ""
        val hour: String
        val min: String
        var filteredHour: Int

        if (selectedHour < 10) {
            hour = "0" +selectedHour.toString()
        }
        else {
            hour = selectedHour.toString()
        }

        if (selectedMin < 10) {
            min = "0" + selectedMin.toString()
        }
        else  {
            min = selectedMin.toString()
        }
        duration = hour + ":" + min
        return duration
    }

    fun validationCheck() {
        lifecycleScope.launch {
            projectData = viewModel.getData()

            if (projectData[0].Date.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_date), getString(R.string.Ok),
                    this@StopWorkAdviceStage1Activity, null
                )
            } else if (projectData[0].Time.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_time), getString(R.string.Ok),
                    this@StopWorkAdviceStage1Activity, null
                )
            }
            else if (projectData[0].HseRepresentative.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_hse_representative), getString(R.string.Ok),
                    this@StopWorkAdviceStage1Activity, null
                )
            }
            else if (projectData[0].NameOfSupervisor.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_name_of_supervisor), getString(R.string.Ok),
                    this@StopWorkAdviceStage1Activity, null
                )
            }
            else if (projectData[0].Designation.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_designtion), getString(R.string.Ok),
                    this@StopWorkAdviceStage1Activity, null
                )
            }else {
                val intent = Intent(this@StopWorkAdviceStage1Activity, StopWorkAdviceStage2Activity::class.java)
                startActivity(intent)
            }
        }
    }
}