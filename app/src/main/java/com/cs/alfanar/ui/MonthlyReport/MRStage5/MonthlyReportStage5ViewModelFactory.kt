package com.cs.alfanar.ui.MonthlyReport.MRStage5

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage5ViewModelFactory (
    private val repository: MonthlyReportStage5Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage5ViewModel(repository) as T
    }
}