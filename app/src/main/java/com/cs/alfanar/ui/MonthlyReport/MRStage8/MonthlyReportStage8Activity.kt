package com.cs.alfanar.ui.MonthlyReport.MRStage8

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.model.monthlyReportMasterData.ToolBoxTraining
import com.cs.alfanar.ui.MonthlyReport.MRStage7.MonthlyReportStage7Adapter
import com.cs.alfanar.ui.MonthlyReport.MRStage9.MonthlyReportStage9Activity
import com.cs.alfanar.ui.mainActivity.MainActivity
import kotlinx.android.synthetic.main.activity_monthly_report_stage7.*

class MonthlyReportStage8Activity : AppCompatActivity() {

    private lateinit var projectData: Project
    val KEY_DATA: String = "KEY_DATA"

    private lateinit var eventAdapter: MonthlyReportStage8Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_stage8)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Project
        projectData = Data!!

        initRecyclerViews()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextClicked(view: View) {
        val intent = Intent(this, MonthlyReportStage9Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    private fun initRecyclerViews(){
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))

        list_topics.layoutManager = LinearLayoutManager(this)
        list_topics.addItemDecoration(itemDecoration)
        eventAdapter = MonthlyReportStage8Adapter()
        list_topics.adapter = eventAdapter

        val trainingTopics: ArrayList<ToolBoxTraining> = ArrayList()
        for (i in 0 until projectData.ToolBoxTraining.size) {
            if (projectData.ToolBoxTraining[i].TalkType == 2) {
                val topic = ToolBoxTraining(projectData.ToolBoxTraining[i].TopicName,
                    projectData.ToolBoxTraining[i].Duration, projectData.ToolBoxTraining[i].TrainingDate,
                    projectData.ToolBoxTraining[i].NoAttendee, projectData.ToolBoxTraining[i].ConductedBy,
                    projectData.ToolBoxTraining[i].TalkType)
                trainingTopics.add(topic)
            }
        }
        eventAdapter.setList(trainingTopics, this@MonthlyReportStage8Activity)
        eventAdapter.notifyDataSetChanged()
    }
}