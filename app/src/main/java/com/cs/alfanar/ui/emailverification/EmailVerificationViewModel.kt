package com.cs.alfanar.ui.emailverification

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.alfanar.R
import com.cs.alfanar.ui.forgotpassword.ForgotPasswordFormState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class EmailVerificationViewModel(private val emailVerificationRepository: EmailVerificationRepository) : ViewModel() {

    private val _EmailVerificationForm = MutableLiveData<EmailVerificationFormState>()
    val EmailVerificationFormState: LiveData<EmailVerificationFormState> = _EmailVerificationForm

    // forgot password api calling
    suspend fun forgotPassword(
        inputJson: String
    ) = withContext(Dispatchers.IO) { emailVerificationRepository.forgotPassword(inputJson) }

    // data validation
    fun forgotPasswordDataChanged(enteredOTP: String, serverOTP: String?) {
        if (enteredOTP.isBlank()) {
            _EmailVerificationForm.value = EmailVerificationFormState(otpError = R.string.validation_error_enter_otp)
        }
        else if (!enteredOTP.equals(serverOTP)) {
            _EmailVerificationForm.value = EmailVerificationFormState(otpError  = R.string.validation_error_invalid_otp)
        }
        else {
            _EmailVerificationForm.value = EmailVerificationFormState(isDataValid = true)
        }
    }
}