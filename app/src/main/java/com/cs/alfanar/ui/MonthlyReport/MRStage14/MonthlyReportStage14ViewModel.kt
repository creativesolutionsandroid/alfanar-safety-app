package com.cs.alfanar.ui.MonthlyReport.MRStage14

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.monthlyReport.*
import com.cs.alfanar.ui.MonthlyReport.MRStage13.MonthlyReportStage13Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MonthlyReportStage14ViewModel (private val repository: MonthlyReportStage14Repository) : ViewModel() {

    suspend fun insertMonthlyReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.insertMonthlyReport(inputJson)
    }

    // database requests
    suspend fun saveData(projectData: MRStage9Entity) = repository.saveData(projectData)
    suspend fun updateUnitOne(Unit: String, TypeId: Int) = repository.updateUnitOne(Unit, TypeId)
    suspend fun updateUnitTwo(Unit: String, TypeId: Int) = repository.updateUnitTwo(Unit, TypeId)
    suspend fun updateUnitThree(Unit: String, TypeId: Int) = repository.updateUnitThree(Unit, TypeId)
    suspend fun updateUnitFour(Unit: String, TypeId: Int) = repository.updateUnitFour(Unit, TypeId)
    suspend fun updateRemarks(Unit: String, TypeId: Int) = repository.updateRemarks(Unit, TypeId)
    suspend fun getData() : List<MRStage9Entity> { return repository.getData() }
    suspend fun updateStage9(Stage9: Boolean) = repository.updateStage9(Stage9)

    suspend fun getDashboardData() : List<MRDashboardEntity> { return repository.getDashboardData() }
    suspend fun getProjectData() : List<MRProjectionSelectionEntity> { return repository.getProjectData() }
    suspend fun getStage1Data() : List<MRStage1Entity> { return repository.getStage1Data() }
    suspend fun getStage2Data() : List<MRStage2Entity> { return repository.getStage2Data() }
    suspend fun getStage3Data() : List<MRStage3Entity> { return repository.getStage3Data() }
    suspend fun getStage4Data() : List<MRStage4Entity> { return repository.getStage4Data() }
    suspend fun getStage5Data() : List<MRStage5Entity> { return repository.getStage5Data() }
    suspend fun getMRStage5EventData() : List<MRstage5EventsEntity> { return repository.getMRStage5EventData() }
    suspend fun getStage6Data() : List<MRStage6Entity> { return repository.getStage6Data() }
    suspend fun getMRStage6EventData() : List<MRstage6EventsEntity> { return repository.getMRStage6EventData() }
    suspend fun getStage9Data() : List<MRStage9Entity> { return repository.getData() }

    suspend fun deleteDashboardData() = repository.deleteDashboardData()
    suspend fun deleteProjectData() = repository.deleteProjectData()
    suspend fun deleteStage1Data() = repository.deleteStage1Data()
    suspend fun deleteStage2Data() = repository.deleteStage2Data()
    suspend fun deleteStage3Data() = repository.deleteStage3Data()
    suspend fun deleteStage4Data() = repository.deleteStage4Data()
    suspend fun deleteStage5Data() = repository.deleteStage5Data()
    suspend fun deleteMRStage5EventData() = repository.deleteMRStage5EventData()
    suspend fun deleteStage6Data() = repository.deleteStage6Data()
    suspend fun deleteMRStage6EventData() = repository.deleteMRStage6EventData()
    suspend fun deleteStage9Data() = repository.deleteStage9Data()
}