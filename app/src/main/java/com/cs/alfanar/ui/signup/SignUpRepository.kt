package com.cs.alfanar.ui.signup

import com.cs.alfanar.model.masterData.MasterDataResponse
import com.cs.alfanar.model.signupData.SignUpResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class SignUpRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getMasterData(): MasterDataResponse {
        return apiRequest { api.getMasterData() }
    }

    suspend fun registerUser(inputJson : String): SignUpResponse {
        return apiRequest { api.registerUser(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}