package com.cs.alfanar.ui.changePassword

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ChangePasswordViewModelFactory (
    private val repository: ChangePasswordRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ChangePasswordViewModel(repository) as T
    }
}