package com.cs.alfanar.ui.MonthlyReport.MRStage11

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage9Entity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class MonthlyReportStage11Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    // database requests
    suspend fun saveData(projectData: MRStage9Entity) = db.getMRStage9Dao().insert(projectData)
    suspend fun updateUnitOne(Unit: String, TypeId: Int) = db.getMRStage9Dao().updateUnitOne(Unit, TypeId)
    suspend fun updateUnitTwo(Unit: String, TypeId: Int) = db.getMRStage9Dao().updateUnitTwo(Unit, TypeId)
    suspend fun updateUnitThree(Unit: String, TypeId: Int) = db.getMRStage9Dao().updateUnitThree(Unit, TypeId)
    suspend fun updateUnitFour(Unit: String, TypeId: Int) = db.getMRStage9Dao().updateUnitFour(Unit, TypeId)
    suspend fun updateRemarks(Unit: String, TypeId: Int) = db.getMRStage9Dao().updateRemarks(Unit, TypeId)
    suspend fun getData() : List<MRStage9Entity> { return db.getMRStage9Dao().getData() }
}