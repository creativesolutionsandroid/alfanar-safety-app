package com.cs.alfanar.ui.weeklyReport.weeklyReportSummary

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.dailyReportSummary.DailyReportSummaryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WeeklyReportSummaryViewModel (private val repository: WeeklyReportSummaryRepository) : ViewModel() {

    suspend fun getWeeklyReportSummary(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getWeeklyReportSummary(inputJson)
    }

    suspend fun UpdateReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.UpdateReportStatus(inputJson)
    }

}