package com.cs.alfanar.ui.weeklyReport.weeklyReportStage4

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ItemWeeklyReportStage3ObservationsBinding
import com.cs.alfanar.model.weeklyReport.Observation

class ObservationAdapter : RecyclerView.Adapter<ObservationViewHolder>() {
    private val subscribersList = ArrayList<Observation>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObservationViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemWeeklyReportStage3ObservationsBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_weekly_report_stage3_observations,
                parent,
                false
            )
        return ObservationViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: ObservationViewHolder, position: Int) {
        holder.bind(subscribersList[position], appContext)
    }

    fun setList(subscribers: List<Observation>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class ObservationViewHolder(val binding: ItemWeeklyReportStage3ObservationsBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: Observation, context: Context) {
        binding.serialNumber.text = data.ProjectId.toString() + "."
        binding.topic.text = (data.OpenCount + data.CloseCount).toString()
        binding.tiltleStatusOpen.text = data.OpenCount.toString()
        binding.titleStatusClose.text = data.CloseCount.toString()
        binding.titlePercentage.text = data.Percentage.toString()
    }
}