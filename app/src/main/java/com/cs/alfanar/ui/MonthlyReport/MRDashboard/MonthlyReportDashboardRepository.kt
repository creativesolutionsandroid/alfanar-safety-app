package com.cs.alfanar.ui.MonthlyReport.MRDashboard

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.monthlyReport.MRDashboardEntity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class MonthlyReportDashboardRepository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    // database requests
    suspend fun saveData(projectData: MRDashboardEntity) = db.getMRDashboardDao().insert(projectData)

    suspend fun getData() : List<MRDashboardEntity> {
        return db.getMRDashboardDao().getData()
    }
}