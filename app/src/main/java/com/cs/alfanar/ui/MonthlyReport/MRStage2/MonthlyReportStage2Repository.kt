package com.cs.alfanar.ui.MonthlyReport.MRStage2

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage2Entity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class MonthlyReportStage2Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    // database requests
    suspend fun saveData(projectData: MRStage2Entity) = db.getMRStage2Dao().insert(projectData)

    suspend fun updateMTC(MTC: Int) =
        db.getMRStage2Dao().updateMTC(MTC)

    suspend fun updateFAC(FAC: Int) =
        db.getMRStage2Dao().updateFAC(FAC)

    suspend fun updatePdAlfanar(PdAlfanar: Int) =
        db.getMRStage2Dao().updatePdAlfanar(PdAlfanar)

    suspend fun updatePdSC(PdSC: Int) =
        db.getMRStage2Dao().updatePdSC(PdSC)

    suspend fun updateNmAlfanar(NmAlfanar: Int) =
        db.getMRStage2Dao().updateNmAlfanar(NmAlfanar)

    suspend fun updateNmSC(NmSC: Int) =
        db.getMRStage2Dao().updateNmSC(NmSC)

    suspend fun updateMVA(MVA: Int) =
        db.getMRStage2Dao().updateMVA(MVA)

    suspend fun updateCampInspection(CampInspection: Int) =
        db.getMRStage2Dao().updateCampInspection(CampInspection)

    suspend fun updateEIC(EIC: Int) =
        db.getMRStage2Dao().updateEIC(EIC)

    suspend fun updateEmergencyDrillConducte(EmergencyDrillConducte: Int) =
        db.getMRStage2Dao().updateEmergencyDrillConducte(EmergencyDrillConducte)

    suspend fun getData() : List<MRStage2Entity> {
        return db.getMRStage2Dao().getData()
    }
}