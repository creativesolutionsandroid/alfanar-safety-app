package com.cs.alfanar.ui.weeklyReport.weeklyReportStage3

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage3Entity

class WeeklyReportStage3ViewModel (private val repository: WeeklyReportStage3Repository) : ViewModel() {

    private val _Form = MutableLiveData<WeeklyReportStage3FormState>()
    val formState: LiveData<WeeklyReportStage3FormState> = _Form

    // database requests
    suspend fun saveData(data: WRstage3Entity): Long = repository.saveData(data)

    suspend fun getData(): List<WRstage3Entity> = repository.getData()

    // data validation
    fun dataValidation(topic: String, duration: Boolean, date: Boolean, attendee: String, conductedBy: String) {
        if (topic.isBlank()) {
            _Form.value = WeeklyReportStage3FormState(topicError = R.string.error_topic)
        }
        else if (!duration) {
            _Form.value = WeeklyReportStage3FormState(durationError = R.string.error_duration)
        }
        else if (!date) {
            _Form.value = WeeklyReportStage3FormState(dateError = R.string.error_date)
        }
        else if (attendee.isBlank()) {
            _Form.value = WeeklyReportStage3FormState(attendeeError = R.string.error_attendees)
        }
        else if (conductedBy.isBlank()) {
            _Form.value = WeeklyReportStage3FormState(conductedByError = R.string.error_conducted_by)
        }
        else {
            _Form.value = WeeklyReportStage3FormState(isDataValid = true)
        }
    }
}