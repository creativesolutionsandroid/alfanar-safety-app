package com.cs.alfanar.ui.nearMissIncident.Summary

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NearMissIncidentSummaryViewModel  (private val repository: NearMissIncidentSummaryRepository) : ViewModel() {

    suspend fun getWeeklyReportSummary(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getWeeklyReportSummary(inputJson)
    }

    suspend fun updateNearMissStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.updateNearMissStatus(inputJson)
    }

}