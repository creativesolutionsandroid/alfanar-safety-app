package com.cs.alfanar.ui.emergencyNumbers

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class EmergencyNumbersViewModel (private val repository: EmergencyNumbersRepository) : ViewModel() {

    // get emergency numbers api calling
    suspend fun getEmergencyNumbers(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getEmergencyNumbers(inputJson)
    }

}