package com.cs.alfanar.ui.weeklyReport.weeklyReportStage1

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage1Entity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class WeeklyReportStage1Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    // database requests
    suspend fun saveData(projectData: WRstage1Entity) = db.getWRstage1Dao().insert(projectData)

    suspend fun getData(): List<WRstage1Entity> = db.getWRstage1Dao().getData()

    suspend fun getDataByType(reportType: Int): List<WRstage1Entity> = db.getWRstage1Dao().getDataByType(reportType)

    suspend fun updateReportType(ReportType: Int) = db.getWRstage1Dao().updateReportType(ReportType)

    suspend fun updateManpowerAtSite(ManpowerAtSite: Int, ReportType: Int)
            = db.getWRstage1Dao().updateManpowerAtSite(ManpowerAtSite, ReportType)

    suspend fun updateManHour(ManHour: Int, ReportType: Int) = db.getWRstage1Dao().updateManHour(ManHour,ReportType)

    suspend fun updateSafeManHours(SafeManHours: Int, ReportType: Int) =
        db.getWRstage1Dao().updateSafeManHours(SafeManHours, ReportType)

    suspend fun updateCumulativeManHour(CumulativeManHour: Int, ReportType: Int) =
        db.getWRstage1Dao().updateCumulativeManHour(CumulativeManHour, ReportType)

    suspend fun updateLostTimeInjuries(LostTimeInjuries: Int, ReportType: Int) =
        db.getWRstage1Dao().updateLostTimeInjuries(LostTimeInjuries, ReportType)

    suspend fun updateDaysLost(DaysLost: Int, ReportType: Int) = db.getWRstage1Dao().updateDaysLost(DaysLost, ReportType)

    suspend fun updateAccidents(Accidents: Int, ReportType: Int) = db.getWRstage1Dao().updateAccidents(Accidents, ReportType)

    suspend fun updateAccidentInvestigation(AccidentInvestigation: Int, ReportType: Int) =
        db.getWRstage1Dao().updateAccidentInvestigation(AccidentInvestigation, ReportType)

    suspend fun updateMedicalTCase(FirstAidCases: Int, ReportType: Int) =
        db.getWRstage1Dao().updateMedicalTCase(FirstAidCases, ReportType)

    suspend fun updateNearMiss(FirstAidCases: Int, ReportType: Int) =
        db.getWRstage1Dao().updateNearMiss(FirstAidCases, ReportType)

    suspend fun updateFirstAidCases(FirstAidCases: Int, ReportType: Int) =
        db.getWRstage1Dao().updateFirstAidCases(FirstAidCases, ReportType)

    suspend fun updateSickLeave(SickLeave: Int, ReportType: Int) =
        db.getWRstage1Dao().updateSickLeave(SickLeave, ReportType)

    suspend fun updateAlfanarSafety(AlfanarSafety: Int, ReportType: Int) =
        db.getWRstage1Dao().updateAlfanarSafety(AlfanarSafety, ReportType)

    suspend fun updateAttendee(Attendee: Int, ReportType: Int) =
        db.getWRstage1Dao().updateAttendee(Attendee, ReportType)
}