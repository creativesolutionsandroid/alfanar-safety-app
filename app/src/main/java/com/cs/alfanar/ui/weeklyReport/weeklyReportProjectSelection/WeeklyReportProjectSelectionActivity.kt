package com.cs.alfanar.ui.weeklyReport.weeklyReportProjectSelection

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.View.OnTouchListener
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.adapters.DailyReportProjectAdapter
import com.cs.alfanar.adapters.WeeklyReportProjectAdapter
import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.weeklyReport.WRprojectSelectionEntity
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.dailyReport.Data
import com.cs.alfanar.model.weeklyReport.Project
import com.cs.alfanar.ui.weeklyReport.weeklyReportDashboard.WeeklyReportDashboardActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_daily_report.*
import kotlinx.android.synthetic.main.activity_daily_report.et_location
import kotlinx.android.synthetic.main.activity_daily_report.project_id
import kotlinx.android.synthetic.main.activity_daily_report.spinner_project_name
import kotlinx.android.synthetic.main.activity_weekly_report_project_selection.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class WeeklyReportProjectSelectionActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: WeeklyReportProjectSelectionViewModelFactory by instance()

    private lateinit var viewModel: WeeklyReportProjectSelectionViewModel

//    private val TAG : String = this::class.java.simpleName
    private var projectsList: List<Project> = mutableListOf()
    
    private var selectedProjectPos: Int = 0
    private var projectName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weekly_report_project_selection)
        viewModel = ViewModelProvider(this, factory).get(WeeklyReportProjectSelectionViewModel::class.java)

        fetchDataFromDb()
        addTextWatcher()

        // Disabling keyboard for spinners
        spinner_project_name.setInputType(InputType.TYPE_NULL)
        et_location.setImeOptions(EditorInfo.IME_ACTION_DONE)
        et_location.setRawInputType(InputType.TYPE_CLASS_TEXT)
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextButtonClicked(view: View) {
        if (et_location.text.toString().trim().length == 0) {
            et_location.error = resources.getString(R.string.validation_error_empty_field)
        }
        else {
            startActivity(Intent(this, WeeklyReportDashboardActivity::class.java))
        }
    }

    fun deleteData() {
        lifecycleScope.launch {  viewModel.deleteData() }
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                projectName = projectData[0].projectName
                spinner_project_name.setText(projectData[0].projectName)
                project_id.text = projectData[0].projectCode
                et_location.setText(projectData[0].location)
                getProjectDataForDailyReportApi()
            }
            else {
                val projectData = WRprojectSelectionEntity(1, 0, "", "")
                viewModel.saveData(projectData)
                getProjectDataForDailyReportApi()
            }
        }
    }

    fun prepareInputJsonForgetProjectData(): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())

//        Log.d(TAG, "prepareInputJsonForgetProjectData: " + parentObj.toString())
        return parentObj.toString()
    }
    
    private fun getProjectDataForDailyReportApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getProjectDataForWeeklyReport(prepareInputJsonForgetProjectData())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    projectsList = response.Data.Projects
                    if (projectsList.size > 0) {
                        for (i in 1 until projectsList.size) {
                            if (projectsList[i].ProjectNameEn.equals(projectName)) {
                                selectedProjectPos = i
                                break
                            }
                        }
                        setProjectSpinnerData()
                    }
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@WeeklyReportProjectSelectionActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@WeeklyReportProjectSelectionActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@WeeklyReportProjectSelectionActivity,
                    null
                )
            }
        }
    }

    fun setProjectSpinnerData() {
        val projectAdapter: WeeklyReportProjectAdapter =
            WeeklyReportProjectAdapter(
                this,
                android.R.layout.simple_list_item_1,
                projectsList
            )
        spinner_project_name.setAdapter(projectAdapter)

        spinner_project_name.setText(projectsList.get(selectedProjectPos).ProjectNameEn) // by default first project will be selected
        project_id.text = projectsList.get(selectedProjectPos).ProjectCode
        projectName = projectsList[selectedProjectPos].ProjectNameEn
        updateProjectDetails(selectedProjectPos)

        spinner_project_name.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = projectsList.get(position).ProjectNameEn
                project_id.text = projectsList.get(position).ProjectCode
                spinner_project_name.setText(selectedItem)
                updateProjectDetails(position)
                hideKeyboard(et_location)
            }

        spinner_project_name.setOnTouchListener(View.OnTouchListener { view, motionEvent ->
            hideKeyboard(et_location)
            return@OnTouchListener true
        })

        spinner_project_name.afterTextChanged {
            if (spinner_project_name.text.toString().length == 1) {
                spinner_project_name.setText("")
            }
            else if (!spinner_project_name.text.toString().equals(projectsList[selectedProjectPos].ProjectNameEn)) {
                spinner_project_name.setText(projectsList[selectedProjectPos].ProjectNameEn)
            }
        }

        // Disabling keyboard for spinners
        spinner_project_name.setInputType(InputType.TYPE_NULL)
    }

    fun addTextWatcher() {
        et_location.afterTextChanged { updateLocation(et_location.text.toString().trim()) }
    }
    
    fun updateProjectDetails(position: Int) {
        lifecycleScope.launch {
            viewModel.updateProject(projectsList[position].ProjectId, projectsList[position].ProjectNameEn, projectsList[position].ProjectCode)
        }
    }

    fun updateLocation(location: String) {
        lifecycleScope.launch {
            viewModel.updateLocation(location)
        }
    }
    
}