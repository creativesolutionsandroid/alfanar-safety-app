package com.cs.alfanar.ui.MonthlyReport.MRStage8

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ItemMonthlyTrainingTopicBinding
import com.cs.alfanar.model.monthlyReportMasterData.ToolBoxTraining
import java.time.Duration

class MonthlyReportStage8Adapter : RecyclerView.Adapter<MyViewHolder>() {
    private val subscribersList = ArrayList<ToolBoxTraining>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemMonthlyTrainingTopicBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_monthly_training_topic,
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position], appContext, (position + 1))
    }

    fun setList(subscribers: List<ToolBoxTraining>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemMonthlyTrainingTopicBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: ToolBoxTraining, context: Context, id: Int) {

        binding.serialNumber.text = "" + id + "."
        binding.topic.text = data.TopicName
        binding.duration.text = getDuration(data.Duration)
        binding.date.text = getDate(data.ConductedBy)
        binding.attendeeCount.text = data.NoAttendee.toString()
    }

    fun getDate(TrainingDate: String): String {
        val splitDate = TrainingDate.split("T")
        return splitDate[0]
    }

    fun getDuration(mins: Int): String {
        var duration: String = ""
        val hour: String
        val min: String
        var selectedHour: Int = 0
        var selectedMin: Int = 0
        if (mins < 60) {
            selectedMin = mins
        }
        else {
            selectedHour = mins / 60
            selectedMin = mins - (selectedHour * 60)
        }
        if (selectedHour < 10) {
            hour = "0" +selectedHour.toString()
        }
        else {
            hour = selectedHour.toString()
        }

        if (selectedMin < 10) {
            min = "0" + selectedMin.toString()
        }
        else  {
            min = selectedMin.toString()
        }
        duration = hour + ":" + min
        return duration
    }
}