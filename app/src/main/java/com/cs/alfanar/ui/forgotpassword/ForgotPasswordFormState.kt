package com.cs.alfanar.ui.forgotpassword

/**
 * Data validation state of the forgot password form.
 */
data class ForgotPasswordFormState(val emailError: Int? = null,
                                   val isDataValid: Boolean = false)