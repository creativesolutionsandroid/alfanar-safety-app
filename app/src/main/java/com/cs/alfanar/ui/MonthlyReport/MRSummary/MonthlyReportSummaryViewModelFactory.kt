package com.cs.alfanar.ui.MonthlyReport.MRSummary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportSummaryViewModelFactory  (
    private val repository: MonthlyReportSummaryRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportSummaryViewModel(repository) as T
    }
}