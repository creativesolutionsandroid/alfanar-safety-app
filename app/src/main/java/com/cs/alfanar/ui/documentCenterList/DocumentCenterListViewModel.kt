package com.cs.alfanar.ui.documentCenterList

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DocumentCenterListViewModel(private val respository: DocumentCenterListRepository) : ViewModel() {

    // get documents list api calling
    suspend fun getDocumentCenterList(inputJson : String) = withContext(Dispatchers.IO) {
        respository.getDocumentCenterList(inputJson)
    }

}