package com.cs.alfanar.ui.weeklyReport.weeklyReportDetails

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.dailyReportDetails.DailyReportDetailsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WeeklyReportDetailsViewModel (private val repository: WeeklyReportDetailsRepository) : ViewModel() {

    suspend fun getWeeklyReportDetails(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getWeeklyReportDetails(inputJson)
    }

    suspend fun UpdateReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.UpdateReportStatus(inputJson)
    }

}