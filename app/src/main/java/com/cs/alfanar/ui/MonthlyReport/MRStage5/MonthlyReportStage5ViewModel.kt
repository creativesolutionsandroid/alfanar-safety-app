package com.cs.alfanar.ui.MonthlyReport.MRStage5

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage5Entity
import com.cs.alfanar.data.db.entities.monthlyReport.MRstage5EventsEntity

class MonthlyReportStage5ViewModel (private val repository: MonthlyReportStage5Repository) : ViewModel() {

    suspend fun saveEventData(data: MRstage5EventsEntity): Long = repository.saveEventData(data)
    suspend fun getEventData(): List<MRstage5EventsEntity> = repository.getEventData()
    suspend fun updateEventData(name: String, id: Int) = repository.updateEventData(name, id)

    // database requests
    suspend fun saveData(projectData: MRStage5Entity) = repository.saveData(projectData)
    suspend fun updateSiteManagement(SiteManagement: Int) = repository.updateSiteManagement(SiteManagement)
    suspend fun updateSafetyAudits(SafetyAudits: Int) = repository.updateSafetyAudits(SafetyAudits)
    suspend fun updateHighManagement(HighManagement: Int) = repository.updateHighManagement(HighManagement)
    suspend fun updateAdministrative(Administrative: Int) = repository.updateAdministrative(Administrative)
    suspend fun getData() : List<MRStage5Entity> { return repository.getData() }
    suspend fun updateStage3(Stage3: Boolean) = repository.updateStage3(Stage3)
}