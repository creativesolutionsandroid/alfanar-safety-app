package com.cs.alfanar.ui.weeklyReport.weeklyReportStage3

data class WeeklyReportStage3FormState (
    val topicError: Int? = null,
    val durationError: Int? = null,
    val dateError: Int? = null,
    val attendeeError: Int? = null,
    val conductedByError: Int? = null,
    val isDataValid: Boolean = false
)