package com.cs.alfanar.ui.weeklyReport.weeklyReportStage3

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage3Entity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class WeeklyReportStage3Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {


    // database requests
    suspend fun saveData(data: WRstage3Entity): Long = db.getWRstage3Dao().insert(data)

    suspend fun getData(): List<WRstage3Entity> = db.getWRstage3Dao().getData()

}