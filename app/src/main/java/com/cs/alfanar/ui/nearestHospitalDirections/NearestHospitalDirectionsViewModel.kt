package com.cs.alfanar.ui.nearestHospitalDirections

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.nearestHospital.NearestHospitalRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NearestHospitalDirectionsViewModel (private val repository: NearestHospitalDirectionsRepository) : ViewModel()  {

    // get hospital directions api calling
    suspend fun getHospitalDirection(origin : String?, destination : String?, api_key: String) = withContext(Dispatchers.IO) {
        repository.getHospitalDirection(origin, destination, api_key)
    }
}