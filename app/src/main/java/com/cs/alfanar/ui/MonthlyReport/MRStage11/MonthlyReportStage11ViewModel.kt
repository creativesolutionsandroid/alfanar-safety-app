package com.cs.alfanar.ui.MonthlyReport.MRStage11

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage9Entity

class MonthlyReportStage11ViewModel  (private val repository: MonthlyReportStage11Repository) : ViewModel() {

    // database requests
    suspend fun saveData(projectData: MRStage9Entity) = repository.saveData(projectData)
    suspend fun updateUnitOne(Unit: String, TypeId: Int) = repository.updateUnitOne(Unit, TypeId)
    suspend fun updateUnitTwo(Unit: String, TypeId: Int) = repository.updateUnitTwo(Unit, TypeId)
    suspend fun updateUnitThree(Unit: String, TypeId: Int) = repository.updateUnitThree(Unit, TypeId)
    suspend fun updateUnitFour(Unit: String, TypeId: Int) = repository.updateUnitFour(Unit, TypeId)
    suspend fun updateRemarks(Unit: String, TypeId: Int) = repository.updateRemarks(Unit, TypeId)
    suspend fun getData() : List<MRStage9Entity> { return repository.getData() }

}