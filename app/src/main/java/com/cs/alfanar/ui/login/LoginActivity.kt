package com.cs.alfanar.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ActivityLoginBinding
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import androidx.lifecycle.*
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.forgotpassword.ForgotPasswordActivity
import com.cs.alfanar.ui.signup.SignUpActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_splash_screen.*
import kotlinx.android.synthetic.main.activity_weekly_report_stage2.*
import kotlinx.android.synthetic.main.activity_welcome_screen.*
import kotlinx.android.synthetic.main.activity_welcome_screen.alfanar_logo
import kotlinx.coroutines.launch
import org.json.JSONObject

class LoginActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: LoginViewModelFactory by instance()

    private lateinit var viewModel: LoginViewModel
    private lateinit var binding: ActivityLoginBinding

    private val TAG : String = this::class.java.simpleName
    private var isTextWatchersEnabled: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        viewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)

        setObserver()
    }

    fun disableLoginButton() {
        btn_login.isClickable = false
        cardView_login.alpha = 0.4f
    }

    fun enableLoginButton() {
        btn_login.isClickable = true
        cardView_login.alpha = 1.0f
    }

    fun setObserver() {
        viewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            if (loginState.usernameError != null) {
                binding.etEmail.error = getString(loginState.usernameError)
                enableLoginButton()
            }
            if (loginState.passwordError != null) {
                enableLoginButton()
                binding.etPassword.error = getString(loginState.passwordError)
            }
            if (loginState.isDataValid) {

                loginUser()
            }
        })
    }

    private fun loginUser() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            viewModel.clearStage1Data()
            viewModel.clearStage2Data()
            viewModel.clearStage3Data()
            viewModel.clearProjectData()
            viewModel.clearChecklistData()
            viewModel.clearOtherHealthData()
            viewModel.clearHSEData()
            viewModel.clearVulnerbaleData()
            viewModel.clearDrillData()

            viewModel.deleteAccidentIncidentData()
            viewModel.deleteAccidentInvestigationData()
            viewModel.deleteNearMissData()
            viewModel.deleteCommunicationData()
            viewModel.deleteStopWorkData()
            viewModel.deleteEmployeeData()
            viewModel.deleteTrainingAttendanceData()

            viewModel.deleteDashboardData()
            viewModel.deleteProjectData()
            viewModel.deleteStage1Data()
            viewModel.deleteStage2Data()
            viewModel.deleteStage3Data()
            viewModel.deleteStage4Data()
            viewModel.deleteStage5Data()
            viewModel.deleteMRStage5EventData()
            viewModel.deleteStage6Data()
            viewModel.deleteMRStage6EventData()
            viewModel.deleteStage9Data()
            try {
                val response = viewModel.LoginUser(prepareInputJsonForLogin())
                Log.d(TAG, "response: "+response.toString())
                enableLoginButton()
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    viewModel.saveLoggedInUser(this@LoginActivity, response.Data)
                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    val imageViewPair = androidx.core.util.Pair.create<View, String>(alfanar_logo, "transition_image")
                    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this@LoginActivity, imageViewPair)
                    startActivity(intent)
//                    Handler(Looper.getMainLooper()).postDelayed({
                        finish()
//                    }, 5000)
                } else {
                    // status false from api
                    closeLoadingDialog()
                    enableLoginButton()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@LoginActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                enableLoginButton()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@LoginActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                enableLoginButton()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@LoginActivity,
                    null
                )
            }
        }
    }

    fun onLoginClicked(view: View) {
        disableLoginButton()
        doValidateAndLogin()
    }

    fun onSignUpClicked(view: View) {
        val intent = Intent(this, SignUpActivity::class.java)
        val button1ViewPair = androidx.core.util.Pair.create<View, String>(cardView_cancel, "transition_btn1")
        val button2ViewPair = androidx.core.util.Pair.create<View, String>(cardView_login, "transition_btn2")
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, button1ViewPair, button2ViewPair)
        startActivity(intent, options.toBundle())
        finish()
    }

    fun onForgotPasswordClicked(view: View) {
        startActivity(Intent(this, ForgotPasswordActivity::class.java))
        finish()
    }

    fun onCancelClicked(view: View) {
        supportFinishAfterTransition()
    }

    fun doValidateAndLogin(){
        // calling validations method in view model
        viewModel.loginDataChanged(
            binding.etEmail.text.toString().trim(),
            binding.etPassword.text.toString().trim()
        )

        // Checking and enabling text watchers
//        if (!isTextWatchersEnabled)
//            addTextWatchers()
    }

    fun prepareInputJsonForLogin(): String {
        val parentObj = JSONObject()

        val email: String = binding.etEmail.text.toString().trim()
        val password: String = binding.etPassword.text.toString().trim()

        parentObj.put("UserName", email)
        parentObj.put("Password", password)
        parentObj.put("DeviceToken", -1)
        parentObj.put("DeviceVersion", appVersion())
        parentObj.put("Language", "En")
        parentObj.put("DeviceType", "Android")

        Log.d(TAG, "prepareInputJsonForLogin: " + parentObj.toString())
        return parentObj.toString()
    }

    fun addTextWatchers(){
        isTextWatchersEnabled = true

        // adding text watchers for edittexts
        binding.etEmail.afterTextChanged {
            if (viewModel.isUserNameValid(binding.etEmail.text.toString().trim()))
                binding.inputEmailLayout.isErrorEnabled = false
        }

        binding.etPassword.afterTextChanged {
            if (viewModel.isValidPassword(binding.etPassword.text.toString().trim()))
            binding.inputPasswordLayout.isErrorEnabled = false
        }
    }
}