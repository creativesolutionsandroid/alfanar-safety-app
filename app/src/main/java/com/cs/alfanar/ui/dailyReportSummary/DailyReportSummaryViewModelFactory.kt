package com.cs.alfanar.ui.dailyReportSummary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DailyReportSummaryViewModelFactory (
    private val repository: DailyReportSummaryRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DailyReportSummaryViewModel(repository) as T
    }
}