package com.cs.alfanar.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.model.loginData.LoginResponse
import com.cs.alfanar.network.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody


/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class LoginRepository(private val api: ApiInterface,
                      private val db: AppDatabase
) : SafeApiRequest(){

    suspend fun LoginUser(inputJson: String): LoginResponse = withContext(Dispatchers.IO) {
        return@withContext apiRequest { api.LoginUser(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun clearStage1Data() = db.getWRstage1Dao().deleteData()
    suspend fun clearStage2Data() = db.getWRstage2Dao().deleteDataUsingId()
    suspend fun clearStage3Data() = db.getWRstage3Dao().deleteDataUsingId()
    suspend fun clearProjectData() = db.getWRprojectSelectionDao().deleteData()
    suspend fun clearChecklistData() = db.getWRstage4ChecklistDao().deleteDataUsingId()
    suspend fun clearOtherHealthData() = db.getWRstage4OtherHealthDao().deleteDataUsingId()
    suspend fun clearHSEData() = db.getWRstage4HSEDao().deleteDataUsingId()
    suspend fun clearVulnerbaleData() = db.getWRstage4VulnerableDao().deleteDataUsingId()
    suspend fun clearDrillData() = db.getWRstage4DrillDao().deleteDataUsingId()

    suspend fun deleteAccidentIncidentData() = db.getAccidentIncidentDao().deleteData()
    suspend fun deleteAccidentInvestigationData() = db.getAccidentInvestigationDao().deleteData()
    suspend fun deleteNearMissData() = db.getNearMissIncidentDao().deleteData()
    suspend fun deleteCommunicationData() = db.getStopWorkCommunicationDao().deleteCommunicationData()
    suspend fun deleteStopWorkData() = db.getStopWorkCommunicationDao().deleteStopWorkData()
    suspend fun deleteEmployeeData() = db.getTrainingAttendanceRecordEmployeeDao().deleteData()
    suspend fun deleteTrainingAttendanceData() = db.getTrainingAttendanceRecordDao().deleteData()

    suspend fun deleteDashboardData() = db.getMRDashboardDao().deleteData()
    suspend fun deleteProjectData() = db.getMRProjectionSelectionDao().deleteData()
    suspend fun deleteStage1Data() = db.getMRStage1Dao().deleteData()
    suspend fun deleteStage2Data() = db.getMRStage2Dao().deleteData()
    suspend fun deleteStage3Data() = db.getMRStage3Dao().deleteData()
    suspend fun deleteStage4Data() = db.getMRStage4Dao().deleteData()
    suspend fun deleteStage5Data() = db.getMRStage5Dao().deleteData()
    suspend fun deleteMRStage5EventData() = db.getMRStage5EventDao().deleteData()
    suspend fun deleteStage6Data() = db.getMRStage6Dao().deleteData()
    suspend fun deleteMRStage6EventData() = db.getMRStage6EventDao().deleteData()
    suspend fun deleteStage9Data() = db.getMRStage9Dao().deleteData()
}