package com.cs.alfanar.ui.weeklyReport.weeklyReportStage4

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.weeklyReport.*
import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.weeklyReport.WeeklyProjectsData
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class WeeklyReportStage4Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    //api requests
    suspend fun getProjectDataForWeeklyReport(inputJson : String): WeeklyProjectsData {
        return apiRequest { api.getProjectDataForWeeklyReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun insertWeeklyReport(inputJson : String): BasicResponse {
        return apiRequest { api.insertWeeklyReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // database requests
    suspend fun saveCheckListData(data: WRstage4ChecklistEntity): Long = db.getWRstage4ChecklistDao().insert(data)
    suspend fun getCheckListData(): List<WRstage4ChecklistEntity> = db.getWRstage4ChecklistDao().getData()

    suspend fun saveOtherHealthData(data: WRstage4OtherHealthEntity): Long = db.getWRstage4OtherHealthDao().insert(data)
    suspend fun getOtherHealthData(): List<WRstage4OtherHealthEntity> = db.getWRstage4OtherHealthDao().getData()
    suspend fun updateOtherHealthData(name: String, id: Int) = db.getWRstage4OtherHealthDao().updateData(name, id)

    suspend fun saveHSEData(data: WRstage4HSEEntity): Long = db.getWRstage4HSEDao().insert(data)
    suspend fun getHSEData(): List<WRstage4HSEEntity> = db.getWRstage4HSEDao().getData()
    suspend fun updateHSEData(name: String, id: Int) = db.getWRstage4HSEDao().updateData(name, id)

    suspend fun saveVulnerableData(data: WRstage4VulnerableEntity): Long = db.getWRstage4VulnerableDao().insert(data)
    suspend fun getVulnerableData(): List<WRstage4VulnerableEntity> = db.getWRstage4VulnerableDao().getData()
    suspend fun updateVulnerableData(name: String, id: Int) = db.getWRstage4VulnerableDao().updateData(name, id)

    suspend fun updateDrillData(data: WRstage4EmergencyDrillEntity) = db.getWRstage4DrillDao().insert(data)
    suspend fun getDrillData(): List<WRstage4EmergencyDrillEntity> = db.getWRstage4DrillDao().getData()

    suspend fun getStage1Data(): List<WRstage1Entity> = db.getWRstage1Dao().getData()
    suspend fun getStage2Data(): List<WRstage2Entity> = db.getWRstage2Dao().getData()
    suspend fun getStage3Data(): List<WRstage3Entity> = db.getWRstage3Dao().getData()
    suspend fun getProjectData() : List<WRprojectSelectionEntity> { return db.getWRprojectSelectionDao().getData() }

    suspend fun clearStage1Data() = db.getWRstage1Dao().deleteData()
    suspend fun clearStage2Data() = db.getWRstage2Dao().deleteDataUsingId()
    suspend fun clearStage3Data() = db.getWRstage3Dao().deleteDataUsingId()
    suspend fun clearProjectData() = db.getWRprojectSelectionDao().deleteData()
    suspend fun clearChecklistData() = db.getWRstage4ChecklistDao().deleteDataUsingId()
    suspend fun clearOtherHealthData() = db.getWRstage4OtherHealthDao().deleteDataUsingId()
    suspend fun clearHSEData() = db.getWRstage4HSEDao().deleteDataUsingId()
    suspend fun clearVulnerbaleData() = db.getWRstage4VulnerableDao().deleteDataUsingId()
    suspend fun clearDrillData() = db.getWRstage4DrillDao().deleteDataUsingId()
}