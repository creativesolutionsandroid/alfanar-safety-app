package com.cs.alfanar.ui.MonthlyReport.MRStage11

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage11ViewModelFactory(
    private val repository: MonthlyReportStage11Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage11ViewModel(repository) as T
    }
}