package com.cs.alfanar.ui.weeklyReport.weeklyReportProjectSelection

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.weeklyReport.WRprojectSelectionEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WeeklyReportProjectSelectionViewModel (private val repository: WeeklyReportProjectSelectionRepository) : ViewModel() {

    //api requests
    // get project data api calling
    suspend fun getProjectDataForWeeklyReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getProjectDataForWeeklyReport(inputJson)
    }

    // database request
    suspend fun getData() : List<WRprojectSelectionEntity> {
        return repository.getData()
    }

    suspend fun saveData(projectData: WRprojectSelectionEntity) = repository.saveData(projectData)

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) = repository.updateProject(projectId, projectName, projectCode)

    suspend fun updateLocation(location: String) = repository.updateLocation(location)

    suspend fun deleteData() = repository.deleteData()
}