package com.cs.alfanar.ui.resetpassword

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ActivityEmailVerificationBinding
import com.cs.alfanar.databinding.ActivityResetPasswordBinding
import com.cs.alfanar.ui.emailverification.EmailVerificationViewModel
import com.cs.alfanar.ui.emailverification.EmailVerificationViewModelFactory
import com.cs.alfanar.ui.login.LoginActivity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.signup.SignUpSuccessActivity
import com.cs.alfanar.utils.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ResetPasswordActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: ResetPasswordViewModelFactory by instance()

    private lateinit var viewModel: ResetPasswordViewModel
    private lateinit var binding: ActivityResetPasswordBinding

//    private val TAG : String = this::class.java.simpleName

    private var serverOTP: String? = ""
    private var emailId: String? = ""

    private val OTP_KEY: String = "OTP_KEY"
    private val EMAIL_KEY: String = "EMAIL_KEY"

    private var isTextWatchersEnabled: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password)

        viewModel = ViewModelProvider(this, factory).get(ResetPasswordViewModel::class.java)

        serverOTP = intent.getStringExtra(OTP_KEY)
        emailId = intent.getStringExtra(EMAIL_KEY)

        viewModel.resetPasswordFormState.observe(this, Observer {
            val resetPasswordFormState: ResetPasswordFormState = it ?: return@Observer
            if (resetPasswordFormState.newPasswordError != null) {
                binding.etPassword.error = getString(resetPasswordFormState.newPasswordError)
            }
            else if (resetPasswordFormState.confirmPasswordError != null) {
                binding.etConfirmPassword.error = getString(resetPasswordFormState.confirmPasswordError)
            }
            if (resetPasswordFormState.isDataValid) {
                resetPasswordApi()
            }
        })
    }

    fun OnSubmitClicked(view: View) {
        doValidateAndSubmit()
    }

    fun onBackClicked(view: View) {
        finish()
    }

    fun doValidateAndSubmit() {
        viewModel.resetPasswordDataChanged(binding.etPassword.text.toString(), binding.etConfirmPassword.text.toString())

        // Checking and enabling text watchers
//        if (!isTextWatchersEnabled)
//            addTextWatchers()
    }

    fun addTextWatchers() {
        isTextWatchersEnabled = true

        binding.etPassword.afterTextChanged {
            binding.inputPasswordLayout.isErrorEnabled = false
            doValidateAndSubmit()
        }

        binding.etConfirmPassword.afterTextChanged {
            binding.inputConfirmPasswordLayout.isErrorEnabled = false
            doValidateAndSubmit()
        }
    }

    fun prepareInputJsonForResetPassword(): String {
        val parentObj = JSONObject()

        val newPassword: String = binding.etPassword.text.toString().trim()

        parentObj.put("Email", emailId)
        parentObj.put("OTP", serverOTP)
        parentObj.put("NewPassword", newPassword)

//        Log.d(TAG, "prepareInputJsonForLogin: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun resetPasswordApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.resetPassword(prepareInputJsonForResetPassword())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()

                    val intent = Intent(this@ResetPasswordActivity, LoginActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)

                    showOneButtonAlertDialog(getString(R.string.text_reset_password_success), getString(R.string.Ok),
                        this@ResetPasswordActivity, intent)
//                    finish()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@ResetPasswordActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@ResetPasswordActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@ResetPasswordActivity,
                    null
                )
            }
        }
    }

}