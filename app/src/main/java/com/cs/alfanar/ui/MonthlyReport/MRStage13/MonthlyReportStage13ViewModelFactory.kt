package com.cs.alfanar.ui.MonthlyReport.MRStage13

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage13ViewModelFactory (
    private val repository: MonthlyReportStage13Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage13ViewModel(repository) as T
    }
}