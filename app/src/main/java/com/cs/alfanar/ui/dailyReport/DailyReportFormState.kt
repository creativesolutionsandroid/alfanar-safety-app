package com.cs.alfanar.ui.dailyReport

data class DailyReportFormState(val manPowerError: Int? = null,
                                val locationError: Int? = null,
                                val activityError: Int? = null,
                                val unsafeActError: Int? = null,
                                val correctiveError: Int? = null,
                                val actionTakenError: Int? = null,
                                val areaError: Int? = null,
                                val isDataValid: Boolean = false)