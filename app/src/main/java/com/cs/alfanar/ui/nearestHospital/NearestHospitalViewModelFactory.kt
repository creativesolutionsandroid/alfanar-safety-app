package com.cs.alfanar.ui.nearestHospital

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class NearestHospitalViewModelFactory (
    private val repository: NearestHospitalRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NearestHospitalViewModel(repository) as T
    }
}