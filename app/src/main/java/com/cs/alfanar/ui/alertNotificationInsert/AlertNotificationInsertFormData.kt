package com.cs.alfanar.ui.alertNotificationInsert

data class AlertNotificationInsertFormState(val descriptionError: Int? = null,
                                      val isDataValid: Boolean = false)