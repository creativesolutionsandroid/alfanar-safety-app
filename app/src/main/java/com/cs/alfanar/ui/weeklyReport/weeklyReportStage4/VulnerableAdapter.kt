package com.cs.alfanar.ui.weeklyReport.weeklyReportStage4

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4VulnerableEntity
import com.cs.alfanar.databinding.ItemWeeklyReportStage3EdittextBinding
import com.cs.alfanar.utils.afterTextChanged

class VulnerableAdapter(private val onCountChanged:(String, Int)->Unit)
    : RecyclerView.Adapter<VulnerableViewHolder>() {
    private val subscribersList = ArrayList<WRstage4VulnerableEntity>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VulnerableViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemWeeklyReportStage3EdittextBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_weekly_report_stage3_edittext,
                parent,
                false
            )
        return VulnerableViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: VulnerableViewHolder, position: Int) {
        holder.bind(subscribersList[position], onCountChanged, appContext)
    }

    fun setList(subscribers: List<WRstage4VulnerableEntity>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class VulnerableViewHolder(val binding: ItemWeeklyReportStage3EdittextBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: WRstage4VulnerableEntity, onCountChanged:(String, Int)->Unit, context: Context) {
        Log.d("WeeklyReportStag", "bind: "+data.Name)
        binding.edittext.setText(data.Name)
        binding.edittext.afterTextChanged { onCountChanged(binding.edittext.text.toString(), adapterPosition) }

    }
}