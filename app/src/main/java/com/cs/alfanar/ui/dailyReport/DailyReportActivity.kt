package com.cs.alfanar.ui.dailyReport

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.adapters.DailyReportProjectAdapter
import com.cs.alfanar.adapters.ProjectAdapter
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.dailyReport.CalendarData
import com.cs.alfanar.model.dailyReport.Data
import com.cs.alfanar.model.masterData.Project
import com.cs.alfanar.ui.dailyReportSummary.DailyReportSummaryActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_daily_report.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*


class DailyReportActivity : AppCompatActivity() , KodeinAware {

    override val kodein by kodein()
    private val factory: DailyReportViewModelFactory by instance()

    private lateinit var viewModel: DailyReportViewModel
    private lateinit var adapter: DailyReportCalendarAdapter

//    private val TAG : String = this::class.java.simpleName

    private var projectsList: List<Data> = mutableListOf()
    private var currentWeekDates: MutableList<CalendarData> = mutableListOf()
    private var projectSelected: Int = 0
    private var selectedData = CalendarData("", "", false, false, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_daily_report)
        viewModel = ViewModelProvider(this, factory).get(DailyReportViewModel::class.java)

        getProjectDataForDailyReportApi()
        initRecyclerView()
        setObserver()
        setImeOptionsForEdittexts()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onSubmitClicked(view: View) {
        viewModel.dataValidation(man_power.text.toString().trim(),
            et_location.text.toString().trim(),
            et_activity.text.toString().trim(),
            et_unsafe_act.text.toString().trim(),
            et_corrective_action.text.toString().trim(),
            et_action_taken_by_hse_officer.text.toString().trim(),
            et_area_activity_incharge.text.toString().trim()
        )
    }

    fun setImeOptionsForEdittexts() {
        et_location.setImeOptions(EditorInfo.IME_ACTION_NEXT)
        et_activity.setImeOptions(EditorInfo.IME_ACTION_NEXT)
        et_unsafe_act.setImeOptions(EditorInfo.IME_ACTION_NEXT)
        et_corrective_action.setImeOptions(EditorInfo.IME_ACTION_NEXT)
        et_action_taken_by_hse_officer.setImeOptions(EditorInfo.IME_ACTION_NEXT)
        et_area_activity_incharge.setImeOptions(EditorInfo.IME_ACTION_DONE)

        et_location.setRawInputType(InputType.TYPE_CLASS_TEXT)
        et_activity.setRawInputType(InputType.TYPE_CLASS_TEXT)
        et_unsafe_act.setRawInputType(InputType.TYPE_CLASS_TEXT)
        et_corrective_action.setRawInputType(InputType.TYPE_CLASS_TEXT)
        et_action_taken_by_hse_officer.setRawInputType(InputType.TYPE_CLASS_TEXT)
        et_area_activity_incharge.setRawInputType(InputType.TYPE_CLASS_TEXT)
    }

    fun clearEditTexts() {
        man_power.setText("")
        et_location.setText("")
        et_activity.setText("")
        et_unsafe_act.setText("")
        et_corrective_action.setText("")
        et_action_taken_by_hse_officer.setText("")
        et_area_activity_incharge.setText("")
    }

    fun setObserver() {
        viewModel.formState.observe(this, androidx.lifecycle.Observer {
            val dailyReportFormState : DailyReportFormState = it  ?: return@Observer
            if (dailyReportFormState.manPowerError != null) {
                man_power.error = getString(dailyReportFormState.manPowerError)
                scrollTo(man_power, scrollView)
            }
            if (dailyReportFormState.locationError != null) {
                et_location.error = getString(dailyReportFormState.locationError)
                scrollTo(et_location, scrollView)
            }
            if (dailyReportFormState.activityError != null) {
                et_activity.error = getString(dailyReportFormState.activityError)
                scrollTo(et_activity, scrollView)
            }
            if (dailyReportFormState.unsafeActError != null) {
                et_unsafe_act.error = getString(dailyReportFormState.unsafeActError)
                scrollTo(et_unsafe_act, scrollView)
            }
            if (dailyReportFormState.correctiveError != null) {
                et_corrective_action.error = getString(dailyReportFormState.correctiveError)
                scrollTo(et_corrective_action, scrollView)
            }
            if (dailyReportFormState.actionTakenError != null) {
                et_action_taken_by_hse_officer.error = getString(dailyReportFormState.actionTakenError)
                scrollTo(et_action_taken_by_hse_officer, scrollView)
            }
            if (dailyReportFormState.areaError != null) {
                et_area_activity_incharge.error = getString(dailyReportFormState.areaError)
                scrollTo(et_area_activity_incharge, scrollView)
            }
            if (selectedData.date.equals("")) {
                showOneButtonAlertDialog(resources.getString(R.string.daily_report_select_calendar_date),
                    resources.getString(R.string.Ok), this, null)
            }
            else if (dailyReportFormState.isDataValid) {
                InsertProjectDataForDailyReportApi()
//                showDialog()
            }
        })
    }

    private fun initRecyclerView(){
        val layoutManager = GridLayoutManager(this, 7)
        calendar.setLayoutManager(layoutManager)

        adapter = DailyReportCalendarAdapter({ selectedItem: CalendarData ->listItemClicked(selectedItem)})
        calendar.adapter = adapter
    }

    private fun listItemClicked(data: CalendarData){
        if (!data.isFutureDate && !data.isReported) { //Only pending dates will be selected
            selectedData = data
            formatDate(selectedData.date)
        }
    }

    private fun prepareCalendarData() {
        currentWeekDates.clear()
        var isFutureDate: Boolean = false
        for (i in 0 until projectsList.get(projectSelected).WeekDays.size) {

            val calendarData = CalendarData(projectsList.get(projectSelected).WeekDays.get(i).WeekDate,
                projectsList.get(projectSelected).WeekDays.get(i).DayName,
                if (projectsList.get(projectSelected).WeekDays.get(i).IsToday == 1) true else false ,
                if (projectsList.get(projectSelected).WeekDays.get(i).IsReported == 1) true else false ,
                isFutureDate)

            if (projectsList.get(projectSelected).WeekDays.get(i).IsToday == 1) {
                isFutureDate = true
                if (projectsList.get(projectSelected).WeekDays.get(i).IsReported == 0) {
                    selectedData = calendarData
                    formatDate(selectedData.date)
                    checkDeadline()
                }
            }
            currentWeekDates.add(calendarData)
        }

        adapter.setList(currentWeekDates, this@DailyReportActivity)
        adapter.notifyDataSetChanged()
        calendar.scheduleLayoutAnimation()
    }

    fun checkDeadline() {
        val currentTime = Calendar.getInstance()
        val serverTime = Calendar.getInstance()

        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val serverDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)

        val todayDate = dateFormat.format(currentTime.time)
        serverTime.time = serverDateFormat.parse(todayDate + " "+ projectsList.get(projectSelected).MaxReportTime)

        if (currentTime.after(serverTime)) {
            val message = resources.getString(R.string.daily_report_deadline_messgae) + " " +formattedTime(projectsList.get(projectSelected).MaxReportTime)
            showOneButtonAlertDialog(
                message,
            resources.getString(R.string.Ok), this, null)
        }
    }

    fun formattedTime(time: String) : String {
        val inputFormatter = SimpleDateFormat("HH:mm:ss", Locale.ENGLISH)
        val outputFormatter = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
        return outputFormatter.format(inputFormatter.parse(time))
    }

    fun setProjectSpinnerData() {
        val projectAdapter: DailyReportProjectAdapter =
            DailyReportProjectAdapter(
                this,
                android.R.layout.simple_list_item_1,
                projectsList
            )
        spinner_project_name.setAdapter(projectAdapter)

        spinner_project_name.setText(projectsList.get(0).ProjectNameEn) // by default first project will be selected
        project_id.text = projectsList.get(0).ProjectCode

        spinner_project_name.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = projectsList.get(position).ProjectNameEn
                projectSelected = position
                project_id.text = projectsList.get(position).ProjectCode
                spinner_project_name.setText(selectedItem)
                prepareCalendarData()
            }

        // Disabling keyboard for spinners
        spinner_project_name.setInputType(InputType.TYPE_NULL)
    }

    fun prepareInputJsonForgetProjectData(): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())

//        Log.d(TAG, "prepareInputJsonForgetProjectData: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getProjectDataForDailyReportApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getProjectDataForDailyReport(prepareInputJsonForgetProjectData())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    Log.d("TAG", "getProjectDataForDailyReportApi: "+response)
                    projectsList = response.Data
                    if (projectsList.size > 0) {
                        setProjectSpinnerData()
                        prepareCalendarData()
                    }
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@DailyReportActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@DailyReportActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@DailyReportActivity,
                    null
                )
            }
        }
    }

    fun showDialog() {
        var customDialog: AlertDialog? = null
        val dialogBuilder =
            AlertDialog.Builder(this)

        val inflater = layoutInflater
        val layout: Int = R.layout.dialog_message
        val dialogView = inflater.inflate(layout, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)

        val desc = dialogView.findViewById<View>(R.id.desc) as TextView
        val yes = dialogView.findViewById<View>(R.id.pos_btn) as TextView
        val no = dialogView.findViewById<View>(R.id.ngt_btn) as TextView

        desc.text = String.format(resources.getString(R.string.daily_report_submit_status), selectedData.date,
            projectsList.get(projectSelected).ProjectNameEn)
        no.text = resources.getString(R.string.label_edit)
        customDialog = dialogBuilder.create()
        customDialog.show()
        val finalCustomDialog = customDialog

        yes.setOnClickListener {
            finalCustomDialog!!.dismiss()
            InsertProjectDataForDailyReportApi()
        }

        no.setOnClickListener {
            finalCustomDialog!!.dismiss()
        }

        val lp = WindowManager.LayoutParams()
        val window = customDialog.window
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        lp.copyFrom(window.attributes)
        //This makes the dialog take up the full width
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val screenWidth = size.x
        val d = screenWidth * 0.85
        lp.width = d.toInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
    }

    fun prepareInputJsonForInsertProjectData(): String {
        val parentObj = JSONObject()

        parentObj.put("ProjectId", projectsList.get(projectSelected).ProjectId)
        parentObj.put("TotalManPower", man_power.text.toString().trim())
        parentObj.put("Location", et_location.text.toString().trim())
        parentObj.put("Activity", et_activity.text.toString().trim())
        parentObj.put("UnSafeAct", et_unsafe_act.text.toString().trim())
        parentObj.put("CorrectionAction", et_corrective_action.text.toString().trim())
        parentObj.put("HSEOfficerAction", et_action_taken_by_hse_officer.text.toString().trim())
        parentObj.put("ActivityIncharge", et_area_activity_incharge.text.toString().trim())
        parentObj.put("ReportDate", selectedData.date)
        parentObj.put("SubmittedBy", UserPreferences(this).userId())

//        Log.d(TAG, "prepareInputJsonForInsertProjectData: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun InsertProjectDataForDailyReportApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.insertDailyReport(prepareInputJsonForInsertProjectData())
                closeLoadingDialog()
                if (response.Status) {
                    // status true from api
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@DailyReportActivity,
                        Intent(this@DailyReportActivity, DailyReportSummaryActivity::class.java)
                    )
                }
                else {
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@DailyReportActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@DailyReportActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@DailyReportActivity,
                    null
                )
            }
        }
    }

    fun formatDate(date: String) {
        val inputFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val outputFormatter = SimpleDateFormat("MMM dd", Locale.ENGLISH)
        title_date.text = String.format(resources.getString(R.string.label_date),
            outputFormatter.format(inputFormatter.parse(date)))
    }
}