package com.cs.alfanar.ui.MonthlyReport.MRStage4

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage4Entity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class MonthlyReportStage4Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    // database requests
    suspend fun saveData(projectData: MRStage4Entity) = db.getMRStage4Dao().insert(projectData)

    suspend fun updateManagerAlf(ManagerAlf: Int) =
        db.getMRStage4Dao().updateManagerAlf(ManagerAlf)

    suspend fun updateManagerSub(ManagerSub: Int) =
        db.getMRStage4Dao().updateManagerSub(ManagerSub)

    suspend fun updateEngineerAlf(EngineerAlf: Int) =
        db.getMRStage4Dao().updateEngineerAlf(EngineerAlf)

    suspend fun updateEngineerSub(EngineerSub: Int) =
        db.getMRStage4Dao().updateEngineerSub(EngineerSub)

    suspend fun updateSupervisorAlf(SupervisorAlf: Int) =
        db.getMRStage4Dao().updateSupervisorAlf(SupervisorAlf)

    suspend fun updateSupervisorSub(SupervisorSub: Int) =
        db.getMRStage4Dao().updateSupervisorSub(SupervisorSub)

    suspend fun updateOfficerAlf(OfficerAlf: Int) =
        db.getMRStage4Dao().updateOfficerAlf(OfficerAlf)

    suspend fun updateOfficerSub(OfficerSub: Int) =
        db.getMRStage4Dao().updateOfficerSub(OfficerSub)

    suspend fun updateNurseAlf(NurseAlf: Int) =
        db.getMRStage4Dao().updateNurseAlf(NurseAlf)

    suspend fun updateNurseSub(NurseSub: Int) =
        db.getMRStage4Dao().updateNurseSub(NurseSub)

    suspend fun updateFirstAidAlf(FirstAidAlf: Int) =
        db.getMRStage4Dao().updateFirstAidAlf(FirstAidAlf)

    suspend fun updateFirstAidSub(FirstAidSub: Int) =
        db.getMRStage4Dao().updateFirstAidSub(FirstAidSub)

    suspend fun updateNote(Note: String) =
        db.getMRStage4Dao().updateNote(Note)
    
    suspend fun getData() : List<MRStage4Entity> {
        return db.getMRStage4Dao().getData()
    }

    suspend fun updateStage2(Stage2: Boolean) =
        db.getMRDashboardDao().updateStage2(Stage2)
}