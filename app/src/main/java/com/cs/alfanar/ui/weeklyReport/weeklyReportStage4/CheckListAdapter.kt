package com.cs.alfanar.ui.weeklyReport.weeklyReportStage4

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4ChecklistEntity
import com.cs.alfanar.databinding.ItemWeeklyReportChecklistBinding

class CheckListAdapter : RecyclerView.Adapter<MyViewHolder>() {
    private val subscribersList = ArrayList<WRstage4ChecklistEntity>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemWeeklyReportChecklistBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_weekly_report_checklist,
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position], appContext)
    }

    fun setList(subscribers: List<WRstage4ChecklistEntity>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemWeeklyReportChecklistBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: WRstage4ChecklistEntity, context: Context) {

        binding.serialNumber.text = data.id.toString() + "."
        binding.topic.text = data.ChecklistName
        if (data.NoOfEquipments < 10) {
//            binding.date.text = String.format(context.resources.getString(R.string.text_equipments, "0" + data.NoOfEquipments.toString()))
            binding.date.text =  "0" + data.NoOfEquipments.toString()
        } else {
//            binding.date.text = String.format(context.resources.getString(R.string.text_equipments, data.NoOfEquipments.toString()))
            binding.date.text = data.NoOfEquipments.toString()
        }

        if (data.NoOfChecklist < 10) {
//            binding.attendeeCount.text = String.format(context.resources.getString(R.string.text_checklist, "0"+data.NoOfChecklist.toString()))
            binding.attendeeCount.text =  "0"+data.NoOfChecklist.toString()
        }
        else {
//            binding.attendeeCount.text = String.format(context.resources.getString(R.string.text_checklist, data.NoOfChecklist.toString()))
            binding.attendeeCount.text = data.NoOfChecklist.toString()
        }
    }
}