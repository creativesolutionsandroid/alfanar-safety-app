package com.cs.alfanar.ui.MonthlyReport.MRSummaryDetails

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.weeklyReport.weeklyReportDetails.WeeklyReportDetailsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MonthlyReportSummaryDetailsViewModel (private val repository: MonthlyReportSummaryDetailsRepository) : ViewModel() {

    suspend fun getWeeklyReportDetails(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getMonthlyReportDetails(inputJson)
    }

    suspend fun UpdateReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.updateMonthlyReportStatus(inputJson)
    }

}