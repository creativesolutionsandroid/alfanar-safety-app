package com.cs.alfanar.ui.trainingAttendanceRecord.Stage1

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.adapters.AccidentIncidentProjectAdapter
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEmployeeEntity
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEntity
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.accidentIncident.Project
import com.cs.alfanar.ui.stopWorkAdvice.SummaryDetails.StopWorkAdviceDetailsActivity
import com.cs.alfanar.ui.trainingAttendanceRecord.Summary.TrainingAttendanceRecordSummaryActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_stop_work_advice_step2.*
import kotlinx.android.synthetic.main.activity_training_attendance_record.*
import kotlinx.android.synthetic.main.activity_training_attendance_record.date
import kotlinx.android.synthetic.main.activity_training_attendance_record.employee_Advice_name
import kotlinx.android.synthetic.main.activity_training_attendance_record.employee_designation
import kotlinx.android.synthetic.main.activity_training_attendance_record.employee_mode
import kotlinx.android.synthetic.main.activity_training_attendance_record.project_id
import kotlinx.android.synthetic.main.activity_training_attendance_record.spinner_project_name
import kotlinx.android.synthetic.main.activity_training_attendance_record.time
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*

class TrainingAttendanceRecordActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: TrainingAttendanceRecordViewModelFactory by instance()

    private lateinit var viewModel: TrainingAttendanceRecordViewModel

//    private val TAG : String = this::class.java.simpleName

    private var projectsList: List<Project> = mutableListOf()
    private var projectData: List<TrainingAttendanceRecordEntity> = mutableListOf()
    private var employeeList: List<TrainingAttendanceRecordEmployeeEntity> = mutableListOf()

    private lateinit var adapter: TrainingAttendanceRecordAdapter

    private var selectedProjectPos: Int = 0
    private var projectName: String = ""

    var todayCalendar = Calendar.getInstance()
    var selectedDate: Int = 0
    var selectedMonth: Int = 0
    var selectedYear: Int = 0
    var selectedHour : Int = 0
    var selectedMin : Int = 0
    var isDurationSet: Boolean = false
    var isDateSet: Boolean = false
    val KEY_BACK_ACTION : String = "KEY_BACK_ACTION"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_training_attendance_record)

        viewModel = ViewModelProvider(this, factory).get(TrainingAttendanceRecordViewModel::class.java)

        initRecyclerView()
        fetchDataFromDb()
        addTextWatcher()

        ed_venu.setImeOptions(EditorInfo.IME_ACTION_NEXT)
        ed_venu.setRawInputType(InputType.TYPE_CLASS_TEXT)

        et_instructos.setImeOptions(EditorInfo.IME_ACTION_DONE)
        et_instructos.setRawInputType(InputType.TYPE_CLASS_TEXT)

        et_topic_training.setImeOptions(EditorInfo.IME_ACTION_DONE)
        et_topic_training.setRawInputType(InputType.TYPE_CLASS_TEXT)
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun ontimeClicked(view: View) {
        showCustomTimePicker()
    }

    fun onDateClicked(view: View) {
        showDatePicker()
    }

    fun onSubmitClicked(view: View) {
        validationCheck()
    }

    private fun initRecyclerView(){
        list_employees.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_employees.addItemDecoration(itemDecoration)

        adapter = TrainingAttendanceRecordAdapter()
        list_employees.adapter = adapter
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            projectData = viewModel.getData()
            if (projectData.size > 0) {
                projectName = projectData[0].projectName
                employeeList = viewModel.getEmployeeData()

                spinner_project_name.setText(projectData[0].projectName)
                project_id.text = projectData[0].projectCode
                date.text = projectData[0].Date
                time.text = projectData[0].Time
                ed_venu.setText(projectData[0].Venue)
                et_instructos.setText(projectData[0].InstructorName)
                et_topic_training.setText(projectData[0].TopicOfTraining)

                adapter.setList(employeeList, this@TrainingAttendanceRecordActivity)
                adapter.notifyDataSetChanged()

                getMasterDataApi()
            }
            else {
                val EmptyData = TrainingAttendanceRecordEntity(1, "", 0, "", "", ""
                    , "",  "",  "")
                viewModel.saveData(EmptyData)
                projectData = viewModel.getData()
                getMasterDataApi()
            }
        }
    }

    fun onEmployeeAdded(view: View) {
        val name = employee_Advice_name.text.trim()
        val designtion = employee_designation.text.trim()
        val fileNumber = employee_mode.text.trim()
        val department = et_department.text.trim()

        if (name.length == 0) {
            showOneButtonAlertDialog(
                getString(R.string.please_enter_name), getString(R.string.Ok),
                this@TrainingAttendanceRecordActivity, null
            )
        }
        else if (designtion.length == 0) {
            showOneButtonAlertDialog(
                getString(R.string.please_enter_designtion), getString(R.string.Ok),
                this@TrainingAttendanceRecordActivity, null
            )
        }
        else if (fileNumber.length == 0) {
            showOneButtonAlertDialog(
                getString(R.string.please_enter_file_number), getString(R.string.Ok),
                this@TrainingAttendanceRecordActivity, null
            )
        }
        else if (department.length == 0) {
            showOneButtonAlertDialog(
                getString(R.string.please_enter_department), getString(R.string.Ok),
                this@TrainingAttendanceRecordActivity, null
            )
        }
        else {
            val data = TrainingAttendanceRecordEmployeeEntity (employeeList.size + 1 ,
                name.toString(), designtion.toString(), fileNumber.toString(), department.toString())
            lifecycleScope.launch {
                viewModel.saveEmployeeData(data)
                employeeList = viewModel.getEmployeeData()
                adapter.setList(employeeList, this@TrainingAttendanceRecordActivity)
                adapter.notifyDataSetChanged()

                employee_Advice_name.setText("")
                employee_designation.setText("")
                employee_mode.setText("")
                et_department.setText("")
            }
        }
    }

    fun prepareInputJsonToGetMasterData(): String {
        val parentObj = JSONObject()
        parentObj.put("ActionBy", UserPreferences(this).userId())

//        Log.d(TAG, "prepareInputJsonToGetMasterData: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getMasterDataApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.GetProjectData(prepareInputJsonToGetMasterData())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    projectsList = response.Data.ProjectList
                    if (projectsList.size > 0) {
                        for (i in 1 until projectsList.size) {
                            if (projectsList[i].ProjectNameEn.equals(projectName)) {
                                selectedProjectPos = i
                                break
                            }
                        }
                        setProjectSpinnerData()
                    }
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@TrainingAttendanceRecordActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@TrainingAttendanceRecordActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@TrainingAttendanceRecordActivity,
                    null
                )
            }
        }
    }

    fun setProjectSpinnerData() {
        val projectAdapter: AccidentIncidentProjectAdapter =
            AccidentIncidentProjectAdapter(
                this,
                android.R.layout.simple_list_item_1,
                projectsList
            )
        spinner_project_name.setAdapter(projectAdapter)

        spinner_project_name.setText(projectsList.get(selectedProjectPos).ProjectNameEn) // by default first project will be selected
        project_id.text = projectsList.get(selectedProjectPos).ProjectCode
        projectName = projectsList[selectedProjectPos].ProjectNameEn
        updateProjectDetails(selectedProjectPos)

        spinner_project_name.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = projectsList.get(position).ProjectNameEn
                project_id.text = projectsList.get(position).ProjectCode
                spinner_project_name.setText(selectedItem)
                updateProjectDetails(position)
            }

        // Disabling keyboard for spinners
        spinner_project_name.setInputType(InputType.TYPE_NULL)
    }

    fun addTextWatcher() {
        ed_venu.afterTextChanged { updateVenue(ed_venu.text.toString().trim()) }
        et_instructos.afterTextChanged { updateInstructorName(et_instructos.text.toString().trim()) }
        et_topic_training.afterTextChanged { updateTopicOfTraining((et_topic_training.text.toString().trim())) }
    }

    fun updateVenue(Name: String) {
        lifecycleScope.launch {
            viewModel.updateVenue(Name)
        }
    }

    fun updateInstructorName(Name: String) {
        lifecycleScope.launch {
            viewModel.updateInstructorName(Name)
        }
    }

    fun updateTopicOfTraining(Name: String) {
        lifecycleScope.launch {
            viewModel.updateTopicOfTraining(Name)
        }
    }

    fun updateProjectDetails(position: Int) {
        lifecycleScope.launch {
            viewModel.updateProject(projectsList[position].ProjectId, projectsList[position].ProjectNameEn, projectsList[position].ProjectCode)
        }
    }

    fun updateDate() {
        val date = "" + getDate() + "-" + getMonth() + "-" + selectedYear
        lifecycleScope.launch {
            viewModel.updateDate(date)
        }
    }

    fun updateTime() {
        val time = getDuration()
        lifecycleScope.launch {
            viewModel.updateTime(time)
        }
    }

    fun getMonth(): String {
        var month: String = ""
        if (selectedMonth < 9) {
            month = "0" + (selectedMonth + 1)
        }
        else {
            month = "" + (selectedMonth + 1)
        }
        return month
    }

    fun getDate(): String {
        var month: String = ""
        if (selectedDate < 9) {
            month = "0" + selectedDate
        }
        else {
            month = "" + selectedDate
        }
        return month
    }

    fun showDatePicker() {
        selectedYear = todayCalendar.get(Calendar.YEAR)
        selectedMonth = todayCalendar.get(Calendar.MONTH)
        selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            todayCalendar.set(Calendar.YEAR, year)
            todayCalendar.set(Calendar.MONTH, monthOfYear)
            todayCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            selectedYear = todayCalendar.get(Calendar.YEAR)
            selectedMonth = todayCalendar.get(Calendar.MONTH)
            selectedDate = todayCalendar.get(Calendar.DAY_OF_MONTH)

            val dateSelected = "" + getDate() + "-" + getMonth() + "-" + selectedYear
            date.text = dateSelected
            isDateSet = true
            updateDate()
        }, selectedYear, selectedMonth, selectedDate)

        val c = Calendar.getInstance()
        c.add(Calendar.DATE, -30) // show previous 7 days only

        dpd.datePicker.minDate = c.timeInMillis
        dpd.datePicker.maxDate = System.currentTimeMillis()
        dpd.show()
    }

    fun showCustomTimePicker() {
        val myTimeListener =
            TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                if (view.isShown) {
                    isDurationSet = true
                    selectedHour = hourOfDay
                    selectedMin = minute
                    time.text = getDuration()
                    updateTime()
                }
            }
        val timePickerDialog = CustomTimePickerDialog(
            this,
            myTimeListener,
            selectedHour,
            selectedMin,
            true
        )
        timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        timePickerDialog.show()
    }

    fun getDuration(): String {
        var duration: String = ""
        val hour: String
        val min: String
        var filteredHour: Int

        if (selectedHour < 10) {
            hour = "0" +selectedHour.toString()
        }
        else {
            hour = selectedHour.toString()
        }

        if (selectedMin < 10) {
            min = "0" + selectedMin.toString()
        }
        else  {
            min = selectedMin.toString()
        }
        duration = hour + ":" + min
        return duration
    }

    fun validationCheck() {
        lifecycleScope.launch {
            projectData = viewModel.getData()

            if (projectData[0].Venue.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_venue), getString(R.string.Ok),
                    this@TrainingAttendanceRecordActivity, null
                )
            }
            else if (projectData[0].InstructorName.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_instructor_name), getString(R.string.Ok),
                    this@TrainingAttendanceRecordActivity, null
                )
            }
            else if (projectData[0].Date.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_date), getString(R.string.Ok),
                    this@TrainingAttendanceRecordActivity, null
                )
            } else if (projectData[0].Time.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_select_time), getString(R.string.Ok),
                    this@TrainingAttendanceRecordActivity, null
                )
            }
            else if (projectData[0].TopicOfTraining.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_training_topic), getString(R.string.Ok),
                    this@TrainingAttendanceRecordActivity, null
                )
            }
            else if (employeeList.size == 0) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_employees_list), getString(R.string.Ok),
                    this@TrainingAttendanceRecordActivity, null
                )
            }
            else {
                insertStopWorkDataApi()
            }
        }
    }


    fun getFormattedDate(date: String): String {
        val calendar = Calendar.getInstance()
        calendar.time = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US).parse(date)
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US).format(calendar.time)
    }

    fun prepareInputJsonToInsertData(): String {
        val parentObj = JSONObject()
        parentObj.put("ProjectId", projectData[0].projectId)
        parentObj.put("ReportDate", getFormattedDate(projectData[0].Date + " " + projectData[0].Time))
        parentObj.put("SubmittedBy", UserPreferences(this).userId())
        parentObj.put("Venue", projectData[0].Venue)
        parentObj.put("InstructorName", projectData[0].InstructorName)
        parentObj.put("TopicName", projectData[0].TopicOfTraining)

        val employeeArray = JSONArray()
        for (i in 0 until employeeList.size) {
            val communicationObj = JSONObject()
            communicationObj.put("EmpName", employeeList[i].Name)
            communicationObj.put("Designation", employeeList[i].Designation)
            communicationObj.put("FileNo", employeeList[i].FileNumber)
            communicationObj.put("Department", employeeList[i].Department)
            employeeArray.put(communicationObj)
        }
        parentObj.put("lstTraningAttendanceDetail", employeeArray)

//        Log.d(TAG, "prepareInputJsonToGetMasterData: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun insertStopWorkDataApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.InsertTrainingAttendanceReport(
                    prepareInputJsonToInsertData()
                )
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()

                    lifecycleScope.launch {
                        viewModel.deleteEmployeeData()
                        viewModel.deleteData()
                    }

                    val intent = Intent(
                        this@TrainingAttendanceRecordActivity,
                        TrainingAttendanceRecordSummaryActivity::class.java
                    )
                    intent.putExtra(KEY_BACK_ACTION, "home_screen")
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@TrainingAttendanceRecordActivity,
                        intent
                    )
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@TrainingAttendanceRecordActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@TrainingAttendanceRecordActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@TrainingAttendanceRecordActivity,
                    null
                )
            }
        }
    }
}