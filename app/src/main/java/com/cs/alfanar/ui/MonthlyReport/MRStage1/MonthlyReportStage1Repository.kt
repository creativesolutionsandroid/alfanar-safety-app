package com.cs.alfanar.ui.MonthlyReport.MRStage1

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage1Entity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class MonthlyReportStage1Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    // database requests
    suspend fun saveData(projectData: MRStage1Entity) = db.getMRStage1Dao().insert(projectData)

    suspend fun updateSafeManHoursWorked(SafeManHoursWorked: Int) =
        db.getMRStage1Dao().updateSafeManHoursWorked(SafeManHoursWorked)

    suspend fun updateTotalManHours(TotalManHours: Int) =
        db.getMRStage1Dao().updateTotalManHours(TotalManHours)

    suspend fun updateDirectAlfanar(DirectAlfanar: Int) =
        db.getMRStage1Dao().updateDirectAlfanar(DirectAlfanar)

    suspend fun updateDirectSC(DirectSC: Int) =
        db.getMRStage1Dao().updateDirectSC(DirectSC)

    suspend fun updateInDirectAlfanar(InDirectAlfanar: Int) =
        db.getMRStage1Dao().updateInDirectAlfanar(InDirectAlfanar)

    suspend fun updateInDirectSC(InDirectSC: Int) =
        db.getMRStage1Dao().updateInDirectSC(InDirectSC)

    suspend fun updateFatal(Fatal: Int) =
        db.getMRStage1Dao().updateFatal(Fatal)

    suspend fun updateLWC(LWC: Int) =
        db.getMRStage1Dao().updateLWC(LWC)

    suspend fun getData() : List<MRStage1Entity> {
        return db.getMRStage1Dao().getData()
    }
}