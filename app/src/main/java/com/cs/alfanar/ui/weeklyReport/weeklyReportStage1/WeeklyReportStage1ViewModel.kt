package com.cs.alfanar.ui.weeklyReport.weeklyReportStage1

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage1Entity
import com.cs.alfanar.ui.weeklyReport.weeklyReportProjectSelection.WeeklyReportProjectSelectionRepository

class WeeklyReportStage1ViewModel (private val repository: WeeklyReportStage1Repository) : ViewModel() {

    // database requests
    suspend fun saveData(projectData: WRstage1Entity) = repository.saveData(projectData)

    suspend fun getData(): List<WRstage1Entity> = repository.getData()

    suspend fun getDataByType(reportType: Int): List<WRstage1Entity> = repository.getDataByType(reportType)

    suspend fun updateReportType(ReportType: Int) = repository.updateReportType(ReportType)

    suspend fun updateManpowerAtSite(ManpowerAtSite: Int, ReportType: Int) =
        repository.updateManpowerAtSite(ManpowerAtSite, ReportType)

    suspend fun updateManHour(ManHour: Int, ReportType: Int) = repository.updateManHour(ManHour, ReportType)

    suspend fun updateSafeManHours(SafeManHours: Int, ReportType: Int) =
        repository.updateSafeManHours(SafeManHours, ReportType)

    suspend fun updateCumulativeManHour(CumulativeManHour: Int, ReportType: Int) =
        repository.updateCumulativeManHour(CumulativeManHour, ReportType)

    suspend fun updateLostTimeInjuries(LostTimeInjuries: Int, ReportType: Int) =
        repository.updateLostTimeInjuries(LostTimeInjuries, ReportType)

    suspend fun updateDaysLost(DaysLost: Int, ReportType: Int) = repository.updateDaysLost(DaysLost, ReportType)

    suspend fun updateAccidents(Accidents: Int, ReportType: Int) = repository.updateAccidents(Accidents, ReportType)

    suspend fun updateAccidentInvestigation(AccidentInvestigation: Int, ReportType: Int) =
        repository.updateAccidentInvestigation(AccidentInvestigation, ReportType)

    suspend fun updateFirstAidCases(FirstAidCases: Int, ReportType: Int) =
        repository.updateFirstAidCases(FirstAidCases, ReportType)

    suspend fun updateMedicalTCase(FirstAidCases: Int, ReportType: Int) =
        repository.updateMedicalTCase(FirstAidCases, ReportType)

    suspend fun updateNearMiss(FirstAidCases: Int, ReportType: Int) =
        repository.updateNearMiss(FirstAidCases, ReportType)

    suspend fun updateSickLeave(SickLeave: Int, ReportType: Int) = repository.updateSickLeave(SickLeave, ReportType)

    suspend fun updateAlfanarSafety(AlfanarSafety: Int, ReportType: Int) =
        repository.updateAlfanarSafety(AlfanarSafety, ReportType)

    suspend fun updateAttendee(Attendee: Int, ReportType: Int) = repository.updateAttendee(Attendee, ReportType)

}