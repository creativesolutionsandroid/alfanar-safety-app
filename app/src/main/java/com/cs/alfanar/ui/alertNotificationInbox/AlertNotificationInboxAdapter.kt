package com.cs.alfanar.ui.alertNotificationInbox

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.databinding.ItemAlertNotificationInboxBinding
import com.cs.alfanar.model.alertNotificationInbox.Data
import com.cs.alfanar.network.ApiInterface
import com.cs.alfanar.utils.ALERT_NOTIFICATION_INBOX_IMAGE_URL
import com.cs.alfanar.utils.ALERT_TYPE_IMAGE_URL

class AlertNotificationInboxAdapter (private val clickListener:(Data)->Unit)
    : RecyclerView.Adapter<MyViewHolder>()
{
    private val subscribersList = ArrayList<Data>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : ItemAlertNotificationInboxBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_alert_notification_inbox,parent,false)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        Log.d("TAG", "getItemCount: "+subscribersList.size)
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position],clickListener, appContext)
    }

    fun setList(subscribers: List<Data>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemAlertNotificationInboxBinding): RecyclerView.ViewHolder(binding.root){

    fun bind(data: Data, clickListener:(Data)->Unit, context: Context){
        binding.userName.text = data.EmployeeName
        binding.message.text = data.AlertMessage
        binding.date.text = data.CreatedOn

        if (data.IsAcknowledged) {
            binding.btnAcknowledge.visibility = View.GONE
        }
        else {
            binding.btnAcknowledge.visibility = View.VISIBLE
        }

        if (data.Image.equals("")) {
            binding.uploadImage.visibility = View.GONE
        }
        else {
            binding.uploadImage.visibility = View.VISIBLE
            Glide.with(context).load(ApiInterface.BASE_URL
                     + ALERT_NOTIFICATION_INBOX_IMAGE_URL + data.Image).into(binding.uploadImage)
        }

        binding.btnAcknowledge.setOnClickListener{
            clickListener(data)
        }
    }
}