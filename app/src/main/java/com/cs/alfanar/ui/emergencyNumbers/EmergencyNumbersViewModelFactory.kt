package com.cs.alfanar.ui.emergencyNumbers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class EmergencyNumbersViewModelFactory (
    private val repository: EmergencyNumbersRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EmergencyNumbersViewModel(repository) as T
    }
}