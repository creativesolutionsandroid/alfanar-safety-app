package com.cs.alfanar.ui.welcomeScreen

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.MyApplication
import com.cs.alfanar.R
import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.ui.login.LoginActivity
import com.cs.alfanar.ui.signup.SignUpActivity
import com.cs.alfanar.utils.appVersion
import com.cs.alfanar.utils.appVersionCode
import kotlinx.android.synthetic.main.activity_splash_screen.alfanar_logo
import kotlinx.android.synthetic.main.activity_welcome_screen.*
import kotlinx.coroutines.launch


class WelcomeScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_screen)

        findViewById<TextView>(R.id.text_version).setText(
            String.format(resources.getString(R.string.str_version), appVersion(), appVersionCode()))

        UserPreferences(this).saveUserMode(1)
        UserPreferences(this).saveBaseUrl("http://cswebapps.com/alfanarapi/")
    }

    fun onUserModeChanged(view: View) {
        userMode.setOnClickListener {
            if (UserPreferences(this).userMode() == 1) {
                userMode.setImageDrawable(getDrawable(R.drawable.switch_off))
                label_user_mode.text = "Development"
                UserPreferences(this).saveUserMode(0)
                UserPreferences(this).saveBaseUrl("http://csadms.com/alfanarapi/")
            }
            else {
                userMode.setImageDrawable(getDrawable(R.drawable.switch_on))
                label_user_mode.text = "Test"
                UserPreferences(this).saveUserMode(1)
                UserPreferences(this).saveBaseUrl("http://cswebapps.com/alfanarapi/")
            }
        }
    }

    fun onLoginClicked(view : View) {
//        startActivity(Intent(this, LoginActivity::class.java))
        val intent = Intent(this, LoginActivity::class.java)
        val imageViewPair = androidx.core.util.Pair.create<View, String>(alfanar_logo, "transition_image")
        val button1ViewPair = androidx.core.util.Pair.create<View, String>(cardView_cancel, "transition_btn1")
        val button2ViewPair = androidx.core.util.Pair.create<View, String>(cardView_login, "transition_btn2")
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imageViewPair, button1ViewPair, button2ViewPair)
        startActivity(intent, options.toBundle())
    }

    fun onSignupClicked(view : View) {
//        startActivity(Intent(this, SignUpActivity::class.java))
        val intent = Intent(this, SignUpActivity::class.java)
        val button1ViewPair = androidx.core.util.Pair.create<View, String>(cardView_cancel, "transition_btn1")
        val button2ViewPair = androidx.core.util.Pair.create<View, String>(cardView_login, "transition_btn2")
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, button1ViewPair, button2ViewPair)
        startActivity(intent, options.toBundle())
    }

    override fun onBackPressed() {
//        super.onBackPressed()
        finish()
    }
}