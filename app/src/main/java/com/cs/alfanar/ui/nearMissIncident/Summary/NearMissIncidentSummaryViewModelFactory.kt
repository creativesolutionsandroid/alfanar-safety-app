package com.cs.alfanar.ui.nearMissIncident.Summary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class NearMissIncidentSummaryViewModelFactory (
    private val repository: NearMissIncidentSummaryRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NearMissIncidentSummaryViewModel(repository) as T
    }
}