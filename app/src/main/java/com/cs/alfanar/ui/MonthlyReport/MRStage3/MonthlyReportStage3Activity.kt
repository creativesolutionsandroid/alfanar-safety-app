package com.cs.alfanar.ui.MonthlyReport.MRStage3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage3Entity
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.ui.MonthlyReport.MRStage4.MonthlyReportStage4Activity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.afterTextChanged
import kotlinx.android.synthetic.main.activity_monthly_report_stage3.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MonthlyReportStage3Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val gslctory: MonthlyReportStage3ViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportStage3ViewModel

//    private val TAG : String = this::class.java.simpleName

    private lateinit var projectData: Project
    val KEY_DATA: String = "KEY_DATA"
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_stage3)

        viewModel = ViewModelProvider(this, gslctory).get(MonthlyReportStage3ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Project
        projectData = Data!!

        fetchDataFromDb()
        addTextWatcher()

        tbt_alfanar.text = ""+Data.ToolBoxTraining.size
        tbt_sub.text = ""+Data.ToolBoxTraining.size
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextClicked(view: View) {
        lifecycleScope.launch {
            viewModel.updateStage1(true)
        }
        val intent = Intent(this, MonthlyReportStage4Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                awards.setText(projectData[0].MonthlyEHSAward.toString())
                gslc.setText(projectData[0].GSLC.toString())
                violations_raised.setText(projectData[0].Violations.toString())
                fic_management.setText(projectData[0].Management.toString())
                fic_client.setText(projectData[0].Clients.toString())
                fic_alfanar.setText(projectData[0].FICAlfanar.toString())
                fic_sub.setText(projectData[0].FICSub.toString())
                safe_man_days.setText(projectData[0].SMD.toString())
                updateCumulativeHours()
            }
            else {
                val projectData = MRStage3Entity(1, 0, 0, 0
                    , 0, 0, 0, 0, 0)
                viewModel.saveData(projectData)
                updateCumulativeHours()
            }
            awards.requestFocus()
            awards.setSelection(awards.text.length)
            addFocusListener()
        }
    }

    fun updateCumulativeHours() {
        lifecycleScope.launch {
            val dbData = viewModel.getData()

//            val awards: Int = dbData[0].MonthlyEHSAward
            val awards: Int = 0
            val awardsCum: Int = projectData.MonthlyCummulative[0].MonthlyEHSAward
            awards_cumulative.text = ""+ (awards + awardsCum)

//            val gslc: Int = dbData[0].GSLC
            val gslc: Int = 0
            val gslcCum: Int = projectData.MonthlyCummulative[0].GenSickLeaveCase
            gslc_cumulative.text = ""+ (gslc + gslcCum)

//            val pdAlfanar: Int = dbData[0].Violations
            val pdAlfanar: Int = 0
            val pdAlfanarCum: Int = projectData.MonthlyCummulative[0].ViolationsRaisedClient
            violations_raised_cumulative.text = ""+ (pdAlfanar + pdAlfanarCum)

//            val pdSub: Int = dbData[0].Management
            val pdSub: Int = 0
            val pdSubCum: Int = projectData.MonthlyCummulative[0].SiteVisitMgAlfanar
            fic_management_cumulative.text = ""+ (pdSub + pdSubCum)

//            val nearMissAlfanar: Int = dbData[0].Clients
            val nearMissAlfanar: Int = 0
            val nearMissAlfanarCum: Int = projectData.MonthlyCummulative[0].SiteVisitMgSubCont
            fic_client_cumulative.text = ""+ (nearMissAlfanar + nearMissAlfanarCum)

//            val nearMissSub: Int = dbData[0].FICAlfanar
            val nearMissSub: Int = 0
            val nearMissSubCum: Int = projectData.MonthlyCummulative[0].FireIncidentCase
            fic_alfanar_cumulative.text = ""+ (nearMissSub + nearMissSubCum)

//            val tbt: Int = dbData[0].FICSub
            val tbt: Int = 0
            val tbtCum: Int = projectData.MonthlyCummulative[0].FireIncidentCaseSubCont
            fic_sub_cumulative.text = ""+ (tbt + tbtCum)

//            val campInspection: Int = dbData[0].SMD
            val campInspection: Int = 0
            val campInspectionCum: Int = projectData.MonthlyCummulative[0].SafeManDays
            safe_man_days_cumulative.text = ""+ (campInspection + campInspectionCum)
        }
    }

    fun addFocusListener() {
        awards.setOnFocusChangeListener { view, b ->
            if (awards.text.toString().length == 0) {
                awards.setText("0")
                awards.setSelection(awards.text.toString().length)
            }
        }
        gslc.setOnFocusChangeListener { view, b ->
            if (gslc.text.toString().length == 0) {
                gslc.setText("0")
                gslc.setSelection(gslc.text.toString().length)
            }
        }
        violations_raised.setOnFocusChangeListener { view, b ->
            if (violations_raised.text.toString().length == 0) {
                violations_raised.setText("0")
                violations_raised.setSelection(violations_raised.text.toString().length)
            }
        }
        fic_management.setOnFocusChangeListener { view, b ->
            if (fic_management.text.toString().length == 0) {
                fic_management.setText("0")
                fic_management.setSelection(fic_management.text.toString().length)
            }
        }
        fic_client.setOnFocusChangeListener { view, b ->
            if (fic_client.text.toString().length == 0) {
                fic_client.setText("0")
                fic_client.setSelection(fic_client.text.toString().length)
            }
        }
        fic_alfanar.setOnFocusChangeListener { view, b ->
            if (fic_alfanar.text.toString().length == 0) {
                fic_alfanar.setText("0")
                fic_alfanar.setSelection(fic_alfanar.text.toString().length)
            }
        }
        fic_sub.setOnFocusChangeListener { view, b ->
            if (fic_sub.text.toString().length == 0) {
                fic_sub.setText("0")
                fic_sub.setSelection(fic_sub.text.toString().length)
            }
        }
        safe_man_days.setOnFocusChangeListener { view, b ->
            if (safe_man_days.text.toString().length == 0) {
                safe_man_days.setText("0")
                safe_man_days.setSelection(safe_man_days.text.toString().length)
            }
        }
    }

    fun addTextWatcher() {
        awards.afterTextChanged {
            if (awards.text.toString().length > 0) {
                updateawards(awards.text.toString())
            }
            else {
                updateawards("0")
            }
        }
        gslc.afterTextChanged {
            if (gslc.text.toString().length > 0) {
                updategslc(gslc.text.toString())
            }
            else {
                updategslc("0")
            }
        }
        violations_raised.afterTextChanged {
            if (violations_raised.text.toString().length > 0) {
                updatePdAlfanar(violations_raised.text.toString())
            }
            else {
                updatePdAlfanar("0")
            }
        }
        fic_management.afterTextChanged {
            if (fic_management.text.toString().length > 0) {
                updatePdSC(fic_management.text.toString())
            }
            else {
                updatePdSC("0")
            }
        }
        fic_client.afterTextChanged {
            if (fic_client.text.toString().length > 0) {
                updateNmAlfanar(fic_client.text.toString())
            }
            else {
                updateNmAlfanar("0")
            }
        }
        fic_alfanar.afterTextChanged {
            if (fic_alfanar.text.toString().length > 0) {
                updateNmSC(fic_alfanar.text.toString())
            }
            else {
                updateNmSC("0")
            }
        }
        fic_sub.afterTextChanged {
            if (fic_sub.text.toString().length > 0) {
                updatetbt(fic_sub.text.toString())
            }
            else {
                updatetbt("0")
            }
        }
        safe_man_days.afterTextChanged {
            if (safe_man_days.text.toString().length > 0) {
                updateCampInspection(safe_man_days.text.toString())
            }
            else {
                updateCampInspection("0")
            }
        }
    }

    fun updateawards(awards: String) {
        lifecycleScope.launch {
            viewModel.updateMonthlyEHSAward(awards.toInt())
        }
        updateCumulativeHours()
    }

    fun updategslc(gslc: String) {
        lifecycleScope.launch {
            viewModel.updateGSLC(gslc.toInt())
        }
        updateCumulativeHours()
    }

    fun updatePdAlfanar(PdAlfanar: String) {
        lifecycleScope.launch {
            viewModel.updateViolations(PdAlfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updatePdSC(PdSC: String) {
        lifecycleScope.launch {
            viewModel.updateManagement(PdSC.toInt())
        }
        updateCumulativeHours()
    }

    fun updateNmAlfanar(NmAlfanar: String) {
        lifecycleScope.launch {
            viewModel.updateClients(NmAlfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updateNmSC(NmSC: String) {
        lifecycleScope.launch {
            viewModel.updateFICAlfanar(NmSC.toInt())
        }
        updateCumulativeHours()
    }

    fun updatetbt(tbt: String) {
        lifecycleScope.launch {
            viewModel.updateFICSub(tbt.toInt())
        }
        updateCumulativeHours()
    }

    fun updateCampInspection(CampInspection: String) {
        lifecycleScope.launch {
            viewModel.updateSMD(CampInspection.toInt())
        }
        updateCumulativeHours()
    }
}