package com.cs.alfanar.ui.MonthlyReport.MRStage2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage2Entity
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.ui.MonthlyReport.MRStage3.MonthlyReportStage3Activity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.afterTextChanged
import kotlinx.android.synthetic.main.activity_monthly_report_stage1.*
import kotlinx.android.synthetic.main.activity_monthly_report_stage2.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MonthlyReportStage2Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: MonthlyReportStage2ViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportStage2ViewModel

//    private val TAG : String = this::class.java.simpleName

    private lateinit var projectData: Project
    val KEY_DATA: String = "KEY_DATA"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_stage2)

        viewModel = ViewModelProvider(this, factory).get(MonthlyReportStage2ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Project
        projectData = Data!!

        fetchDataFromDb()
        addTextWatcher()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextClicked(view: View) {
        val intent = Intent(this, MonthlyReportStage3Activity::class.java)
        intent.putExtra(KEY_DATA, projectData)
        startActivity(intent)
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                mtc.setText(projectData[0].MTC.toString())
                fac.setText(projectData[0].FAC.toString())
                property_damage_alfanar.setText(projectData[0].PdAlfanar.toString())
                property_damage_sub.setText(projectData[0].PdSC.toString())
                near_miss_alfanar.setText(projectData[0].NmAlfanar.toString())
                near_miss_sub.setText(projectData[0].NmSC.toString())
                mva.setText(projectData[0].MVA.toString())
                camp_inspection.setText(projectData[0].CampInspection.toString())
                eic.setText(projectData[0].EIC.toString())
                emergency_drill_conducte.setText(projectData[0].EmergencyDrillConducte.toString())
            }
            else {
                val projectData = MRStage2Entity(1, 0, 0, 0
                    , 0, 0, 0, 0, 0,0,0)
                viewModel.saveData(projectData)
            }
            mtc.requestFocus()
            mtc.setSelection(mtc.text.length)
            updateCumulativeHours()
            addFocusListener()
        }
    }

    fun updateCumulativeHours() {
        lifecycleScope.launch {
            val dbData = viewModel.getData()

//            val mtc: Int = dbData[0].MTC
            val mtc: Int = 0
            val mtcCum: Int = projectData.MonthlyCummulative[0].MedicalTCase
            mtc_cumulative.text = ""+ (mtc + mtcCum)

//            val fac: Int = dbData[0].FAC
            val fac: Int = 0
            val facCum: Int = projectData.MonthlyCummulative[0].CummNoFirstAid
            fac_cumulative.text = ""+ (fac + facCum)

//            val pdAlfanar: Int = dbData[0].PdAlfanar
            val pdAlfanar: Int = 0
            val pdAlfanarCum: Int = projectData.MonthlyCummulative[0].PropertyDamageAlfanar
            property_damage_alfanar_cumulative.text = ""+ (pdAlfanar + pdAlfanarCum)

//            val pdSub: Int = dbData[0].PdSC
            val pdSub: Int = 0
            val pdSubCum: Int = projectData.MonthlyCummulative[0].PropertyDamageSubCont
            property_damage_sub_cumulative.text = ""+ (pdSub + pdSubCum)

//            val nearMissAlfanar: Int = dbData[0].NmAlfanar
            val nearMissAlfanar: Int = 0
            val nearMissAlfanarCum: Int = projectData.MonthlyCummulative[0].NearMissAlfanar
            near_miss_alfanar_cumulative.text = ""+ (nearMissAlfanar + nearMissAlfanarCum)

//            val nearMissSub: Int = dbData[0].NmSC
            val nearMissSub: Int = 0
            val nearMissSubCum: Int = projectData.MonthlyCummulative[0].NearMissSubCont
            near_miss_sub_cumulative.text = ""+ (nearMissSub + nearMissSubCum)

//            val mva: Int = dbData[0].MVA
            val mva: Int = 0
            val mvaCum: Int = projectData.MonthlyCummulative[0].MotorVehicleAcc
            mva_cumulative.text = ""+ (mva + mvaCum)

//            val campInspection: Int = dbData[0].CampInspection
            val campInspection: Int = 0
            val campInspectionCum: Int = projectData.MonthlyCummulative[0].NoCampInspection
            camp_inspection_cumulative.text = ""+ (campInspection + campInspectionCum)

//            val eic: Int = dbData[0].EIC
            val eic: Int = 0
            val eicCum: Int = projectData.MonthlyCummulative[0].EnvrnIncidentCase
            eic_cumulative.text = ""+ (eic + eicCum)

//            val emergencyDrill: Int = dbData[0].EmergencyDrillConducte
            val emergencyDrill: Int = 0
            val emergencyDrillCum: Int = projectData.MonthlyCummulative[0].EmergencyDrill
            emergency_drill_conducte_cumulative.text = ""+ (emergencyDrill + emergencyDrillCum)
        }
    }

    fun addFocusListener() {
        mtc.setOnFocusChangeListener { view, b ->
            if (mtc.text.toString().length == 0) {
                mtc.setText("0")
                mtc.setSelection(mtc.text.toString().length)
            }
        }
//        fac.setOnFocusChangeListener { view, b ->
//            if (fac.text.toString().length == 0) {
//                fac.setText("0")
//                fac.setSelection(fac.text.toString().length)
//            }
//        }
        property_damage_alfanar.setOnFocusChangeListener { view, b ->
            if (property_damage_alfanar.text.toString().length == 0) {
                property_damage_alfanar.setText("0")
                property_damage_alfanar.setSelection(property_damage_alfanar.text.toString().length)
            }
        }
        property_damage_sub.setOnFocusChangeListener { view, b ->
            if (property_damage_sub.text.toString().length == 0) {
                property_damage_sub.setText("0")
                property_damage_sub.setSelection(property_damage_sub.text.toString().length)
            }
        }
        near_miss_alfanar.setOnFocusChangeListener { view, b ->
            if (near_miss_alfanar.text.toString().length == 0) {
                near_miss_alfanar.setText("0")
                near_miss_alfanar.setSelection(near_miss_alfanar.text.toString().length)
            }
        }
        near_miss_sub.setOnFocusChangeListener { view, b ->
            if (near_miss_sub.text.toString().length == 0) {
                near_miss_sub.setText("0")
                near_miss_sub.setSelection(near_miss_sub.text.toString().length)
            }
        }
        mva.setOnFocusChangeListener { view, b ->
            if (mva.text.toString().length == 0) {
                mva.setText("0")
                mva.setSelection(mva.text.toString().length)
            }
        }
        camp_inspection.setOnFocusChangeListener { view, b ->
            if (camp_inspection.text.toString().length == 0) {
                camp_inspection.setText("0")
                camp_inspection.setSelection(camp_inspection.text.toString().length)
            }
        }
        eic.setOnFocusChangeListener { view, b ->
            if (eic.text.toString().length == 0) {
                eic.setText("0")
                eic.setSelection(eic.text.toString().length)
            }
        }
        emergency_drill_conducte.setOnFocusChangeListener { view, b ->
            if (emergency_drill_conducte.text.toString().length == 0) {
                emergency_drill_conducte.setText("0")
                emergency_drill_conducte.setSelection(emergency_drill_conducte.text.toString().length)
            }
        }
    }

    fun addTextWatcher() {
        mtc.afterTextChanged {
            if (mtc.text.toString().length > 0) {
                updateMTC(mtc.text.toString())
            }
            else {
                updateMTC("0")
            }
        }
//        fac.afterTextChanged {
//            if (fac.text.toString().length > 0) {
//                updateFAC(fac.text.toString())
//            }
//            else {
//                updateFAC("0")
//            }
//        }
        property_damage_alfanar.afterTextChanged {
            if (property_damage_alfanar.text.toString().length > 0) {
                updatePdAlfanar(property_damage_alfanar.text.toString())
            }
            else {
                updatePdAlfanar("0")
            }
        }
        property_damage_sub.afterTextChanged {
            if (property_damage_sub.text.toString().length > 0) {
                updatePdSC(property_damage_sub.text.toString())
            }
            else {
                updatePdSC("0")
            }
        }
        near_miss_alfanar.afterTextChanged {
            if (near_miss_alfanar.text.toString().length > 0) {
                updateNmAlfanar(near_miss_alfanar.text.toString())
            }
            else {
                updateNmAlfanar("0")
            }
        }
        near_miss_sub.afterTextChanged {
            if (near_miss_sub.text.toString().length > 0) {
                updateNmSC(near_miss_sub.text.toString())
            }
            else {
                updateNmSC("0")
            }
        }
        mva.afterTextChanged {
            if (mva.text.toString().length > 0) {
                updateMVA(mva.text.toString())
            }
            else {
                updateMVA("0")
            }
        }
        camp_inspection.afterTextChanged {
            if (camp_inspection.text.toString().length > 0) {
                updateCampInspection(camp_inspection.text.toString())
            }
            else {
                updateCampInspection("0")
            }
        }
        eic.afterTextChanged {
            if (eic.text.toString().length > 0) {
                updateEic(eic.text.toString())
            }
            else {
                updateEic("0")
            }
        }
        emergency_drill_conducte.afterTextChanged {
            if (emergency_drill_conducte.text.toString().length > 0) {
                updateEmergencyDrill(emergency_drill_conducte.text.toString())
            }
            else {
                updateEmergencyDrill("0")
            }
        }
    }

    fun updateMTC(MTC: String) {
        lifecycleScope.launch {
            viewModel.updateMTC(MTC.toInt())
        }
        updateCumulativeHours()
    }

    fun updateFAC(FAC: String) {
        lifecycleScope.launch {
            viewModel.updateFAC(FAC.toInt())
        }
        updateCumulativeHours()
    }

    fun updatePdAlfanar(PdAlfanar: String) {
        lifecycleScope.launch {
            viewModel.updatePdAlfanar(PdAlfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updatePdSC(PdSC: String) {
        lifecycleScope.launch {
            viewModel.updatePdSC(PdSC.toInt())
        }
        updateCumulativeHours()
    }

    fun updateNmAlfanar(NmAlfanar: String) {
        lifecycleScope.launch {
            viewModel.updateNmAlfanar(NmAlfanar.toInt())
        }
        updateCumulativeHours()
    }

    fun updateNmSC(NmSC: String) {
        lifecycleScope.launch {
            viewModel.updateNmSC(NmSC.toInt())
        }
        updateCumulativeHours()
    }

    fun updateMVA(MVA: String) {
        lifecycleScope.launch {
            viewModel.updateMVA(MVA.toInt())
        }
        updateCumulativeHours()
    }

    fun updateCampInspection(CampInspection: String) {
        lifecycleScope.launch {
            viewModel.updateCampInspection(CampInspection.toInt())
        }
        updateCumulativeHours()
    }

    fun updateEic(Eic: String) {
        lifecycleScope.launch {
            viewModel.updateEIC(Eic.toInt())
        }
        updateCumulativeHours()
    }

    fun updateEmergencyDrill(EmergencyDrill: String) {
        lifecycleScope.launch {
            viewModel.updateEmergencyDrillConducte(EmergencyDrill.toInt())
        }
        updateCumulativeHours()
    }
}