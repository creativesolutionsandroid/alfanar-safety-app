package com.cs.alfanar.ui.MonthlyReport.MRStage2

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage2Entity

class MonthlyReportStage2ViewModel (private val repository: MonthlyReportStage2Repository) : ViewModel() {

    // database requests
    suspend fun saveData(projectData: MRStage2Entity) = repository.saveData(projectData)

    suspend fun updateMTC(MTC: Int) =
        repository.updateMTC(MTC)

    suspend fun updateFAC(FAC: Int) =
        repository.updateFAC(FAC)

    suspend fun updatePdAlfanar(PdAlfanar: Int) =
        repository.updatePdAlfanar(PdAlfanar)

    suspend fun updatePdSC(PdSC: Int) =
        repository.updatePdSC(PdSC)

    suspend fun updateNmAlfanar(NmAlfanar: Int) =
        repository.updateNmAlfanar(NmAlfanar)

    suspend fun updateNmSC(NmSC: Int) =
        repository.updateNmSC(NmSC)

    suspend fun updateMVA(MVA: Int) =
        repository.updateMVA(MVA)

    suspend fun updateCampInspection(CampInspection: Int) =
        repository.updateCampInspection(CampInspection)

    suspend fun updateEIC(EIC: Int) =
        repository.updateEIC(EIC)

    suspend fun updateEmergencyDrillConducte(EmergencyDrillConducte: Int) =
        repository.updateEmergencyDrillConducte(EmergencyDrillConducte)

    suspend fun getData() : List<MRStage2Entity> {
        return repository.getData()
    }
}