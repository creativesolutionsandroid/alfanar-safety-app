package com.cs.alfanar.ui.weeklyReport.weeklyReportStage4

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4ChecklistEntity
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4OtherHealthEntity
import com.cs.alfanar.databinding.ItemWeeklyReportStage3EdittextBinding
import com.cs.alfanar.utils.afterTextChanged

class OtherHealthAdapter(private val onCountChanged:(String, Int)->Unit)
    : RecyclerView.Adapter<OtherHealthViewHolder>() {
    private val subscribersList = ArrayList<WRstage4OtherHealthEntity>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OtherHealthViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemWeeklyReportStage3EdittextBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_weekly_report_stage3_edittext,
                parent,
                false
            )
        return OtherHealthViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: OtherHealthViewHolder, position: Int) {
        holder.bind(subscribersList[position], onCountChanged, appContext)
    }

    fun setList(subscribers: List<WRstage4OtherHealthEntity>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class OtherHealthViewHolder(val binding: ItemWeeklyReportStage3EdittextBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: WRstage4OtherHealthEntity, onCountChanged:(String, Int)->Unit, context: Context) {
        Log.d("WeeklyReportStag", "bind: "+data.Name)
        binding.edittext.setText(data.Name)
        binding.edittext.afterTextChanged { onCountChanged(binding.edittext.text.toString(), adapterPosition) }

    }
}