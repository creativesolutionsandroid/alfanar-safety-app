package com.cs.alfanar.ui.trainingAttendanceRecord.SummaryDetails

import androidx.lifecycle.ViewModel
import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.trainingAttendanceSummary.TrainingAttendanceSummaryResponse
import com.cs.alfanar.network.ApiInterface
import com.cs.alfanar.ui.stopWorkAdvice.SummaryDetails.StopWorkAdviceDetailsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class TrainingAttendanceDetailsViewModel (private val repository: TrainingAttendanceDetailsRepository) : ViewModel() {

    suspend fun getAccidentIncidentSummary(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getAccidentIncidentSummary(inputJson)
    }

    suspend fun UpdateReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.UpdateReportStatus(inputJson)
    }

}