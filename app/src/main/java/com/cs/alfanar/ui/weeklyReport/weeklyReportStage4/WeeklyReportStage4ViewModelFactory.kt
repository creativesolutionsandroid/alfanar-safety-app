package com.cs.alfanar.ui.weeklyReport.weeklyReportStage4

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WeeklyReportStage4ViewModelFactory (
    private val repository: WeeklyReportStage4Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeeklyReportStage4ViewModel(repository) as T
    }
}