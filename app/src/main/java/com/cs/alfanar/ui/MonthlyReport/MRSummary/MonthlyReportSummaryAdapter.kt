package com.cs.alfanar.ui.MonthlyReport.MRSummary

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ItemWeeklySummaryReportBinding
import com.cs.alfanar.model.MonthlyReportSummary.Data
import com.cs.alfanar.utils.DAILY_REPORT_STATUS_OPEN

class MonthlyReportSummaryAdapter(
    private val clickListener: (Data, Int, ActivityOptionsCompat) -> Unit,
    private val statusListener: (Data, Int) -> Unit
)
    : RecyclerView.Adapter<MyViewHolder>()
{
    private val subscribersList = ArrayList<Data>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding : ItemWeeklySummaryReportBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_weekly_summary_report,
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position], clickListener, statusListener, appContext)
    }

    fun setList(subscribers: List<Data>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemWeeklySummaryReportBinding): RecyclerView.ViewHolder(binding.root){

    fun bind(
        data: Data,
        clickListener: (Data, Int, ActivityOptionsCompat) -> Unit,
        statusListener: (Data, Int) -> Unit,
        context: Context
    ){

        binding.itemNumber.text = data.ReportId
        binding.reportDate.text = data.ReportDate
        binding.location.text = data.ProjectCode
        binding.status.text = data.StatusName

        if (data.ReportStatus == DAILY_REPORT_STATUS_OPEN) { // open
            binding.switchStatus.setImageDrawable(context.resources.getDrawable(R.drawable.switch_on))
        }
        else { // close
            binding.switchStatus.setImageDrawable(context.resources.getDrawable(R.drawable.switch_off))
        }

        binding.switchStatus.setOnClickListener{
            if (data.ReportStatus == DAILY_REPORT_STATUS_OPEN)
                statusListener(data, adapterPosition)
        }

        binding.itemLayout.setOnClickListener{
            val imageViewPair = androidx.core.util.Pair.create<View, String>(
                binding.titleItemNumber,
                "transition_tilte_item_number"
            )
            val button1ViewPair = androidx.core.util.Pair.create<View, String>(
                binding.itemNumber,
                "transition_item_number"
            )
            val button2ViewPair = androidx.core.util.Pair.create<View, String>(
                binding.itemLayout,
                "transition_layout"
            )
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                (context as Activity)!!,
                imageViewPair,
                button1ViewPair,
                button2ViewPair
            )
            clickListener(data, adapterPosition, options)
        }
    }

}