package com.cs.alfanar.ui.safetySummaryReport

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.MonthlyReport.MRSummaryDetails.MonthlyReportSummaryDetailsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SafetySummaryReportViewModel (private val repository: SafetySummaryReportRepository) : ViewModel() {

    suspend fun getSafetySummaryReponse(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getSafetySummaryReponse(inputJson)
    }

}