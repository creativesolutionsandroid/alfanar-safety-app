package com.cs.alfanar.ui.documentCenterItem

import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView.SmoothScroller
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.documentCenterItemData.Data
import com.cs.alfanar.network.ApiInterface
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.*
import com.master.exoplayer.MasterExoPlayerHelper
import kotlinx.android.synthetic.main.activity_document_center_item.*
import kotlinx.android.synthetic.main.activity_document_center_list.list_document_center
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class DocumentCenterItemActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: DocumentCenterItemViewModelFactory by instance()

    private lateinit var viewModel: DocumentCenterItemViewModel
    private lateinit var adapter: DocumentCenterItemAdapter
    private lateinit var videoAdapter: DocumentCenterVideoAdapter

//    private val TAG : String = this::class.java.simpleName
    private var documentTypeID: Int? = 0
    private val DATA_KEY: String = "DATA_KEY"
    private var linearLayoutManager: LinearLayoutManager? = null

    var thumbNails : MutableList<Bitmap?> = ArrayList()
    var videosList : MutableList<Data> = ArrayList()
    var pdfList : MutableList<Data> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document_center_item)

        viewModel = ViewModelProvider(this, factory).get(DocumentCenterItemViewModel::class.java)

        val listData = intent.getParcelableExtra(DATA_KEY)  as?  com.cs.alfanar.model.documentCenterListData.Data
        title_document_type.text = listData?.TypeName
        documentTypeID = listData?.Id


        initRecyclerView()
        getDocumentListApi()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun prepareInputJsonForDocumentList(): String {
        val parentObj = JSONObject()
        parentObj.put("TypeId", documentTypeID)

//        Log.d(TAG, "prepareInputJsonForDocumentList: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getDocumentListApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getDocumentsByTypeId(prepareInputJsonForDocumentList())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    if (documentTypeID != 3) {
                        pdfList.addAll(response.Data)
                        adapter.setList(pdfList, this@DocumentCenterItemActivity)
                        adapter.notifyDataSetChanged()
                        list_document_center.scheduleLayoutAnimation()
                    }
                    else {
                        videosList.addAll(response.Data)

                        // creating empty bitmaps to prepare thumbnails
                        val conf = Bitmap.Config.ARGB_8888 // see other conf types
                        val bitmap = Bitmap.createBitmap(100,100, conf) // this creates a MUTABLE bitmap
                        for (i in 0 until videosList.size) {
                            thumbNails.add(bitmap)
                        }

                        videoAdapter.setList(videosList, thumbNails, this@DocumentCenterItemActivity)
                        videoAdapter.notifyDataSetChanged()
//                        generateThumnails().execute()
                    }

                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@DocumentCenterItemActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@DocumentCenterItemActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@DocumentCenterItemActivity,
                    null
                )
            }
        }
    }

    private fun initRecyclerView(){
        linearLayoutManager = LinearLayoutManager(this)
        list_document_center.layoutManager = linearLayoutManager
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_document_center.addItemDecoration(itemDecoration)

        if (documentTypeID != 3) {
            adapter =
                DocumentCenterItemAdapter({ selectedItem: Data, position: Int -> listItemClicked(selectedItem, position) })
            list_document_center.adapter = adapter
        }
        else {
            val masterExoPlayerHelper = MasterExoPlayerHelper(mContext = this, id = R.id.videoView, loop = 1, autoPlay = false,
            useController = true, playStrategy = 1.0f, muteStrategy = 0, defaultMute = false, thumbHideDelay = 1000)
            masterExoPlayerHelper.makeLifeCycleAware(this)
            masterExoPlayerHelper.attachToRecyclerView(list_document_center)

            videoAdapter =
                DocumentCenterVideoAdapter({ selectedItem: Data, position: Int -> listItemClicked(selectedItem, position) })
            list_document_center.adapter = videoAdapter
        }
    }

    private fun listItemClicked(data: Data, position: Int){
        if (documentTypeID != 3) {
            intent = Intent(this, DocumentPdfViewerActivity::class.java)
            intent.putExtra(DATA_KEY, data)
            startActivity(intent)
        }
        else {
            val smoothScroller: SmoothScroller = object : LinearSmoothScroller(this) {
                override fun getVerticalSnapPreference(): Int {
                    return SNAP_TO_START
                }
            }
            smoothScroller.setTargetPosition(position);
            linearLayoutManager?.startSmoothScroll(smoothScroller);

//            list_document_center.smoothScrollToPosition(position)
//            Log.d(TAG, "findFirstVisibleItemPosition: "+linearLayoutManager?.findFirstVisibleItemPosition())
//            if (linearLayoutManager?.findFirstVisibleItemPosition() != position) {
////                linearLayoutManager?.scrollToPositionWithOffset(position, 2)
//                list_document_center.smoothScrollToPosition(position+1)
//
//            }
        }
    }

    @Throws(Throwable::class)
    fun retriveVideoFrameFromVideo(videoPath: String?): Bitmap? {
        var bitmap: Bitmap? = null
        var mediaMetadataRetriever: MediaMetadataRetriever? = null
        try {
            mediaMetadataRetriever = MediaMetadataRetriever()
            if (Build.VERSION.SDK_INT >= 14) mediaMetadataRetriever.setDataSource(
                videoPath,
                HashMap()
            ) else mediaMetadataRetriever.setDataSource(videoPath)
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.frameAtTime
        } catch (e: Exception) {
            e.printStackTrace()
            throw Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.message)
        } finally {
            mediaMetadataRetriever?.release()
        }
        return bitmap
    }

   inner class generateThumnails() : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg params: Void?): String? {
            for (i in 0 until videosList.size) {
                thumbNails.set(i, retriveVideoFrameFromVideo(
                    ApiInterface.BASE_URL
                        +DOCUMENT_URL + videosList.get(i).DocumentCopy))
                videoAdapter.setList(videosList, thumbNails, this@DocumentCenterItemActivity)
                runOnUiThread { videoAdapter.notifyItemChanged(i) }
            }

            return null
        }
    }
}