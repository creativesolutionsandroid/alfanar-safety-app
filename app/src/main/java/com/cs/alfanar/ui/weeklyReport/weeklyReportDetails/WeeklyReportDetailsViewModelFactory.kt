package com.cs.alfanar.ui.weeklyReport.weeklyReportDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WeeklyReportDetailsViewModelFactory (
    private val repository: WeeklyReportDetailsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeeklyReportDetailsViewModel(repository) as T
    }
}