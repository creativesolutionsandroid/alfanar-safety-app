package com.cs.alfanar.ui.MonthlyReport.MRStage12

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage9Entity
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.ui.MonthlyReport.MRStage10.MonthlyReportStage10Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage13.MonthlyReportStage13Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage9.MonthlyReportStage9ViewModel
import com.cs.alfanar.ui.MonthlyReport.MRStage9.MonthlyReportStage9ViewModelFactory
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.afterTextChanged
import kotlinx.android.synthetic.main.activity_monthly_report_stage9.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MonthlyReportStage12Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val viewModelFactory: MonthlyReportStage12ViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportStage12ViewModel

//    private val TAG : String = this::class.java.simpleName

    private lateinit var projectData: Project
    val KEY_DATA: String = "KEY_DATA"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_stage12)

        viewModel = ViewModelProvider(this, viewModelFactory).get(MonthlyReportStage12ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Project
        projectData = Data!!

        fetchDataFromDb()
        addTextWatcher()

        remarks_1.setImeOptions(EditorInfo.IME_ACTION_DONE)
        remarks_2.setImeOptions(EditorInfo.IME_ACTION_DONE)
        remarks_3.setImeOptions(EditorInfo.IME_ACTION_DONE)
        remarks_1.setRawInputType(InputType.TYPE_CLASS_TEXT)
        remarks_2.setRawInputType(InputType.TYPE_CLASS_TEXT)
        remarks_3.setRawInputType(InputType.TYPE_CLASS_TEXT)
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextClicked(view: View) {
        lifecycleScope.launch {
            val data = viewModel.getData()
//            if (data[9].Remarks.equals("")) { /*Commented on 06-01-2020 after client request*/
//                remarks_1.error = getString(R.string.validation_error_empty_field)
//            }
//            else
                if (data[10].UnitOne.equals("")) {
                tanker_trip_2.error = getString(R.string.validation_error_empty_field)
            }
            else if (data[10].UnitTwo.equals("")) {
                capacity_2.error = getString(R.string.validation_error_empty_field)
            }
//            else if (data[10].Remarks.equals("")) { /*Commented on 06-01-2020 after client request*/
//                remarks_2.error = getString(R.string.validation_error_empty_field)
//            }
            else if (data[11].UnitOne.equals("")) {
                tanker_trip_3.error = getString(R.string.validation_error_empty_field)
            }
            else if (data[11].UnitTwo.equals("")) {
                capacity_3.error = getString(R.string.validation_error_empty_field)
            }
//            else if (data[11].Remarks.equals("")) { /*Commented on 06-01-2020 after client request*/
//                remarks_3.error = getString(R.string.validation_error_empty_field)
//            }
            else {
                val intent = Intent(this@MonthlyReportStage12Activity, MonthlyReportStage13Activity::class.java)
                intent.putExtra(KEY_DATA, projectData)
                startActivity(intent)
            }
        }
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 9) {
                remarks_1.setText(projectData[9].Remarks)
                tanker_trip_2.setText(projectData[10].UnitOne)
                capacity_2.setText(projectData[10].UnitTwo)
                remarks_2.setText(projectData[10].Remarks)
                tanker_trip_3.setText(projectData[11].UnitOne)
                capacity_3.setText(projectData[11].UnitTwo)
                remarks_3.setText(projectData[11].Remarks)
            }
            else {
                val reportOne = MRStage9Entity(10, "", "", "","","")
                viewModel.saveData(reportOne)
                val reportTwo = MRStage9Entity(11, "", "", "","","")
                viewModel.saveData(reportTwo)
                val reportThree = MRStage9Entity(12, "", "", "","","")
                viewModel.saveData(reportThree)
            }
            remarks_1.requestFocus()
            remarks_1.setSelection(remarks_1.text.length)
        }
    }

    fun addTextWatcher() {
        remarks_1.afterTextChanged {
            if (remarks_1.text.toString().length > 0) {
                updateremarks_1(remarks_1.text.toString())
            }
            else {
                updateremarks_1("")
            }
        }
        tanker_trip_2.afterTextChanged {
            if (tanker_trip_2.text.toString().length > 0) {
                updatetanker_trip_2(tanker_trip_2.text.toString())
            }
            else {
                updatetanker_trip_2("")
            }
        }
        capacity_2.afterTextChanged {
            if (capacity_2.text.toString().length > 0) {
                update_capacity_2(capacity_2.text.toString())
            }
            else {
                update_capacity_2("")
            }
        }
        remarks_2.afterTextChanged {
            if (remarks_2.text.toString().length > 0) {
                update_remarks_2(remarks_2.text.toString())
            }
            else {
                update_remarks_2("")
            }
        }
        tanker_trip_3.afterTextChanged {
            if (tanker_trip_3.text.toString().length > 0) {
                update_tanker_trip_3(tanker_trip_3.text.toString())
            }
            else {
                update_tanker_trip_3("")
            }
        }
        capacity_3.afterTextChanged {
            if (capacity_3.text.toString().length > 0) {
                update_capacity_3(capacity_3.text.toString())
            }
            else {
                update_capacity_3("")
            }
        }
        remarks_3.afterTextChanged {
            if (remarks_3.text.toString().length > 0) {
                update_remarks_3(remarks_3.text.toString())
            }
            else {
                update_remarks_3("")
            }
        }
    }

    fun updateremarks_1(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateRemarks(tanker_trip_1, 10)
        }
    }
    fun updatetanker_trip_2(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateUnitOne(tanker_trip_1, 11)
        }
    }
    fun update_capacity_2(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateUnitTwo(tanker_trip_1, 11)
        }
    }
    fun update_remarks_2(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateRemarks(tanker_trip_1, 11)
        }
    }
    fun update_tanker_trip_3(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateUnitOne(tanker_trip_1, 12)
        }
    }
    fun update_capacity_3(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateUnitTwo(tanker_trip_1, 12)
        }
    }
    fun update_remarks_3(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateRemarks(tanker_trip_1, 12)
        }
    }
}