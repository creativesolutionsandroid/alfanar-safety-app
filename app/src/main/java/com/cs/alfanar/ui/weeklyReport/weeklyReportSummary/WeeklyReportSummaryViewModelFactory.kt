package com.cs.alfanar.ui.weeklyReport.weeklyReportSummary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WeeklyReportSummaryViewModelFactory (
    private val repository: WeeklyReportSummaryRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeeklyReportSummaryViewModel(repository) as T
    }
}