package com.cs.alfanar.ui.nearestHospital

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.model.nearestHospital.Result
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.nearestHospitalDirections.NearestHospitalDirectionsActivity
import com.cs.alfanar.utils.*
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.activity_nearest_hospital.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class NearestHospitalActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: NearestHospitalViewModelFactory by instance()

    private lateinit var viewModel: NearestHospitalViewModel
    private lateinit var adapter: NearestHospitalAdapter

//    private val TAG : String = this::class.java.simpleName

    private val LOCATION_REQUEST: Int = 1
    private val REQUEST_CHECK_SETTINGS: Int = 2
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private lateinit var currentLocation: Location
    private val KEY_CURRENT_LOCATION: String = "KEY_CURRENT_LOCATION"
    private val KEY_HOSPITAL_LOCATION: String = "KEY_HOSPITAL_LOCATION"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nearest_hospital)

        viewModel = ViewModelProvider(this, factory).get(NearestHospitalViewModel::class.java)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        initRecyclerView()
        checkLocationPermission()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    private fun checkLocationPermission() {
        if (!canGetLocation()) {
            requestPermission()
        }
        else {
            getLastKnownLocation()
//            displayLocationSettingsRequest(this)
        }

    }

    @SuppressLint("MissingPermission")
    fun getLastKnownLocation() {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location->
                if (location != null) {
                    currentLocation = location
                    if (currentLocation.latitude.compareTo(0) == 0 && currentLocation.longitude.compareTo(0) == 0) {
                        list_nearest_hospitals.visibility = GONE
                        empty_list_message.visibility = VISIBLE
                    }
                    else {
                        getHospitalListApi()
                    }
                }
            }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            LOCATION_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                    if (canGetLocation()) {
                        getLastKnownLocation()
//                        displayLocationSettingsRequest(this)
                    }
                } else {
                    toast("Location permission denied, unable to show nearby hospitals")
                    list_nearest_hospitals.visibility = GONE
                    empty_list_message.visibility = VISIBLE
                }
                return
            }
        }
    }

    private fun displayLocationSettingsRequest(context: Context) {
        val googleApiClient: GoogleApiClient = GoogleApiClient.Builder(context)
            .addApi(LocationServices.API).build()
        googleApiClient.connect()
        val locationRequest: LocationRequest = LocationRequest.create()
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequest.setInterval(10000)
        locationRequest.setFastestInterval(10000 / 2)
        val builder: LocationSettingsRequest.Builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)
        val result: PendingResult<LocationSettingsResult> =
            LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
        result.setResultCallback(object : ResultCallback<LocationSettingsResult?> {
            override fun onResult(result: LocationSettingsResult) {
                val status: Status = result.getStatus()
                when (status.getStatusCode()) {
                    LocationSettingsStatusCodes.SUCCESS -> getLastKnownLocation()
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(
                                this@NearestHospitalActivity,
                                REQUEST_CHECK_SETTINGS
                            )
                        } catch (e: IntentSender.SendIntentException) {
//                            Log.i(TAG, "PendingIntent unable to execute request.")
                        }
                    }
//                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
//                        TAG,
//                        "Location settings are inadequate, and cannot be fixed here. Dialog not created."
//                    )
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CHECK_SETTINGS ->
                when (resultCode) {
                Activity.RESULT_OK -> {
                    getLastKnownLocation()
                }
                Activity.RESULT_CANCELED -> {
                    toast("Location services disabled, unable to show nearby hospitals")
                    list_nearest_hospitals.visibility = GONE
                    empty_list_message.visibility = VISIBLE
                }
            }
        }
    }


    private fun canGetLocation(): Boolean {
        if ((ContextCompat.checkSelfPermission(this@NearestHospitalActivity,
                Manifest.permission.ACCESS_FINE_LOCATION) ===
                    PackageManager.PERMISSION_GRANTED)) {
                return true
        }
        return false
    }

    private fun requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this@NearestHospitalActivity,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this@NearestHospitalActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST)
        } else {
            ActivityCompat.requestPermissions(this@NearestHospitalActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST)
        }
    }

    private fun getHospitalListApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val locationString: String = currentLocation.latitude.toString() + "," + currentLocation.longitude.toString()
                val response = viewModel.getHospitalsList(locationString, GOOGLE_MAPS_API_KEY)
                if (response.status.equals("OK")) {
                    // status true from api
                    closeLoadingDialog()
                    list_nearest_hospitals.visibility = VISIBLE
                    empty_list_message.visibility = GONE
                    adapter.setList(response.results, this@NearestHospitalActivity, currentLocation)
                    adapter.notifyDataSetChanged()
                    list_nearest_hospitals.scheduleLayoutAnimation()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    list_nearest_hospitals.visibility = GONE
                    empty_list_message.visibility = VISIBLE
                    showOneButtonAlertDialog(
                        getString(R.string.str_cannot_reach_server),
                        getString(R.string.Ok),
                        this@NearestHospitalActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                list_nearest_hospitals.visibility = GONE
                empty_list_message.visibility = VISIBLE
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@NearestHospitalActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                list_nearest_hospitals.visibility = GONE
                empty_list_message.visibility = VISIBLE
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@NearestHospitalActivity,
                    null
                )
            }
        }
    }

    private fun initRecyclerView(){
        list_nearest_hospitals.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_1dp, null))
        list_nearest_hospitals.addItemDecoration(itemDecoration)

        adapter = NearestHospitalAdapter({ selectedItem: Result ->listItemClicked(selectedItem)})
        list_nearest_hospitals.adapter = adapter
    }

    private fun listItemClicked(data: Result){
        val currentLocationString: String = currentLocation.latitude.toString() + "," + currentLocation.longitude.toString()
        val hospitalLocationString: String = data.geometry.location.lat.toString() + "," + data.geometry.location.lng.toString()

        intent = Intent(this, NearestHospitalDirectionsActivity::class.java)
        intent.putExtra(KEY_CURRENT_LOCATION, currentLocationString)
        intent.putExtra(KEY_HOSPITAL_LOCATION, hospitalLocationString)
        startActivity(intent)
    }
}