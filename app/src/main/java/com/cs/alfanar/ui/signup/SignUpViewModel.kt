package com.cs.alfanar.ui.signup

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.alfanar.R
import com.cs.alfanar.model.masterData.MasterDataResponse
import com.cs.alfanar.model.masterData.Nationality
import com.cs.alfanar.model.masterData.Project
import com.cs.alfanar.model.masterData.Role
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.regex.Pattern

class SignUpViewModel(private val repository: SignUpRepository) : ViewModel() {

    private val _signUpForm = MutableLiveData<SignUpFormState>()
    val signUpFormState: LiveData<SignUpFormState> = _signUpForm

    private var _roleList = MutableLiveData<List<Role>>()
    private var _projectList = MutableLiveData<List<Project>>()
    private var _nationalitiesList = MutableLiveData<List<Nationality>>()

    val roleListState: LiveData<List<Role>> = _roleList
    val projectListState: LiveData<List<Project>> = _projectList
    val nationalitiesListState: LiveData<List<Nationality>> = _nationalitiesList

    // binding master data after getting response from api
    fun bind(masterDataResponse: MasterDataResponse) {
        _roleList.postValue(masterDataResponse.Data.RoleList)
        _projectList.postValue(masterDataResponse.Data.ProjectList)
        _nationalitiesList.postValue(masterDataResponse.Data.Nationalities)
    }

    // Master data api calling
    suspend fun getMasterData() = withContext(Dispatchers.IO) { repository.getMasterData() }

    // Registration api calling
    suspend fun registerUser(inputJson : String) = withContext(Dispatchers.IO) { repository.registerUser(inputJson) }

    // data validation
    fun signUpDataChanged(employeeFileNumber: String, name: String, email: String, mobile: String, password: String, confirmPassword: String) {
        if (employeeFileNumber.isEmpty()) {
            _signUpForm.value = SignUpFormState(employeeFileError = R.string.validation_error_enter_employee_number)
        }
        else if (name.isBlank()) {
            _signUpForm.value = SignUpFormState(nameError = R.string.validation_error_enter_name)
        }
        else if (email.isBlank()) {
            _signUpForm.value = SignUpFormState(emailError = R.string.validation_error_enter_email)
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _signUpForm.value = SignUpFormState(emailError = R.string.validation_error_invalid_email)
        }
        else if (mobile.isBlank()) {
            _signUpForm.value = SignUpFormState(mobileError = R.string.validation_error_enter_mobile)
        }
        else if (!isValidSaudiMobile(mobile)) {
            _signUpForm.value = SignUpFormState(mobileError = R.string.validation_error_invalid_mobile)
        }
        else if (password.isBlank()) {
            _signUpForm.value = SignUpFormState(passwordError = R.string.validation_error_enter_password)
        }
        else if (!isValidPassword(password)) {
            _signUpForm.value = SignUpFormState(passwordError = R.string.validation_error_invalid_password)
        }
        else if (confirmPassword.isBlank() || !confirmPassword.matches(password.toRegex())) {
            _signUpForm.value = SignUpFormState(confirmPasswordError = R.string.validation_error_invalid_confirm_password)
        }
        else {
            _signUpForm.value = SignUpFormState(isDataValid = true)
        }
    }

    // checking
    fun isValidSaudiMobile(data: String): Boolean {
        if (data.length == 9 && data.startsWith("5"))
            return true
        return false
    }

    /**
     * Checks if the password is valid as per the following password policy.
     * Password should be minimum minimum 8 characters long.
     * Password should contain at least one number.
     * Password should contain at least one capital letter.
     * Password should contain at least one small letter.
     * Password should contain at least one special character.
     * Allowed special characters: "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
     *
     * @param data - String
     * @return - true if the password is valid as per the password policy.
     */
    fun isValidPassword(data: String): Boolean {
        val str : String = data
        var valid = true

        // Password policy check
        // Password should be minimum minimum 8 characters long
        if (str.length < 8) {
            valid = false
        }
        // Password should contain at least one number
        var exp = ".*[0-9].*"
        var pattern = Pattern.compile(exp, Pattern.CASE_INSENSITIVE)
        var matcher = pattern.matcher(str)
        if (!matcher.matches()) {
            valid = false
        }

        // Password should contain at least one capital letter
        exp = ".*[A-Z].*"
        pattern = Pattern.compile(exp)
        matcher = pattern.matcher(str)
        if (!matcher.matches()) {
            valid = false
        }

        // Password should contain at least one small letter
        exp = ".*[a-z].*"
        pattern = Pattern.compile(exp)
        matcher = pattern.matcher(str)
        if (!matcher.matches()) {
            valid = false
        }

        // commented code - validtion to check special character
//        // Password should contain at least one special character
//        // Allowed special characters : "~!@#$%^&*()-_=+|/,."';:{}[]<>?"
//        exp = ".*[~!@#\$%\\^&*()\\-_=+\\|\\[{\\]};:'\",<.>/?].*"
//        pattern = Pattern.compile(exp)
//        matcher = pattern.matcher(str)
//        if (!matcher.matches()) {
//            valid = false
//        }

        return valid
    }
}