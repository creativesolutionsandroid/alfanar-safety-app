package com.cs.alfanar.ui.weeklyReport.weeklyReportDashboard

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.weeklyReport.*

class WeeklyReportDashboardViewModel (private val repository: WeeklyReportDashboardRepository) : ViewModel() {

    suspend fun getStage1Data(): List<WRstage1Entity> = repository.getStage1Data()
    suspend fun getStage2Data(): List<WRstage2Entity> = repository.getStage2Data()
    suspend fun getStage3Data(): List<WRstage3Entity> = repository.getStage3Data()
    suspend fun getCheckListData(): List<WRstage4ChecklistEntity> = repository.getCheckListData()
    suspend fun getDrillListData(): List<WRstage4EmergencyDrillEntity> = repository.getDrillListData()
    suspend fun getOtherHealthData(): List<WRstage4OtherHealthEntity> = repository.getOtherHealthData()
    suspend fun getHSEData(): List<WRstage4HSEEntity> = repository.getHSEData()
    suspend fun getVulnerableData(): List<WRstage4VulnerableEntity> = repository.getVulnerableData()

}