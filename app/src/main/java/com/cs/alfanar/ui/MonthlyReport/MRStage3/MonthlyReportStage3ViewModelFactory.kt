package com.cs.alfanar.ui.MonthlyReport.MRStage3

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage3ViewModelFactory (
    private val repository: MonthlyReportStage3Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage3ViewModel(repository) as T
    }
}