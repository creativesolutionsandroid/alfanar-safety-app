package com.cs.alfanar.ui.weeklyReport.weeklyReportStage2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage2Entity

class WeeklyReportStage2ViewModel (private val repository: WeeklyReportStage2Repository) : ViewModel() {

    private val _Form = MutableLiveData<WeeklyReportStage2FormState>()
    val formState: LiveData<WeeklyReportStage2FormState> = _Form
    
    // database requests
    suspend fun saveData(data: WRstage2Entity): Long = repository.saveData(data)

    suspend fun getData(): List<WRstage2Entity> = repository.getData()

    // data validation
    fun dataValidation(topic: String, duration: Boolean, date: Boolean, attendee: String, conductedBy: String) {
        if (topic.isBlank()) {
            _Form.value = WeeklyReportStage2FormState(topicError = R.string.error_topic)
        }
        else if (!duration) {
            _Form.value = WeeklyReportStage2FormState(durationError = R.string.error_duration)
        }
        else if (!date) {
            _Form.value = WeeklyReportStage2FormState(dateError = R.string.error_date)
        }
        else if (attendee.isBlank()) {
            _Form.value = WeeklyReportStage2FormState(attendeeError = R.string.error_attendees)
        }
        else if (conductedBy.isBlank()) {
            _Form.value = WeeklyReportStage2FormState(conductedByError = R.string.error_conducted_by)
        }
        else {
            _Form.value = WeeklyReportStage2FormState(isDataValid = true)
        }
    }
}