package com.cs.alfanar.ui.signup

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.adapters.NationalityAdapter
import com.cs.alfanar.adapters.ProjectAdapter
import com.cs.alfanar.adapters.RoleAdapter
import com.cs.alfanar.model.masterData.Nationality
import com.cs.alfanar.model.masterData.Project
import com.cs.alfanar.model.masterData.Role
import com.cs.alfanar.databinding.ActivitySignUpBinding
import com.cs.alfanar.utils.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class SignUpActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: SignUpViewModelFactory by instance()

    private lateinit var viewModel: SignUpViewModel
    private lateinit var binding: ActivitySignUpBinding

    private var roleIdSelected: Int = 0
    private var projectIdSelected: Int = 0
    private var nationalityIdSelected: Int = 0

    private var isTextWatchersEnabled: Boolean = false

//    private val TAG: String = this::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)

        viewModel = ViewModelProvider(this, factory).get(SignUpViewModel::class.java)

        // calling api to get master data for spinners
        callMasterDataApi()

        // observers(spinners) will be called after getting response from server
        viewModel.roleListState.observe(this, Observer {
            val roleList: List<Role> = it ?: return@Observer
            setRoleSpinnerData(roleList)
        })

        viewModel.projectListState.observe(this, Observer {
            val projectList: List<Project> = it ?: return@Observer
            setProjectSpinnerData(projectList)
        })

        viewModel.nationalitiesListState.observe(this, Observer {
            val nationality = it ?: return@Observer
            setNationalitySpinnerData(nationality)
        })

        // observers(fields) will be called after validations
        viewModel.signUpFormState.observe(this, Observer {
            val signUpFormState: SignUpFormState = it ?: return@Observer

            if (signUpFormState.employeeFileError != null) {
                binding.etSignupFileNumber.error = getString(signUpFormState.employeeFileError)
                scrollTo(binding.etSignupFileNumber, binding.scrollView)
            }
            else if (signUpFormState.nameError != null) {
                binding.etSignupName.error = getString(signUpFormState.nameError)
                scrollTo(binding.etSignupName, binding.scrollView)
            }
            else if (roleIdSelected == 0) {
                showOneButtonAlertDialog(
                    getString(R.string.validation_error_select_position),
                    getString(R.string.Ok),
                    this@SignUpActivity,
                    null
                )
                scrollTo(binding.spinnerRole, binding.scrollView)
            }
            else if (projectIdSelected == 0) {
                showOneButtonAlertDialog(
                    getString(R.string.validation_error_select_project),
                    getString(R.string.Ok),
                    this@SignUpActivity,
                    null
                )
               scrollTo(binding.spinnerProject, binding.scrollView)
            }
            else if (nationalityIdSelected == 0) {
                showOneButtonAlertDialog(
                    getString(R.string.validation_error_select_nationality),
                    getString(R.string.Ok),
                    this@SignUpActivity,
                    null
                )
                scrollTo(binding.spinnerNationality, binding.scrollView)
            }
            else if (signUpFormState.emailError != null) {
                binding.etSignupEmail.error = getString(signUpFormState.emailError)
                scrollTo(binding.etSignupEmail, binding.scrollView)
            }
            else if (signUpFormState.mobileError != null) {
                binding.etSignupMobile.error = getString(signUpFormState.mobileError)
            }
            else if (signUpFormState.passwordError != null) {
                binding.etSignupPassword.error = getString(signUpFormState.passwordError)
            }
            else if (signUpFormState.confirmPasswordError != null) {
                binding.etSignupConfirmPassword.error = getString(signUpFormState.confirmPasswordError)
            }

            if (signUpFormState.isDataValid) {
                callSignUpApi()
            }
        })

        // Disabling keyboard for spinners
        binding.spinnerRole.setInputType(InputType.TYPE_NULL)
        binding.spinnerProject.setInputType(InputType.TYPE_NULL)
        binding.spinnerNationality.setInputType(InputType.TYPE_NULL)

    }

    fun onSubmitClicked(view: View) {
        doValidateAndRegister()
    }

    fun onCancelClicked(view: View) {
        supportFinishAfterTransition()
    }

    fun doValidateAndRegister() {
        // Checking and enabling text watchers
//        if (!isTextWatchersEnabled)
//            addTextWatchers()

        // calling validations method in view model
        viewModel.signUpDataChanged(
            binding.etSignupFileNumber.text.toString().trim(),
            binding.etSignupName.text.toString().trim(),
            binding.etSignupEmail.text.toString().trim(),
            binding.etSignupMobile.text.toString().trim(),
            binding.etSignupPassword.text.toString().trim(),
            binding.etSignupConfirmPassword.text.toString().trim()
        )
    }

    fun callMasterDataApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getMasterData()
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    viewModel.bind(response)
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(response.MessageEn, getString(R.string.Ok), this@SignUpActivity,
                    null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(getString(R.string.str_cannot_reach_server), getString(R.string.Ok), this@SignUpActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(getString(R.string.str_connection_error), getString(R.string.Ok), this@SignUpActivity,
                    null
                )
            }
        }
    }

    fun callSignUpApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.registerUser(prepareInputJsonForSignUp())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    startActivity(Intent(applicationContext, SignUpSuccessActivity::class.java))
                    finish()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@SignUpActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@SignUpActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@SignUpActivity,
                    null
                )
            }
        }
    }

    fun prepareInputJsonForSignUp(): String {
        val parentObj = JSONObject()

        val employeeFileNumber: String = binding.etSignupFileNumber.text.toString().trim()
        val name: String = binding.etSignupName.text.toString().trim()
        val email: String = binding.etSignupEmail.text.toString().trim()
        val mobile: String = binding.etSignupMobile.text.toString().trim()
        val password: String = binding.etSignupPassword.text.toString().trim()

        parentObj.put("EmployeeNo", employeeFileNumber)
        parentObj.put("EmployeeName", name)
        parentObj.put("RoleId", roleIdSelected)
        parentObj.put("ProjectId", projectIdSelected)
        parentObj.put("NationalityId", nationalityIdSelected)
        parentObj.put("Email", email)
        parentObj.put("Mobile", "+966" + mobile)
        parentObj.put("Password", password)
        parentObj.put("DeviceToken", -1)
        parentObj.put("DeviceVersion", "V" + appVersion())
        parentObj.put("Language", "En")
        parentObj.put("DeviceType", "Android")

//        Log.d(TAG, "prepareInputJsonForSignUp: " + parentObj.toString())
        return parentObj.toString()
    }

    fun setRoleSpinnerData(roleList: List<Role>) {
        val roleAdapter: RoleAdapter =
            RoleAdapter(
                this,
                android.R.layout.simple_list_item_1,
                roleList
            )
        binding.spinnerRole.setAdapter(roleAdapter)

        binding.spinnerRole.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = roleList.get(position).NameEn
                roleIdSelected = roleList.get(position).Id

                binding.spinnerRole.setText(selectedItem)
                binding.layoutRoleSpinner.isErrorEnabled = false
                hideKeyBoard()
            }

        binding.spinnerRole.setOnTouchListener(View.OnTouchListener { view, motionEvent ->
            hideKeyBoard()
            return@OnTouchListener true
        })

    }

    fun setProjectSpinnerData(projectList: List<Project>) {
        val projectAdapter: ProjectAdapter =
            ProjectAdapter(
                this,
                android.R.layout.simple_list_item_1,
                projectList
            )
        binding.spinnerProject.setAdapter(projectAdapter)

        binding.spinnerProject.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = projectList.get(position).ProjectNameEn
                projectIdSelected = projectList.get(position).Id

                binding.spinnerProject.setText(selectedItem)
                binding.layoutProjectSpinner.isErrorEnabled = false
                hideKeyBoard()
            }

        binding.spinnerProject.setOnTouchListener(View.OnTouchListener { view, motionEvent ->
            hideKeyBoard()
            return@OnTouchListener true
        })
    }

    fun setNationalitySpinnerData(nationalityList: List<Nationality>) {
        val nationalityAdapter: NationalityAdapter =
            NationalityAdapter(
                this,
                android.R.layout.simple_list_item_1,
                nationalityList
            )
        binding.spinnerNationality.setAdapter(nationalityAdapter)

        binding.spinnerNationality.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = nationalityList.get(position).NameEn
                nationalityIdSelected = nationalityList.get(position).Id

                binding.spinnerNationality.setText(selectedItem)
                binding.layoutNationalitySpinner.isErrorEnabled = false
                hideKeyBoard()
            }

        binding.spinnerNationality.setOnTouchListener(View.OnTouchListener { view, motionEvent ->
            hideKeyBoard()
            return@OnTouchListener true
        })
    }

    fun hideKeyBoard() {
        hideKeyboard(binding.etSignupName)
        hideKeyboard(binding.etSignupFileNumber)
        hideKeyboard(binding.etSignupEmail)
        hideKeyboard(binding.etSignupConfirmPassword)
        hideKeyboard(binding.etSignupPassword)
    }

    fun addTextWatchers() {
        isTextWatchersEnabled = true
        // adding text watchers for edittexts
        binding.etSignupFileNumber.afterTextChanged {
            binding.inputFileNumberLayout.isErrorEnabled = false
//            doValidateAndRegister()
        }

        binding.etSignupName.afterTextChanged {
            binding.inputNameLayout.isErrorEnabled = false
//            doValidateAndRegister()
        }

        binding.etSignupEmail.afterTextChanged {
            binding.inputEmailLayout.isErrorEnabled = false
//            doValidateAndRegister()
        }

        binding.etSignupMobile.afterTextChanged {
            binding.inputMobileLayout.isErrorEnabled = false
//            doValidateAndRegister()
        }

        binding.etSignupPassword.afterTextChanged {
            binding.inputPasswordLayout.isErrorEnabled = false
//            doValidateAndRegister()
        }

        binding.etSignupConfirmPassword.afterTextChanged {
            binding.inputConfirmPasswordLayout.isErrorEnabled = false
//            doValidateAndRegister()
        }
    }
}

