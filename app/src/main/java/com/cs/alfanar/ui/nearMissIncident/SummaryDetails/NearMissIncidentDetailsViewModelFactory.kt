package com.cs.alfanar.ui.nearMissIncident.SummaryDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class NearMissIncidentDetailsViewModelFactory (
    private val repository: NearMissIncidentDetailsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NearMissIncidentDetailsViewModel(repository) as T
    }
}