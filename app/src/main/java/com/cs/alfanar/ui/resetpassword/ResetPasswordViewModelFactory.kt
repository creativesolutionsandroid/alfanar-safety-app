package com.cs.alfanar.ui.resetpassword

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ResetPasswordViewModelFactory (
    private val repository: ResetPasswordRespository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ResetPasswordViewModel(repository) as T
    }
}