package com.cs.alfanar.ui.accidentAndIncident.Stage3

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest

class AccidentStage3Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    // DB requests
    suspend fun getData() : List<AccidentIncidentEntity> {
        return db.getAccidentIncidentDao().getData()
    }

    suspend fun updatevehicleApplicable(vehicleApplicable: Boolean) =
        db.getAccidentIncidentDao().updatevehicleApplicable(vehicleApplicable)

    suspend fun updateModel(Model: String) =
        db.getAccidentIncidentDao().updateModel(Model)

    suspend fun updatePlate(Plate: String) =
        db.getAccidentIncidentDao().updatePlate(Plate)

    suspend fun updateVehicleID(VehicleID: String) =
        db.getAccidentIncidentDao().updateVehicleID(VehicleID)

    suspend fun updateNoOfVehicleInvolved(NoOfVehicleInvolved: String) =
        db.getAccidentIncidentDao().updateNoOfVehicleInvolved(NoOfVehicleInvolved)

    suspend fun updateOverTime(OverTime: Boolean) =
        db.getAccidentIncidentDao().updateOverTime(OverTime)

    suspend fun updateReportingDate(ReportingDate: String) =
        db.getAccidentIncidentDao().updateReportingDate(ReportingDate)

    suspend fun updateReportingTime(ReportingTime: String) =
        db.getAccidentIncidentDao().updateReportingTime(ReportingTime)

    suspend fun updateOverTimeHours(OverTimeHours: String) =
        db.getAccidentIncidentDao().updateOverTimeHours(OverTimeHours)

    suspend fun updateHospital(Hospital: Boolean) =
        db.getAccidentIncidentDao().updateHospital(Hospital)

    suspend fun updatePolice(Police: Boolean) =
        db.getAccidentIncidentDao().updatePolice(Police)

    suspend fun updateCivilDefence(CivilDefence: Boolean) =
        db.getAccidentIncidentDao().updateCivilDefence(CivilDefence)

}