package com.cs.alfanar.ui.nearestHospitalDirections

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class NearestHospitalDirectionsViewModelFactory (
    private val repository: NearestHospitalDirectionsRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NearestHospitalDirectionsViewModel(repository) as T
    }
}