package com.cs.alfanar.ui.weeklyReport.weeklyReportStage2

data class WeeklyReportStage2FormState(
    val topicError: Int? = null,
    val durationError: Int? = null,
    val dateError: Int? = null,
    val attendeeError: Int? = null,
    val conductedByError: Int? = null,
    val isDataValid: Boolean = false
)