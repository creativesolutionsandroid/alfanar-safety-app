package com.cs.alfanar.ui.settings

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.cs.alfanar.R
import com.cs.alfanar.ui.changePassword.ChangePasswordActivity
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onChangePasswordClicked(view: View) {
        startActivity(Intent(this, ChangePasswordActivity::class.java))
    }

}