package com.cs.alfanar.ui.weeklyReport.weeklyReportStage4

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.weeklyReport.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WeeklyReportStage4ViewModel (private val repository: WeeklyReportStage4Repository) : ViewModel() {

    //api requests
    // get project data api calling
    suspend fun getProjectDataForWeeklyReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getProjectDataForWeeklyReport(inputJson)
    }

    suspend fun insertWeeklyReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.insertWeeklyReport(inputJson)
    }

    // database requests
    suspend fun saveCheckListData(data: WRstage4ChecklistEntity): Long = repository.saveCheckListData(data)
    suspend fun getCheckListData(): List<WRstage4ChecklistEntity> = repository.getCheckListData()

    suspend fun saveOtherHealthData(data: WRstage4OtherHealthEntity): Long = repository.saveOtherHealthData(data)
    suspend fun getOtherHealthData(): List<WRstage4OtherHealthEntity> = repository.getOtherHealthData()
    suspend fun updateOtherHealthData(name: String, id: Int) = repository.updateOtherHealthData(name, id)

    suspend fun saveHSEData(data: WRstage4HSEEntity): Long = repository.saveHSEData(data)
    suspend fun getHSEData(): List<WRstage4HSEEntity> = repository.getHSEData()
    suspend fun updateHSEData(name: String, id: Int) = repository.updateHSEData(name, id)

    suspend fun saveVulnerableData(data: WRstage4VulnerableEntity): Long = repository.saveVulnerableData(data)
    suspend fun getVulnerableData(): List<WRstage4VulnerableEntity> = repository.getVulnerableData()
    suspend fun updateVulnerableData(name: String, id: Int) = repository.updateVulnerableData(name, id)

    suspend fun updateDrillData(data: WRstage4EmergencyDrillEntity) = repository.updateDrillData(data)
    suspend fun getDrillData(): List<WRstage4EmergencyDrillEntity> = repository.getDrillData()

    suspend fun getStage1Data(): List<WRstage1Entity> =  repository.getStage1Data()
    suspend fun getStage2Data(): List<WRstage2Entity> = repository.getStage2Data()
    suspend fun getStage3Data(): List<WRstage3Entity> = repository.getStage3Data()
    suspend fun getProjectData() : List<WRprojectSelectionEntity> = repository.getProjectData()

    suspend fun clearStage1Data() = repository.clearStage1Data()
    suspend fun clearStage2Data() = repository.clearStage2Data()
    suspend fun clearStage3Data() = repository.clearStage3Data()
    suspend fun clearProjectData() = repository.clearProjectData()
    suspend fun clearChecklistData() = repository.clearChecklistData()
    suspend fun clearOtherHealthData() = repository.clearOtherHealthData()
    suspend fun clearHSEData() = repository.clearHSEData()
    suspend fun clearVulnerbaleData() = repository.clearVulnerbaleData()
    suspend fun clearDrillData() = repository.clearDrillData()
}