package com.cs.alfanar.ui.alertNotification

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AlertNotificationViewModel (private val repository: AlertNotificationRepository) : ViewModel() {

    // get Alert notifications list api calling
    suspend fun getAlertNotifications(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getAlertNotifications(inputJson)
    }
}