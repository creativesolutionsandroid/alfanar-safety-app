package com.cs.alfanar.ui.dashboard

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.databinding.DashboardFragmentBinding
import com.cs.alfanar.utils.ROLE_REGULAR_USER
import kotlinx.android.synthetic.main.dashboard_fragment.*


class DashboardFragment : Fragment() {

    companion object {
        fun newInstance() =
            DashboardFragment()
    }

    private lateinit var viewModel: DashboardViewModel
    private lateinit var binding: DashboardFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.dashboard_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)

        if (UserPreferences(context).roleId() == ROLE_REGULAR_USER) {
            layout_safety_officer.visibility = View.GONE
            layout_employee_details.visibility = View.VISIBLE
        }

        binding.employeeUsername.text = UserPreferences(
            context = context
        ).employeeName()?.split(' ')?.joinToString(" ") { it.capitalize() }
        binding.employeePosition.text = UserPreferences(
            context = context
        ).employeeRole()?.split(' ')?.joinToString(" ") { it.capitalize() }

        val rotate = RotateAnimation(
            0f,
            0f,
            Animation.ZORDER_TOP,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        rotate.duration = 5000
        rotate.interpolator = LinearInterpolator()
        binding.iconDaily.startAnimation(rotate)
    }
}