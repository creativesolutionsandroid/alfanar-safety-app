package com.cs.alfanar.ui.alertNotificationInsert

import android.Manifest
import android.app.AlertDialog
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.databinding.ActivityAlertNotificationInsertBinding
import com.cs.alfanar.model.emergencyNumbers.Data
import com.cs.alfanar.ui.alertNotificationInbox.AlertNotificationInboxActivity
import com.cs.alfanar.ui.emergencyNumbers.EmergencyNumbersAdapter
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_alert_notification_insert.*
import kotlinx.android.synthetic.main.activity_alert_notification_insert.upload_image
import kotlinx.android.synthetic.main.activity_nearest_hospital.*
import kotlinx.android.synthetic.main.activity_stop_work_advice_step2.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

class AlertNotificationInsertActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: AlertNotificationInsertViewModelFactory by instance()

    private lateinit var binding: ActivityAlertNotificationInsertBinding
    private lateinit var viewModel: AlertNotificationInsertViewModel
    private lateinit var adapter: EmergencyNumbersAdapter

    private val TAG : String = this::class.java.simpleName

    private val DATA_KEY: String = "DATA_KEY"
    private var alertNotificationTypeId: Int = 0

    private val CALL_PHONE_REQUEST: Int = 1
    private lateinit var selectedMobileNumberData: Data

    private val STORAGE_REQUEST: Int = 1001
    private val CAMERA_REQUEST: Int = 1002
    private val PICK_IMAGE_FROM_CAMERA = 1
    private val PICK_IMAGE_FROM_GALLERY = 2
    var isCamera = false
    var thumbnail: Bitmap? = null

    var pictureName: String? = null
    var base64String = ""
    var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_alert_notification_insert)

        viewModel = ViewModelProvider(this, factory).get(AlertNotificationInsertViewModel::class.java)

        alertNotificationTypeId = intent.getIntExtra(DATA_KEY, 0)

        initRecyclerView()
        getDocumentListApi()

        viewModel.DescriptionFormState.observe(this, Observer {
            val formState: AlertNotificationInsertFormState = it ?: return@Observer
            if (formState.descriptionError != null) {
                binding.etAlertDescription.error = getString(formState.descriptionError)
            }
            if (formState.isDataValid) {
                hideKeyboard(currentFocus ?: View(this))
                insertAlertNotificationApi()
            }
        })

        no_cb.setOnClickListener {
            if (no_cb.isChecked) {
                pictureName = ""
                base64String = ""
                upload_image.setImageDrawable(getDrawable(R.drawable.ic_upload_image))
            }
        }
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onImageClicked(view: View) {
//        if (yes_checkbox.isChecked) {
        yes_cb.isChecked = true
        checkStoragePermission()
//        }
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onSendButtonClicked(view: View) {
        viewModel.descriptionFieldValidator(binding.etAlertDescription.text.toString())
    }

    fun prepareInputJsonForEmergencyNumbers(): String {
        val parentObj = JSONObject()

        parentObj.put(
            "CountryId", UserPreferences(
                this
            ).countryId()
        )

//        Log.d(TAG, "prepareInputJsonForEmergencyNumbers: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getDocumentListApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getEmergencyNumbers(prepareInputJsonForEmergencyNumbers())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    adapter.setList(response.Data, this@AlertNotificationInsertActivity)
                    adapter.notifyDataSetChanged()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@AlertNotificationInsertActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@AlertNotificationInsertActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@AlertNotificationInsertActivity,
                    null
                )
            }
        }
    }

    private fun initRecyclerView(){
        list_alert_notification.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_alert_notification.addItemDecoration(itemDecoration)

        adapter = EmergencyNumbersAdapter({ selectedItem: Data -> listItemClicked(selectedItem) })
        list_alert_notification.adapter = adapter
    }

    fun prepareInputJsonToInsertNotification(): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())
        parentObj.put("AlertTypeId", alertNotificationTypeId)
        parentObj.put("ProjectId", UserPreferences(this).projectId())
        parentObj.put("MessageEn", et_alert_description.text.toString())

        if (!base64String.equals("")) {
            val imageObj = JSONObject()
            imageObj.put("FileName", pictureName)
            imageObj.put("Base64FileData", base64String)
            imageObj.put("FileUploadLocation", "AlertNotification")
            parentObj.put("Image", imageObj)
        }
        else {
            parentObj.put("Image", null)
        }
        Log.d(TAG, "prepareInputJsonForEmergencyNumbers: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun insertAlertNotificationApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.insertAlertNotifications(
                    prepareInputJsonToInsertNotification()
                )
                closeLoadingDialog()
                if (response.Status) {
                    binding.etAlertDescription.setText("")
                    upload_image.setImageDrawable(getDrawable(R.drawable.ic_upload_image))
                    thumbnail = null
                    pictureName = ""
                    base64String = ""
                    no_cb.isChecked = true
                    toast(response.MessageEn)
                    startActivity(
                        Intent(
                            this@AlertNotificationInsertActivity,
                            AlertNotificationInboxActivity::class.java
                        )
                    )
                    finish()
                }
                else {
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@AlertNotificationInsertActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@AlertNotificationInsertActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@AlertNotificationInsertActivity,
                    null
                )
            }
        }
    }

    private fun listItemClicked(data: Data){
        selectedMobileNumberData = data
        checkPermission()
    }

    private fun showAlertDialogAndCall() {
        intent = Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + selectedMobileNumberData.Number));
        showTwoButtonsDialog(
            this, String.format(
                resources.getString(R.string.call_phone_message),
                selectedMobileNumberData.DeptNameEn
            ),
            resources.getString(R.string.yes), resources.getString(R.string.no), intent
        )
    }

    private fun checkPermission() {
        if (!canCall()) {
            requestPermission()
        }
        else {
            showAlertDialogAndCall()
        }
    }

    private fun canCall(): Boolean {
        if ((ContextCompat.checkSelfPermission(
                this@AlertNotificationInsertActivity,
                Manifest.permission.CALL_PHONE
            ) ===
                    PackageManager.PERMISSION_GRANTED)) {
            return true
        }
        return false
    }

    private fun requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this@AlertNotificationInsertActivity,
                Manifest.permission.CALL_PHONE
            )) {
            ActivityCompat.requestPermissions(
                this@AlertNotificationInsertActivity,
                arrayOf(Manifest.permission.CALL_PHONE), CALL_PHONE_REQUEST
            )
        } else {
            ActivityCompat.requestPermissions(
                this@AlertNotificationInsertActivity,
                arrayOf(Manifest.permission.CALL_PHONE), CALL_PHONE_REQUEST
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CALL_PHONE_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    if (canCall()) {
                        showAlertDialogAndCall()
                    }
                } else {
                    toast(getString(R.string.call_permission_failure_message))
                }
                return
            }

            STORAGE_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    if (!canAccessStorage()) {
                        toast("Storage permission denied, unable to upload photo")
                    } else if (!canAccessCamera()) {
                        requestCameraPermission()
                    }
                } else {
                    toast("Storage permission denied, unable to upload photo")
                }
                return
            }

            CAMERA_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    if (!canAccessCamera()) {
                        toast("Camera permission denied, unable to upload photo")
                    } else {
                        ShowImageSelectionPopup()
                    }
                } else {
                    toast("Camera permission denied, unable to upload photo")
                }
                return
            }
        }
    }

    private fun checkStoragePermission() {
        if (!canAccessStorage()) {
            requestStoragePermission()
        }
        else if (!canAccessStorage()) {
            requestCameraPermission()
        }
        else {
            ShowImageSelectionPopup()
        }
    }

    private fun canAccessStorage(): Boolean {
        if ((ContextCompat.checkSelfPermission(
                this@AlertNotificationInsertActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) ===
                    PackageManager.PERMISSION_GRANTED)) {
            return true
        }
        return false
    }

    private fun canAccessCamera(): Boolean {
        if ((ContextCompat.checkSelfPermission(
                this@AlertNotificationInsertActivity,
                Manifest.permission.CAMERA
            ) ===
                    PackageManager.PERMISSION_GRANTED)) {
            return true
        }
        return false
    }

    private fun requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this@AlertNotificationInsertActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )) {
            ActivityCompat.requestPermissions(
                this@AlertNotificationInsertActivity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_REQUEST
            )
        } else {
            ActivityCompat.requestPermissions(
                this@AlertNotificationInsertActivity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_REQUEST
            )
        }
    }

    private fun requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this@AlertNotificationInsertActivity,
                Manifest.permission.CAMERA
            )) {
            ActivityCompat.requestPermissions(
                this@AlertNotificationInsertActivity,
                arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST
            )
        } else {
            ActivityCompat.requestPermissions(
                this@AlertNotificationInsertActivity,
                arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST
            )
        }
    }

    fun ShowImageSelectionPopup() {
        var customDialog: AlertDialog? = null
        val dialogBuilder = AlertDialog.Builder(this@AlertNotificationInsertActivity)
        // ...Irrelevant code for customizing the buttons and title
        val inflater = layoutInflater
        val layout = R.layout.alert_dialog_camera

        val dialogView = inflater.inflate(layout, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)
        val gallery_layout = dialogView.findViewById<View>(R.id.gallery_layout) as TextView
        val camera_layout = dialogView.findViewById<View>(R.id.camera_layout) as TextView
        val cancel = dialogView.findViewById<View>(R.id.cancel) as TextView
        customDialog = dialogBuilder.create()
        customDialog.show()
        val finalCustomDialog = customDialog

        camera_layout.setOnClickListener {
            openCamera()
            finalCustomDialog!!.dismiss()
        }
        gallery_layout.setOnClickListener {
            openGallery()
            finalCustomDialog!!.dismiss()
        }
        cancel.setOnClickListener { finalCustomDialog!!.dismiss() }

        val lp = WindowManager.LayoutParams()
        val window = customDialog.window
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        lp.copyFrom(window.attributes)
        //This makes the dialog take up the full width
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val screenWidth = size.x
        val d = screenWidth * 0.85
        lp.width = d.toInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
    }

    fun openGallery() {
        val currentapiVersion = Build.VERSION.SDK_INT
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessStorage()) {
                requestStoragePermission()
                isCamera = false
            } else {
                val intent = Intent()
                intent.type = "image/*"
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(intent, "Select Picture"),
                    PICK_IMAGE_FROM_GALLERY
                )
            }
        } else {
            val intent = Intent()
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                PICK_IMAGE_FROM_GALLERY
            )
        }
    }

    fun openCamera() {
        val currentapiVersion = Build.VERSION.SDK_INT
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                isCamera = true
                requestCameraPermission()
            } else {
                val values = ContentValues()
                values.put(MediaStore.Images.Media.TITLE, "New Picture")
                values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera")
                imageUri = contentResolver.insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values
                )
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(
                    intent,
                    PICK_IMAGE_FROM_CAMERA
                )
            }
        } else {
            val values = ContentValues()
            values.put(MediaStore.Images.Media.TITLE, "New Picture")
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera")
            imageUri = contentResolver.insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values
            )
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
            startActivityForResult(
                intent,
                PICK_IMAGE_FROM_CAMERA
            )
        }
    }

    fun getRealPathFromURI(contentUri: Uri?): String? {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = managedQuery(contentUri, proj, null, null, null)
        val column_index = cursor
            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE_FROM_CAMERA && resultCode == RESULT_OK) {
            val path: String? = getRealPathFromURI(imageUri)
            val bmOptions = BitmapFactory.Options()
            bmOptions.inScaled = false

            thumbnail = BitmapFactory.decodeFile(path, bmOptions)
//            thumbnail = data!!.extras!!["data"] as Bitmap?

            val originalWidth = thumbnail!!.width
            val originalHeight = thumbnail!!.height
            val outputWidth = (originalWidth * 0.75).toInt()
            val outputHeight = (originalHeight * 0.75).toInt()

            thumbnail = Bitmap.createScaledBitmap(
                thumbnail!!, outputWidth, outputHeight,
                false
            )

            val stream = ByteArrayOutputStream()
            thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 60, stream)
            val imageBytes = stream.toByteArray()
            base64String = Base64.encodeToString(imageBytes, Base64.DEFAULT)

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(Date())
            pictureName = "Image_$timeStamp.jpg"

            displayImage()
        }

        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            val uri = data!!.data
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(contentResolver, uri)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            val originalWidth = thumbnail!!.width
            val originalHeight = thumbnail!!.height
            val outputWidth = (originalWidth * 0.75).toInt()
            val outputHeight = (originalHeight * 0.75).toInt()
            thumbnail = Bitmap.createScaledBitmap(
                thumbnail!!, outputWidth, outputHeight,
                false
            )

            val stream = ByteArrayOutputStream()
            thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 60, stream)
            val imageBytes = stream.toByteArray()
            base64String = Base64.encodeToString(imageBytes, Base64.DEFAULT)

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(Date())
            pictureName = "Image_$timeStamp.jpg"

            displayImage()
        }
    }

    fun displayImage() {
        val imageBytes = Base64.decode(base64String, Base64.DEFAULT)
        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        upload_image.setImageBitmap(decodedImage)
    }
}