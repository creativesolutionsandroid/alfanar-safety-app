package com.cs.alfanar.ui.resetpassword

/**
 * Data validation state of the reset password form.
 */
data class ResetPasswordFormState(val newPasswordError: Int? = null,
                                  val confirmPasswordError: Int? = null,
                                   val isDataValid: Boolean = false)