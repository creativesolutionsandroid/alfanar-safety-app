package com.cs.alfanar.ui.forgotpassword

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cs.alfanar.ui.login.LoginRepository
import com.cs.alfanar.ui.login.LoginViewModel

class ForgotPasswordViewModelFactory(
    private val repository: ForgotPasswordRepository
    ) : ViewModelProvider.NewInstanceFactory() {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ForgotPasswordViewModel(repository) as T
        }
}