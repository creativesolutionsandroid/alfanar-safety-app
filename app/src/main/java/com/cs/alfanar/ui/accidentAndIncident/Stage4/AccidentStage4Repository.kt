package com.cs.alfanar.ui.accidentAndIncident.Stage4

import androidx.room.Query
import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentInvestigationEntity
import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.accidentIncident.AccidentMasterDataResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class AccidentStage4Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun InsertAccidentIncidentReport(inputJson : String): BasicResponse {
        return apiRequest { api.InsertAccidentIncidentReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // DB requests
    suspend fun getData() : List<AccidentInvestigationEntity> {
        return db.getAccidentInvestigationDao().getData()
    }

    suspend fun insert(projectData: AccidentInvestigationEntity) = db.getAccidentInvestigationDao().insert(projectData)

    suspend fun getDataBasedOnTypeId(TypeId: Int): List<AccidentInvestigationEntity> {
        return db.getAccidentInvestigationDao().getDataBasedOnTypeId(TypeId)
    }

    suspend fun updateData(SelectId: Int, SelectName: String, TypeId: Int) =
        db.getAccidentInvestigationDao().updateData(SelectId, SelectName, TypeId)

    suspend fun updateSafetyMeasureOther(SafetyMeasureOther: String) =
        db.getAccidentIncidentDao().updateSafetyMeasureOther(SafetyMeasureOther)

    suspend fun getSafetyMeasureOther() : List<AccidentIncidentEntity> {
        return db.getAccidentIncidentDao().getData()
    }

    suspend fun deleteAccidentIncidentData() = db.getAccidentIncidentDao().deleteData()

    suspend fun deleteAccidentInvestigationData() = db.getAccidentInvestigationDao().deleteData()
}