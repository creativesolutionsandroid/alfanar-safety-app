package com.cs.alfanar.ui.MonthlyReport.MRProjectSelection

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.monthlyReport.MRProjectionSelectionEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MonthlyReportProjectSelectionViewModel (private val repository: MonthlyReportProjectSelectionRepository) : ViewModel() {

    //api requests
    // get project data api calling
    suspend fun getProjectDataForMonthlyReport(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getProjectDataForMonthlyReport(inputJson)
    }

    // database request
    suspend fun getData() : List<MRProjectionSelectionEntity> {
        return repository.getData()
    }

    suspend fun saveData(projectData: MRProjectionSelectionEntity) = repository.saveData(projectData)

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) = repository.updateProject(projectId, projectName, projectCode)

    suspend fun updateLocation(location: String) = repository.updateLocation(location)

}