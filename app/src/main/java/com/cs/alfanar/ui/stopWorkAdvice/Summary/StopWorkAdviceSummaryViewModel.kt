package com.cs.alfanar.ui.stopWorkAdvice.Summary

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class StopWorkAdviceSummaryViewModel (private val repository: StopWorkAdviceSummaryRepository) : ViewModel() {

    suspend fun getAccidentIncidentSummary(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getAccidentIncidentSummary(inputJson)
    }

    suspend fun UpdateReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.UpdateReportStatus(inputJson)
    }

}