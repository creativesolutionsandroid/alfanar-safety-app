package com.cs.alfanar.ui.emergencyNumbers

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.emergencyNumbers.Data
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_emergency_numbers.*
import kotlinx.android.synthetic.main.activity_nearest_hospital.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class EmergencyNumbersActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: EmergencyNumbersViewModelFactory by instance()

    private lateinit var viewModel: EmergencyNumbersViewModel
    private lateinit var adapter: EmergencyNumbersAdapter

//    private val TAG : String = this::class.java.simpleName
    private val CALL_PHONE_REQUEST: Int = 1
    private lateinit var selectedMobileNumberData: Data

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_emergency_numbers)

        viewModel = ViewModelProvider(this, factory).get(EmergencyNumbersViewModel::class.java)

        initRecyclerView()
        getDocumentListApi()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun prepareInputJsonForEmergencyNumbers(): String {
        val parentObj = JSONObject()

        parentObj.put("CountryId", UserPreferences(
            this
        ).countryId())

//        Log.d(TAG, "prepareInputJsonForEmergencyNumbers: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getDocumentListApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getEmergencyNumbers(prepareInputJsonForEmergencyNumbers())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    adapter.setList(response.Data, this@EmergencyNumbersActivity)
                    adapter.notifyDataSetChanged()
                    list_emergency_numbers.scheduleLayoutAnimation()
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@EmergencyNumbersActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@EmergencyNumbersActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@EmergencyNumbersActivity,
                    null
                )
            }
        }
    }

    private fun initRecyclerView(){
        list_emergency_numbers.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_emergency_numbers.addItemDecoration(itemDecoration)

        adapter = EmergencyNumbersAdapter({ selectedItem: Data ->listItemClicked(selectedItem)})
        list_emergency_numbers.adapter = adapter
    }

    private fun listItemClicked(data: Data){
        selectedMobileNumberData = data
        checkPermission()
    }

    private fun showAlertDialogAndCall() {
        intent = Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:"+selectedMobileNumberData.Number));
        showTwoButtonsDialog(this, String.format(resources.getString(R.string.call_phone_message), selectedMobileNumberData.DeptNameEn),
            resources.getString(R.string.yes), resources.getString(R.string.no), intent)
    }

    private fun checkPermission() {
        if (!canCall()) {
            requestPermission()
        }
        else {
            showAlertDialogAndCall()
        }
    }

    private fun canCall(): Boolean {
        if ((ContextCompat.checkSelfPermission(this@EmergencyNumbersActivity,
                Manifest.permission.CALL_PHONE) ===
                    PackageManager.PERMISSION_GRANTED)) {
            return true
        }
        return false
    }

    private fun requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this@EmergencyNumbersActivity,
                Manifest.permission.CALL_PHONE)) {
            ActivityCompat.requestPermissions(this@EmergencyNumbersActivity,
                arrayOf(Manifest.permission.CALL_PHONE), CALL_PHONE_REQUEST)
        } else {
            ActivityCompat.requestPermissions(this@EmergencyNumbersActivity,
                arrayOf(Manifest.permission.CALL_PHONE), CALL_PHONE_REQUEST)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            CALL_PHONE_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                    if (canCall()) {
                        showAlertDialogAndCall()
                    }
                } else {
                    toast(getString(R.string.call_permission_failure_message))
                }
                return
            }
        }
    }
}