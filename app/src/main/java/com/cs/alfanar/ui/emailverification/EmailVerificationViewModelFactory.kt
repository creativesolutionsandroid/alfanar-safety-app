package com.cs.alfanar.ui.emailverification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class EmailVerificationViewModelFactory (
private val repository: EmailVerificationRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EmailVerificationViewModel(repository) as T
    }
}