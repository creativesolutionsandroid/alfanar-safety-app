package com.cs.alfanar.ui.MonthlyReport.MRDashboard

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportDashboardViewModelFactory (
    private val repository: MonthlyReportDashboardRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportDashboardViewModel(repository) as T
    }
}