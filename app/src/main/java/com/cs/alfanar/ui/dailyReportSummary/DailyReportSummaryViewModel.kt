package com.cs.alfanar.ui.dailyReportSummary

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.dailyReport.DailyReportRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DailyReportSummaryViewModel (private val repository: DailyReportSummaryRepository) : ViewModel() {

    // get daily report summary
    suspend fun getDailyReportSummary(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getDailyReportSummary(inputJson)
    }

    suspend fun UpdateDailyReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.UpdateDailyReportStatus(inputJson)
    }

}