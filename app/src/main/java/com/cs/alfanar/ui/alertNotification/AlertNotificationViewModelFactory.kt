package com.cs.alfanar.ui.alertNotification

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AlertNotificationViewModelFactory (
    private val repository: AlertNotificationRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AlertNotificationViewModel(repository) as T
    }
}