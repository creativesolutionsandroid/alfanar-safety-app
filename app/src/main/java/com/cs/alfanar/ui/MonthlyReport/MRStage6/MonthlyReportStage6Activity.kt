package com.cs.alfanar.ui.MonthlyReport.MRStage6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage5Entity
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage6Entity
import com.cs.alfanar.data.db.entities.monthlyReport.MRstage5EventsEntity
import com.cs.alfanar.data.db.entities.monthlyReport.MRstage6EventsEntity
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.ui.MonthlyReport.MRStage5.MRStage5EventAdapter
import com.cs.alfanar.ui.MonthlyReport.MRStage5.MonthlyReportStage5ViewModel
import com.cs.alfanar.ui.MonthlyReport.MRStage5.MonthlyReportStage5ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage7.MonthlyReportStage7Activity
import com.cs.alfanar.ui.MonthlyReport.MRStage9.MonthlyReportStage9Activity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.utils.afterTextChanged
import com.cs.alfanar.utils.showOneButtonAlertDialog
import kotlinx.android.synthetic.main.activity_monthly_report_stage5.*
import kotlinx.android.synthetic.main.activity_monthly_report_stage5.list_events
import kotlinx.android.synthetic.main.activity_monthly_report_stage6.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MonthlyReportStage6Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val ehs_manager_subtory: MonthlyReportStage6ViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportStage6ViewModel

//    private val TAG : String = this::class.java.simpleName

    private lateinit var projectData: Project
    val KEY_DATA: String = "KEY_DATA"

    private lateinit var eventAdapter: MRStage6EventAdapter
    var eventData: MutableList<MRstage6EventsEntity> = mutableListOf()
    
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_stage6)

        viewModel = ViewModelProvider(this, ehs_manager_subtory).get(MonthlyReportStage6ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Project
        projectData = Data!!

        initRecyclerViews()
        fetchDataFromDb()
        fetchEventDataFromDb()
        addTextWatcher()
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextClicked(view: View) {
        lifecycleScope.launch {
            val dataList = viewModel.getEventData()
            if (dataList.size == 1 && dataList[0].Name.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.error_safety_highlights), getString(R.string.Ok),
                    this@MonthlyReportStage6Activity, null
                )
            } else {
                viewModel.updateStage4(true)
                val intent = Intent(this@MonthlyReportStage6Activity, MonthlyReportStage7Activity::class.java)
                intent.putExtra(KEY_DATA, projectData)
                startActivity(intent)
            }
        }
    }

    fun OtherEventPlusClicked(view: View) {
        lifecycleScope.launch {
            val dataList = viewModel.getEventData()
            eventData.clear()
            eventData.addAll(dataList)
            if (eventData.size > 0 && !eventData[(eventData.size - 1)].Name.equals("")) {
                val data = MRstage6EventsEntity(eventData.size + 1, "")
                viewModel.saveEventData(data)
                fetchEventDataFromDb()
            }
        }
    }

    fun fetchEventDataFromDb() {
        lifecycleScope.launch {
            val dataList = viewModel.getEventData()
            eventData.clear()
            eventData.addAll(dataList)
            if (eventData.size > 0) {
                eventAdapter.setList(eventData, this@MonthlyReportStage6Activity)
                eventAdapter.notifyDataSetChanged()
            }
            else {
                // Adding empty data
                val data = MRstage6EventsEntity(1, "")
                viewModel.saveEventData(data)
                fetchEventDataFromDb()
            }
        }
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                positive_alfanar.setText(projectData[0].PositiveAlf.toString())
                positive_sub.setText(projectData[0].PositiveSub.toString())
                negative_alfanar.setText(projectData[0].NegativeAlf.toString())
                negative_sub.setText(projectData[0].NegativeSub.toString())
                pending_alfanar.setText(projectData[0].PendingAlf.toString())
                pending_sub.setText(projectData[0].PendingSub.toString())
                closed_alfanar.setText(projectData[0].ClosedAlf.toString())
                closed_sub.setText(projectData[0].ClosedSub.toString())
                updateCumulativeHours()
            }
            else {
                val projectData = MRStage6Entity(1, 0, 0, 0,0,0,
                    0,0,0)
                viewModel.saveData(projectData)
                updateCumulativeHours()
            }
            positive_alfanar.requestFocus()
            positive_alfanar.setSelection(positive_alfanar.text.length)
            addFocusListener()
        }
    }

    private fun initRecyclerViews(){
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))

        list_events.layoutManager = LinearLayoutManager(this)
        list_events.addItemDecoration(itemDecoration)
        eventAdapter = MRStage6EventAdapter({name: String, position: Int ->onEventUpdated(name,position)})
        list_events.adapter = eventAdapter
    }

    private fun onEventUpdated(name: String, position: Int){
        lifecycleScope.launch {
            viewModel.updateEventData(name, eventData.get(position).id)
            if (position == 0 && name.equals("")) {
                viewModel.updateStage4(false)
            }
            else {
                viewModel.updateStage4(true)
            }
        }
    }

    fun updateCumulativeHours() {
        lifecycleScope.launch {
            val dbData = viewModel.getData()

            val positiveAlfanar: Int = dbData[0].PositiveAlf
            val positiveSub: Int = dbData[0].PositiveSub
            val positiveCum: Int = projectData.MonthlyObservFinding[0].ObsPositive
            positive_this_month.text = ""+ (positiveAlfanar + positiveSub)
//            positive_accumulative.text = ""+ (positiveAlfanar + positiveSub + positiveCum)
            positive_accumulative.text = ""+ (positiveCum)

            val negativeAlfanar: Int = dbData[0].NegativeAlf
            val negativeSub: Int = dbData[0].NegativeSub
            val negativeCum: Int = projectData.MonthlyObservFinding[0].ObsNegative
            negative_this_month.text = ""+ (negativeAlfanar + negativeSub)
//            negative_accumulative.text = ""+ (negativeAlfanar + negativeSub + negativeCum)
            negative_accumulative.text = ""+ (negativeCum)

            total_alfanar.text = ""+ (positiveAlfanar + negativeAlfanar)
            total_sub.text = ""+ (positiveSub + negativeSub)
            total_this_month.text = ""+ (positiveAlfanar + positiveSub + negativeAlfanar + negativeSub)
//            total_accumulative.text = ""+ (positiveAlfanar + positiveSub + negativeAlfanar + negativeSub + positiveCum + negativeCum)
            total_accumulative.text = ""+ (positiveCum + negativeCum)

            val pendingAlfanar: Int = dbData[0].PendingAlf
            val pendingSub: Int = dbData[0].PendingSub
            val pendingCum: Int = projectData.MonthlyObservFinding[0].RemarksPending
            pending_this_month.text = ""+ (pendingAlfanar + pendingSub)
//            pending_accumulative.text = ""+ (pendingAlfanar + pendingSub + pendingCum)
            pending_accumulative.text = ""+ (pendingCum)

            val closedAlfanar: Int = dbData[0].ClosedAlf
            val closedSub: Int = dbData[0].ClosedSub
            val closedCum: Int = projectData.MonthlyObservFinding[0].RemarksClosed
            closed_this_month.text = ""+ (closedAlfanar + closedSub)
//            closed_accumulative.text = ""+ (closedAlfanar + closedSub + closedCum)
            closed_accumulative.text = ""+ (closedCum)
        }
    }

    fun addFocusListener() {
        positive_alfanar.setOnFocusChangeListener { view, b ->
            if (positive_alfanar.text.toString().length == 0) {
                positive_alfanar.setText("0")
                positive_alfanar.setSelection(positive_alfanar.text.toString().length)
            }
        }
        positive_sub.setOnFocusChangeListener { view, b ->
            if (positive_sub.text.toString().length == 0) {
                positive_sub.setText("0")
                positive_sub.setSelection(positive_sub.text.toString().length)
            }
        }
        negative_alfanar.setOnFocusChangeListener { view, b ->
            if (negative_alfanar.text.toString().length == 0) {
                negative_alfanar.setText("0")
                negative_alfanar.setSelection(negative_alfanar.text.toString().length)
            }
        }
        negative_sub.setOnFocusChangeListener { view, b ->
            if (negative_sub.text.toString().length == 0) {
                negative_sub.setText("0")
                negative_sub.setSelection(negative_sub.text.toString().length)
            }
        }
        pending_alfanar.setOnFocusChangeListener { view, b ->
            if (pending_alfanar.text.toString().length == 0) {
                pending_alfanar.setText("0")
                pending_alfanar.setSelection(pending_alfanar.text.toString().length)
            }
        }
        pending_sub.setOnFocusChangeListener { view, b ->
            if (pending_sub.text.toString().length == 0) {
                pending_sub.setText("0")
                pending_sub.setSelection(pending_sub.text.toString().length)
            }
        }
        closed_alfanar.setOnFocusChangeListener { view, b ->
            if (closed_alfanar.text.toString().length == 0) {
                closed_alfanar.setText("0")
                closed_alfanar.setSelection(closed_alfanar.text.toString().length)
            }
        }
        closed_sub.setOnFocusChangeListener { view, b ->
            if (closed_sub.text.toString().length == 0) {
                closed_sub.setText("0")
                closed_sub.setSelection(closed_sub.text.toString().length)
            }
        }
    }

    fun addTextWatcher() {
        positive_alfanar.afterTextChanged {
            if (positive_alfanar.text.toString().length > 0) {
                updatepositive_alfanar(positive_alfanar.text.toString())
            }
            else {
                updatepositive_alfanar("0")
            }
        }
        positive_sub.afterTextChanged {
            if (positive_sub.text.toString().length > 0) {
                updatepositive_sub(positive_sub.text.toString())
            }
            else {
                updatepositive_sub("0")
            }
        }
        negative_alfanar.afterTextChanged {
            if (negative_alfanar.text.toString().length > 0) {
                updatenegative_alfanar(negative_alfanar.text.toString())
            }
            else {
                updatenegative_alfanar("0")
            }
        }
        negative_sub.afterTextChanged {
            if (negative_sub.text.toString().length > 0) {
                updatenegative_sub(negative_sub.text.toString())
            }
            else {
                updatenegative_sub("0")
            }
        }
        pending_alfanar.afterTextChanged {
            if (pending_alfanar.text.toString().length > 0) {
                update_pending_alfanar(pending_alfanar.text.toString())
            }
            else {
                update_pending_alfanar("0")
            }
        }
        pending_sub.afterTextChanged {
            if (pending_sub.text.toString().length > 0) {
                update_pending_sub(pending_sub.text.toString())
            }
            else {
                update_pending_sub("0")
            }
        }
        closed_alfanar.afterTextChanged {
            if (closed_alfanar.text.toString().length > 0) {
                update_closed_alfanar(closed_alfanar.text.toString())
            }
            else {
                update_closed_alfanar("0")
            }
        }
        closed_sub.afterTextChanged {
            if (closed_sub.text.toString().length > 0) {
                update_closed_sub(closed_sub.text.toString())
            }
            else {
                update_closed_sub("0")
            }
        }
    }

    fun updatepositive_alfanar(positive_alfanar: String) {
        lifecycleScope.launch {
            viewModel.updatePositiveAlf(positive_alfanar.toInt())
        }
        updateCumulativeHours()
    }
//
    fun updatepositive_sub(positive_sub: String) {
        lifecycleScope.launch {
            viewModel.updatePositiveSub(positive_sub.toInt())
        }
        updateCumulativeHours()
    }
    fun updatenegative_alfanar(positive_alfanar: String) {
        lifecycleScope.launch {
            viewModel.updateNegativeAlf(positive_alfanar.toInt())
        }
        updateCumulativeHours()
    }
    fun updatenegative_sub(positive_sub: String) {
        lifecycleScope.launch {
            viewModel.updateNegativeSub(positive_sub.toInt())
        }
        updateCumulativeHours()
    }
    fun update_pending_alfanar(positive_alfanar: String) {
        lifecycleScope.launch {
            viewModel.updatePendingAlf(positive_alfanar.toInt())
        }
        updateCumulativeHours()
    }
    fun update_pending_sub(positive_sub: String) {
        lifecycleScope.launch {
            viewModel.updatePendingSub(positive_sub.toInt())
        }
        updateCumulativeHours()
    }
    fun update_closed_alfanar(positive_alfanar: String) {
        lifecycleScope.launch {
            viewModel.updateClosedAlf(positive_alfanar.toInt())
        }
        updateCumulativeHours()
    }
    fun update_closed_sub(positive_sub: String) {
        lifecycleScope.launch {
            viewModel.updateClosedSub(positive_sub.toInt())
        }
        updateCumulativeHours()
    }
}