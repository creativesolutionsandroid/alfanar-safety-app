package com.cs.alfanar.ui.emailverification

data class EmailVerificationFormState(val otpError: Int? = null,
                                   val isDataValid: Boolean = false)