package com.cs.alfanar.ui.nearMissIncident.stage2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class NearMissIncidentStage2ViewModelFactory (
    private val repository: NearMissIncidentStage2Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NearMissIncidentStage2ViewModel(repository) as T
    }
}