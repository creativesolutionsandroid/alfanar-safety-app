package com.cs.alfanar.ui.login

data class LoginData(
    var username: String,
    var password: String
)