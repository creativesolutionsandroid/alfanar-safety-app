package com.cs.alfanar.ui.MonthlyReport.MRStage6

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage6ViewModelFactory (
    private val repository: MonthlyReportStage6Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage6ViewModel(repository) as T
    }
}