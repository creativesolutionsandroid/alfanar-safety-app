package com.cs.alfanar.ui.MonthlyReport.MRStage9

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage9ViewModelFactory (
    private val repository: MonthlyReportStage9Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage9ViewModel(repository) as T
    }
}