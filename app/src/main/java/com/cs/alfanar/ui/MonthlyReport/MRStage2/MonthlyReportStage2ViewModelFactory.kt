package com.cs.alfanar.ui.MonthlyReport.MRStage2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MonthlyReportStage2ViewModelFactory (
    private val repository: MonthlyReportStage2Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MonthlyReportStage2ViewModel(repository) as T
    }
}