package com.cs.alfanar.ui.MonthlyReport.MRStage14

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage9Entity
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.ui.MonthlyReport.MRSummary.MonthlyReportSummaryActivity
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_monthly_report_stage9.*
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MonthlyReportStage14Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val viewModelFactory: MonthlyReportStage14ViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportStage14ViewModel

//    private val TAG : String = this::class.java.simpleName

    private lateinit var projectData: Project
    val KEY_DATA: String = "KEY_DATA"
    val KEY_BACK_ACTION : String = "KEY_BACK_ACTION"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_stage14)

        viewModel = ViewModelProvider(this, viewModelFactory).get(MonthlyReportStage14ViewModel::class.java)

        val Data = intent.getParcelableExtra(KEY_DATA) as? Project
        projectData = Data!!

        fetchDataFromDb()
        addTextWatcher()

        remarks_1.setImeOptions(EditorInfo.IME_ACTION_DONE)
        remarks_2.setImeOptions(EditorInfo.IME_ACTION_DONE)
        remarks_3.setImeOptions(EditorInfo.IME_ACTION_DONE)
        remarks_1.setRawInputType(InputType.TYPE_CLASS_TEXT)
        remarks_2.setRawInputType(InputType.TYPE_CLASS_TEXT)
        remarks_3.setRawInputType(InputType.TYPE_CLASS_TEXT)
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onSubmitClicked(view: View) {
        lifecycleScope.launch {
            val data = viewModel.getData()
            if (data[15].UnitOne.equals("")) {
                tanker_trip_1.error = getString(R.string.validation_error_empty_field)
            }
            else if (data[15].UnitTwo.equals("")) {
                capacity_1.error = getString(R.string.validation_error_empty_field)
            }
//            else if (data[15].Remarks.equals("")) { /*Commented on 06-01-2020 after client request*/
//                remarks_1.error = getString(R.string.validation_error_empty_field)
//            }
            else if (data[16].UnitOne.equals("")) {
                tanker_trip_2.error = getString(R.string.validation_error_empty_field)
            }
            else if (data[16].UnitTwo.equals("")) {
                capacity_2.error = getString(R.string.validation_error_empty_field)
            }
//            else if (data[16].Remarks.equals("")) { /*Commented on 06-01-2020 after client request*/
//                remarks_2.error = getString(R.string.validation_error_empty_field)
//            }
            else if (data[17].UnitOne.equals("")) {
                tanker_trip_3.error = getString(R.string.validation_error_empty_field)
            }
            else if (data[17].UnitTwo.equals("")) {
                capacity_3.error = getString(R.string.validation_error_empty_field)
            }
//            else if (data[17].Remarks.equals("")) { /*Commented on 06-01-2020 after client request*/
//                remarks_3.error = getString(R.string.validation_error_empty_field)
//            }
            else {
                validateMonthlyData()
            }
        }
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 15) {
                tanker_trip_1.setText(projectData[15].UnitOne)
                capacity_1.setText(projectData[15].UnitTwo)
                remarks_1.setText(projectData[15].Remarks)
                tanker_trip_2.setText(projectData[16].UnitOne)
                capacity_2.setText(projectData[16].UnitTwo)
                remarks_2.setText(projectData[16].Remarks)
                tanker_trip_3.setText(projectData[17].UnitOne)
                capacity_3.setText(projectData[17].UnitTwo)
                remarks_3.setText(projectData[17].Remarks)
            }
            else {
                val reportOne = MRStage9Entity(16, "", "", "","","")
                viewModel.saveData(reportOne)
                val reportTwo = MRStage9Entity(17, "", "", "","","")
                viewModel.saveData(reportTwo)
                val reportThree = MRStage9Entity(18, "", "", "","","")
                viewModel.saveData(reportThree)
            }
            tanker_trip_1.requestFocus()
            tanker_trip_1.setSelection(tanker_trip_1.text.length)
        }
    }

    fun addTextWatcher() {
        tanker_trip_1.afterTextChanged {
            if (tanker_trip_1.text.toString().length > 0) {
                updateTanker_trip_1(tanker_trip_1.text.toString())
            }
            else {
                updateTanker_trip_1("")
            }
            checkStautus()
        }
        capacity_1.afterTextChanged {
            if (capacity_1.text.toString().length > 0) {
                updateCapacity_1(capacity_1.text.toString())
            }
            else {
                updateCapacity_1("")
            }
            checkStautus()
        }
        remarks_1.afterTextChanged {
            if (remarks_1.text.toString().length > 0) {
                updateremarks_1(remarks_1.text.toString())
            }
            else {
                updateremarks_1("")
            }
            checkStautus()
        }
        tanker_trip_2.afterTextChanged {
            if (tanker_trip_2.text.toString().length > 0) {
                updatetanker_trip_2(tanker_trip_2.text.toString())
            }
            else {
                updatetanker_trip_2("")
            }
            checkStautus()
        }
        capacity_2.afterTextChanged {
            if (capacity_2.text.toString().length > 0) {
                update_capacity_2(capacity_2.text.toString())
            }
            else {
                update_capacity_2("")
            }
            checkStautus()
        }
        remarks_2.afterTextChanged {
            if (remarks_2.text.toString().length > 0) {
                update_remarks_2(remarks_2.text.toString())
            }
            else {
                update_remarks_2("")
            }
            checkStautus()
        }
        tanker_trip_3.afterTextChanged {
            if (tanker_trip_3.text.toString().length > 0) {
                update_tanker_trip_3(tanker_trip_3.text.toString())
            }
            else {
                update_tanker_trip_3("")
            }
            checkStautus()
        }
        capacity_3.afterTextChanged {
            if (capacity_3.text.toString().length > 0) {
                update_capacity_3(capacity_3.text.toString())
            }
            else {
                update_capacity_3("")
            }
            checkStautus()
        }
        remarks_3.afterTextChanged {
            if (remarks_3.text.toString().length > 0) {
                update_remarks_3(remarks_3.text.toString())
            }
            else {
                update_remarks_3("")
            }
            checkStautus()
        }
    }

    fun updateTanker_trip_1(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateUnitOne(tanker_trip_1, 16)
        }
    }
    //
    fun updateCapacity_1(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateUnitTwo(tanker_trip_1, 16)
        }
    }
    fun updateremarks_1(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateRemarks(tanker_trip_1, 16)
        }
    }
    fun updatetanker_trip_2(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateUnitOne(tanker_trip_1, 17)
        }
    }
    fun update_capacity_2(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateUnitTwo(tanker_trip_1, 17)
        }
    }
    fun update_remarks_2(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateRemarks(tanker_trip_1, 17)
        }
    }
    fun update_tanker_trip_3(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateUnitOne(tanker_trip_1, 18)
        }
    }
    fun update_capacity_3(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateUnitTwo(tanker_trip_1, 18)
        }
    }
    fun update_remarks_3(tanker_trip_1: String) {
        lifecycleScope.launch {
            viewModel.updateRemarks(tanker_trip_1, 18)
        }
    }

    fun checkStautus() {
        lifecycleScope.launch {
            val data = viewModel.getData()
            if (!data[15].UnitOne.equals("") && !data[15].UnitTwo.equals("") &&
                !data[15].Remarks.equals("") && !data[16].UnitOne.equals("") &&
                !data[16].UnitTwo.equals("") && !data[16].Remarks.equals("") &&
                !data[17].UnitOne.equals("") && !data[17].UnitTwo.equals("") && !data[17].Remarks.equals("")) {
                viewModel.updateStage9(true)
            }
            else {
                viewModel.updateStage9(false)
            }
        }
    }

    fun validateMonthlyData() {
        lifecycleScope.launch {
            val DashboardData = viewModel.getDashboardData()
            val projectData = viewModel.getProjectData()
            val stage1Data = viewModel.getStage1Data()
            val stage2Data = viewModel.getStage2Data()
            val stage3Data = viewModel.getStage3Data()
            val stage4Data = viewModel.getStage4Data()
            val stage5Data = viewModel.getStage5Data()
            val stage5EventData = viewModel.getMRStage5EventData()
            val stage6Data = viewModel.getStage6Data()
            val stage6EventData = viewModel.getMRStage6EventData()
            val stage9Data = viewModel.getStage9Data()

            if (!DashboardData[0].Stage2) {
                showOneButtonAlertDialog(getString(R.string.error_safety_team_details), getString(R.string.Ok),
                    this@MonthlyReportStage14Activity, null)
            }
            else if (stage1Data.size == 0 || stage2Data.size == 0 || stage3Data.size == 0) {
                showOneButtonAlertDialog(getString(R.string.error_monthly_cumulative_stats), getString(R.string.Ok),
                    this@MonthlyReportStage14Activity, null)
            }
            else if (!DashboardData[0].Stage3) {
                showOneButtonAlertDialog(getString(R.string.error_narration), getString(R.string.Ok),
                    this@MonthlyReportStage14Activity, null)
            }
            else if (!DashboardData[0].Stage4) {
                showOneButtonAlertDialog(getString(R.string.error_safety_highlights), getString(R.string.Ok),
                    this@MonthlyReportStage14Activity, null)
            }
            else {
                val parentObj = JSONObject()
                parentObj.put("ProjectId", projectData[0].projectId)
                parentObj.put("SubmittedBy", UserPreferences(this@MonthlyReportStage14Activity).userId())

                val stage1Obj = JSONObject()
                stage1Obj.put("SafeManHoursWorked", stage1Data[0].SafeManHoursWorked)
                stage1Obj.put("TotalManHours", stage1Data[0].TotalManHours)
                stage1Obj.put("ManPowerAvgDWAlfanar", stage1Data[0].DirectAlfanar)
                stage1Obj.put("ManPowerAvgDWSubCont", stage1Data[0].DirectSC)
                stage1Obj.put("ManPowerAvgIDOAlfanar", stage1Data[0].InDirectAlfanar)
                stage1Obj.put("ManPowerAvgIDOSubCont", stage1Data[0].InDirectSC)
                stage1Obj.put("FatalCase", stage1Data[0].Fatal)
                stage1Obj.put("LostWorkdayCase", stage1Data[0].LWC)
                stage1Obj.put("MedicalTCase", stage2Data[0].MTC)
                stage1Obj.put("PropertyDamageAlfanar", stage2Data[0].PdAlfanar)
                stage1Obj.put("PropertyDamageSubCont", stage2Data[0].PdSC)
                stage1Obj.put("NearMissAlfanar", stage2Data[0].NmAlfanar)
                stage1Obj.put("NearMissSubCont", stage2Data[0].NmSC)
                stage1Obj.put("MotorVehicleAcc", stage2Data[0].MVA)
                stage1Obj.put("NoCampInspection", stage2Data[0].CampInspection)
                stage1Obj.put("EnvrnIncidentCase", stage2Data[0].EIC)
                stage1Obj.put("EmergencyDrill", stage2Data[0].EmergencyDrillConducte)
                stage1Obj.put("MonthlyEHSAward", stage3Data[0].MonthlyEHSAward)
                stage1Obj.put("GenSickLeaveCase", stage3Data[0].GSLC)
                stage1Obj.put("ViolationsRaisedClient", stage3Data[0].Violations)
                stage1Obj.put("SiteVisitMgAlfanar", stage3Data[0].Management)
                stage1Obj.put("SiteVisitMgSubCont", stage3Data[0].Clients)
                stage1Obj.put("FireIncidentCase", stage3Data[0].FICAlfanar)
                stage1Obj.put("FireIncidentCaseSubCont", stage3Data[0].FICSub)
                stage1Obj.put("SafeManDays", stage3Data[0].SMD)
                parentObj.put("ReportDetail", stage1Obj)

                val stage4Array = JSONArray()
                val stage4AlfObj = JSONObject()
                stage4AlfObj.put("SiteEHSMgr", stage4Data[0].ManagerAlf)
                stage4AlfObj.put("SiteEHSEngg", stage4Data[0].EngineerAlf)
                stage4AlfObj.put("EHSSup", stage4Data[0].SupervisorAlf)
                stage4AlfObj.put("EHSOfficer", stage4Data[0].OfficerAlf)
                stage4AlfObj.put("Nurse", stage4Data[0].NurseAlf)
                stage4AlfObj.put("FirstAider", stage4Data[0].FirstAidAlf)
                stage4AlfObj.put("Note", stage4Data[0].Note)
                stage4AlfObj.put("CoType", 1)
                stage4Array.put(stage4AlfObj)

                val stage4SubObj = JSONObject()
                stage4SubObj.put("SiteEHSMgr", stage4Data[0].ManagerSub)
                stage4SubObj.put("SiteEHSEngg", stage4Data[0].EngineerSub)
                stage4SubObj.put("EHSSup", stage4Data[0].SupervisorSub)
                stage4SubObj.put("EHSOfficer", stage4Data[0].OfficerSub)
                stage4SubObj.put("Nurse", stage4Data[0].NurseSub)
                stage4SubObj.put("FirstAider", stage4Data[0].FirstAidSub)
                stage4SubObj.put("Note", stage4Data[0].Note)
                stage4SubObj.put("CoType", 2)
                stage4Array.put(stage4SubObj)
                parentObj.put("lstReportSafetyTeam", stage4Array)

                val stage5EventArray = JSONArray()
                for (i in 0 until stage5EventData.size) {
                    if (!stage5EventData[i].Name.equals("")) {
                        val stage5EventObj = JSONObject()
                        stage5EventObj.put("Description", stage5EventData[i].Name)
                        stage5EventObj.put("Type", 1)
                        stage5EventArray.put(stage5EventObj)
                    }
                }
                for (i in 0 until stage6EventData.size) {
                    if (!stage6EventData[i].Name.equals("")) {
                        val stage5EventObj = JSONObject()
                        stage5EventObj.put("Description", stage6EventData[i].Name)
                        stage5EventObj.put("Type", 2)
                        stage5EventArray.put(stage5EventObj)
                    }
                }
                parentObj.put("lstReportNarrationHighlights", stage5EventArray)

                val stage5Obj = JSONObject()
                stage5Obj.put("SiteMgmntSafetyWalk", stage5Data[0].SiteManagement)
                stage5Obj.put("SafetyAudits", stage5Data[0].SafetyAudits)
                stage5Obj.put("HighMgmntWalk", stage5Data[0].HighManagement)
                stage5Obj.put("AdminFieldSafetyAudit", stage5Data[0].Administrative)
                parentObj.put("ReportMonthlyOtherEHSActivity", stage5Obj)

                val stage6Array = JSONArray()
                val stage6AlfObj = JSONObject()
                stage6AlfObj.put("ObsPositive", stage6Data[0].PositiveAlf)
                stage6AlfObj.put("ObsNegative", stage6Data[0].NegativeAlf)
                stage6AlfObj.put("RemarksPending", stage6Data[0].PendingAlf)
                stage6AlfObj.put("RemarksClosed", stage6Data[0].ClosedAlf)
                stage6AlfObj.put("CoType", 1)
                stage6Array.put(stage6AlfObj)

                val stage6SubObj = JSONObject()
                stage6SubObj.put("ObsPositive", stage6Data[0].PositiveSub)
                stage6SubObj.put("ObsNegative", stage6Data[0].NegativeSub)
                stage6SubObj.put("RemarksPending", stage6Data[0].PendingSub)
                stage6SubObj.put("RemarksClosed", stage6Data[0].ClosedSub)
                stage6SubObj.put("CoType", 2)
                stage6Array.put(stage6SubObj)
                parentObj.put("lstReportObservationFindings", stage6Array)

                val stage9Obj = JSONObject()
                val stage9Obj1 = JSONObject()
                stage9Obj1.put("TankerTrips", stage9Data[0].UnitOne)
                stage9Obj1.put("Capacity", stage9Data[0].UnitTwo)
                stage9Obj1.put("Remarks", stage9Data[0].Remarks)
                stage9Obj.put("SweetWaterTankers",stage9Obj1.toString())

                val stage9Obj2 = JSONObject()
                stage9Obj2.put("TankerTrips", stage9Data[1].UnitOne)
                stage9Obj2.put("Capacity", stage9Data[1].UnitTwo)
                stage9Obj2.put("Remarks", stage9Data[1].Remarks)
                stage9Obj.put("TapWaterTankers",stage9Obj2.toString())

                val stage9Obj3 = JSONObject()
                stage9Obj3.put("TankerTrips", stage9Data[2].UnitOne)
                stage9Obj3.put("Capacity", stage9Data[2].UnitTwo)
                stage9Obj3.put("Remarks", stage9Data[2].Remarks)
                stage9Obj.put("SewageTankers",stage9Obj3.toString())

                val stage9Obj4 = JSONObject()
                stage9Obj4.put("Diesel", stage9Data[3].UnitOne)
                stage9Obj4.put("Petrol", stage9Data[3].UnitTwo)
                stage9Obj4.put("rmrkNoofGenerators", stage9Data[3].Remarks)
                stage9Obj.put("FuelConsumed",stage9Obj4.toString())

                val stage9Obj5 = JSONObject()
                stage9Obj5.put("Generator", stage9Data[4].UnitOne)
                stage9Obj5.put("ClientProvided", stage9Data[4].UnitTwo)
                stage9Obj5.put("Remarks", stage9Data[4].Remarks)
                stage9Obj.put("ElectricityConsumed",stage9Obj5.toString())

                val stage9Obj6 = JSONObject()
                stage9Obj6.put("DustArea", stage9Data[5].UnitOne)
                stage9Obj6.put("Spray", stage9Data[5].UnitTwo)
                stage9Obj6.put("Tankers", stage9Data[5].UnitThree)
                stage9Obj6.put("Remarks", stage9Data[5].Remarks)
                stage9Obj.put("DustControl",stage9Obj6.toString())

                val stage9Obj7 = JSONObject()
                stage9Obj7.put("Con", stage9Data[6].UnitOne)
                stage9Obj7.put("Steel", stage9Data[6].UnitTwo)
                stage9Obj7.put("Wood", stage9Data[6].UnitThree)
                stage9Obj7.put("Chem", stage9Data[6].UnitFour)
                stage9Obj7.put("OfficewastebagsRemarks", stage9Data[6].Remarks)
                stage9Obj.put("WasteConcrete",stage9Obj7.toString())

                val stage9Obj8 = JSONObject()
                stage9Obj8.put("Type", stage9Data[7].UnitOne)
                stage9Obj8.put("Quantity", stage9Data[7].UnitTwo)
                stage9Obj8.put("Remarks", stage9Data[7].Remarks)
                stage9Obj.put("QtyWaterRecycled",stage9Obj8.toString())

                val stage9Obj9 = JSONObject()
                stage9Obj9.put("dB", stage9Data[8].UnitOne)
                stage9Obj9.put("Area", stage9Data[8].UnitTwo)
                stage9Obj9.put("Remarks", stage9Data[8].Remarks)
                stage9Obj.put("NoiseLevelMonitored",stage9Obj9.toString())

                val stage9Obj10 = JSONObject()
                stage9Obj10.put("OfficeWasteRemarks", stage9Data[9].Remarks)
                stage9Obj.put("PapereRimsConsumed",stage9Obj10.toString())

                val stage9Obj11 = JSONObject()
                stage9Obj11.put("High", stage9Data[10].UnitOne)
                stage9Obj11.put("Low", stage9Data[10].UnitTwo)
                stage9Obj11.put("Remarks", stage9Data[10].Remarks)
                stage9Obj.put("DaytimeTemp",stage9Obj11.toString())

                val stage9Obj12 = JSONObject()
                stage9Obj12.put("OilDrumsUsed", stage9Data[11].UnitOne)
                stage9Obj12.put("Spillage", stage9Data[11].UnitTwo)
                stage9Obj12.put("Remarks", stage9Data[11].Remarks)
                stage9Obj.put("OilChemicalCase",stage9Obj12.toString())

                val stage9Obj13 = JSONObject()
                stage9Obj13.put("Portable", stage9Data[12].UnitOne)
                stage9Obj13.put("Fixed", stage9Data[12].UnitTwo)
                stage9Obj13.put("IncludingOfficeRemarks", stage9Data[12].Remarks)
                stage9Obj.put("PortableFixedToilets",stage9Obj13.toString())

                val stage9Obj14 = JSONObject()
                stage9Obj14.put("Skips", stage9Data[13].UnitOne)
                stage9Obj14.put("TruckTrips", stage9Data[13].UnitTwo)
                stage9Obj14.put("Remarks", stage9Data[13].Remarks)
                stage9Obj.put("SkipsOnSite",stage9Obj14.toString())

                val stage9Obj15 = JSONObject()
                stage9Obj15.put("H/SCase", stage9Data[14].UnitOne)
                stage9Obj15.put("RestRoom", stage9Data[14].UnitTwo)
                stage9Obj15.put("Remarks", stage9Data[14].Remarks)
                stage9Obj.put("HeatStressRest",stage9Obj15.toString())

                val stage9Obj16 = JSONObject()
                stage9Obj16.put("DrinkingStation", stage9Data[15].UnitOne)
                stage9Obj16.put("Coolers", stage9Data[15].UnitTwo)
                stage9Obj16.put("Remarks", stage9Data[15].Remarks)
                stage9Obj.put("DrinkingWaterStations",stage9Obj16.toString())

                val stage9Obj17 = JSONObject()
                stage9Obj17.put("UsedinSite", stage9Data[16].UnitOne)
                stage9Obj17.put("Officeused", stage9Data[16].UnitTwo)
                stage9Obj17.put("Remarks", stage9Data[16].Remarks)
                stage9Obj.put("SmokingShelters",stage9Obj17.toString())

                val stage9Obj18 = JSONObject()
                stage9Obj18.put("High", stage9Data[17].UnitOne)
                stage9Obj18.put("Low", stage9Data[17].UnitTwo)
                stage9Obj18.put("Remarks", stage9Data[17].Remarks)
                stage9Obj.put("WindSpeedMonitored",stage9Obj18.toString())

                parentObj.put("EnvironmetalDetail", stage9Obj)
//                Log.d(TAG, "validateMonthlyData: "+parentObj.toString())

                submitWeeklyReportApi(parentObj.toString())
            }
        }
    }

    private fun submitWeeklyReportApi(inputJson: String) {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.insertMonthlyReport(inputJson)
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    lifecycleScope.launch {
                        viewModel.deleteDashboardData()
                        viewModel.deleteProjectData()
                        viewModel.deleteStage1Data()
                        viewModel.deleteStage2Data()
                        viewModel.deleteStage3Data()
                        viewModel.deleteStage4Data()
                        viewModel.deleteStage5Data()
                        viewModel.deleteMRStage5EventData()
                        viewModel.deleteStage6Data()
                        viewModel.deleteMRStage6EventData()
                        viewModel.deleteStage9Data()
                    }
                    val intent = Intent(this@MonthlyReportStage14Activity, MonthlyReportSummaryActivity::class.java)
                    intent.putExtra(KEY_BACK_ACTION, "home_screen")
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@MonthlyReportStage14Activity,
                        intent
                    )
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@MonthlyReportStage14Activity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@MonthlyReportStage14Activity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@MonthlyReportStage14Activity,
                    null
                )
            }
        }
    }
}