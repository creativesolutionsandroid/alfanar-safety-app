package com.cs.alfanar.ui.accidentAndIncident.Summary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AccidentIncidentSummaryViewModelFactory (
    private val repository: AccidentIncidentSummaryRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AccidentIncidentSummaryViewModel(repository) as T
    }
}