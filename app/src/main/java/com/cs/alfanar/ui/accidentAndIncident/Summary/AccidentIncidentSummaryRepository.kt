package com.cs.alfanar.ui.accidentAndIncident.Summary

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.accidentIncidentSummary.AccidentIncidentSummaryResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class AccidentIncidentSummaryRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getAccidentIncidentSummary(inputJson : String): AccidentIncidentSummaryResponse {
        return apiRequest { api.getAccidentIncidentSummary(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun UpdateReportStatus(inputJson : String): BasicResponse {
        return apiRequest { api.updateAccidentIncidentStatus(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}