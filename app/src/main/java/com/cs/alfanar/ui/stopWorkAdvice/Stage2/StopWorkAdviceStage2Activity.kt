package com.cs.alfanar.ui.stopWorkAdvice.Stage2

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.InputType
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkAdviceEntity
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkCommunicationEntity
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.ui.mainActivity.MainActivity
import com.cs.alfanar.ui.nearMissIncident.Summary.NearMissIncidentSummaryActivity
import com.cs.alfanar.ui.stopWorkAdvice.Summary.StopWorkAdviceSummaryActivity
import com.cs.alfanar.ui.stopWorkAdvice.SummaryDetails.StopWorkAdviceDetailsActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_near_miss_report_stage2.*
import kotlinx.android.synthetic.main.activity_stop_work_advice_step2.*
import kotlinx.android.synthetic.main.activity_stop_work_advice_step2.employee_designation
import kotlinx.android.synthetic.main.activity_stop_work_advice_step2.no_checkbox
import kotlinx.android.synthetic.main.activity_stop_work_advice_step2.upload_image
import kotlinx.android.synthetic.main.activity_stop_work_advice_step2.yes_checkbox
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

class StopWorkAdviceStage2Activity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: StopWorkAdviceStage2ViewModelFactory by instance()

    private lateinit var viewModel: StopWorkAdviceStage2ViewModel

//    private val TAG : String = this::class.java.simpleName

    private var projectData: List<StopWorkAdviceEntity> = mutableListOf()
    private var communicationList: List<StopWorkCommunicationEntity> = mutableListOf()
    private var informationList: List<StopWorkCommunicationEntity> = mutableListOf()

    private lateinit var communicationAdapter: StopWorkCommunicationAdapter
    private lateinit var informationAdapter: StopWorkInformtionAdapter

    private val STORAGE_REQUEST: Int = 1001
    private val CAMERA_REQUEST: Int = 1002
    private val PICK_IMAGE_FROM_CAMERA = 1
    private val PICK_IMAGE_FROM_GALLERY = 2

    var isCamera = false
    var thumbnail: Bitmap? = null

    var pictureName: String? = null
    var base64String = ""
    val KEY_BACK_ACTION : String = "KEY_BACK_ACTION"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stop_work_advice_step2)

        viewModel = ViewModelProvider(this, factory).get(StopWorkAdviceStage2ViewModel::class.java)

        employee_name.setImeOptions(EditorInfo.IME_ACTION_DONE)
        employee_name.setRawInputType(InputType.TYPE_CLASS_TEXT)

        initRecyclerView()
        fetchDataFromDb()
        employee_name.afterTextChanged { updateDescription(employee_name.text.toString().trim()) }

        no_checkbox.setOnClickListener {
            if (no_checkbox.isChecked) {
                pictureName = ""
                base64String = ""
                updateImage()
                upload_image.setImageDrawable(getDrawable(R.drawable.ic_upload_image))
            }
        }
    }

    fun onHomeIconClicked(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onImageClicked(view: View) {
//        if (yes_checkbox.isChecked) {
            checkStoragePermission()
//        }
    }

    fun onSubmitClicked(view: View) {
        validationCheck()
    }

    fun updateDescription(Name: String) {
        lifecycleScope.launch {
            viewModel.updateDescription(Name)
        }
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            projectData = viewModel.getData()
            employee_name.setText(projectData[0].Description)
            communicationList = viewModel.getCommunicationData()
            informationList = viewModel.getInformationData()

            communicationAdapter.setList(communicationList, this@StopWorkAdviceStage2Activity)
            communicationAdapter.notifyDataSetChanged()

            informationAdapter.setList(informationList, this@StopWorkAdviceStage2Activity)
            informationAdapter.notifyDataSetChanged()

            base64String = projectData[0].Image
            pictureName = projectData[0].ImageName
            if (projectData[0].ImageName.equals(""))
                no_checkbox.isChecked = true
            else
                yes_checkbox.isChecked = true
            displayImage()
        }
    }

    private fun initRecyclerView(){
        list_communication.layoutManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        itemDecoration.setDrawable(resources.getDrawable(R.drawable.recycler_divider_16dp, null))
        list_communication.addItemDecoration(itemDecoration)

        communicationAdapter = StopWorkCommunicationAdapter()
        list_communication.adapter = communicationAdapter

        list_information.layoutManager = LinearLayoutManager(this)
        list_information.addItemDecoration(itemDecoration)

        informationAdapter = StopWorkInformtionAdapter()
        list_information.adapter = informationAdapter
    }

    fun onCommunicationAdded(view: View) {
        val name = employee_Advice_name.text.trim()
        val designation = employee_designation.text.trim()
        val mode = employee_mode.text.trim()

        if (name.length == 0) {
            showOneButtonAlertDialog(
                getString(R.string.please_enter_name), getString(R.string.Ok),
                this@StopWorkAdviceStage2Activity, null
            )
        }
        else if (designation.length == 0) {
            showOneButtonAlertDialog(
                getString(R.string.please_enter_designtion), getString(R.string.Ok),
                this@StopWorkAdviceStage2Activity, null
            )
        }
        else if (mode.length == 0) {
            showOneButtonAlertDialog(
                getString(R.string.please_enter_mode), getString(R.string.Ok),
                this@StopWorkAdviceStage2Activity, null
            )
        }
        else {
            val idToInsert = communicationList.size + informationList.size
            val data = StopWorkCommunicationEntity((idToInsert + 1), 1, name.toString(), designation.toString(), mode.toString())
            lifecycleScope.launch {
                viewModel.saveData(data)
                fetchDataFromDb()
            }

            employee_Advice_name.setText("")
            employee_designation.setText("")
            employee_mode.setText("")
        }
    }

    fun onInformationAdded(view: View) {
        val name = employee_information_Advice_name.text.trim()
        val designation = employee_information_designation.text.trim()
        val mode = employee_information_mode.text.trim()

        if (name.length == 0) {
            showOneButtonAlertDialog(
                getString(R.string.please_enter_name), getString(R.string.Ok),
                this@StopWorkAdviceStage2Activity, null
            )
        }
        else if (designation.length == 0) {
            showOneButtonAlertDialog(
                getString(R.string.please_enter_designtion), getString(R.string.Ok),
                this@StopWorkAdviceStage2Activity, null
            )
        }
        else if (mode.length == 0) {
            showOneButtonAlertDialog(
                getString(R.string.please_enter_mode), getString(R.string.Ok),
                this@StopWorkAdviceStage2Activity, null
            )
        }
        else {
            val idToInsert = communicationList.size + informationList.size
            val data = StopWorkCommunicationEntity((idToInsert + 1), 2, name.toString(), designation.toString(), mode.toString())
            lifecycleScope.launch {
                viewModel.saveData(data)
                fetchDataFromDb()
            }

            employee_information_Advice_name.setText("")
            employee_information_designation.setText("")
            employee_information_mode.setText("")
        }
    }

    private fun checkStoragePermission() {
        if (!canAccessStorage()) {
            requestStoragePermission()
        }
        else if (!canAccessStorage()) {
            requestCameraPermission()
        }
        else {
            ShowImageSelectionPopup()
        }
    }

    private fun canAccessStorage(): Boolean {
        if ((ContextCompat.checkSelfPermission(
                this@StopWorkAdviceStage2Activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) ===
                    PackageManager.PERMISSION_GRANTED)) {
            return true
        }
        return false
    }

    private fun canAccessCamera(): Boolean {
        if ((ContextCompat.checkSelfPermission(
                this@StopWorkAdviceStage2Activity,
                Manifest.permission.CAMERA
            ) ===
                    PackageManager.PERMISSION_GRANTED)) {
            return true
        }
        return false
    }

    private fun requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this@StopWorkAdviceStage2Activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )) {
            ActivityCompat.requestPermissions(
                this@StopWorkAdviceStage2Activity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_REQUEST
            )
        } else {
            ActivityCompat.requestPermissions(
                this@StopWorkAdviceStage2Activity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_REQUEST
            )
        }
    }

    private fun requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this@StopWorkAdviceStage2Activity,
                Manifest.permission.CAMERA
            )) {
            ActivityCompat.requestPermissions(
                this@StopWorkAdviceStage2Activity,
                arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST
            )
        } else {
            ActivityCompat.requestPermissions(
                this@StopWorkAdviceStage2Activity,
                arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            STORAGE_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    if (!canAccessStorage()) {
                        toast("Storage permission denied, unable to upload photo")
                    } else if (!canAccessCamera()) {
                        requestCameraPermission()
                    }
                } else {
                    toast("Storage permission denied, unable to upload photo")
                }
                return
            }

            CAMERA_REQUEST -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    if (!canAccessCamera()) {
                        toast("Camera permission denied, unable to upload photo")
                    } else {
                        ShowImageSelectionPopup()
                    }
                } else {
                    toast("Camera permission denied, unable to upload photo")
                }
                return
            }
        }
    }

    fun ShowImageSelectionPopup() {
        var customDialog: AlertDialog? = null
        val dialogBuilder = AlertDialog.Builder(this@StopWorkAdviceStage2Activity)
        // ...Irrelevant code for customizing the buttons and title
        val inflater = layoutInflater
        val layout = R.layout.alert_dialog_camera

        val dialogView = inflater.inflate(layout, null)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setCancelable(false)
        val gallery_layout = dialogView.findViewById<View>(R.id.gallery_layout) as TextView
        val camera_layout = dialogView.findViewById<View>(R.id.camera_layout) as TextView
        val cancel = dialogView.findViewById<View>(R.id.cancel) as TextView
        customDialog = dialogBuilder.create()
        customDialog.show()
        val finalCustomDialog = customDialog

        camera_layout.setOnClickListener {
            openCamera()
            finalCustomDialog!!.dismiss()
        }
        gallery_layout.setOnClickListener {
            openGallery()
            finalCustomDialog!!.dismiss()
        }
        cancel.setOnClickListener { finalCustomDialog!!.dismiss() }

        val lp = WindowManager.LayoutParams()
        val window = customDialog.window
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        lp.copyFrom(window.attributes)
        //This makes the dialog take up the full width
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val screenWidth = size.x
        val d = screenWidth * 0.85
        lp.width = d.toInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.attributes = lp
    }

    fun openGallery() {
        val currentapiVersion = Build.VERSION.SDK_INT
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessStorage()) {
                requestStoragePermission()
                isCamera = false
            } else {
                val intent = Intent()
                intent.type = "image/*"
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(intent, "Select Picture"),
                    PICK_IMAGE_FROM_GALLERY
                )
            }
        } else {
            val intent = Intent()
            intent.type = "image/*"
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false)
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                PICK_IMAGE_FROM_GALLERY
            )
        }
    }

    fun openCamera() {
        val currentapiVersion = Build.VERSION.SDK_INT
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            if (!canAccessCamera()) {
                isCamera = true
                requestCameraPermission()
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(
                    cameraIntent,
                    PICK_IMAGE_FROM_CAMERA
                )
            }
        } else {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(
                cameraIntent,
                PICK_IMAGE_FROM_CAMERA
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_IMAGE_FROM_CAMERA && resultCode == RESULT_OK) {
            thumbnail = data!!.extras!!["data"] as Bitmap?

            val originalWidth = thumbnail!!.width
            val originalHeight = thumbnail!!.height
            val outputWidth = (originalWidth * 0.75).toInt()
            val outputHeight = (originalHeight * 0.75).toInt()

            thumbnail = Bitmap.createScaledBitmap(
                thumbnail!!, outputWidth, outputHeight,
                false
            )

            val stream = ByteArrayOutputStream()
            thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 60, stream)
            val imageBytes = stream.toByteArray()
            base64String = Base64.encodeToString(imageBytes, Base64.DEFAULT)

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(Date())
            pictureName = "Image_$timeStamp.jpg"

            updateImage()
            displayImage()
        }

        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            val uri = data!!.data
            try {
                thumbnail = MediaStore.Images.Media.getBitmap(contentResolver, uri)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            val originalWidth = thumbnail!!.width
            val originalHeight = thumbnail!!.height
            val outputWidth = (originalWidth * 0.75).toInt()
            val outputHeight = (originalHeight * 0.75).toInt()
            thumbnail = Bitmap.createScaledBitmap(
                thumbnail!!, outputWidth, outputHeight,
                false
            )

            val stream = ByteArrayOutputStream()
            thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 60, stream)
            val imageBytes = stream.toByteArray()
            base64String = Base64.encodeToString(imageBytes, Base64.DEFAULT)

            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US).format(Date())
            pictureName = "Image_$timeStamp.jpg"

            updateImage()
            displayImage()
        }
    }

    fun displayImage() {
        val imageBytes = Base64.decode(base64String, Base64.DEFAULT)
        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        upload_image.setImageBitmap(decodedImage)
    }

    fun updateImage() {
        lifecycleScope.launch {
            viewModel.updateImage(base64String, pictureName!!)
        }
    }

    fun validationCheck() {
        lifecycleScope.launch {
            projectData = viewModel.getData()

            if (projectData[0].Description.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_enter_stop_work_description), getString(R.string.Ok),
                    this@StopWorkAdviceStage2Activity, null
                )
            }
//            else if (communicationList.size == 0) {
//                showOneButtonAlertDialog(
//                    getString(R.string.please_enter_stop_work_communicated_to), getString(R.string.Ok),
//                    this@StopWorkAdviceStage2Activity, null
//                )
            else if (pictureName.equals("")) {
                showOneButtonAlertDialog(
                    getString(R.string.please_upload_sketch_image), getString(R.string.Ok),
                    this@StopWorkAdviceStage2Activity, null
                )
            }
            else {
               insertStopWorkDataApi()
//                prepareInputJsonToGetMasterData()
            }
        }
    }

    fun getFormattedDate(date: String): String {
        val calendar = Calendar.getInstance()
        calendar.time = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US).parse(date)
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US).format(calendar.time)
    }

    fun prepareInputJsonToGetMasterData(): String {
        val parentObj = JSONObject()
        parentObj.put("ProjectId", projectData[0].projectId)
//        parentObj.put("Location", projectData[0].Location)
        parentObj.put("ReportDate", getFormattedDate(projectData[0].Date + " " + projectData[0].Time))
        parentObj.put("SubmittedBy", UserPreferences(this).userId())

        val stopWorkAdviceObj = JSONObject()
        stopWorkAdviceObj.put("HSERepresentative", projectData[0].HseRepresentative)
        stopWorkAdviceObj.put("AcivitySupervisor", projectData[0].NameOfSupervisor)
        stopWorkAdviceObj.put("Designation", projectData[0].Designation)
        stopWorkAdviceObj.put("StopDateTime", getFormattedDate(projectData[0].Date + " " + projectData[0].Time))
        stopWorkAdviceObj.put("StopWorkDescription", projectData[0].Description)

        val photoObj = JSONObject()
//        if (yes_checkbox.isChecked) {
            photoObj.put("FileName", projectData[0].ImageName)
            photoObj.put("FileUploadLocation", "/StopWorkAdvice/")
            photoObj.put("FileURLLocation", "/StopWorkAdvice/")
            photoObj.put("Base64FileData", projectData[0].Image)
            stopWorkAdviceObj.put("SketchImageAttr", photoObj)
//        }
//        else {
//            stopWorkAdviceObj.put("SketchImageAttr", "null")
//        }
        parentObj.put("StopWorkAdviceDetail", stopWorkAdviceObj)

        val communicationArray = JSONArray()
        for (i in 0 until communicationList.size) {
            val communicationObj = JSONObject()
            communicationObj.put("StopWorkType", 1)
            communicationObj.put("Name", communicationList[i].Name)
            communicationObj.put("Designation", communicationList[i].Designation)
            communicationObj.put("Mode", communicationList[i].Mode)
            communicationArray.put(communicationObj)
        }

        for (i in 0 until informationList.size) {
            val communicationObj = JSONObject()
            communicationObj.put("StopWorkType", 2)
            communicationObj.put("Name", informationList[i].Name)
            communicationObj.put("Designation", informationList[i].Designation)
            communicationObj.put("Mode", informationList[i].Mode)
            communicationArray.put(communicationObj)
        }
        parentObj.put("StopWorkAdviceCommunication", communicationArray)

//        Log.d(TAG, "prepareInputJsonToGetMasterData: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun insertStopWorkDataApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.InsertStopWorkAdviceReport(
                    prepareInputJsonToGetMasterData()
                )
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()

                    lifecycleScope.launch {
                        viewModel.deleteCommunicationData()
                        viewModel.deleteStopWorkData()
                    }

                    val intent = Intent(
                        this@StopWorkAdviceStage2Activity,
                        StopWorkAdviceSummaryActivity::class.java
                    )
                    intent.putExtra(KEY_BACK_ACTION, "home_screen")
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@StopWorkAdviceStage2Activity,
                        intent
                    )
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@StopWorkAdviceStage2Activity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@StopWorkAdviceStage2Activity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@StopWorkAdviceStage2Activity,
                    null
                )
            }
        }
    }
}