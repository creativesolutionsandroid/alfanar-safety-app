package com.cs.alfanar.ui.nearMissIncident.stage1

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class NearMissIncidentStage1ViewModelFactory (
    private val repository: NearMissIncidentStage1Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NearMissIncidentStage1ViewModel(repository) as T
    }
}