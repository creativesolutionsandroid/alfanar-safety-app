package com.cs.alfanar.ui.changePassword

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.databinding.ActivityChangePasswordBinding
import com.cs.alfanar.utils.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ChangePasswordActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: ChangePasswordViewModelFactory by instance()

    private lateinit var viewModel: ChangePasswordViewModel
    private lateinit var binding: ActivityChangePasswordBinding

//    private val TAG : String = this::class.java.simpleName
    private var isTextWatchersEnabled: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password)

        viewModel = ViewModelProvider(this, factory).get(ChangePasswordViewModel::class.java)

        viewModel.changePasswordFormState.observe(this, Observer {
            val changePasswordFormState: ChangePasswordFormState = it ?: return@Observer
            if (changePasswordFormState.oldPasswordError != null) {
                binding.etOldPassword.error = getString(changePasswordFormState.oldPasswordError)
            }
            else if (changePasswordFormState.newPasswordError != null) {
                binding.etPassword.error = getString(changePasswordFormState.newPasswordError)
            }
            else if (changePasswordFormState.confirmPasswordError != null) {
                binding.etConfirmPassword.error = getString(changePasswordFormState.confirmPasswordError)
            }
            if (changePasswordFormState.isDataValid) {
                changePasswordApi()
            }
        })
    }
    
    fun OnSubmitClicked(view: View) {
        doValidateAndSubmit()
    }
    
    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun doValidateAndSubmit() {
        viewModel.DataChanged(binding.etOldPassword.text.toString(),binding.etPassword.text.toString(), binding.etConfirmPassword.text.toString())

        // Checking and enabling text watchers
//        if (!isTextWatchersEnabled)
//            addTextWatchers()
    }

    fun addTextWatchers() {
        isTextWatchersEnabled = true

        binding.etOldPassword.afterTextChanged {
            binding.inputOldPasswordLayout.isErrorEnabled = false
            doValidateAndSubmit()
        }

        binding.etPassword.afterTextChanged {
            binding.inputPasswordLayout.isErrorEnabled = false
            doValidateAndSubmit()
        }

        binding.etConfirmPassword.afterTextChanged {
            binding.inputConfirmPasswordLayout.isErrorEnabled = false
            doValidateAndSubmit()
        }
    }

    fun prepareInputJsonForResetPassword(): String {
        val parentObj = JSONObject()

        val oldPassword: String = binding.etOldPassword.text.toString().trim()
        val newPassword: String = binding.etPassword.text.toString().trim()

        parentObj.put("Id", UserPreferences(this).userId())
        parentObj.put("OldPassword", oldPassword)
        parentObj.put("NewPassword", newPassword)

//        Log.d(TAG, "prepareInputJsonForLogin: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun changePasswordApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.changePassword(prepareInputJsonForResetPassword())
                closeLoadingDialog()
                if (response.Status) {
                    toast(response.MessageEn)
                    finish()
                }
                else {
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@ChangePasswordActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@ChangePasswordActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@ChangePasswordActivity,
                    null
                )
            }
        }
    }
}