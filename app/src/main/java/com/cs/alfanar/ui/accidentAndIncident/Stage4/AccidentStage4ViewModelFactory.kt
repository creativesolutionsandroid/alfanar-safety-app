package com.cs.alfanar.ui.accidentAndIncident.Stage4

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class AccidentStage4ViewModelFactory (
    private val repository: AccidentStage4Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AccidentStage4ViewModel(repository) as T
    }
}