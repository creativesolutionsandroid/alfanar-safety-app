package com.cs.alfanar.ui.MonthlyReport.MRStage14

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.monthlyReport.*
import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class MonthlyReportStage14Repository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    suspend fun insertMonthlyReport(inputJson : String): BasicResponse {
        return apiRequest { api.insertMonthlyReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // database requests
    suspend fun saveData(projectData: MRStage9Entity) = db.getMRStage9Dao().insert(projectData)
    suspend fun updateUnitOne(Unit: String, TypeId: Int) = db.getMRStage9Dao().updateUnitOne(Unit, TypeId)
    suspend fun updateUnitTwo(Unit: String, TypeId: Int) = db.getMRStage9Dao().updateUnitTwo(Unit, TypeId)
    suspend fun updateUnitThree(Unit: String, TypeId: Int) = db.getMRStage9Dao().updateUnitThree(Unit, TypeId)
    suspend fun updateUnitFour(Unit: String, TypeId: Int) = db.getMRStage9Dao().updateUnitFour(Unit, TypeId)
    suspend fun updateRemarks(Unit: String, TypeId: Int) = db.getMRStage9Dao().updateRemarks(Unit, TypeId)
    suspend fun getData() : List<MRStage9Entity> { return db.getMRStage9Dao().getData() }
    suspend fun updateStage9(Stage9: Boolean) = db.getMRDashboardDao().updateStage9(Stage9)

    suspend fun getDashboardData() : List<MRDashboardEntity> { return db.getMRDashboardDao().getData() }
    suspend fun getProjectData() : List<MRProjectionSelectionEntity> { return db.getMRProjectionSelectionDao().getData() }
    suspend fun getStage1Data() : List<MRStage1Entity> { return db.getMRStage1Dao().getData() }
    suspend fun getStage2Data() : List<MRStage2Entity> { return db.getMRStage2Dao().getData() }
    suspend fun getStage3Data() : List<MRStage3Entity> { return db.getMRStage3Dao().getData() }
    suspend fun getStage4Data() : List<MRStage4Entity> { return db.getMRStage4Dao().getData() }
    suspend fun getStage5Data() : List<MRStage5Entity> { return db.getMRStage5Dao().getData() }
    suspend fun getMRStage5EventData() : List<MRstage5EventsEntity> { return db.getMRStage5EventDao().getData() }
    suspend fun getStage6Data() : List<MRStage6Entity> { return db.getMRStage6Dao().getData() }
    suspend fun getMRStage6EventData() : List<MRstage6EventsEntity> { return db.getMRStage6EventDao().getData() }

    suspend fun deleteDashboardData() = db.getMRDashboardDao().deleteData()
    suspend fun deleteProjectData() = db.getMRProjectionSelectionDao().deleteData()
    suspend fun deleteStage1Data() = db.getMRStage1Dao().deleteData()
    suspend fun deleteStage2Data() = db.getMRStage2Dao().deleteData()
    suspend fun deleteStage3Data() = db.getMRStage3Dao().deleteData()
    suspend fun deleteStage4Data() = db.getMRStage4Dao().deleteData()
    suspend fun deleteStage5Data() = db.getMRStage5Dao().deleteData()
    suspend fun deleteMRStage5EventData() = db.getMRStage5EventDao().deleteData()
    suspend fun deleteStage6Data() = db.getMRStage6Dao().deleteData()
    suspend fun deleteMRStage6EventData() = db.getMRStage6EventDao().deleteData()
    suspend fun deleteStage9Data() = db.getMRStage9Dao().deleteData()
}