package com.cs.alfanar.ui.signup

/**
 * Data validation state of the signup form.
 */
data class SignUpFormState(val employeeFileError: Int? = null,
                           val nameError: Int? = null,
                           val emailError: Int? = null,
                           val mobileError: Int? = null,
                           val passwordError: Int? = null,
                           val confirmPasswordError: Int? = null,
                           val isDataValid: Boolean = false)