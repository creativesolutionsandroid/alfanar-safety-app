package com.cs.alfanar.ui.safetySummaryReport

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.ui.MonthlyReport.MRProjectSelection.MonthlyReportProjectSelectionViewModel
import com.cs.alfanar.ui.MonthlyReport.MRProjectSelection.MonthlyReportProjectSelectionViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRSummary.MonthlyReportSummaryActivity
import com.cs.alfanar.ui.MonthlyReport.MRSummary.MonthlyReportSummaryAdapter
import com.cs.alfanar.ui.accidentAndIncident.Summary.AccidentIncidentSummaryActivity
import com.cs.alfanar.ui.dailyReportSummary.DailyReportSummaryActivity
import com.cs.alfanar.ui.nearMissIncident.Summary.NearMissIncidentSummaryActivity
import com.cs.alfanar.ui.stopWorkAdvice.Summary.StopWorkAdviceSummaryActivity
import com.cs.alfanar.ui.trainingAttendanceRecord.Summary.TrainingAttendanceRecordSummaryActivity
import com.cs.alfanar.ui.trainingAttendanceRecord.SummaryDetails.TrainingAttendanceDetailsActivity
import com.cs.alfanar.ui.trainingAttendanceRecord.SummaryDetails.TrainingAttendanceDetailsViewModel
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_monthly_report_project_selection.*
import kotlinx.android.synthetic.main.activity_safety_summary_report.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SafetySummaryReportActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: SafetySummaryReportViewModelFactory by instance()

    private lateinit var viewModel: SafetySummaryReportViewModel

//    private val TAG : String = this::class.java.simpleName

    val KEY_BACK_ACTION : String = "KEY_BACK_ACTION"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_safety_summary_report)

        viewModel = ViewModelProvider(this, factory).get(SafetySummaryReportViewModel::class.java)

        getSafetySummaryCountsApi()
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onDailyReportsClicked(view: View) {
        startActivity(Intent(this, DailyReportSummaryActivity::class.java))
    }

    fun onWeeklyReportsClicked(view: View) {
        intent = Intent(this, WeeklyReportSummaryActivity::class.java)
        intent.putExtra(KEY_BACK_ACTION, "finish")
        startActivity(intent)
    }

    fun onMonthlyReportsClicked(view: View) {
        intent = Intent(this, MonthlyReportSummaryActivity::class.java)
        intent.putExtra(KEY_BACK_ACTION, "finish")
        startActivity(intent)
    }

    fun onAccidentIncidentReportsClicked(view: View) {
        intent = Intent(this, AccidentIncidentSummaryActivity::class.java)
        intent.putExtra(KEY_BACK_ACTION, "finish")
        startActivity(intent)
    }

    fun onNearMissIncidentReportsClicked(view: View) {
        intent = Intent(this, NearMissIncidentSummaryActivity::class.java)
        intent.putExtra(KEY_BACK_ACTION, "finish")
        startActivity(intent)
    }

    fun onStopWorkReportsClicked(view: View) {
        intent = Intent(this, StopWorkAdviceSummaryActivity::class.java)
        intent.putExtra(KEY_BACK_ACTION, "finish")
        startActivity(intent)
    }

    fun onTrainingAttendanceClicked(view: View) {
        intent = Intent(this, TrainingAttendanceRecordSummaryActivity::class.java)
        intent.putExtra(KEY_BACK_ACTION, "finish")
        startActivity(intent)
    }

    fun prepareInputJsonForgetProjectData(): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())

//        Log.d(TAG, "prepareInputJsonForgetProjectData: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getSafetySummaryCountsApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getSafetySummaryReponse(prepareInputJsonForgetProjectData())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    label_daily_report_count.text = response.Data[0].DailyCount.toString()
                    label_weekly_report_count.text = response.Data[0].WeeklyCount.toString()
                    label_monthly_report_count.text = response.Data[0].MonthlyCount.toString()
                    label_accident_incident_count.text = response.Data[0].AccidentIncidentCount.toString()
                    label_near_miss_incident_count.text = response.Data[0].NearMissCount.toString()
                    label_stop_work_advice_count.text = response.Data[0].StopWorkAdviceCount.toString()
                    label_attendance_report_count.text = response.Data[0].TrainingAttendanceCount.toString()

                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@SafetySummaryReportActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@SafetySummaryReportActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@SafetySummaryReportActivity,
                    null
                )
            }
        }
    }
}