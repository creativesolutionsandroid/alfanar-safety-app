package com.cs.alfanar.ui.accidentAndIncident.Summary

import androidx.lifecycle.ViewModel
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccidentIncidentSummaryViewModel (private val repository: AccidentIncidentSummaryRepository) : ViewModel() {

    suspend fun getAccidentIncidentSummary(inputJson : String) = withContext(Dispatchers.IO) {
        repository.getAccidentIncidentSummary(inputJson)
    }

    suspend fun UpdateReportStatus(inputJson : String) = withContext(Dispatchers.IO) {
        repository.UpdateReportStatus(inputJson)
    }

}