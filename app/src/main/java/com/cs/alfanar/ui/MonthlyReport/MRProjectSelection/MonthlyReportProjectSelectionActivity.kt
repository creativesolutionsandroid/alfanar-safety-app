package com.cs.alfanar.ui.MonthlyReport.MRProjectSelection

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.cs.alfanar.R
import com.cs.alfanar.adapters.MonthlyReportProjectAdapter
import com.cs.alfanar.adapters.WeeklyReportProjectAdapter
import com.cs.alfanar.data.db.entities.monthlyReport.MRProjectionSelectionEntity
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.monthlyReportMasterData.Project
import com.cs.alfanar.ui.MonthlyReport.MRDashboard.MonthlyReportDashboardActivity
import com.cs.alfanar.ui.weeklyReport.weeklyReportDashboard.WeeklyReportDashboardActivity
import com.cs.alfanar.utils.*
import kotlinx.android.synthetic.main.activity_daily_report.*
import kotlinx.android.synthetic.main.activity_daily_report.et_location
import kotlinx.android.synthetic.main.activity_daily_report.project_id
import kotlinx.android.synthetic.main.activity_daily_report.spinner_project_name
import kotlinx.android.synthetic.main.activity_monthly_report_project_selection.*
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MonthlyReportProjectSelectionActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: MonthlyReportProjectSelectionViewModelFactory by instance()

    private lateinit var viewModel: MonthlyReportProjectSelectionViewModel

//    private val TAG : String = this::class.java.simpleName

    private var projectsList: List<Project> = mutableListOf()
    val KEY_DATA: String = "KEY_DATA"

    private var selectedProjectPos: Int = 0
    private var projectName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_monthly_report_project_selection)

        viewModel = ViewModelProvider(this, factory).get(MonthlyReportProjectSelectionViewModel::class.java)

        fetchDataFromDb()
        addTextWatcher()

        // Disabling keyboard for spinners
        spinner_project_name.setInputType(InputType.TYPE_NULL)
        et_location.setImeOptions(EditorInfo.IME_ACTION_DONE)
        et_location.setRawInputType(InputType.TYPE_CLASS_TEXT)
    }

    fun onBackButtonClicked(view: View) {
        finish()
    }

    fun onNextButtonClicked(view: View) {
//        if (et_location.text.toString().trim().length == 0) {
//            et_location.error = resources.getString(R.string.validation_error_empty_field)
//        }
//        else {
            if (projectsList.size > 0) {
                val intent = Intent(this, MonthlyReportDashboardActivity::class.java)
                intent.putExtra(KEY_DATA, projectsList.toArrayList().get(selectedProjectPos))
                startActivity(intent)
            }
//        }
    }

    fun <T> List<T>.toArrayList(): ArrayList<T>{
        return ArrayList(this)
    }

    fun fetchDataFromDb() {
        lifecycleScope.launch {
            val projectData = viewModel.getData()
            if (projectData.size > 0) {
                projectName = projectData[0].projectName
                spinner_project_name.setText(projectData[0].projectName)
                project_id.text = projectData[0].projectCode
                et_location.setText(projectData[0].location)
                getProjectDataForDailyReportApi()
            }
            else {
                val projectData = MRProjectionSelectionEntity(1, 0, "", "")
                viewModel.saveData(projectData)
                getProjectDataForDailyReportApi()
            }
        }
    }

    fun prepareInputJsonForgetProjectData(): String {
        val parentObj = JSONObject()

        parentObj.put("UserId", UserPreferences(this).userId())

//        Log.d(TAG, "prepareInputJsonForgetProjectData: " + parentObj.toString())
        return parentObj.toString()
    }

    private fun getProjectDataForDailyReportApi() {
        showLoadingDialog(this)
        lifecycleScope.launch {
            try {
                val response = viewModel.getProjectDataForMonthlyReport(prepareInputJsonForgetProjectData())
                if (response.Status) {
                    // status true from api
                    closeLoadingDialog()
                    projectsList = response.Data.Projects
                    if (projectsList.size > 0) {
                        cardView_next.alpha = 1.0f
                        cardView_next.isClickable = true
                        for (i in 1 until projectsList.size) {
                            if (projectsList[i].ProjectNameEn.equals(projectName)) {
                                selectedProjectPos = i
                                break
                            }
                        }
                        setProjectSpinnerData()
                    }
                } else {
                    // status false from api
                    closeLoadingDialog()
                    showOneButtonAlertDialog(
                        response.MessageEn,
                        getString(R.string.Ok),
                        this@MonthlyReportProjectSelectionActivity,
                        null
                    )
                }
            } catch (e: ApiException) {
                closeLoadingDialog()
                e.printStackTrace()
                showOneButtonAlertDialog(
                    getString(R.string.str_cannot_reach_server),
                    getString(R.string.Ok),
                    this@MonthlyReportProjectSelectionActivity,
                    null
                )
            } catch (e: NoInternetException) {
                closeLoadingDialog()
                showOneButtonAlertDialog(
                    getString(R.string.str_connection_error),
                    getString(R.string.Ok),
                    this@MonthlyReportProjectSelectionActivity,
                    null
                )
            }
        }
    }

    fun setProjectSpinnerData() {
        val projectAdapter: MonthlyReportProjectAdapter =
            MonthlyReportProjectAdapter(
                this,
                android.R.layout.simple_list_item_1,
                projectsList
            )
        spinner_project_name.setAdapter(projectAdapter)

        spinner_project_name.setText(projectsList.get(selectedProjectPos).ProjectNameEn) // by default first project will be selected
        project_id.text = projectsList.get(selectedProjectPos).ProjectCode
        projectName = projectsList[selectedProjectPos].ProjectNameEn
        updateProjectDetails(selectedProjectPos)

        spinner_project_name.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val selectedItem = projectsList.get(position).ProjectNameEn
                project_id.text = projectsList.get(position).ProjectCode
                spinner_project_name.setText(selectedItem)
                updateProjectDetails(position)
            }

        // Disabling keyboard for spinners
        spinner_project_name.setInputType(InputType.TYPE_NULL)
    }


    fun addTextWatcher() {
        et_location.afterTextChanged { updateLocation(et_location.text.toString().trim()) }
    }
    fun updateProjectDetails(position: Int) {
        lifecycleScope.launch {
            viewModel.updateProject(projectsList[position].ProjectId, projectsList[position].ProjectNameEn, projectsList[position].ProjectCode)
        }
    }

    fun updateLocation(location: String) {
        lifecycleScope.launch {
            viewModel.updateLocation(location)
        }
    }
}