package com.cs.alfanar.ui.MonthlyReport.MRStage5

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage5Entity
import com.cs.alfanar.data.db.entities.monthlyReport.MRstage5EventsEntity
import com.cs.alfanar.databinding.ItemWeeklyReportStage3EdittextBinding
import com.cs.alfanar.utils.afterTextChanged

class MRStage5EventAdapter(private val onCountChanged:(String, Int)->Unit)
    : RecyclerView.Adapter<OtherHealthViewHolder>() {
    private val subscribersList = ArrayList<MRstage5EventsEntity>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OtherHealthViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemWeeklyReportStage3EdittextBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_weekly_report_stage3_edittext,
                parent,
                false
            )
        return OtherHealthViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: OtherHealthViewHolder, position: Int) {
        holder.bind(subscribersList[position], onCountChanged, appContext, subscribersList.size)
    }

    fun setList(subscribers: List<MRstage5EventsEntity>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class OtherHealthViewHolder(val binding: ItemWeeklyReportStage3EdittextBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: MRstage5EventsEntity, onCountChanged:(String, Int)->Unit, context: Context, size: Int) {
        binding.edittext.setText(data.Name)
        binding.edittext.afterTextChanged { onCountChanged(binding.edittext.text.toString(), adapterPosition) }
        binding.edittext.requestFocus()
    }
}