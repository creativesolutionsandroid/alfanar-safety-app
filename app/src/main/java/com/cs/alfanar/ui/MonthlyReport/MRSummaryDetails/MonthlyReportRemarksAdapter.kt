package com.cs.alfanar.ui.MonthlyReport.MRSummaryDetails

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cs.alfanar.R
import com.cs.alfanar.databinding.ItemDailyReportRemarksBinding
import com.cs.alfanar.model.MonthlyReportDetails.JRemarksJson

class MonthlyReportRemarksAdapter: RecyclerView.Adapter<MyViewHolder>() {
    private val subscribersList = ArrayList<JRemarksJson>()
    private lateinit var appContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemDailyReportRemarksBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_daily_report_remarks,
                parent,
                false
            )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return subscribersList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(subscribersList[position], appContext)
    }

    fun setList(subscribers: List<JRemarksJson>, context: Context) {
        subscribersList.clear()
        subscribersList.addAll(subscribers)
        appContext = context
    }

}

class MyViewHolder(val binding: ItemDailyReportRemarksBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: JRemarksJson, context: Context) {

        binding.remark1.text = data.Remarks
        binding.remark1OfficerName.text = String.format(
            context.resources.getString(R.string.remarks_employee_name),
            data.EmployeeName?.split(' ')?.joinToString(" ") { it.capitalize() }
        )

    }

}