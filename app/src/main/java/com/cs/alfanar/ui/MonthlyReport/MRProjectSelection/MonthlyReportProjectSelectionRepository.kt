package com.cs.alfanar.ui.MonthlyReport.MRProjectSelection

import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.db.entities.monthlyReport.MRProjectionSelectionEntity
import com.cs.alfanar.model.monthlyReportMasterData.MonthlyMasterData
import com.cs.alfanar.model.weeklyReport.WeeklyProjectsData
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class MonthlyReportProjectSelectionRepository (
    private val api: ApiInterface,
    private val db: AppDatabase
) : SafeApiRequest() {

    //api requests
    suspend fun getProjectDataForMonthlyReport(inputJson : String): MonthlyMasterData {
        return apiRequest { api.getProjectDataForMonthlyReport(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    // database requests
    suspend fun saveData(projectData: MRProjectionSelectionEntity) = db.getMRProjectionSelectionDao().insert(projectData)

    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String) =
        db.getMRProjectionSelectionDao().updateProject(projectId, projectName, projectCode)

    suspend fun updateLocation(location: String) = db.getMRProjectionSelectionDao().updateLocation(location)

    suspend fun getData() : List<MRProjectionSelectionEntity> {
        return db.getMRProjectionSelectionDao().getData()
    }
}