package com.cs.alfanar.ui.MonthlyReport.MRDashboard

import androidx.lifecycle.ViewModel
import com.cs.alfanar.data.db.entities.monthlyReport.MRDashboardEntity

class MonthlyReportDashboardViewModel (private val repository: MonthlyReportDashboardRepository) : ViewModel() {

    // database requests
    suspend fun saveData(projectData: MRDashboardEntity) = repository.saveData(projectData)

    suspend fun getData() : List<MRDashboardEntity> {
        return repository.getData()
    }

}