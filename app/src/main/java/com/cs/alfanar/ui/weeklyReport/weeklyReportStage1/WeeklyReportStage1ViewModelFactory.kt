package com.cs.alfanar.ui.weeklyReport.weeklyReportStage1

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WeeklyReportStage1ViewModelFactory (
    private val repository: WeeklyReportStage1Repository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeeklyReportStage1ViewModel(repository) as T
    }
}