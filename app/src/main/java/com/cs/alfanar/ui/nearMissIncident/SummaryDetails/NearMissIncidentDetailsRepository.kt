package com.cs.alfanar.ui.nearMissIncident.SummaryDetails

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.accidentIncidentSummary.AccidentIncidentSummaryResponse
import com.cs.alfanar.model.nearMissIncidentSummary.NearMissIncidentSummary
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class NearMissIncidentDetailsRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getWeeklyReportSummary(inputJson : String): NearMissIncidentSummary {
        return apiRequest { api.getNearMissReportSummary(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

    suspend fun updateNearMissStatus(inputJson : String): BasicResponse {
        return apiRequest { api.updateNearMissStatus(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }
}