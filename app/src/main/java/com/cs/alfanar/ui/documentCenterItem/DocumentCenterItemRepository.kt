package com.cs.alfanar.ui.documentCenterItem

import com.cs.alfanar.model.documentCenterItemData.DocumentCenterItemResponse
import com.cs.alfanar.network.ApiInterface
import net.simplifiedcoding.mvvmsampleapp.data.network.SafeApiRequest
import okhttp3.MediaType
import okhttp3.RequestBody

class DocumentCenterItemRepository (private val api: ApiInterface) : SafeApiRequest(){

    suspend fun getDocumentsByTypeId(inputJson : String): DocumentCenterItemResponse {
        return apiRequest { api.getDocumentsByTypeId(
            RequestBody.create(MediaType.parse("application/json"), inputJson)) }
    }

}