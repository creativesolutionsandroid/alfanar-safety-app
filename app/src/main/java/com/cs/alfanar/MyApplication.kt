package com.cs.alfanar

import android.app.Application
import android.content.Context
import com.cs.alfanar.data.db.AppDatabase
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.network.ApiInterface
import com.cs.alfanar.network.GoogleApiInterface
import com.cs.alfanar.ui.MonthlyReport.MRDashboard.MonthlyReportDashboardRepository
import com.cs.alfanar.ui.MonthlyReport.MRDashboard.MonthlyReportDashboardViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRProjectSelection.MonthlyReportProjectSelectionRepository
import com.cs.alfanar.ui.MonthlyReport.MRProjectSelection.MonthlyReportProjectSelectionViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage1.MonthlyReportStage1Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage1.MonthlyReportStage1ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage10.MonthlyReportStage10Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage10.MonthlyReportStage10ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage11.MonthlyReportStage11Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage11.MonthlyReportStage11ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage12.MonthlyReportStage12Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage12.MonthlyReportStage12ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage13.MonthlyReportStage13Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage13.MonthlyReportStage13ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage14.MonthlyReportStage14Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage14.MonthlyReportStage14ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage2.MonthlyReportStage2Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage2.MonthlyReportStage2ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage3.MonthlyReportStage3Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage3.MonthlyReportStage3ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage4.MonthlyReportStage4Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage4.MonthlyReportStage4ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage5.MonthlyReportStage5Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage5.MonthlyReportStage5ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage6.MonthlyReportStage6Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage6.MonthlyReportStage6ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRStage9.MonthlyReportStage9Repository
import com.cs.alfanar.ui.MonthlyReport.MRStage9.MonthlyReportStage9ViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRSummary.MonthlyReportSummaryRepository
import com.cs.alfanar.ui.MonthlyReport.MRSummary.MonthlyReportSummaryViewModelFactory
import com.cs.alfanar.ui.MonthlyReport.MRSummaryDetails.MonthlyReportSummaryDetailsRepository
import com.cs.alfanar.ui.MonthlyReport.MRSummaryDetails.MonthlyReportSummaryDetailsViewModelFactory
import com.cs.alfanar.ui.accidentAndIncident.Stage1.AccidentStage1Repository
import com.cs.alfanar.ui.accidentAndIncident.Stage1.AccidentStage1ViewModelFactory
import com.cs.alfanar.ui.accidentAndIncident.Stage2.AccidentStage2Repository
import com.cs.alfanar.ui.accidentAndIncident.Stage2.AccidentStage2ViewModelFactory
import com.cs.alfanar.ui.accidentAndIncident.Stage3.AccidentStage3Repository
import com.cs.alfanar.ui.accidentAndIncident.Stage3.AccidentStage3ViewModelFactory
import com.cs.alfanar.ui.accidentAndIncident.Stage4.AccidentStage4Repository
import com.cs.alfanar.ui.accidentAndIncident.Stage4.AccidentStage4ViewModelFactory
import com.cs.alfanar.ui.accidentAndIncident.Summary.AccidentIncidentSummaryRepository
import com.cs.alfanar.ui.accidentAndIncident.Summary.AccidentIncidentSummaryViewModelFactory
import com.cs.alfanar.ui.accidentAndIncident.SummaryDetails.AccidentIncidentDetailsRepository
import com.cs.alfanar.ui.accidentAndIncident.SummaryDetails.AccidentIncidentDetailsViewModelFactory
import com.cs.alfanar.ui.alertNotification.AlertNotificationRepository
import com.cs.alfanar.ui.alertNotification.AlertNotificationViewModelFactory
import com.cs.alfanar.ui.alertNotificationInbox.AlertNotificationInboxRepository
import com.cs.alfanar.ui.alertNotificationInbox.AlertNotificationInboxViewModelFactory
import com.cs.alfanar.ui.alertNotificationInsert.AlertNotificationInsertRepository
import com.cs.alfanar.ui.alertNotificationInsert.AlertNotificationInsertViewModelFactory
import com.cs.alfanar.ui.changePassword.ChangePasswordRepository
import com.cs.alfanar.ui.changePassword.ChangePasswordViewModelFactory
import com.cs.alfanar.ui.dailyReport.DailyReportRepository
import com.cs.alfanar.ui.dailyReport.DailyReportViewModelFactory
import com.cs.alfanar.ui.dailyReportDetails.DailyReportDetailsRepository
import com.cs.alfanar.ui.dailyReportDetails.DailyReportDetailsViewModelFactory
import com.cs.alfanar.ui.dailyReportSummary.DailyReportSummaryRepository
import com.cs.alfanar.ui.dailyReportSummary.DailyReportSummaryViewModelFactory
import com.cs.alfanar.ui.documentCenterItem.DocumentCenterItemRepository
import com.cs.alfanar.ui.documentCenterItem.DocumentCenterItemViewModelFactory
import com.cs.alfanar.ui.documentCenterList.DocumentCenterListRepository
import com.cs.alfanar.ui.documentCenterList.DocumentCenterListViewModelFactory
import com.cs.alfanar.ui.emailverification.EmailVerificationRepository
import com.cs.alfanar.ui.emailverification.EmailVerificationViewModelFactory
import com.cs.alfanar.ui.emergencyNumbers.EmergencyNumbersRepository
import com.cs.alfanar.ui.emergencyNumbers.EmergencyNumbersViewModelFactory
import com.cs.alfanar.ui.forgotpassword.ForgotPasswordRepository
import com.cs.alfanar.ui.forgotpassword.ForgotPasswordViewModelFactory
import com.cs.alfanar.ui.login.LoginRepository
import com.cs.alfanar.ui.login.LoginViewModelFactory
import com.cs.alfanar.ui.nearMissIncident.Summary.NearMissIncidentSummaryRepository
import com.cs.alfanar.ui.nearMissIncident.Summary.NearMissIncidentSummaryViewModelFactory
import com.cs.alfanar.ui.nearMissIncident.SummaryDetails.NearMissIncidentDetailsRepository
import com.cs.alfanar.ui.nearMissIncident.SummaryDetails.NearMissIncidentDetailsViewModelFactory
import com.cs.alfanar.ui.nearMissIncident.stage1.NearMissIncidentStage1Repository
import com.cs.alfanar.ui.nearMissIncident.stage1.NearMissIncidentStage1ViewModelFactory
import com.cs.alfanar.ui.nearMissIncident.stage2.NearMissIncidentStage2Repository
import com.cs.alfanar.ui.nearMissIncident.stage2.NearMissIncidentStage2ViewModelFactory
import com.cs.alfanar.ui.nearestHospital.NearestHospitalRepository
import com.cs.alfanar.ui.nearestHospital.NearestHospitalViewModelFactory
import com.cs.alfanar.ui.nearestHospitalDirections.NearestHospitalDirectionsRepository
import com.cs.alfanar.ui.nearestHospitalDirections.NearestHospitalDirectionsViewModelFactory
import com.cs.alfanar.ui.resetpassword.ResetPasswordRespository
import com.cs.alfanar.ui.resetpassword.ResetPasswordViewModelFactory
import com.cs.alfanar.ui.safetySummaryReport.SafetySummaryReportRepository
import com.cs.alfanar.ui.safetySummaryReport.SafetySummaryReportViewModelFactory
import com.cs.alfanar.ui.signup.SignUpRepository
import com.cs.alfanar.ui.signup.SignUpViewModelFactory
import com.cs.alfanar.ui.stopWorkAdvice.Stage1.StopWorkAdviceStage1Repository
import com.cs.alfanar.ui.stopWorkAdvice.Stage1.StopWorkAdviceStage1ViewModelFactory
import com.cs.alfanar.ui.stopWorkAdvice.Stage2.StopWorkAdviceStage2Repository
import com.cs.alfanar.ui.stopWorkAdvice.Stage2.StopWorkAdviceStage2ViewModelFactory
import com.cs.alfanar.ui.stopWorkAdvice.Summary.StopWorkAdviceSummaryRepository
import com.cs.alfanar.ui.stopWorkAdvice.Summary.StopWorkAdviceSummaryViewModelFactory
import com.cs.alfanar.ui.stopWorkAdvice.SummaryDetails.StopWorkAdviceDetailsRepository
import com.cs.alfanar.ui.stopWorkAdvice.SummaryDetails.StopWorkAdviceDetailsViewModelFactory
import com.cs.alfanar.ui.trainingAttendanceRecord.Stage1.TrainingAttendanceRecordRepository
import com.cs.alfanar.ui.trainingAttendanceRecord.Stage1.TrainingAttendanceRecordViewModelFactory
import com.cs.alfanar.ui.trainingAttendanceRecord.Summary.TrainingAttendanceRecordSummaryRepository
import com.cs.alfanar.ui.trainingAttendanceRecord.Summary.TrainingAttendanceRecordSummaryViewModelFactory
import com.cs.alfanar.ui.trainingAttendanceRecord.SummaryDetails.TrainingAttendanceDetailsRepository
import com.cs.alfanar.ui.trainingAttendanceRecord.SummaryDetails.TrainingAttendanceDetailsViewModelFactory
import com.cs.alfanar.ui.weeklyReport.weeklyReportDashboard.WeeklyReportDashboardRepository
import com.cs.alfanar.ui.weeklyReport.weeklyReportDashboard.WeeklyReportDashboardViewModelFactory
import com.cs.alfanar.ui.weeklyReport.weeklyReportDetails.WeeklyReportDetailsRepository
import com.cs.alfanar.ui.weeklyReport.weeklyReportDetails.WeeklyReportDetailsViewModelFactory
import com.cs.alfanar.ui.weeklyReport.weeklyReportProjectSelection.WeeklyReportProjectSelectionRepository
import com.cs.alfanar.ui.weeklyReport.weeklyReportProjectSelection.WeeklyReportProjectSelectionViewModelFactory
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage1.WeeklyReportStage1Repository
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage1.WeeklyReportStage1ViewModelFactory
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage2.WeeklyReportStage2Repository
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage2.WeeklyReportStage2ViewModelFactory
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage3.WeeklyReportStage3Repository
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage3.WeeklyReportStage3ViewModelFactory
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage4.WeeklyReportStage4Repository
import com.cs.alfanar.ui.weeklyReport.weeklyReportStage4.WeeklyReportStage4ViewModelFactory
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryRepository
import com.cs.alfanar.ui.weeklyReport.weeklyReportSummary.WeeklyReportSummaryViewModelFactory
import com.danikula.videocache.HttpProxyCacheServer
import net.simplifiedcoding.mvvmsampleapp.data.network.NetworkConnectionInterceptor
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton


class MyApplication : Application(), KodeinAware {

    override fun onCreate() {
        super.onCreate()
    }

    private var proxy: HttpProxyCacheServer? = null

    companion object {

        private lateinit var context: Context
        fun setContext(con: Context) {
            context=con
        }

        fun getContext(): Context {
            return context
        }

        fun getProxy(context: Context): HttpProxyCacheServer? {
            val app: MyApplication = context.getApplicationContext() as MyApplication
            if (app.proxy == null) {
                app.proxy = app.newProxy()
                return app.newProxy()
            } else {
                return app.proxy
            }
        }
    }

    private fun newProxy(): HttpProxyCacheServer? {
        return HttpProxyCacheServer(this)
    }

    override val kodein = Kodein.lazy {
        import(androidXModule(this@MyApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { ApiInterface(instance()) }
        bind() from singleton { GoogleApiInterface(instance()) }
        bind() from singleton { UserPreferences(instance()) }
        bind() from singleton { AppDatabase(instance()) }

        // Login
        bind() from singleton { LoginRepository(instance(), instance()) }
        bind() from provider { LoginViewModelFactory(instance()) }

        // SignUp
        bind() from singleton { SignUpRepository(instance()) }
        bind() from provider { SignUpViewModelFactory(instance()) }

        // Forgot Password
        bind() from singleton { ForgotPasswordRepository(instance()) }
        bind() from provider { ForgotPasswordViewModelFactory(instance()) }

        // Email Verification
        bind() from singleton { EmailVerificationRepository(instance()) }
        bind() from provider { EmailVerificationViewModelFactory(instance()) }

        // Reset Password
        bind() from singleton { ResetPasswordRespository(instance()) }
        bind() from provider { ResetPasswordViewModelFactory(instance()) }

        // Change Password
        bind() from singleton { ChangePasswordRepository(instance()) }
        bind() from provider { ChangePasswordViewModelFactory(instance()) }


        // Document center List
        bind() from singleton { DocumentCenterListRepository(instance()) }
        bind() from provider { DocumentCenterListViewModelFactory(instance()) }

        // Document center Item
        bind() from singleton { DocumentCenterItemRepository(instance()) }
        bind() from provider { DocumentCenterItemViewModelFactory(instance()) }

        // Emergency Numbers
        bind() from singleton { EmergencyNumbersRepository(instance()) }
        bind() from provider { EmergencyNumbersViewModelFactory(instance()) }

        // Alert Notification
        bind() from singleton { AlertNotificationRepository(instance()) }
        bind() from provider { AlertNotificationViewModelFactory(instance()) }

        // Alert Notification Inbox
        bind() from singleton { AlertNotificationInboxRepository(instance()) }
        bind() from provider { AlertNotificationInboxViewModelFactory(instance()) }

        // Alert Notification Insert
        bind() from singleton { AlertNotificationInsertRepository(instance()) }
        bind() from provider { AlertNotificationInsertViewModelFactory(instance()) }

        // Nearest hospital
        bind() from singleton { NearestHospitalRepository(instance()) }
        bind() from provider { NearestHospitalViewModelFactory(instance()) }

        // Nearest hospital directions
        bind() from singleton { NearestHospitalDirectionsRepository(instance()) }
        bind() from provider { NearestHospitalDirectionsViewModelFactory(instance()) }

        // Daily Reports
        bind() from singleton { DailyReportRepository(instance()) }
        bind() from provider { DailyReportViewModelFactory(instance()) }

        // Daily Reports summary
        bind() from singleton { DailyReportSummaryRepository(instance()) }
        bind() from provider { DailyReportSummaryViewModelFactory(instance()) }

        // Daily Reports details
        bind() from singleton { DailyReportDetailsRepository(instance()) }
        bind() from provider { DailyReportDetailsViewModelFactory(instance()) }

        // Weekly Report Project selection
        bind() from singleton { WeeklyReportProjectSelectionRepository(instance(), instance()) }
        bind() from provider { WeeklyReportProjectSelectionViewModelFactory(instance()) }

        // Weekly Report Dashboard
        bind() from singleton { WeeklyReportDashboardRepository(instance(), instance()) }
        bind() from provider { WeeklyReportDashboardViewModelFactory(instance()) }

        // Weekly Report stage1
        bind() from singleton { WeeklyReportStage1Repository(instance(), instance()) }
        bind() from provider { WeeklyReportStage1ViewModelFactory(instance()) }

        // Weekly Report stage2
        bind() from singleton { WeeklyReportStage2Repository(instance(), instance()) }
        bind() from provider { WeeklyReportStage2ViewModelFactory(instance()) }

        // Weekly Report stage3
        bind() from singleton { WeeklyReportStage3Repository(instance(), instance()) }
        bind() from provider { WeeklyReportStage3ViewModelFactory(instance()) }

        // Weekly Report stage4
        bind() from singleton { WeeklyReportStage4Repository(instance(), instance()) }
        bind() from provider { WeeklyReportStage4ViewModelFactory(instance()) }

        // Weekly Report summary
        bind() from singleton { WeeklyReportSummaryRepository(instance()) }
        bind() from provider { WeeklyReportSummaryViewModelFactory(instance()) }

        // Weekly Report details
        bind() from singleton { WeeklyReportDetailsRepository(instance()) }
        bind() from provider { WeeklyReportDetailsViewModelFactory(instance()) }

        // Accident incident stage1
        bind() from singleton { AccidentStage1Repository(instance(), instance()) }
        bind() from provider { AccidentStage1ViewModelFactory(instance()) }

        // Accident incident stage2
        bind() from singleton { AccidentStage2Repository(instance(), instance()) }
        bind() from provider { AccidentStage2ViewModelFactory(instance()) }

        // Accident incident stage3
        bind() from singleton { AccidentStage3Repository(instance(), instance()) }
        bind() from provider { AccidentStage3ViewModelFactory(instance()) }

        // Accident incident stage4
        bind() from singleton { AccidentStage4Repository(instance(), instance()) }
        bind() from provider { AccidentStage4ViewModelFactory(instance()) }

        // Accident incident summary
        bind() from singleton { AccidentIncidentSummaryRepository(instance()) }
        bind() from provider { AccidentIncidentSummaryViewModelFactory(instance()) }

        // Accident incident details
        bind() from singleton { AccidentIncidentDetailsRepository(instance()) }
        bind() from provider { AccidentIncidentDetailsViewModelFactory(instance()) }

        // Near miss incident
        bind() from singleton { NearMissIncidentStage1Repository(instance(), instance()) }
        bind() from provider { NearMissIncidentStage1ViewModelFactory(instance()) }

        // Near miss incident
        bind() from singleton { NearMissIncidentStage2Repository(instance(), instance()) }
        bind() from provider { NearMissIncidentStage2ViewModelFactory(instance()) }

        // Near miss incident Summary
        bind() from singleton { NearMissIncidentSummaryRepository(instance()) }
        bind() from provider { NearMissIncidentSummaryViewModelFactory(instance()) }

        // Near miss incident details
        bind() from singleton { NearMissIncidentDetailsRepository(instance()) }
        bind() from provider { NearMissIncidentDetailsViewModelFactory(instance()) }

        // Stop work advice stage1
        bind() from singleton { StopWorkAdviceStage1Repository(instance(), instance()) }
        bind() from provider { StopWorkAdviceStage1ViewModelFactory(instance()) }

        // Stop work advice stage2
        bind() from singleton { StopWorkAdviceStage2Repository(instance(), instance()) }
        bind() from provider { StopWorkAdviceStage2ViewModelFactory(instance()) }

        // Stop work advice Summary
        bind() from singleton { StopWorkAdviceSummaryRepository(instance()) }
        bind() from provider { StopWorkAdviceSummaryViewModelFactory(instance()) }

        // Stop work advice details
        bind() from singleton { StopWorkAdviceDetailsRepository(instance()) }
        bind() from provider { StopWorkAdviceDetailsViewModelFactory(instance()) }

        // Training Attendance record
        bind() from singleton { TrainingAttendanceRecordRepository(instance(), instance()) }
        bind() from provider { TrainingAttendanceRecordViewModelFactory(instance()) }

        // Training Attendance summary
        bind() from singleton { TrainingAttendanceRecordSummaryRepository(instance()) }
        bind() from provider { TrainingAttendanceRecordSummaryViewModelFactory(instance()) }

        // Training Attendance details
        bind() from singleton { TrainingAttendanceDetailsRepository(instance()) }
        bind() from provider { TrainingAttendanceDetailsViewModelFactory(instance()) }

        // Monthly Report Project selection
        bind() from singleton { MonthlyReportProjectSelectionRepository(instance(), instance()) }
        bind() from provider { MonthlyReportProjectSelectionViewModelFactory(instance()) }

        // Monthly Report Dashboard
        bind() from singleton { MonthlyReportDashboardRepository(instance(), instance()) }
        bind() from provider { MonthlyReportDashboardViewModelFactory(instance()) }

        // Monthly Report Stage1
        bind() from singleton { MonthlyReportStage1Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage1ViewModelFactory(instance()) }

        // Monthly Report Stage2
        bind() from singleton { MonthlyReportStage2Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage2ViewModelFactory(instance()) }

        // Monthly Report Stage3
        bind() from singleton { MonthlyReportStage3Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage3ViewModelFactory(instance()) }

        // Monthly Report Stage4
        bind() from singleton { MonthlyReportStage4Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage4ViewModelFactory(instance()) }

        // Monthly Report Stage5
        bind() from singleton { MonthlyReportStage5Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage5ViewModelFactory(instance()) }

        // Monthly Report Stage6
        bind() from singleton { MonthlyReportStage6Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage6ViewModelFactory(instance()) }

        // Monthly Report Stage9
        bind() from singleton { MonthlyReportStage9Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage9ViewModelFactory(instance()) }

        // Monthly Report Stage10
        bind() from singleton { MonthlyReportStage10Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage10ViewModelFactory(instance()) }

        // Monthly Report Stage11
        bind() from singleton { MonthlyReportStage11Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage11ViewModelFactory(instance()) }

        // Monthly Report Stage12
        bind() from singleton { MonthlyReportStage12Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage12ViewModelFactory(instance()) }

        // Monthly Report Stage13
        bind() from singleton { MonthlyReportStage13Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage13ViewModelFactory(instance()) }

        // Monthly Report Stage14
        bind() from singleton { MonthlyReportStage14Repository(instance(), instance()) }
        bind() from provider { MonthlyReportStage14ViewModelFactory(instance()) }

        // Monthly Report Summary
        bind() from singleton { MonthlyReportSummaryRepository(instance()) }
        bind() from provider { MonthlyReportSummaryViewModelFactory(instance()) }

        // Monthly Report Summary Details
        bind() from singleton { MonthlyReportSummaryDetailsRepository(instance()) }
        bind() from provider { MonthlyReportSummaryDetailsViewModelFactory(instance()) }

        // Safety Summary report
        bind() from singleton { SafetySummaryReportRepository(instance()) }
        bind() from provider { SafetySummaryReportViewModelFactory(instance()) }
    }
}