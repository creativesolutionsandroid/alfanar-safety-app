package com.cs.alfanar.network

import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.alertNotification.AlertNotificationTypeResponse
import com.cs.alfanar.model.documentCenterItemData.DocumentCenterItemResponse
import com.cs.alfanar.model.documentCenterListData.DocumentCenterListResponse
import com.cs.alfanar.model.emergencyNumbers.EmergencyNumbersResponse
import com.cs.alfanar.model.forgotPasswordData.ForgotPasswordResponse
import com.cs.alfanar.model.loginData.LoginResponse
import com.cs.alfanar.model.masterData.MasterDataResponse
import com.cs.alfanar.model.nearestHospital.NearestHospitalResponse
import com.cs.alfanar.model.nearestHospitalDirections.NearestHospitalDirectionsResponse
import com.cs.alfanar.model.signupData.SignUpResponse
import com.cs.alfanar.model.resetPasswordData.ResetPasswordResponse
import net.simplifiedcoding.mvvmsampleapp.data.network.NetworkConnectionInterceptor
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface GoogleApiInterface {

    // Google places api - to get nearby hospitals
    @GET ("place/search/json?rankby=distance&keyword=hospital&sensor=false&libraries=places")
    suspend fun getHospitalsList(
        @Query("location") location: String,
        @Query("key") api_key: String
    ) : Response<NearestHospitalResponse>


    // google directions api - to get directions from current location to hospital
    @GET ("directions/json")
    suspend fun getHospitalDirection(
        @Query("origin") origin: String?,
        @Query("destination") destination: String?,
        @Query("key") api_key: String
    ) : Response<NearestHospitalDirectionsResponse>


    companion object{
        val BASE_URL : String = "https://maps.googleapis.com/maps/api/"

        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ) : GoogleApiInterface{

            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GoogleApiInterface::class.java)
        }
    }

}

