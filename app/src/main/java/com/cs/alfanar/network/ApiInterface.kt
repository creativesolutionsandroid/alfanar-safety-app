package com.cs.alfanar.network

import android.widget.Toast
import com.cs.alfanar.MyApplication
import com.cs.alfanar.data.preferences.UserPreferences
import com.cs.alfanar.model.BasicResponse
import com.cs.alfanar.model.MonthlyReportDetails.MonthlyReportDetails
import com.cs.alfanar.model.MonthlyReportSummary.MonthlyReportSummary
import com.cs.alfanar.model.SafetySummaryReport.SafetySummaryReponse
import com.cs.alfanar.model.accidentIncident.AccidentMasterDataResponse
import com.cs.alfanar.model.accidentIncidentSummary.AccidentIncidentSummaryResponse
import com.cs.alfanar.model.alertNotification.AlertNotificationTypeResponse
import com.cs.alfanar.model.alertNotificationInbox.AlertNotificationInboxResponse
import com.cs.alfanar.model.dailyReport.GetProjectsData
import com.cs.alfanar.model.dailyReportSummary.DailyReportSummaryResponse
import com.cs.alfanar.model.documentCenterItemData.DocumentCenterItemResponse
import com.cs.alfanar.model.documentCenterListData.DocumentCenterListResponse
import com.cs.alfanar.model.emergencyNumbers.EmergencyNumbersResponse
import com.cs.alfanar.model.forgotPasswordData.ForgotPasswordResponse
import com.cs.alfanar.model.loginData.LoginResponse
import com.cs.alfanar.model.masterData.MasterDataResponse
import com.cs.alfanar.model.monthlyReportMasterData.MonthlyMasterData
import com.cs.alfanar.model.nearMissIncidentSummary.NearMissIncidentSummary
import com.cs.alfanar.model.signupData.SignUpResponse
import com.cs.alfanar.model.resetPasswordData.ResetPasswordResponse
import com.cs.alfanar.model.trainingAttendanceSummary.TrainingAttendanceSummaryResponse
import com.cs.alfanar.model.weeklyReport.WeeklyProjectsData
import com.cs.alfanar.model.weeklyReportDetails.WeeklyReportDetailsResponse
import com.cs.alfanar.model.weeklyReportSummary.WeeklyReportSummaryResponse
import net.simplifiedcoding.mvvmsampleapp.data.network.NetworkConnectionInterceptor
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {

    // Login
    @POST("api/Users/NewAuthentication")
    suspend fun LoginUser(@Body requestBody: RequestBody) : Response<LoginResponse>

    // Spinners data for sign up
    @POST("api/Users/NewGetMasterData")
    suspend fun getMasterData() :  Response<MasterDataResponse>

    // sign up
    @POST ("api/Users/NewRegistration")
    suspend fun registerUser(@Body requestBody: RequestBody) : Response<SignUpResponse>

    // forgot password
    @POST ("api/Users/NewSendForgotOTP")
    suspend fun forgotPassword(@Body requestBody: RequestBody) : Response<ForgotPasswordResponse>

    // reset password
    @POST ("api/Users/NewForgotPassword")
    suspend fun resetPassword(@Body requestBody: RequestBody) : Response<ResetPasswordResponse>

    // change password
    @POST ("api/Users/NewChangePassword")
    suspend fun changePassword(@Body requestBody: RequestBody) : Response<BasicResponse>

    // get documents list and count
    @POST ("api/Users/NewGetDocumentCenterCount")
    suspend fun getDocumentCenterList(@Body requestBody: RequestBody) : Response<DocumentCenterListResponse>

    // get documents by type id
    @POST ("api/Users/NewGetDocumentCenterDocsByTypeId")
    suspend fun getDocumentsByTypeId(@Body requestBody: RequestBody) : Response<DocumentCenterItemResponse>

    // Emergency Numbers
    @POST ("api/Users/NewGetEmergencyNumberByCountryId")
    suspend fun getEmergencyNumbers(@Body requestBody: RequestBody) : Response<EmergencyNumbersResponse>

    // get alert notification list
    @POST ("api/Users/NewGetAlertNotificationType")
    suspend fun getAlertNotifications(@Body requestBody: RequestBody) : Response<AlertNotificationTypeResponse>

    // get alert notification inbox list
    @POST ("api/AlertTypesAPI/GetAlertMessagesList")
    suspend fun getAlertNotificationsInbox(@Body requestBody: RequestBody) : Response<AlertNotificationInboxResponse>

    // acknowledge alert notification inbox list
    @POST ("api/Users/AcknowledgeAlertNotification")
    suspend fun acknowledgeAlertNotification(@Body requestBody: RequestBody) : Response<BasicResponse>

    // insert alert notification
    @POST ("api/Users/NewAddAlertNotification")
    suspend fun insertAlertNotifications(@Body requestBody: RequestBody) : Response<BasicResponse>

    // get projects data for daily report
    @POST ("api/ReportsAPI/GetUserDailyReportWeekData")
    suspend fun getProjectDataForDailyReport(@Body requestBody: RequestBody) : Response<GetProjectsData>

    // insert projects data for daily report
    @POST ("api/ReportsAPI/InsertDailyReport")
    suspend fun insertDailyReport(@Body requestBody: RequestBody) : Response<BasicResponse>

    // Daily report summary
    @POST ("api/ReportsAPI/GetFilterDailyReports")
    suspend fun getDailyReportSummary(@Body requestBody: RequestBody) : Response<DailyReportSummaryResponse>

    // Update Daily Report Status
    @POST ("api/ReportsAPI/UpdateDailyReportStatus")
    suspend fun UpdateDailyReportStatus(@Body requestBody: RequestBody) : Response<BasicResponse>

    // get projects data for weekly report
    @POST ("api/ReportsAPI/GetUserWeeklyReportWeekData")
    suspend fun getProjectDataForWeeklyReport(@Body requestBody: RequestBody) : Response<WeeklyProjectsData>

    // insert weekly report
    @POST ("api/ReportsAPI/InsertWeeklyReport")
    suspend fun insertWeeklyReport(@Body requestBody: RequestBody) : Response<BasicResponse>

    // Weekly report summary
    @POST ("api/ReportsAPI/GetWeeklyReportList")
    suspend fun getWeeklyReportSummary(@Body requestBody: RequestBody) : Response<WeeklyReportSummaryResponse>

    // Weekly report details
    @POST ("api/ReportsAPI/GetWeeklyReportList")
    suspend fun getWeeklyReportDetails(@Body requestBody: RequestBody) : Response<WeeklyReportDetailsResponse>

    // Weekly report details
    @POST ("api/ReportsAPI/UpdateWeeklyReportStatus")
    suspend fun updateWeeklyReportStatus(@Body requestBody: RequestBody) : Response<BasicResponse>

    // Accident get master data
    @POST ("api/ReportsAPI/GetAccidentIncidentReportMasterData")
    suspend fun GetAccidentIncidentReportMasterData(@Body requestBody: RequestBody) : Response<AccidentMasterDataResponse>// Accident get master data

    @POST ("api/ReportsAPI/InsertAccidentIncidentReport")
    suspend fun InsertAccidentIncidentReport(@Body requestBody: RequestBody) : Response<BasicResponse>

    // Weekly report summary
    @POST ("api/ReportsAPI/GetAccidentIncidentReportList")
    suspend fun getAccidentIncidentSummary(@Body requestBody: RequestBody) : Response<AccidentIncidentSummaryResponse>

    // Weekly report details
    @POST ("api/ReportsAPI/GetAccidentIncidentReportList")
    suspend fun getAccidentIncidentDetails(@Body requestBody: RequestBody) : Response<WeeklyReportDetailsResponse>

    // Weekly report details
    @POST ("api/ReportsAPI/UpdateAccidentReportStatus")
    suspend fun updateAccidentIncidentStatus(@Body requestBody: RequestBody) : Response<BasicResponse>

    // Insert Near miss report incident
    @POST ("api/ReportsAPI/InsertNearMissReport")
    suspend fun InsertNearMissReport(@Body requestBody: RequestBody) : Response<BasicResponse>

    // Near Miss summary
    @POST ("api/ReportsAPI/GetNearMissReportList")
    suspend fun getNearMissReportSummary(@Body requestBody: RequestBody) : Response<NearMissIncidentSummary>

    // update near miss
    @POST ("api/ReportsAPI/UpdateAccidentReportStatus")
    suspend fun updateNearMissStatus(@Body requestBody: RequestBody) : Response<BasicResponse>

    // stop work advice
    @POST ("api/ReportsAPI/InsertStopWorkAdviceReport")
    suspend fun InsertStopWorkAdviceReport(@Body requestBody: RequestBody) : Response<BasicResponse>

    // Weekly report summary
    @POST ("api/ReportsAPI/GetStopWorkAdviceReportList")
    suspend fun GetStopWorkAdviceReportList(@Body requestBody: RequestBody) : Response<AccidentIncidentSummaryResponse>

    // Weekly report details
//    @POST ("ReportsAPI/UpdateAccidentReportStatus")
//    suspend fun updateAccidentIncidentStatus(@Body requestBody: RequestBody) : Response<BasicResponse>

    // Insert training attendance
    @POST ("api/ReportsAPI/InsertTrainingAttendanceReport")
    suspend fun InsertTrainingAttendanceReport(@Body requestBody: RequestBody) : Response<BasicResponse>

    // training attendance summary
    @POST ("api/ReportsAPI/GetTrainingAttendanceReportList")
    suspend fun GetTrainingAttendanceReportList(@Body requestBody: RequestBody) : Response<TrainingAttendanceSummaryResponse>

    // get projects data for weekly report
    @POST ("api/ReportsAPI/GetUserMonthlyReportMasterData")
    suspend fun getProjectDataForMonthlyReport(@Body requestBody: RequestBody) : Response<MonthlyMasterData>

    // insert Monthly report
    @POST ("api/ReportsAPI/InsertMonthlyReport")
    suspend fun insertMonthlyReport(@Body requestBody: RequestBody) : Response<BasicResponse>

    // Monthly report summary
    @POST ("api/ReportsAPI/GetMonthlyReportList")
    suspend fun getMonthlyReportSummary(@Body requestBody: RequestBody) : Response<MonthlyReportSummary>

    // Monthly report details
    @POST ("api/ReportsAPI/GetMonthlyReportList")
    suspend fun getMonthlyReportDetails(@Body requestBody: RequestBody) : Response<MonthlyReportDetails>

    // Update Monthly report status
    @POST ("api/ReportsAPI/UpdateMonthlyReportStatus")
    suspend fun updateMonthlyReportStatus(@Body requestBody: RequestBody) : Response<BasicResponse>

    // Update Monthly report status
    @POST ("api/ReportsAPI/GetReportCount")
    suspend fun getSafetySummaryReponse(@Body requestBody: RequestBody) : Response<SafetySummaryReponse>

    companion object{
        // ToDO replace with live URL
//        val BASE_URL : String = "http://csadms.com/alfanarapi/"
//        val BASE_URL : String = "http://cswebapps.com/alfanarapi/"
        val BASE_URL : String = "https://alfanarsafetyapi.azurewebsites.net/"
//        val BASE_URL : String = "https://alfanarsafetyapiqas.alfanar.com/"

        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ) : ApiInterface{

            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
//                .baseUrl(UserPreferences(MyApplication.getContext()).baseUrl())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }
    }
}

