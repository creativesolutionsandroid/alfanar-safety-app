package net.simplifiedcoding.mvvmsampleapp.data.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.cs.alfanar.R
import com.cs.alfanar.utils.ApiException
import com.cs.alfanar.utils.NoInternetException
import okhttp3.Interceptor
import okhttp3.Response
import java.net.SocketException

class NetworkConnectionInterceptor(
    context: Context
) : Interceptor {

    private val applicationContext = context.applicationContext

    override fun intercept(chain: Interceptor.Chain): Response {
        try {
            val builder = chain.request().newBuilder()
            builder.header("API_KEY", "558822@alfanar") // todo api key header
            builder.header("Content-Type", "application/json")
            if (!isInternetAvailable())
                throw NoInternetException(applicationContext.resources.getString(R.string.str_connection_error))
            return chain.proceed(builder.build())
        } catch (e: NoInternetException) {
            throw NoInternetException(applicationContext.resources.getString(R.string.str_connection_error))
        } catch (e: Exception) {
            throw ApiException(applicationContext.resources.getString(R.string.str_cannot_reach_server))
        }
    }

    private fun isInternetAvailable(): Boolean {
        var result = false
        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        connectivityManager?.let {
            it.getNetworkCapabilities(connectivityManager.activeNetwork)?.apply {
                result = when {
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    else -> false
                }
            }
        }
        return result
    }

}