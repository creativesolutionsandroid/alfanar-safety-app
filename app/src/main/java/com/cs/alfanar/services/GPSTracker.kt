package com.cs.alfanar.services

import android.app.AlertDialog
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.*
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import java.util.*

class GPSTracker(context: Context?) : Service(), LocationListener {

    private var mContext: Context? = context

    // flag for GPS Status
    var isGPSEnabled = false

    // flag for network status
    var isNetworkEnabled = false

    var canGetLocation = false

    var location: Location? = null
    var latitude = 0.0
    var longitude = 0.0

    // The minimum distance to change updates in metters
    private val MIN_DISTANCE_CHANGE_FOR_UPDATES: Long = 0 // 0

    // The minimum time beetwen updates in milliseconds
    private val MIN_TIME_BW_UPDATES = 1000 * 5 * 1 // 0 minute
        .toLong()

    // Declaring a Location Manager
    protected var locationManager: LocationManager? = null

    fun getLocationAfterLocationReceived(): Location? {
        try {
            locationManager = mContext?.getSystemService(Context.LOCATION_SERVICE) as LocationManager

            // getting GPS status
            isGPSEnabled = locationManager!!
                .isProviderEnabled(LocationManager.GPS_PROVIDER)

            // getting network status
            isNetworkEnabled = locationManager!!
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                if (locationManager != null && isGPSEnabled) {
                    location = locationManager!!
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    updateGPSCoordinates()
                } else if (locationManager != null) {
                    location = locationManager!!
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                    updateGPSCoordinates()
                }
                canGetLocation = true
                val criteria = Criteria()
                criteria.accuracy = Criteria.ACCURACY_FINE
                criteria.powerRequirement = Criteria.POWER_HIGH
                criteria.isAltitudeRequired = false
                criteria.isSpeedRequired = true
                criteria.speedAccuracy = Criteria.ACCURACY_HIGH
                criteria.isCostAllowed = true
                criteria.isBearingRequired = false
                criteria.horizontalAccuracy = Criteria.ACCURACY_HIGH
                criteria.verticalAccuracy = Criteria.ACCURACY_HIGH
                locationManager!!.requestLocationUpdates(
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(),
                    criteria,
                    this,
                    null
                )

                // if GPS Enabled get lat/long using GPS Services
            }
        } catch (e: Exception) {
            // e.printStackTrace();
            Log.e(
                "Error : Location",
                "Impossible to connect to LocationManager", e
            )
        }
        return location
    }

    fun updateGPSCoordinates() {
        if (location != null) {
            latitude = location!!.latitude
            longitude = location!!.longitude
            Log.d(
                "GPS Enabled", "========================GPS Enabled"
                        + latitude
            )
        }
    }

    /**
     * Stop using GPS listener Calling this function will stop using GPS in your
     * app
     */
    fun stopUsingGPS() {
        if (locationManager != null) {
            locationManager!!.removeUpdates(this@GPSTracker)
        }
    }

    /**
     * Function to get latitude
     */
    fun getLatitudeAfterLocationReceived(): Double {
        if (location != null) {
            latitude = location!!.latitude
        }
        println(latitude)
        return latitude
    }

    /**
     * Function to get longitude
     */
    fun getLongitudeAfterLocationReceived(): Double {
        if (location != null) {
            longitude = location!!.longitude
        }
        return longitude
    }

    /**
     * Function to check GPS/wifi enabled
     */
    fun canGetLocation(): Boolean {
        return canGetLocation
    }

    /**
     * Function to show settings alert dialog
     */
    fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(mContext)

        // Setting Dialog Title
        alertDialog.setTitle("GPS")

        // Setting Dialog Message
        alertDialog.setMessage("GPS Alert")

        // On Pressing Setting button
        alertDialog.setPositiveButton(
            "ok"
        ) { dialog, which ->
            val intent = Intent(
                Settings.ACTION_LOCATION_SOURCE_SETTINGS
            )
            mContext!!.startActivity(intent)
        }

        // On pressing cancel button
        alertDialog.setNegativeButton(
            "Cancel"
        ) { dialog, which -> dialog.cancel() }
        alertDialog.show()
    }

    /**
     * Get list of address by latitude and longitude
     *
     * @return null or List<Address>
    </Address> */
    fun getGeocoderAddress(context: Context?): List<Address>? {
        if (location != null) {
            try {
                val geocoder = Geocoder(context, Locale.ENGLISH)
                Log.i(
                    "", "==========================" + latitude + "---"
                            + longitude
                )
                val addresses =
                    geocoder.getFromLocation(
                        latitude,
                        longitude, 1
                    )
                Log.i(
                    "", "===========address size==============="
                            + addresses.size
                )
                return addresses
            } catch (e: Exception) {
                // e.printStackTrace();
                Log.e(
                    "Error : Geocoder", "Impossible to connect to Geocoder",
                    e
                )
            }
        }
        return null
    }

    /**
     * Try to get AddressLine
     *
     * @return null or addressLine
     */
    fun getAddressLine(context: Context?): String? {
        val addresses =
            getGeocoderAddress(context)
        return if (addresses != null && addresses.size > 0) {
            val address = addresses[0]
            address.getAddressLine(0)
        } else {
            null
        }
    }

    /**
     * Try to get Locality
     *
     * @return null or locality
     */
    fun getSubLocality(context: Context?): String? {
        val addresses =
            getGeocoderAddress(context)
        Log.i(
            "TAG", "=======================getGeocoderAddress==========="
                    + addresses
        )
        return if (addresses != null && addresses.size > 0) {
            val address = addresses[0]
            address.locality
        } else {
            ""
        }
    }

    /**
     * Try to get Postal Code
     *
     * @return null or postalCode
     */
    fun getPostalCode(context: Context?): String? {
        val addresses =
            getGeocoderAddress(context)
        return if (addresses != null && addresses.size > 0) {
            val address = addresses[0]
            address.postalCode
        } else {
            null
        }
    }

    /**
     * Try to get CountryName
     *
     * @return null or postalCode
     */
    fun getCountryName(context: Context?): String? {
        val addresses =
            getGeocoderAddress(context)
        return if (addresses != null && addresses.size > 0) {
            val address = addresses[0]
            address.countryName
        } else {
            null
        }
    }


//    fun onProviderDisabled(provider: String?) {}
//
//    fun onProviderEnabled(provider: String?) {}

    override fun onLocationChanged(location: Location) {
//        TODO("Not yet implemented")
        val intent = Intent("GPSLocationUpdates")
        // You can also include some extra data.
        val b = Bundle()
        b.putParcelable("Location", location)
        intent.putExtra("Location", b)
        LocalBroadcastManager.getInstance(mContext!!).sendBroadcast(intent)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }
}