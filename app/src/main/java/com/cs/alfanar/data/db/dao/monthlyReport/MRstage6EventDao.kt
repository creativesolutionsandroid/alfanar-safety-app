package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRstage6EventsEntity

@Dao
interface MRstage6EventDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: MRstage6EventsEntity): Long

    @Query("SELECT * FROM MRstage6EventsEntity")
    suspend fun getData(): List<MRstage6EventsEntity>

    @Query("UPDATE MRstage6EventsEntity SET Name = :name WHERE id = :id")
    suspend fun updateData(name: String, id: Int)

    @Query("DELETE FROM MRstage6EventsEntity")
    suspend fun deleteData()

}