package com.cs.alfanar.data.db.entities.monthlyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MRProjectionSelectionEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    val projectId: Int = 0,
    var projectName: String = "",
    var projectCode: String = "",
    var location: String = ""
)