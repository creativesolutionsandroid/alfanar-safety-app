package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRProjectionSelectionEntity
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage1Entity

@Dao
interface MRStage1Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: MRStage1Entity): Long

    @Query("UPDATE MRStage1Entity SET SafeManHoursWorked = :SafeManHoursWorked WHERE id = 1")
    suspend fun updateSafeManHoursWorked(SafeManHoursWorked: Int)

    @Query("UPDATE MRStage1Entity SET TotalManHours = :TotalManHours WHERE id = 1")
    suspend fun updateTotalManHours(TotalManHours: Int)

    @Query("UPDATE MRStage1Entity SET DirectAlfanar = :DirectAlfanar WHERE id = 1")
    suspend fun updateDirectAlfanar(DirectAlfanar: Int)

    @Query("UPDATE MRStage1Entity SET DirectSC = :DirectSC WHERE id = 1")
    suspend fun updateDirectSC(DirectSC: Int)

    @Query("UPDATE MRStage1Entity SET InDirectAlfanar = :InDirectAlfanar WHERE id = 1")
    suspend fun updateInDirectAlfanar(InDirectAlfanar: Int)

    @Query("UPDATE MRStage1Entity SET InDirectSC = :InDirectSC WHERE id = 1")
    suspend fun updateInDirectSC(InDirectSC: Int)

    @Query("UPDATE MRStage1Entity SET Fatal = :Fatal WHERE id = 1")
    suspend fun updateFatal(Fatal: Int)

    @Query("UPDATE MRStage1Entity SET LWC = :LWC WHERE id = 1")
    suspend fun updateLWC(LWC: Int)

    @Query("SELECT * FROM MRStage1Entity")
    suspend fun getData(): List<MRStage1Entity>

    @Query("DELETE FROM MRStage1Entity")
    suspend fun deleteData()
}