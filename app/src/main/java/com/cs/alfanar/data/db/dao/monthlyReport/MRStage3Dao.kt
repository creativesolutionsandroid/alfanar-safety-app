package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage3Entity

@Dao
interface MRStage3Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: MRStage3Entity): Long

    @Query("UPDATE MRStage3Entity SET MonthlyEHSAward = :MonthlyEHSAward WHERE id = 1")
    suspend fun updateMonthlyEHSAward(MonthlyEHSAward: Int)

    @Query("UPDATE MRStage3Entity SET GSLC = :GSLC WHERE id = 1")
    suspend fun updateGSLC(GSLC: Int)

    @Query("UPDATE MRStage3Entity SET Violations = :Violations WHERE id = 1")
    suspend fun updateViolations(Violations: Int)

    @Query("UPDATE MRStage3Entity SET Management = :Management WHERE id = 1")
    suspend fun updateManagement(Management: Int)

    @Query("UPDATE MRStage3Entity SET Clients = :Clients WHERE id = 1")
    suspend fun updateClients(Clients: Int)

    @Query("UPDATE MRStage3Entity SET FICAlfanar = :FICAlfanar WHERE id = 1")
    suspend fun updateFICAlfanar(FICAlfanar: Int)

    @Query("UPDATE MRStage3Entity SET FICSub = :FICSub WHERE id = 1")
    suspend fun updateFICSub(FICSub: Int)

    @Query("UPDATE MRStage3Entity SET SMD = :SMD WHERE id = 1")
    suspend fun updateSMD(SMD: Int)

    @Query("SELECT * FROM MRStage3Entity")
    suspend fun getData(): List<MRStage3Entity>

    @Query("DELETE FROM MRStage3Entity")
    suspend fun deleteData()
}