package com.cs.alfanar.data.db.dao.weeklyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4EmergencyDrillEntity

@Dao
interface WRstage4DrillDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: WRstage4EmergencyDrillEntity): Long

    @Query("SELECT * FROM WRstage4EmergencyDrillEntity")
    suspend fun getData(): List<WRstage4EmergencyDrillEntity>

    @Query("DELETE FROM WRstage4EmergencyDrillEntity")
    suspend fun deleteDataUsingId()

}