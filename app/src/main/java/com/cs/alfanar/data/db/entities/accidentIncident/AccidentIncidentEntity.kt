package com.cs.alfanar.data.db.entities.accidentIncident

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AccidentIncidentEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    var projectName: String = "",
    val projectId: Int = 0,
    var projectCode: String = "",
    var Date: String = "",
    var Time: String = "",
    var AccidentType: String = "",
    var AccidentTypeId: Int = 0,
    var AccidentClass: String = "",
    var AccidentClassId: Int = 0,
    var InjuredEmployeeName: String = "",
    var CompanyContractor: String = "",
    var BodyPartInjured: String = "",
    var BodyPartInjuredOther: String = "",
    var BodyPartInjuredId: String = "",
    var Task: String = "",
    var Iqama: String = "",
    var Age: String = "",
    var ShiftTime: String = "",
    var ShiftTimeId: Int = 0,
    var Nationality: String = "",
    var NationalityId: Int = 0,
    var IncidentLocation: String = "",
    var IncidentTime: String = "",
    var CauseOfIncident: String = "",
    var CauseOfIncidentId: Int = 0,
    var FirstAidtreatment: String = "",
    var AccidentDescription: String = "",
    var vehicleApplicable: Boolean = false,
    var Model: String = "",
    var Plate: String = "",
    var VehicleID: String = "",
    var NoOfVehicleInvolved: String = "",
    var OverTime: Boolean = false,
    var ReportingDate: String = "",
    var ReportingTime: String = "",
    var OverTimeHours: String = "",
    var Hospital: Boolean = false,
    var Police: Boolean = false,
    var CivilDefence: Boolean = false,
    var SafetyMeasureOther: String = ""
)