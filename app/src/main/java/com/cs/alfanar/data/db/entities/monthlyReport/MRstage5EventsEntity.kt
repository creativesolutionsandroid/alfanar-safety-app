package com.cs.alfanar.data.db.entities.monthlyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MRstage5EventsEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val Name: String
)