package com.cs.alfanar.data.db.dao.weeklyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4VulnerableEntity

@Dao
interface WRstage4VulnerableDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: WRstage4VulnerableEntity): Long

    @Query("SELECT * FROM WRstage4VulnerableEntity")
    suspend fun getData(): List<WRstage4VulnerableEntity>

    @Query("UPDATE WRstage4VulnerableEntity SET Name = :name WHERE id = :id")
    suspend fun updateData(name: String, id: Int)

    @Query("DELETE FROM WRstage4VulnerableEntity")
    suspend fun deleteDataUsingId()

}