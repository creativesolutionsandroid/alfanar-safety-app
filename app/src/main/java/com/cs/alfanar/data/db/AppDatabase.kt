package com.cs.alfanar.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.cs.alfanar.data.db.dao.accidentIncident.AccidentIncidentDao
import com.cs.alfanar.data.db.dao.accidentIncident.AccidentInvestigationDao
import com.cs.alfanar.data.db.dao.monthlyReport.*
import com.cs.alfanar.data.db.dao.nearMissIncident.NearMissIncidentDao
import com.cs.alfanar.data.db.dao.stopWorkAdvice.StopWorkAdviceDao
import com.cs.alfanar.data.db.dao.stopWorkAdvice.StopWorkCommunicationDao
import com.cs.alfanar.data.db.dao.trainingAttendanceRecord.TrainingAttendanceRecordDao
import com.cs.alfanar.data.db.dao.trainingAttendanceRecord.TrainingAttendanceRecordEmployeeDao
import com.cs.alfanar.data.db.dao.weeklyReport.*
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentInvestigationEntity
import com.cs.alfanar.data.db.entities.monthlyReport.*
import com.cs.alfanar.data.db.entities.nearMissIncident.NearMissIncidentEntity
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkAdviceEntity
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkCommunicationEntity
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEmployeeEntity
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEntity
import com.cs.alfanar.data.db.entities.weeklyReport.*

@Database(
    version = 3,
    entities = [WRprojectSelectionEntity::class, WRstage1Entity::class, WRstage2Entity::class, WRstage3Entity::class,
                WRstage4ChecklistEntity::class, WRstage4HSEEntity::class, WRstage4OtherHealthEntity::class,
                WRstage4VulnerableEntity::class, WRstage4EmergencyDrillEntity::class, AccidentIncidentEntity::class, AccidentInvestigationEntity::class,
                NearMissIncidentEntity::class, StopWorkAdviceEntity::class, StopWorkCommunicationEntity::class,
                TrainingAttendanceRecordEntity::class, TrainingAttendanceRecordEmployeeEntity::class,
                MRProjectionSelectionEntity::class, MRStage1Entity::class, MRStage2Entity::class, MRStage3Entity::class,
                MRStage4Entity::class, MRStage5Entity::class, MRstage5EventsEntity::class, MRStage6Entity::class,
                MRstage6EventsEntity::class, MRStage9Entity::class, MRDashboardEntity::class]
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getWRprojectSelectionDao(): WRprojectSelectionDao
    abstract fun getWRstage1Dao(): WRstage1Dao
    abstract fun getWRstage2Dao(): WRstage2Dao
    abstract fun getWRstage3Dao(): WRstage3Dao
    abstract fun getWRstage4ChecklistDao(): WRstage4ChecklistDao
    abstract fun getWRstage4HSEDao(): WRstage4HSEDao
    abstract fun getWRstage4OtherHealthDao(): WRstage4OtherHealthDao
    abstract fun getWRstage4VulnerableDao(): WRstage4VulnerableDao
    abstract fun getWRstage4DrillDao(): WRstage4DrillDao
    abstract fun getAccidentIncidentDao(): AccidentIncidentDao
    abstract fun getAccidentInvestigationDao(): AccidentInvestigationDao
    abstract fun getNearMissIncidentDao(): NearMissIncidentDao
    abstract fun getStopWorkAdviceDao(): StopWorkAdviceDao
    abstract fun getStopWorkCommunicationDao(): StopWorkCommunicationDao
    abstract fun getTrainingAttendanceRecordDao(): TrainingAttendanceRecordDao
    abstract fun getTrainingAttendanceRecordEmployeeDao(): TrainingAttendanceRecordEmployeeDao
    abstract fun getMRProjectionSelectionDao(): MRProjectionSelectionDao
    abstract fun getMRStage1Dao(): MRStage1Dao
    abstract fun getMRStage2Dao(): MRStage2Dao
    abstract fun getMRStage3Dao(): MRStage3Dao
    abstract fun getMRStage4Dao(): MRStage4Dao
    abstract fun getMRStage5Dao(): MRStage5Dao
    abstract fun getMRStage5EventDao(): MRstage5EventDao
    abstract fun getMRStage6Dao(): MRStage6Dao
    abstract fun getMRStage6EventDao(): MRstage6EventDao
    abstract fun getMRDashboardDao(): MRDashboardDao
    abstract fun getMRStage9Dao(): MRStage9Dao

    companion object {

        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        fun destroy() {
            instance = null
        }

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "AlfanarDatabase.db")
                .fallbackToDestructiveMigration()
                .build()
    }
}