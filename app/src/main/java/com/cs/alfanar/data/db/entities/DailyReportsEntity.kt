package com.cs.alfanar.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DailyReportsEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    var date: String? = null
)