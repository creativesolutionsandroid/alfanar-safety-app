package com.cs.alfanar.data.db.entities.nearMissIncident

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class NearMissIncidentEntity (
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    var projectName: String = "",
    val projectId: Int = 0,
    var projectCode: String = "",
    var Date: String = "",
    var Time: String = "",
    var Location: String = "",
    var ReporterBy: String = "",
    var Designation: String = "",
    var Iqama: String = "",
    var ContactNo: String = "",
    var Description: String = "",
    var Witness: String = "",
    var Improvements: String = "",
    var Image: String = "",
    var ImageName: String = ""
){
}