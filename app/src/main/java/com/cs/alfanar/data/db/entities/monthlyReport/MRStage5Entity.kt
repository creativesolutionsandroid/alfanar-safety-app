package com.cs.alfanar.data.db.entities.monthlyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MRStage5Entity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    val SiteManagement: Int = 0,
    val SafetyAudits: Int = 0,
    val HighManagement: Int = 0,
    val Administrative: Int = 0
)