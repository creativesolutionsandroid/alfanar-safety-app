package com.cs.alfanar.data.db.entities.stopWorkAdvice

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class StopWorkCommunicationEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val type: Int,
    val Name : String,
    val Designation : String,
    val Mode : String
) {
}