package com.cs.alfanar.data.db.entities.monthlyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MRDashboardEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    val Stage1: Boolean = false,
    val Stage2: Boolean = false,
    val Stage3: Boolean = false,
    val Stage4: Boolean = false,
    val Stage5: Boolean = false,
    val Stage6: Boolean = false,
    val Stage7: Boolean = false,
    val Stage8: Boolean = false,
    val Stage9: Boolean = false
)