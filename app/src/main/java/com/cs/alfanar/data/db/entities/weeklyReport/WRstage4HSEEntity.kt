package com.cs.alfanar.data.db.entities.weeklyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WRstage4HSEEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val Name: String
)