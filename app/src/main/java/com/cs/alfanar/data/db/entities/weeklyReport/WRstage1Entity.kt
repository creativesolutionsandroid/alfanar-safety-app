package com.cs.alfanar.data.db.entities.weeklyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WRstage1Entity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val ReportType: Int,
    val ManpowerAtSite: Int,
    val ManHour: Int,
    val SafeManHours: Int,
    val CumulativeManHour: Int,
    val LostTimeInjuries: Int,
    val DaysLost: Int,
    val Accidents: Int,
    val AccidentInvestigation: Int,
    val MedicalTCase: Int,
    val FirstAidCases: Int,
    val NearMiss: Int,
    val SickLeave: Int,
    val AlfanarSafety: Int,
    val Attendee: Int,
    val VisitStatus: Boolean
)