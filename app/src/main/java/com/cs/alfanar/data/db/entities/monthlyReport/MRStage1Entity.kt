package com.cs.alfanar.data.db.entities.monthlyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MRStage1Entity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    val SafeManHoursWorked: Int = 0,
    val TotalManHours: Int = 0,
    val DirectAlfanar: Int = 0,
    val DirectSC: Int = 0,
    val InDirectAlfanar: Int = 0,
    val InDirectSC: Int = 0,
    val Fatal: Int = 0,
    val LWC: Int = 0
)