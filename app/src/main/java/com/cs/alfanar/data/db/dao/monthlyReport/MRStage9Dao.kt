package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage9Entity

@Dao
interface MRStage9Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: MRStage9Entity): Long

    @Query("UPDATE MRStage9Entity SET UnitOne = :UnitOne WHERE TypeId = :TypeId")
    suspend fun updateUnitOne(UnitOne: String, TypeId: Int)

    @Query("UPDATE MRStage9Entity SET UnitTwo = :UnitTwo WHERE TypeId = :TypeId")
    suspend fun updateUnitTwo(UnitTwo: String, TypeId: Int)

    @Query("UPDATE MRStage9Entity SET UnitThree = :UnitThree WHERE TypeId = :TypeId")
    suspend fun updateUnitThree(UnitThree: String, TypeId: Int)

    @Query("UPDATE MRStage9Entity SET UnitFour = :UnitFour WHERE TypeId = :TypeId")
    suspend fun updateUnitFour(UnitFour: String, TypeId: Int)

    @Query("UPDATE MRStage9Entity SET Remarks = :Remarks WHERE TypeId = :TypeId")
    suspend fun updateRemarks(Remarks: String, TypeId: Int)

    @Query("SELECT * FROM MRStage9Entity")
    suspend fun getData(): List<MRStage9Entity>

    @Query("DELETE FROM MRStage9Entity")
    suspend fun deleteData()
}