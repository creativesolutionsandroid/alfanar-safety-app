package com.cs.alfanar.data.db.dao.trainingAttendanceRecord

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEmployeeEntity

@Dao
interface TrainingAttendanceRecordEmployeeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: TrainingAttendanceRecordEmployeeEntity): Long

    @Query("SELECT * FROM TrainingAttendanceRecordEmployeeEntity")
    suspend fun getData(): List<TrainingAttendanceRecordEmployeeEntity>

    @Query("DELETE FROM TrainingAttendanceRecordEmployeeEntity")
    suspend fun deleteData()

}