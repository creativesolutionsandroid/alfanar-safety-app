package com.cs.alfanar.data.db.dao.accidentIncident

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.data.db.entities.weeklyReport.WRprojectSelectionEntity

@Dao
interface AccidentIncidentDao {

    @Query("SELECT * FROM AccidentIncidentEntity")
    suspend fun getData(): List<AccidentIncidentEntity>

    @Query("DELETE FROM AccidentIncidentEntity")
    suspend fun deleteData()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: AccidentIncidentEntity): Long

    @Query("UPDATE AccidentIncidentEntity SET projectId = :projectId, projectName = :projectName, projectCode = :projectCode WHERE id = 1")
    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String)

    @Query("UPDATE AccidentIncidentEntity SET Date = :date WHERE id = 1")
    suspend fun updateDate(date: String)

    @Query("UPDATE AccidentIncidentEntity SET AccidentType = :AccidentType, AccidentTypeId = :AccidentId WHERE id = 1")
    suspend fun updateAccidentType(AccidentType: String, AccidentId: Int)

    @Query("UPDATE AccidentIncidentEntity SET Time = :time WHERE id = 1")
    suspend fun updateTime(time: String)

    @Query("UPDATE AccidentIncidentEntity SET InjuredEmployeeName = :InjuredEmployeeName WHERE id = 1")
    suspend fun updateInjuredEmployeeName(InjuredEmployeeName: String)

    @Query("UPDATE AccidentIncidentEntity SET CompanyContractor = :CompanyContractor WHERE id = 1")
    suspend fun updateCompanyContractor(CompanyContractor: String)

    @Query("UPDATE AccidentIncidentEntity SET AccidentClass = :AccidentClass, AccidentClassId = :AccidentClassId WHERE id = 1")
    suspend fun updateAccidentClass(AccidentClass: String, AccidentClassId: Int)

    @Query("UPDATE AccidentIncidentEntity SET BodyPartInjured = :BodyPartInjured, BodyPartInjuredId = :BodyPartInjuredId WHERE id = 1")
    suspend fun updateBodyPartInjured(BodyPartInjured: String, BodyPartInjuredId: String)

    @Query("UPDATE AccidentIncidentEntity SET BodyPartInjuredOther = :BodyPartInjuredOther WHERE id = 1")
    suspend fun updateBodyPartInjuredOther(BodyPartInjuredOther: String)

    @Query("UPDATE AccidentIncidentEntity SET Task = :Task WHERE id = 1")
    suspend fun updateTask(Task: String)

    @Query("UPDATE AccidentIncidentEntity SET Iqama = :Iqama WHERE id = 1")
    suspend fun updateIqama(Iqama: String)

    @Query("UPDATE AccidentIncidentEntity SET Age = :Age WHERE id = 1")
    suspend fun updateAge(Age: String)

    @Query("UPDATE AccidentIncidentEntity SET ShiftTime = :ShiftTime, ShiftTimeId = :ShiftTimeId WHERE id = 1")
    suspend fun updateShiftTime(ShiftTime: String, ShiftTimeId: Int)

    @Query("UPDATE AccidentIncidentEntity SET Nationality = :Nationality, NationalityId = :NationalityId WHERE id = 1")
    suspend fun updateNationality(Nationality: String, NationalityId: Int)

    @Query("UPDATE AccidentIncidentEntity SET IncidentLocation = :IncidentLocation WHERE id = 1")
    suspend fun updateIncidentLocation(IncidentLocation: String)

    @Query("UPDATE AccidentIncidentEntity SET IncidentTime = :IncidentTime WHERE id = 1")
    suspend fun updateIncidentTime(IncidentTime: String)

    @Query("UPDATE AccidentIncidentEntity SET CauseOfIncident = :CauseOfIncident, CauseOfIncidentId = :CauseOfIncidentId WHERE id = 1")
    suspend fun updateCauseOfIncident(CauseOfIncident: String, CauseOfIncidentId: Int)

    @Query("UPDATE AccidentIncidentEntity SET AccidentDescription = :AccidentDescription WHERE id = 1")
    suspend fun updateAccidentDescription(AccidentDescription: String)

    @Query("UPDATE AccidentIncidentEntity SET FirstAidtreatment = :FirstAidtreatment WHERE id = 1")
    suspend fun updateFirstAidtreatment(FirstAidtreatment: String)

    @Query("UPDATE AccidentIncidentEntity SET vehicleApplicable = :vehicleApplicable WHERE id = 1")
    suspend fun updatevehicleApplicable(vehicleApplicable: Boolean)

    @Query("UPDATE AccidentIncidentEntity SET Model = :Model WHERE id = 1")
    suspend fun updateModel(Model: String)

    @Query("UPDATE AccidentIncidentEntity SET Plate = :Plate WHERE id = 1")
    suspend fun updatePlate(Plate: String)

    @Query("UPDATE AccidentIncidentEntity SET VehicleID = :VehicleID WHERE id = 1")
    suspend fun updateVehicleID(VehicleID: String)

    @Query("UPDATE AccidentIncidentEntity SET NoOfVehicleInvolved = :NoOfVehicleInvolved WHERE id = 1")
    suspend fun updateNoOfVehicleInvolved(NoOfVehicleInvolved: String)

    @Query("UPDATE AccidentIncidentEntity SET OverTime = :OverTime WHERE id = 1")
    suspend fun updateOverTime(OverTime: Boolean)

    @Query("UPDATE AccidentIncidentEntity SET ReportingDate = :ReportingDate WHERE id = 1")
    suspend fun updateReportingDate(ReportingDate: String)

    @Query("UPDATE AccidentIncidentEntity SET ReportingTime = :ReportingTime WHERE id = 1")
    suspend fun updateReportingTime(ReportingTime: String)

    @Query("UPDATE AccidentIncidentEntity SET OverTimeHours = :OverTimeHours WHERE id = 1")
    suspend fun updateOverTimeHours(OverTimeHours: String)

    @Query("UPDATE AccidentIncidentEntity SET Hospital = :Hospital WHERE id = 1")
    suspend fun updateHospital(Hospital: Boolean)

    @Query("UPDATE AccidentIncidentEntity SET Police = :Police WHERE id = 1")
    suspend fun updatePolice(Police: Boolean)

    @Query("UPDATE AccidentIncidentEntity SET CivilDefence = :CivilDefence WHERE id = 1")
    suspend fun updateCivilDefence(CivilDefence: Boolean)

    @Query("UPDATE AccidentIncidentEntity SET SafetyMeasureOther = :SafetyMeasureOther WHERE id = 1")
    suspend fun updateSafetyMeasureOther(SafetyMeasureOther: String)
}