package com.cs.alfanar.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.cs.alfanar.utils.ROLE_REGULAR_USER

private const val KEY_IS_USER_LOGIN = "key_is_user_login"
private const val KEY_USER_ID = "key_user_id"
private const val KEY_USER_MODE = "key_user_mode"
private const val KEY_BASE_URL = "key_base_url"
private const val KEY_EMPLOYEE_NUMBER = "key_employee_no"
private const val KEY_NAME = "key_name"
private const val KEY_EMAIL = "key_email"
private const val KEY_MOBILE = "key_mobile"
private const val KEY_PROJECT_ID = "key_ProjectId"
private const val KEY_PROJECT_NAME = "key_ProjectName"
private const val KEY_ROLE_ID = "key_RoleId"
private const val KEY_ROLE_NAME = "key_RoleEn"
private const val KEY_NATIONALITY_ID = "key_NationalityId"
private const val KEY_NATIONALITY = "key_Nationality"
private const val KEY_COUNTRY_ID = "key_CountryId"
private const val KEY_COUNTRY = "key_CountryNameEn"

class UserPreferences(
    context: Context?
) {

    private val appContext = context?.applicationContext

    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)


    fun saveUserData(isUserLogin : Boolean, userId: Int, employeeNo: String, name: String, email: String, mobile: String,
                      projectId: String, projectName: String, roleId: Int, roleName: String, nationalityId: Int, nationality: String,
                      countryId: Int, countryName: String) {
        preference.edit()
            .putBoolean(KEY_IS_USER_LOGIN, isUserLogin)
            .putInt(KEY_USER_ID, userId)
            .putString(KEY_EMPLOYEE_NUMBER, employeeNo)
            .putString(KEY_NAME, name)
            .putString(KEY_EMAIL, email)
            .putString(KEY_MOBILE, mobile)
            .putString(KEY_PROJECT_ID, projectId)
            .putString(KEY_PROJECT_NAME, projectName)
            .putInt(KEY_ROLE_ID, roleId)
            .putString(KEY_ROLE_NAME, roleName)
            .putInt(KEY_NATIONALITY_ID, nationalityId)
            .putString(KEY_NATIONALITY, nationality)
            .putInt(KEY_COUNTRY_ID, countryId)
            .putString(KEY_COUNTRY, countryName)
            .apply()
    }

    fun clearData() {
        preference.edit().clear().commit()
    }

    fun saveUserMode(userMode : Int) {
        preference.edit()
            .putInt(KEY_USER_MODE, userMode)
            .apply()
    }

    fun userMode(): Int? {
        return preference.getInt(KEY_USER_MODE, 1)
    }

    fun saveBaseUrl(BaseUrl : String) {
        preference.edit()
            .putString(KEY_BASE_URL, BaseUrl)
            .apply()
    }

    fun baseUrl(): String? {
        return preference.getString(KEY_BASE_URL, "")
    }

    fun checkLoginStatus(): Boolean {
        return preference.getBoolean(KEY_IS_USER_LOGIN, false)
    }

    fun userId(): Int? {
        return preference.getInt(KEY_USER_ID, 0)
    }

    fun countryId(): Int? {
        return preference.getInt(KEY_COUNTRY_ID, 1)
    }

    fun projectId(): String? {
        return preference.getString(KEY_PROJECT_ID, "")
    }

    fun projectName(): String? {
        return preference.getString(KEY_PROJECT_NAME, "")
    }

    fun employeeName(): String? {
        return preference.getString(KEY_NAME, "")
    }

    fun employeeRole(): String? {
        return preference.getString(KEY_ROLE_NAME, "")
    }

    fun roleId(): Int? {
        return preference.getInt(KEY_ROLE_ID, ROLE_REGULAR_USER)
    }

}