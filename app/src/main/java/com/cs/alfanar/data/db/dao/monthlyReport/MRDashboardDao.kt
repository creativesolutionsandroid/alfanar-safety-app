package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRDashboardEntity

@Dao
interface MRDashboardDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: MRDashboardEntity): Long

    @Query("UPDATE MRDashboardEntity SET Stage1 = :Stage1 WHERE id = 1")
    suspend fun updateStage1(Stage1: Boolean)

    @Query("UPDATE MRDashboardEntity SET Stage2 = :Stage2 WHERE id = 1")
    suspend fun updateStage2(Stage2: Boolean)

    @Query("UPDATE MRDashboardEntity SET Stage3 = :Stage3 WHERE id = 1")
    suspend fun updateStage3(Stage3: Boolean)

    @Query("UPDATE MRDashboardEntity SET Stage4 = :Stage4 WHERE id = 1")
    suspend fun updateStage4(Stage4: Boolean)

    @Query("UPDATE MRDashboardEntity SET Stage5 = :Stage5 WHERE id = 1")
    suspend fun updateStage5(Stage5: Boolean)

    @Query("UPDATE MRDashboardEntity SET Stage6 = :Stage6 WHERE id = 1")
    suspend fun updateStage6(Stage6: Boolean)

    @Query("UPDATE MRDashboardEntity SET Stage7 = :Stage7 WHERE id = 1")
    suspend fun updateStage7(Stage7: Boolean)

    @Query("UPDATE MRDashboardEntity SET Stage8 = :Stage8 WHERE id = 1")
    suspend fun updateStage8(Stage8: Boolean)

    @Query("UPDATE MRDashboardEntity SET Stage9 = :Stage9 WHERE id = 1")
    suspend fun updateStage9(Stage9: Boolean)

    @Query("SELECT * FROM MRDashboardEntity")
    suspend fun getData(): List<MRDashboardEntity>

    @Query("DELETE FROM MRDashboardEntity")
    suspend fun deleteData()
}