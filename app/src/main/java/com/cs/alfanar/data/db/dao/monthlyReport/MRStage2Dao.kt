package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRProjectionSelectionEntity
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage2Entity

@Dao
interface MRStage2Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: MRStage2Entity): Long

    @Query("UPDATE MRStage2Entity SET MTC = :MTC WHERE id = 1")
    suspend fun updateMTC(MTC: Int)

    @Query("UPDATE MRStage2Entity SET FAC = :FAC WHERE id = 1")
    suspend fun updateFAC(FAC: Int)

    @Query("UPDATE MRStage2Entity SET PdAlfanar = :PdAlfanar WHERE id = 1")
    suspend fun updatePdAlfanar(PdAlfanar: Int)

    @Query("UPDATE MRStage2Entity SET PdSC = :PdSC WHERE id = 1")
    suspend fun updatePdSC(PdSC: Int)

    @Query("UPDATE MRStage2Entity SET NmAlfanar = :NmAlfanar WHERE id = 1")
    suspend fun updateNmAlfanar(NmAlfanar: Int)

    @Query("UPDATE MRStage2Entity SET NmSC = :NmSC WHERE id = 1")
    suspend fun updateNmSC(NmSC: Int)

    @Query("UPDATE MRStage2Entity SET MVA = :MVA WHERE id = 1")
    suspend fun updateMVA(MVA: Int)

    @Query("UPDATE MRStage2Entity SET CampInspection = :CampInspection WHERE id = 1")
    suspend fun updateCampInspection(CampInspection: Int)

    @Query("UPDATE MRStage2Entity SET EIC = :EIC WHERE id = 1")
    suspend fun updateEIC(EIC: Int)

    @Query("UPDATE MRStage2Entity SET EmergencyDrillConducte = :EmergencyDrillConducte WHERE id = 1")
    suspend fun updateEmergencyDrillConducte(EmergencyDrillConducte: Int)

    @Query("SELECT * FROM MRStage2Entity")
    suspend fun getData(): List<MRStage2Entity>

    @Query("DELETE FROM MRStage2Entity")
    suspend fun deleteData()
}