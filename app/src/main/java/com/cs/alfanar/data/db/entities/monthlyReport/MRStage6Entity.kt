package com.cs.alfanar.data.db.entities.monthlyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MRStage6Entity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    val PositiveAlf: Int = 0,
    val PositiveSub: Int = 0,
    val NegativeAlf: Int = 0,
    val NegativeSub: Int = 0,
    val PendingAlf: Int = 0,
    val PendingSub: Int = 0,
    val ClosedAlf: Int = 0,
    val ClosedSub: Int = 0
)