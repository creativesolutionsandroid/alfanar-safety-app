package com.cs.alfanar.data.db.entities.trainingAttendanceRecord

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class TrainingAttendanceRecordEmployeeEntity (
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val Name : String,
    val Designation : String,
    val FileNumber : String,
    val Department : String
) {
}