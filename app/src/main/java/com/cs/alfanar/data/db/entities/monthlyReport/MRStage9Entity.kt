package com.cs.alfanar.data.db.entities.monthlyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MRStage9Entity(
    @PrimaryKey(autoGenerate = false)
    val TypeId: Int = 0,
    val UnitOne: String = "",
    val UnitTwo: String = "",
    val UnitThree: String = "",
    val UnitFour: String = "",
    val Remarks: String = ""
)