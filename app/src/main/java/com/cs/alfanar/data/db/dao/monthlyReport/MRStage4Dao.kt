package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage4Entity

@Dao
interface MRStage4Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: MRStage4Entity): Long

    @Query("UPDATE MRStage4Entity SET ManagerAlf = :ManagerAlf WHERE id = 1")
    suspend fun updateManagerAlf(ManagerAlf: Int)

    @Query("UPDATE MRStage4Entity SET ManagerSub = :ManagerSub WHERE id = 1")
    suspend fun updateManagerSub(ManagerSub: Int)

    @Query("UPDATE MRStage4Entity SET EngineerAlf = :EngineerAlf WHERE id = 1")
    suspend fun updateEngineerAlf(EngineerAlf: Int)

    @Query("UPDATE MRStage4Entity SET EngineerSub = :EngineerSub WHERE id = 1")
    suspend fun updateEngineerSub(EngineerSub: Int)

    @Query("UPDATE MRStage4Entity SET SupervisorAlf = :SupervisorAlf WHERE id = 1")
    suspend fun updateSupervisorAlf(SupervisorAlf: Int)

    @Query("UPDATE MRStage4Entity SET SupervisorSub = :SupervisorSub WHERE id = 1")
    suspend fun updateSupervisorSub(SupervisorSub: Int)

    @Query("UPDATE MRStage4Entity SET OfficerAlf = :OfficerAlf WHERE id = 1")
    suspend fun updateOfficerAlf(OfficerAlf: Int)

    @Query("UPDATE MRStage4Entity SET OfficerSub = :OfficerSub WHERE id = 1")
    suspend fun updateOfficerSub(OfficerSub: Int)

    @Query("UPDATE MRStage4Entity SET NurseAlf = :NurseAlf WHERE id = 1")
    suspend fun updateNurseAlf(NurseAlf: Int)

    @Query("UPDATE MRStage4Entity SET NurseSub = :NurseSub WHERE id = 1")
    suspend fun updateNurseSub(NurseSub: Int)

    @Query("UPDATE MRStage4Entity SET FirstAidAlf = :FirstAidAlf WHERE id = 1")
    suspend fun updateFirstAidAlf(FirstAidAlf: Int)

    @Query("UPDATE MRStage4Entity SET FirstAidSub = :FirstAidSub WHERE id = 1")
    suspend fun updateFirstAidSub(FirstAidSub: Int)

    @Query("UPDATE MRStage4Entity SET Note = :Note WHERE id = 1")
    suspend fun updateNote(Note: String)
    
    @Query("SELECT * FROM MRStage4Entity")
    suspend fun getData(): List<MRStage4Entity>

    @Query("DELETE FROM MRStage4Entity")
    suspend fun deleteData()
}