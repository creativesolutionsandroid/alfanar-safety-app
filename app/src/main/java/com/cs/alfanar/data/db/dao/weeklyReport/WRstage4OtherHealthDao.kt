package com.cs.alfanar.data.db.dao.weeklyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4OtherHealthEntity

@Dao
interface WRstage4OtherHealthDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: WRstage4OtherHealthEntity): Long

    @Query("SELECT * FROM WRstage4OtherHealthEntity")
    suspend fun getData(): List<WRstage4OtherHealthEntity>

    @Query("UPDATE WRstage4OtherHealthEntity SET Name = :name WHERE id = :id")
    suspend fun updateData(name: String, id: Int)

    @Query("DELETE FROM WRstage4OtherHealthEntity")
    suspend fun deleteDataUsingId()

}