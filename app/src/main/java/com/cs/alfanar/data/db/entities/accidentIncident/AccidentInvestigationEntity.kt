package com.cs.alfanar.data.db.entities.accidentIncident

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AccidentInvestigationEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    var typeName: String = "",
    val typeId: Int = 0,
    var selectName: String = "",
    val selectId: Int = 0
)