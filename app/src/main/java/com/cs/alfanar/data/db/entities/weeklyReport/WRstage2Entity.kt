package com.cs.alfanar.data.db.entities.weeklyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WRstage2Entity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val Topic : String,
    val Duration : String,
    val Date : String,
    val Attendee : Int,
    val ConductedBy : String
)