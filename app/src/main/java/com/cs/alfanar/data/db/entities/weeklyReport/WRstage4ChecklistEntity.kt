package com.cs.alfanar.data.db.entities.weeklyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WRstage4ChecklistEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val ChecklistName: String,
    val ChecklistId: Int,
    val NoOfEquipments: Int,
    val NoOfChecklist: Int
)