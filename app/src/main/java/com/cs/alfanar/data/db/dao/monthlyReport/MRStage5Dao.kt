package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage5Entity

@Dao
interface MRStage5Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: MRStage5Entity): Long

    @Query("UPDATE MRStage5Entity SET SiteManagement = :SiteManagement WHERE id = 1")
    suspend fun updateSiteManagement(SiteManagement: Int)

    @Query("UPDATE MRStage5Entity SET SafetyAudits = :SafetyAudits WHERE id = 1")
    suspend fun updateSafetyAudits(SafetyAudits: Int)

    @Query("UPDATE MRStage5Entity SET HighManagement = :HighManagement WHERE id = 1")
    suspend fun updateHighManagement(HighManagement: Int)

    @Query("UPDATE MRStage5Entity SET Administrative = :Administrative WHERE id = 1")
    suspend fun updateAdministrative(Administrative: Int)

    @Query("SELECT * FROM MRStage5Entity")
    suspend fun getData(): List<MRStage5Entity>

    @Query("DELETE FROM MRStage5Entity")
    suspend fun deleteData()
}