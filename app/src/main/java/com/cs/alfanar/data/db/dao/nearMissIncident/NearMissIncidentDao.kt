package com.cs.alfanar.data.db.dao.nearMissIncident

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.data.db.entities.nearMissIncident.NearMissIncidentEntity

@Dao
interface NearMissIncidentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: NearMissIncidentEntity): Long

    @Query("SELECT * FROM NearMissIncidentEntity")
    suspend fun getData(): List<NearMissIncidentEntity>

    @Query("DELETE FROM NearMissIncidentEntity")
    suspend fun deleteData()

    @Query("UPDATE NearMissIncidentEntity SET projectId = :projectId, projectName = :projectName, projectCode = :projectCode WHERE id = 1")
    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String)

    @Query("UPDATE NearMissIncidentEntity SET Date = :date WHERE id = 1")
    suspend fun updateDate(date: String)

    @Query("UPDATE NearMissIncidentEntity SET Time = :Time WHERE id = 1")
    suspend fun updateTime(Time: String)

    @Query("UPDATE NearMissIncidentEntity SET Location = :Location WHERE id = 1")
    suspend fun updateLocation(Location: String)

    @Query("UPDATE NearMissIncidentEntity SET ReporterBy = :ReporterBy WHERE id = 1")
    suspend fun updateReporterBy(ReporterBy: String)

    @Query("UPDATE NearMissIncidentEntity SET Designation = :Designation WHERE id = 1")
    suspend fun updateDesignation(Designation: String)

    @Query("UPDATE NearMissIncidentEntity SET Iqama = :Iqama WHERE id = 1")
    suspend fun updateIqama(Iqama: String)

    @Query("UPDATE NearMissIncidentEntity SET ContactNo = :ContactNo WHERE id = 1")
    suspend fun updateContactNo(ContactNo: String)

    @Query("UPDATE NearMissIncidentEntity SET Description = :Description WHERE id = 1")
    suspend fun updateDescription(Description: String)

    @Query("UPDATE NearMissIncidentEntity SET Witness = :Witness WHERE id = 1")
    suspend fun updateWitness(Witness: String)

    @Query("UPDATE NearMissIncidentEntity SET Improvements = :Improvements WHERE id = 1")
    suspend fun updateImprovements(Improvements: String)

    @Query("UPDATE NearMissIncidentEntity SET Image = :Image, ImageName = :ImageName WHERE id = 1")
    suspend fun updateImage(Image: String, ImageName: String)
}