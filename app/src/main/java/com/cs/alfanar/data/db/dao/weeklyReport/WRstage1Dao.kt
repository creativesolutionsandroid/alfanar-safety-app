package com.cs.alfanar.data.db.dao.weeklyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage1Entity

@Dao
interface WRstage1Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: WRstage1Entity): Long

    @Query("SELECT * FROM WRstage1Entity")
    suspend fun getData(): List<WRstage1Entity>

    @Query("SELECT * FROM WRstage1Entity WHERE ReportType =:reportType")
    suspend fun getDataByType(reportType: Int): List<WRstage1Entity>

    @Query("DELETE FROM WRstage1Entity")
    suspend fun deleteData()

    @Query("UPDATE WRstage1Entity SET ReportType = :ReportType WHERE id = :ReportType")
    suspend fun updateReportType(ReportType: Int)

    @Query("UPDATE WRstage1Entity SET ManpowerAtSite = :ManpowerAtSite WHERE id = :ReportType")
    suspend fun updateManpowerAtSite(ManpowerAtSite: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET ManHour = :ManHour WHERE id = :ReportType")
    suspend fun updateManHour(ManHour: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET SafeManHours = :SafeManHours WHERE id = :ReportType")
    suspend fun updateSafeManHours(SafeManHours: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET CumulativeManHour = :CumulativeManHour WHERE id = :ReportType")
    suspend fun updateCumulativeManHour(CumulativeManHour: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET LostTimeInjuries = :LostTimeInjuries WHERE id = :ReportType")
    suspend fun updateLostTimeInjuries(LostTimeInjuries: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET DaysLost = :DaysLost WHERE id = :ReportType")
    suspend fun updateDaysLost(DaysLost: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET Accidents = :Accidents WHERE id = :ReportType")
    suspend fun updateAccidents(Accidents: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET AccidentInvestigation = :AccidentInvestigation WHERE id = :ReportType")
    suspend fun updateAccidentInvestigation(AccidentInvestigation: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET MedicalTCase = :MedicalTCase WHERE id = :ReportType")
    suspend fun updateMedicalTCase(MedicalTCase: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET FirstAidCases = :FirstAidCases WHERE id = :ReportType")
    suspend fun updateFirstAidCases(FirstAidCases: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET SickLeave = :SickLeave WHERE id = :ReportType")
    suspend fun updateSickLeave(SickLeave: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET NearMiss = :NearMiss WHERE id = :ReportType")
    suspend fun updateNearMiss(NearMiss: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET AlfanarSafety = :AlfanarSafety WHERE id = :ReportType")
    suspend fun updateAlfanarSafety(AlfanarSafety: Int, ReportType: Int)

    @Query("UPDATE WRstage1Entity SET Attendee = :Attendee WHERE id = :ReportType")
    suspend fun updateAttendee(Attendee: Int, ReportType: Int)

}