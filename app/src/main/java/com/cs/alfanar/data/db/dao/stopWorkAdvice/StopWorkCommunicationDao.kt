package com.cs.alfanar.data.db.dao.stopWorkAdvice

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkCommunicationEntity
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage2Entity

@Dao
interface StopWorkCommunicationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: StopWorkCommunicationEntity): Long

    @Query("SELECT * FROM StopWorkCommunicationEntity WHERE type = 1")
    suspend fun getCommunicationData(): List<StopWorkCommunicationEntity>

    @Query("SELECT * FROM StopWorkCommunicationEntity WHERE type = 2")
    suspend fun getInformationData(): List<StopWorkCommunicationEntity>

    @Query("DELETE FROM StopWorkCommunicationEntity")
    suspend fun deleteCommunicationData()

    @Query("DELETE FROM StopWorkAdviceEntity")
    suspend fun deleteStopWorkData()

}