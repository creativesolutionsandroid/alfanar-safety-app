package com.cs.alfanar.data.db.entities.weeklyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WRstage4EmergencyDrillEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val TopicName: String,
    val Date: String,
    val NoOfChecklist: Int
)