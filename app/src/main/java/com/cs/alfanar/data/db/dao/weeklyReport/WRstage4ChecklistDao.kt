package com.cs.alfanar.data.db.dao.weeklyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4ChecklistEntity

@Dao
interface WRstage4ChecklistDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: WRstage4ChecklistEntity): Long

    @Query("SELECT * FROM WRstage4ChecklistEntity")
    suspend fun getData(): List<WRstage4ChecklistEntity>

    @Query("DELETE FROM WRstage4ChecklistEntity")
    suspend fun deleteDataUsingId()

}