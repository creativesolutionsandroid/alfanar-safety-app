package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRStage6Entity

@Dao
interface MRStage6Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: MRStage6Entity): Long

    @Query("UPDATE MRStage6Entity SET PositiveAlf = :PositiveAlf WHERE id = 1")
    suspend fun updatePositiveAlf(PositiveAlf: Int)

    @Query("UPDATE MRStage6Entity SET PositiveSub = :PositiveSub WHERE id = 1")
    suspend fun updatePositiveSub(PositiveSub: Int)

    @Query("UPDATE MRStage6Entity SET NegativeAlf = :NegativeAlf WHERE id = 1")
    suspend fun updateNegativeAlf(NegativeAlf: Int)

    @Query("UPDATE MRStage6Entity SET NegativeSub = :NegativeSub WHERE id = 1")
    suspend fun updateNegativeSub(NegativeSub: Int)

    @Query("UPDATE MRStage6Entity SET PendingAlf = :PendingAlf WHERE id = 1")
    suspend fun updatePendingAlf(PendingAlf: Int)

    @Query("UPDATE MRStage6Entity SET PendingSub = :PendingSub WHERE id = 1")
    suspend fun updatePendingSub(PendingSub: Int)

    @Query("UPDATE MRStage6Entity SET ClosedAlf = :ClosedAlf WHERE id = 1")
    suspend fun updateClosedAlf(ClosedAlf: Int)

    @Query("UPDATE MRStage6Entity SET ClosedSub = :ClosedSub WHERE id = 1")
    suspend fun updateClosedSub(ClosedSub: Int)

    @Query("SELECT * FROM MRStage6Entity")
    suspend fun getData(): List<MRStage6Entity>

    @Query("DELETE FROM MRStage6Entity")
    suspend fun deleteData()
}