package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRProjectionSelectionEntity

@Dao
interface MRProjectionSelectionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: MRProjectionSelectionEntity): Long

    @Query("UPDATE MRProjectionSelectionEntity SET location = :location WHERE id = 1")
    suspend fun updateLocation(location: String)

    @Query("UPDATE MRProjectionSelectionEntity SET projectId = :projectId, projectName = :projectName, projectCode = :projectCode WHERE id = 1")
    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String)

    @Query("SELECT * FROM MRProjectionSelectionEntity")
    suspend fun getData(): List<MRProjectionSelectionEntity>

    @Query("DELETE FROM MRProjectionSelectionEntity")
    suspend fun deleteData()
}