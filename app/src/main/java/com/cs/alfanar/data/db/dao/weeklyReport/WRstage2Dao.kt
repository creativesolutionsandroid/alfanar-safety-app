package com.cs.alfanar.data.db.dao.weeklyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage2Entity

@Dao
interface WRstage2Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: WRstage2Entity): Long

    @Query("SELECT * FROM WRstage2Entity")
    suspend fun getData(): List<WRstage2Entity>

    @Query("DELETE FROM WRstage2Entity")
    suspend fun deleteDataUsingId()

}