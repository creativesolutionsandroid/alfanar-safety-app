package com.cs.alfanar.data.db.dao.weeklyReport

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.weeklyReport.WRprojectSelectionEntity

@Dao
interface WRprojectSelectionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: WRprojectSelectionEntity): Long

    @Query("UPDATE WRprojectSelectionEntity SET location = :location WHERE id = 1")
    suspend fun updateLocation(location: String)

    @Query("UPDATE WRprojectSelectionEntity SET projectId = :projectId, projectName = :projectName, projectCode = :projectCode WHERE id = 1")
    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String)

    @Query("SELECT * FROM WRprojectSelectionEntity")
    suspend fun getData(): List<WRprojectSelectionEntity>

    @Query("DELETE FROM WRprojectSelectionEntity")
    suspend fun deleteData()

}