package com.cs.alfanar.data.db.entities.monthlyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MRStage2Entity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    val MTC: Int = 0,
    val FAC: Int = 0,
    val PdAlfanar: Int = 0,
    val PdSC: Int = 0,
    val NmAlfanar: Int = 0,
    val NmSC: Int = 0,
    val MVA: Int = 0,
    val CampInspection: Int = 0,
    val EIC: Int = 0,
    val EmergencyDrillConducte: Int = 0
)