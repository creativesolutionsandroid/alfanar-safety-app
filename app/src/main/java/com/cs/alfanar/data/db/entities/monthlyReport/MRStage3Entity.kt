package com.cs.alfanar.data.db.entities.monthlyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MRStage3Entity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    val MonthlyEHSAward: Int = 0,
    val GSLC: Int = 0,
    val Violations: Int = 0,
    val Management: Int = 0,
    val Clients: Int = 0,
    val FICAlfanar: Int = 0,
    val FICSub: Int = 0,
//    val TBTAlfanar: Int = 0,
//    val TBTSub: Int = 0,
    val SMD: Int = 0
)