package com.cs.alfanar.data.db.dao.weeklyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage4HSEEntity

@Dao
interface WRstage4HSEDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: WRstage4HSEEntity): Long

    @Query("SELECT * FROM WRstage4HSEEntity")
    suspend fun getData(): List<WRstage4HSEEntity>

    @Query("UPDATE WRstage4HSEEntity SET Name = :name WHERE id = :id")
    suspend fun updateData(name: String, id: Int)

    @Query("DELETE FROM WRstage4HSEEntity")
    suspend fun deleteDataUsingId()

}