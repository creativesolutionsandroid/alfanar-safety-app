package com.cs.alfanar.data.db.entities.monthlyReport

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MRStage4Entity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    val ManagerAlf: Int = 0,
    val ManagerSub: Int = 0,
    val EngineerAlf: Int = 0,
    val EngineerSub: Int = 0,
    val SupervisorAlf: Int = 0,
    val SupervisorSub: Int = 0,
    val OfficerAlf: Int = 0,
    val OfficerSub: Int = 0,
    val NurseAlf: Int = 0,
    val NurseSub: Int = 0,
    val FirstAidAlf: Int = 0,
    val FirstAidSub: Int = 0,
    val Note: String = ""
)