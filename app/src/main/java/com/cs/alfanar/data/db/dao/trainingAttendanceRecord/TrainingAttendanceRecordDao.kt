package com.cs.alfanar.data.db.dao.trainingAttendanceRecord

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkAdviceEntity
import com.cs.alfanar.data.db.entities.trainingAttendanceRecord.TrainingAttendanceRecordEntity

@Dao
interface TrainingAttendanceRecordDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: TrainingAttendanceRecordEntity): Long

    @Query("SELECT * FROM TrainingAttendanceRecordEntity")
    suspend fun getData(): List<TrainingAttendanceRecordEntity>

    @Query("DELETE FROM TrainingAttendanceRecordEntity")
    suspend fun deleteData()

    @Query("UPDATE TrainingAttendanceRecordEntity SET projectId = :projectId, projectName = :projectName, projectCode = :projectCode WHERE id = 1")
    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String)

    @Query("UPDATE TrainingAttendanceRecordEntity SET Date = :date WHERE id = 1")
    suspend fun updateDate(date: String)

    @Query("UPDATE TrainingAttendanceRecordEntity SET Time = :Time WHERE id = 1")
    suspend fun updateTime(Time: String)

    @Query("UPDATE TrainingAttendanceRecordEntity SET Venue = :Venue WHERE id = 1")
    suspend fun updateVenue(Venue: String)

    @Query("UPDATE TrainingAttendanceRecordEntity SET InstructorName = :InstructorName WHERE id = 1")
    suspend fun updateInstructorName(InstructorName: String)

    @Query("UPDATE TrainingAttendanceRecordEntity SET TopicOfTraining = :TopicOfTraining WHERE id = 1")
    suspend fun updateTopicOfTraining(TopicOfTraining: String)
}