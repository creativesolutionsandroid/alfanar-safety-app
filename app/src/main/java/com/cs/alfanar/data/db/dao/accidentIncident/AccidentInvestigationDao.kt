package com.cs.alfanar.data.db.dao.accidentIncident

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentIncidentEntity
import com.cs.alfanar.data.db.entities.accidentIncident.AccidentInvestigationEntity

@Dao
interface AccidentInvestigationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: AccidentInvestigationEntity): Long

    @Query("SELECT * FROM AccidentInvestigationEntity")
    suspend fun getData(): List<AccidentInvestigationEntity>

    @Query("SELECT * FROM AccidentInvestigationEntity WHERE TypeId = :TypeId")
    suspend fun getDataBasedOnTypeId(TypeId: Int): List<AccidentInvestigationEntity>

    @Query("UPDATE AccidentInvestigationEntity SET SelectName  = :SelectName, SelectId = :SelectId WHERE TypeId = :TypeId")
    suspend fun updateData(SelectId: Int, SelectName: String, TypeId: Int)

    @Query("DELETE FROM AccidentInvestigationEntity")
    suspend fun deleteData()
}