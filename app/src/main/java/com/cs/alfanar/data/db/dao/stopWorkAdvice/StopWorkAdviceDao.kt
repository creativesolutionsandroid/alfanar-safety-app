package com.cs.alfanar.data.db.dao.stopWorkAdvice

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.nearMissIncident.NearMissIncidentEntity
import com.cs.alfanar.data.db.entities.stopWorkAdvice.StopWorkAdviceEntity

@Dao
interface StopWorkAdviceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(projectData: StopWorkAdviceEntity): Long

    @Query("SELECT * FROM StopWorkAdviceEntity")
    suspend fun getData(): List<StopWorkAdviceEntity>

    @Query("DELETE FROM StopWorkAdviceEntity")
    suspend fun deleteData()

    @Query("UPDATE StopWorkAdviceEntity SET projectId = :projectId, projectName = :projectName, projectCode = :projectCode WHERE id = 1")
    suspend fun updateProject(projectId: Int, projectName: String, projectCode: String)

    @Query("UPDATE StopWorkAdviceEntity SET Date = :date WHERE id = 1")
    suspend fun updateDate(date: String)

    @Query("UPDATE StopWorkAdviceEntity SET Time = :Time WHERE id = 1")
    suspend fun updateTime(Time: String)

    @Query("UPDATE StopWorkAdviceEntity SET Location = :Location WHERE id = 1")
    suspend fun updateLocation(Location: String)

    @Query("UPDATE StopWorkAdviceEntity SET HseRepresentative = :HseRepresentative WHERE id = 1")
    suspend fun updateHseRepresentative(HseRepresentative: String)

    @Query("UPDATE StopWorkAdviceEntity SET NameOfSupervisor = :NameOfSupervisor WHERE id = 1")
    suspend fun updateNameOfSupervisor(NameOfSupervisor: String)

    @Query("UPDATE StopWorkAdviceEntity SET Designation = :Designation WHERE id = 1")
    suspend fun updateDesignation(Designation: String)

    @Query("UPDATE StopWorkAdviceEntity SET Description = :Description WHERE id = 1")
    suspend fun updateDescription(Description: String)

    @Query("UPDATE StopWorkAdviceEntity SET Image = :Image, ImageName = :ImageName WHERE id = 1")
    suspend fun updateImage(Image: String, ImageName: String)
}