package com.cs.alfanar.data.db.entities.trainingAttendanceRecord

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class TrainingAttendanceRecordEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    var projectName: String = "",
    val projectId: Int = 0,
    var projectCode: String = "",
    var Date: String = "",
    var Time: String = "",
    var Venue: String = "",
    var InstructorName: String = "",
    var TopicOfTraining: String = ""
) {
}