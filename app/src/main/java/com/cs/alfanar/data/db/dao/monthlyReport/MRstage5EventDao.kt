package com.cs.alfanar.data.db.dao.monthlyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.monthlyReport.MRstage5EventsEntity

@Dao
interface MRstage5EventDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: MRstage5EventsEntity): Long

    @Query("SELECT * FROM MRstage5EventsEntity")
    suspend fun getData(): List<MRstage5EventsEntity>

    @Query("UPDATE MRstage5EventsEntity SET Name = :name WHERE id = :id")
    suspend fun updateData(name: String, id: Int)

    @Query("DELETE FROM MRstage5EventsEntity")
    suspend fun deleteData()

}