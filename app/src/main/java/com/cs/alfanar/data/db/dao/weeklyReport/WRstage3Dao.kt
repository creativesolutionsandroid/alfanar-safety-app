package com.cs.alfanar.data.db.dao.weeklyReport

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage2Entity
import com.cs.alfanar.data.db.entities.weeklyReport.WRstage3Entity

@Dao
interface WRstage3Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(data: WRstage3Entity): Long

    @Query("SELECT * FROM WRstage3Entity")
    suspend fun getData(): List<WRstage3Entity>

    @Query("DELETE FROM WRstage3Entity")
    suspend fun deleteDataUsingId()

}