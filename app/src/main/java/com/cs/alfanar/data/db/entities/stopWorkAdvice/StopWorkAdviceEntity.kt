package com.cs.alfanar.data.db.entities.stopWorkAdvice

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class StopWorkAdviceEntity (
    @PrimaryKey(autoGenerate = false)
    val id: Int = 0,
    var projectName: String = "",
    val projectId: Int = 0,
    var projectCode: String = "",
    var Date: String = "",
    var Time: String = "",
    var Location: String = "",
    var HseRepresentative: String = "",
    var NameOfSupervisor: String = "",
    var Designation: String = "",
    var Description: String = "",
    var Image: String = "",
    var ImageName: String = ""
){
}