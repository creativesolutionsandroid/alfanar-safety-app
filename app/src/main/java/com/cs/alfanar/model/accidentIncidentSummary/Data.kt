package com.cs.alfanar.model.accidentIncidentSummary

data class Data(
    val Id: Int,
    val ProjectId: Int,
    val ProjectName: String,
    val ProjectCode: String,
    val ReportId: String,
    val ReportDate: String,
    val SubmittedBy: Int,
    val EmployeeName: String,
    val InjuredEmp: String,
    val StatusName: String,
    val ReportStatus: Int,
    val TotalRecords: Int,
    val JRemarksJson: List<JRemarksJson>
)