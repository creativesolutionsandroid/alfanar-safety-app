package com.cs.alfanar.model.dailyReport

data class GetProjectsData(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: List<Data>
)