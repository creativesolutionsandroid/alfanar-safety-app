package com.cs.alfanar.model.accidentIncident

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Master(
    val Id: Int,
    val ActionName: String,
    val ChildItem: List<ChildItem>
): Parcelable