package com.cs.alfanar.model.masterData

data class Role(
    val Id: Int,
    val NameAr: String,
    val NameEn: String
)