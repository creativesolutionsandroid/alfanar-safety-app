package com.cs.alfanar.model.nearestHospitalDirections

data class Southwest(
    val lat: Double,
    val lng: Double
)