package com.cs.alfanar.model.SafetySummaryReport

data class Data(
    val DailyCount: Int,
    val WeeklyCount: Int,
    val MonthlyCount: Int,
    val AccidentIncidentCount: Int,
    val StopWorkAdviceCount: Int,
    val NearMissCount: Int,
    val TrainingAttendanceCount: Int
)