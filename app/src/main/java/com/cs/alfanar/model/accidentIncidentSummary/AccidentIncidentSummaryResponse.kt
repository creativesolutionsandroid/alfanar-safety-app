package com.cs.alfanar.model.accidentIncidentSummary

data class AccidentIncidentSummaryResponse(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: List<Data>
)