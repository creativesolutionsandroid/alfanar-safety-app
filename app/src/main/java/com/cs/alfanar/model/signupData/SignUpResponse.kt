package com.cs.alfanar.model.signupData

data class SignUpResponse(
    val Data: Data,
    val MessageAr: String,
    val MessageEn: String,
    val Status: Boolean
)