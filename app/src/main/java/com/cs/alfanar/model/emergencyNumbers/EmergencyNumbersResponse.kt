package com.cs.alfanar.model.emergencyNumbers

data class EmergencyNumbersResponse(
    val Data: List<Data>,
    val MessageAr: String,
    val MessageEn: String,
    val Status: Boolean
)