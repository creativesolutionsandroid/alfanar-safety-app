package com.cs.alfanar.model.loginData

data class Data(
    val CountryId: Int,
    val CountryNameEn: String,
    val Email: String,
    val EmployeeName: String,
    val EmployeeNo: String,
    val Id: Int,
    val Language: String,
    val Mobile: String,
    val Nationality: String,
    val NationalityId: Int,
    val ProjectId: Int,
    val ProjectName: String,
    val RoleEn: String,
    val RoleId: Int
)