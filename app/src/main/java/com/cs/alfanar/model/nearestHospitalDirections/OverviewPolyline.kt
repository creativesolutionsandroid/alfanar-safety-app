package com.cs.alfanar.model.nearestHospitalDirections

data class OverviewPolyline(
    val points: String
)