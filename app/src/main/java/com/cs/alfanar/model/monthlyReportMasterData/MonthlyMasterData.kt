package com.cs.alfanar.model.monthlyReportMasterData

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class MonthlyMasterData(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: Data
): Parcelable