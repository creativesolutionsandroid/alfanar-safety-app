package com.cs.alfanar.model.weeklyReport

data class WeeklyProjectsData(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: Data
)