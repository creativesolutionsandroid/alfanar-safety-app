package com.cs.alfanar.model.masterData

data class Project(
    val Id: Int,
    val ProjectCode: String,
    val ProjectNameEn: String,
    val CountryId: Int,
    val CityId: Int
)