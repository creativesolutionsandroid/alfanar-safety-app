package com.cs.alfanar.model.nearestHospital

data class Southwest(
    val lat: Double,
    val lng: Double
)