package com.cs.alfanar.model.nearestHospitalDirections

data class EndLocationX(
    val lat: Double,
    val lng: Double
)