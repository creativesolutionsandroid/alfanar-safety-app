package com.cs.alfanar.model.nearestHospitalDirections

data class Duration(
    val text: String,
    val value: Int
)