package com.cs.alfanar.model.monthlyReportMasterData

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class MonthlyEHSAct(
    val SiteMgmntSafetyWalk: Int,
    val SafetyAudits: Int,
    val HighMgmntWalk: Int,
    val AdminFieldSafetyAudit: Int
): Parcelable