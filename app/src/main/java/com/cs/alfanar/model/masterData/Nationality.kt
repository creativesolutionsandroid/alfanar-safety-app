package com.cs.alfanar.model.masterData

data class Nationality(
    val Id: Int,
    val NameAr: String,
    val NameEn: String
)