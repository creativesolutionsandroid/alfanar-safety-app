package com.cs.alfanar.model.masterData

data class MasterDataResponse(
    val Data: Data,
    val MessageAr: String,
    val MessageEn: String,
    val Status: Boolean
)