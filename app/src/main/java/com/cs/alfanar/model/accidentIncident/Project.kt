package com.cs.alfanar.model.accidentIncident

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Project(
    val ProjectId: Int,
    val ProjectCode: String,
    val ProjectNameEn: String,
    val ProjectNameAr: String,
    val CountryId: Int,
    val CityId: Int
): Parcelable