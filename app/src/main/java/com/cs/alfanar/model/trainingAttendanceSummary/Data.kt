package com.cs.alfanar.model.trainingAttendanceSummary

data class Data(
    val Id: Int,
    val ProjectId: Int,
    val ProjectName: String,
    val ProjectCode: String,
    val ReportId: String,
    val ReportDate: String,
    val Venue: String,
    val InstructorName: String,
    val TopicName: String,
    val SubmittedBy: Int,
    val EmployeeName: String,
    val StatusName: String,
    val ReportStatus: Int,
    val TotalRecords: Int,
    val JRemarksJson: List<JRemarksJson>
)