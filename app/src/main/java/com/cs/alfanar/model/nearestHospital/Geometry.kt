package com.cs.alfanar.model.nearestHospital

data class Geometry(
    val location: Location,
    val viewport: Viewport
)