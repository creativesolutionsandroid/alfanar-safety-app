package com.cs.alfanar.model.resetPasswordData

data class ResetPasswordResponse(
    val MessageAr: String,
    val MessageEn: String,
    val Status: Boolean
)