package com.cs.alfanar.model.MonthlyReportDetails

data class JRemarksJson(
    val Remarks: String,
    val EmployeeName: String,
    val RemarksDate: String
)