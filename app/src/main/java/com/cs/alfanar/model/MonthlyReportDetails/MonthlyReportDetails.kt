package com.cs.alfanar.model.MonthlyReportDetails

data class MonthlyReportDetails(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: List<Data>
)