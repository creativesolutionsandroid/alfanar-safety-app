package com.cs.alfanar.model.documentCenterItemData

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Data(
    val DocumentCopy: String,
    val FileNameAr: String,
    val FileNameEn: String,
    val Id: Int,
    val TypeId: Int,
    val TypeName: String
) : Parcelable