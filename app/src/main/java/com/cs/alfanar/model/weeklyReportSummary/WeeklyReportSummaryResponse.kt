package com.cs.alfanar.model.weeklyReportSummary

data class WeeklyReportSummaryResponse(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: List<Data>
)