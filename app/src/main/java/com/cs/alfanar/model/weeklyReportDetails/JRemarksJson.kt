package com.cs.alfanar.model.weeklyReportDetails

data class JRemarksJson(
    val Remarks: String,
    val EmployeeName: String,
    val RemarksDate: String
)