package com.cs.alfanar.model.monthlyReportMasterData

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class MonthlyCummulative(
    val SafeManHoursWorked: Int,
    val CummNoFirstAid: Int,
    val TotalManHours: Int,
    val ManPowerAvgDWAlfanar: Int,
    val ManPowerAvgDWSubCont: Int,
    val ManPowerAvgIDOAlfanar: Int,
    val ManPowerAvgIDOSubCont: Int,
    val FatalCase: Int,
    val LostWorkdayCase: Int,
    val MedicalTCase: Int,
    val PropertyDamageAlfanar: Int,
    val PropertyDamageSubCont: Int,
    val NearMissAlfanar: Int,
    val NearMissSubCont: Int,
    val MotorVehicleAcc: Int,
    val NoCampInspection: Int,
    val EnvrnIncidentCase: Int,
    val EmergencyDrill: Int,
    val MonthlyEHSAward: Int,
    val GenSickLeaveCase: Int,
    val ViolationsRaisedClient: Int,
    val SiteVisitMgAlfanar: Int,
    val SiteVisitMgSubCont: Int,
    val FireIncidentCase: Int,
    val FireIncidentCaseSubCont: Int,
    val SafeManDays: Int
): Parcelable