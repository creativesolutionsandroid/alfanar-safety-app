package com.cs.alfanar.model.alertNotification

data class Data(
    val Icon: String,
    val Id: Int,
    val TypeNameAr: Any,
    val TypeNameEn: String
)