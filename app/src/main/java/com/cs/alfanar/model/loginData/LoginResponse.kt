package com.cs.alfanar.model.loginData

data class LoginResponse(
    val Data: Data,
    val MessageAr: String,
    val MessageEn: String,
    val Status: Boolean
)