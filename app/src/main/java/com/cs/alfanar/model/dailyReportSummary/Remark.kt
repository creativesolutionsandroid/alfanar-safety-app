package com.cs.alfanar.model.dailyReportSummary

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Remark(
    val Remarks: String,
    val EmployeeName: String
) : Parcelable