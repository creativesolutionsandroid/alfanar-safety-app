package com.cs.alfanar.model.emergencyNumbers

data class Data(
    val CountryId: Int,
    val CountryNameEn: String,
    val DeptNameAr: String,
    val DeptNameEn: String,
    val Icon: String,
    val Id: Int,
    val Number: String
)