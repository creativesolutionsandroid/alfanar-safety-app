package com.cs.alfanar.model.weeklyReportDetails

data class WeeklyReportDetailsResponse(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: List<Data>
)