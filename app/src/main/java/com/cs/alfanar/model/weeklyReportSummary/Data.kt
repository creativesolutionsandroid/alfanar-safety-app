package com.cs.alfanar.model.weeklyReportSummary

data class Data(
    val Id: Int,
    val ProjectId: Int,
    val ProjectName: String,
    val ProjectCode: String,
    val Location: String,
    val ReportId: String,
    val ReportDate: String,
    val SubmittedBy: Int,
    val EmployeeName: String,
    val StatusName: String,
    val ReportStatus: Int,
    val TotalRecords: Int
)