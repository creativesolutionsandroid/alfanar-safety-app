package com.cs.alfanar.model.weeklyReport

data class Observation(
    val ProjectId: Int,
    val OpenCount: Int,
    val CloseCount: Int,
    val Percentage: Double
)