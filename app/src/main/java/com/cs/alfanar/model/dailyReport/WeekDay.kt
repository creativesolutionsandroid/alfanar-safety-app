package com.cs.alfanar.model.dailyReport

data class WeekDay(
    val WeekNo: String,
    val WeekDate: String,
    val DayName: String,
    val IsReported: Int,
    val IsToday: Int
)