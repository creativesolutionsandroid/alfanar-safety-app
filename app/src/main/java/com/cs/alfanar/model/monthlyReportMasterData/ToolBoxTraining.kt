package com.cs.alfanar.model.monthlyReportMasterData

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class ToolBoxTraining(
    val TopicName: String,
    val Duration: Int,
    val TrainingDate: String,
    val NoAttendee: Int,
    val ConductedBy: String,
    val TalkType: Int
): Parcelable