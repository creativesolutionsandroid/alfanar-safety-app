package com.cs.alfanar.model.nearestHospitalDirections

data class Polyline(
    val points: String
)