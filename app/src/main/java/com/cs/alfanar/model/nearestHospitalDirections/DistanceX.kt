package com.cs.alfanar.model.nearestHospitalDirections

data class DistanceX(
    val text: String,
    val value: Int
)