package com.cs.alfanar.model.documentCenterItemData

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class DocumentCenterItemResponse(
    val Data: List<Data>,
    val MessageAr: String,
    val MessageEn: String,
    val Status: Boolean
) : Parcelable