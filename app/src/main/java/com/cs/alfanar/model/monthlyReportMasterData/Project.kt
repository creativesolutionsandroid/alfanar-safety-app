package com.cs.alfanar.model.monthlyReportMasterData

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Project(
    val ProjectId: Int,
    val ProjectCode: String,
    val ProjectNameEn: String,
    val ToolBoxTraining: List<ToolBoxTraining>,
    val MonthlyCummulative: List<MonthlyCummulative>,
    val MonthlyObservFinding: List<MonthlyObservFinding>,
    val MonthlyEHSAct: List<MonthlyEHSAct>,
    val ReportSubmissionTime: String,
    val ReportSubmissionDay: Int
): Parcelable