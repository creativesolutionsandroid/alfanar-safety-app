package com.cs.alfanar.model.weeklyReport

data class Stage1Data(
    val id: Int,
    val name: String,
    val isTextField: Boolean
) {
}