package com.cs.alfanar.model.nearestHospital

data class Northeast(
    val lat: Double,
    val lng: Double
)