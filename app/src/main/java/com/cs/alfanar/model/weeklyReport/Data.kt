package com.cs.alfanar.model.weeklyReport

data class Data(
    val Projects: List<Project>,
    val Checklist: List<Checklist>,
    val Observation: List<Observation>
)