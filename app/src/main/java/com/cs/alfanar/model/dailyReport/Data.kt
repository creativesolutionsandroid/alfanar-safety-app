package com.cs.alfanar.model.dailyReport

data class Data(
    val ProjectId: Int,
    val ProjectCode: String,
    val ProjectNameEn: String,
    val ProjectNameAr: String,
    val CountryId: Int,
    val CityId: Int,
    val MaxReportTime: String,
    val WeekDays: List<WeekDay>
)