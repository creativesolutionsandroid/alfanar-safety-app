package com.cs.alfanar.model.nearestHospitalDirections

data class Bounds(
    val northeast: Northeast,
    val southwest: Southwest
)