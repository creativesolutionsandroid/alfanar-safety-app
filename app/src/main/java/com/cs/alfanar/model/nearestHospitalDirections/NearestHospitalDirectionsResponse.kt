package com.cs.alfanar.model.nearestHospitalDirections

data class NearestHospitalDirectionsResponse(
    val routes: List<Route>,
    val status: String
)