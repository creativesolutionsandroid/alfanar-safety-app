package com.cs.alfanar.model.MonthlyReportDetails


data class Data(
    val Id: Int,
    val ProjectId: Int,
    val ProjectName: String,
    val ProjectCode: String,
    val Location: String,
    val ReportId: String,
    val ReportDate: String,
    val SubmittedBy: Int,
    val ReportStatus: Int,
    val IsAcknowledge: Boolean,
    val AcknowledgedBy: Any,
    val AcknowledgedDate: String,
    val MonthlyDetailsJSON: String,
    val MonthlySafetyTeamAlfanarJSON: String,
    val MonthlySafetyTeamSubConJSON: String,
    val NarrationJSON: String,
    val SafetyHighlightsJSON: String,
    val MonthlyOtherEHSJSON: String,
    val ToolBoxTrainingJSON: String,
    val MonthlyObservationAlfanarJSON: String,
    val MonthlyObservationSubContJSON: String,
    val MonthlyEnvironmentalJSON: String,
    val RemarksJson: String,
    val JRemarksJson: List<JRemarksJson>
)