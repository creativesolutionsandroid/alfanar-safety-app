package com.cs.alfanar.model.trainingAttendanceSummary

data class JRemarksJson(
    val Remarks: String,
    val EmployeeName: String,
    val RemarksDate: String
)