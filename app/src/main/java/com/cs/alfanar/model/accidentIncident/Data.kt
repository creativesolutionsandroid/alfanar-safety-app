package com.cs.alfanar.model.accidentIncident

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class Data(
    val MasterList: List<Master>,
    val ProjectList: List<Project>
): Parcelable