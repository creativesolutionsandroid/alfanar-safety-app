package com.cs.alfanar.model.forgotPasswordData

data class
ForgotPasswordResponse(
    val Data: Data,
    val MessageAr: String,
    val MessageEn: String,
    val Status: Boolean
)