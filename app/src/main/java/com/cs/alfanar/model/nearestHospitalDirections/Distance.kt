package com.cs.alfanar.model.nearestHospitalDirections

data class Distance(
    val text: String,
    val value: Int
)