package com.cs.alfanar.model.nearestHospitalDirections

data class StartLocation(
    val lat: Double,
    val lng: Double
)