package com.cs.alfanar.model.nearestHospital

data class Result(
    val business_status: String,
    val geometry: Geometry,
    val name: String,
    val place_id: String,
    val reference: String,
    val scope: String,
    val vicinity: String
)