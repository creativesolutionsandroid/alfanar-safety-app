package com.cs.alfanar.model.nearestHospitalDirections

data class Northeast(
    val lat: Double,
    val lng: Double
)