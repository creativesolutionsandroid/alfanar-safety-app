package com.cs.alfanar.model.weeklyReport

data class Project(
    val ProjectId: Int,
    val ProjectCode: String,
    val ProjectNameEn: String,
    val ProjectNameAr: String,
    val CountryId: Int,
    val CityId: Int,
    val MaxReportTime: String,
    val MaxReportWeekDay: Int,
    val WeekDays: List<WeekDay>
)