package com.cs.alfanar.model.alertNotificationInbox

data class Data(
    val Id: Int,
    val AlertMessage: String,
    val Image: String,
    val AlertTypeId: Int,
    val AlertTypeName: String,
    val ProjectId: Int,
    val ProjectCode: String,
    val ProjectNameEn: String,
    val SentBy: Int,
    val EmployeeName: String,
    val CreatedOn: String,
    val IsAcknowledged: Boolean,
    val TotalRecords: Int
)