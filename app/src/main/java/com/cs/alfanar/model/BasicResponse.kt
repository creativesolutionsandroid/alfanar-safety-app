package com.cs.alfanar.model

data class BasicResponse(
    val MessageAr: String,
    val MessageEn: String,
    val Status: Boolean
)