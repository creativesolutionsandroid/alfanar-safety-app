package com.cs.alfanar.model.nearestHospitalDirections

data class EndLocation(
    val lat: Double,
    val lng: Double
)