package com.cs.alfanar.model.documentCenterListData


import android.annotation.SuppressLint
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class Data(
    val Counts: Int,
    val Id: Int,
    val TypeName: String
) : Parcelable