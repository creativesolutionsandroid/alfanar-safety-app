package com.cs.alfanar.model.dailyReportSummary

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@SuppressLint("ParcelCreator")
@Parcelize
data class Data(
    val Id: Int,
    val ProjectId: Int,
    val ProjectCode: String,
    val TotalManPower: Int,
    val Location: String,
    val Activity: String,
    val UnSafeAct: String,
    val CorrectionAction: String,
    val HSEOfficerAction: String,
    val ActivityIncharge: String,
    val ReportId: String,
    val ReportDate: String,
    val SubmittedBy: Int,
    var ReportStatus: Int,
    var ReportStatusEn: String,
    val CreatedOn: String,
    val Remarks: List<Remark>
) : Parcelable