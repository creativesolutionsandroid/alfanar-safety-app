package com.cs.alfanar.model.nearestHospitalDirections

data class StartLocationX(
    val lat: Double,
    val lng: Double
)