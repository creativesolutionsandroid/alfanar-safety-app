package com.cs.alfanar.model.nearMissIncidentSummary

data class NearMissIncidentSummary(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: List<Data>
)