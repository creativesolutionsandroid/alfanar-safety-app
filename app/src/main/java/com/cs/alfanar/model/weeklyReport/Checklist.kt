package com.cs.alfanar.model.weeklyReport

data class Checklist(
    val Id: Int,
    val CheckListNameEn: String,
    val CheckListNameAr: Any,
    val ReferenceNo: String
)