package com.cs.alfanar.model.SafetySummaryReport

data class SafetySummaryReponse(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: List<Data>
)