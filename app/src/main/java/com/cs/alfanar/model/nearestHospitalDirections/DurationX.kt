package com.cs.alfanar.model.nearestHospitalDirections

data class DurationX(
    val text: String,
    val value: Int
)