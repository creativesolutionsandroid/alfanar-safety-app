package com.cs.alfanar.model.dailyReport

data class CalendarData(
    val date: String,
    val week: String,
    val isToday: Boolean,
    val isReported: Boolean,
    val isFutureDate: Boolean
)