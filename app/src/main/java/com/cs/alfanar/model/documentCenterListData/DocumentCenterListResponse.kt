package com.cs.alfanar.model.documentCenterListData


import android.annotation.SuppressLint
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@SuppressLint("ParcelCreator")
@Parcelize
data class DocumentCenterListResponse(
    val Data: List<Data>,
    val MessageAr: String,
    val MessageEn: String,
    val Status: Boolean
) : Parcelable