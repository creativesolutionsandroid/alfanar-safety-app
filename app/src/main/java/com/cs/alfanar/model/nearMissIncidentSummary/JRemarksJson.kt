package com.cs.alfanar.model.nearMissIncidentSummary

data class JRemarksJson(
    val Remarks: String,
    val EmployeeName: String,
    val RemarksDate: String
)