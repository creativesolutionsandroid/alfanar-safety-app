package com.cs.alfanar.model.signupData

data class Data(
    val Email: String,
    val EmployeeName: String,
    val EmployeeNo: String,
    val Id: Int,
    val Language: String,
    val Mobile: String
)