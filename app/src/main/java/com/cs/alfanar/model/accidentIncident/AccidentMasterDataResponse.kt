package com.cs.alfanar.model.accidentIncident

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class AccidentMasterDataResponse(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: Data
): Parcelable