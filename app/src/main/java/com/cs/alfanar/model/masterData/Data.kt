package com.cs.alfanar.model.masterData

data class Data(
    val Nationalities: List<Nationality>,
    val ProjectList: List<Project>,
    val RoleList: List<Role>
)