package com.cs.alfanar.model.alertNotification

data class AlertNotificationTypeResponse(
    val Data: List<Data>,
    val MessageAr: String,
    val MessageEn: String,
    val Status: Boolean
)