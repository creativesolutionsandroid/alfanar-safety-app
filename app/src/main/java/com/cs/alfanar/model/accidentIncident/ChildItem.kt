package com.cs.alfanar.model.accidentIncident

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class ChildItem(
    val Id: Int,
    val ParentId: Int,
    val ActionName: String
): Parcelable