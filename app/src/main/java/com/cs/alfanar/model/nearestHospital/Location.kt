package com.cs.alfanar.model.nearestHospital

data class Location(
    val lat: Double,
    val lng: Double
)