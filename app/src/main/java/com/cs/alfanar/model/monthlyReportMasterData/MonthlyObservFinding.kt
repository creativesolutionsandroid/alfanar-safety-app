package com.cs.alfanar.model.monthlyReportMasterData

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
data class MonthlyObservFinding(
    val ObsPositive: Int,
    val ObsNegative: Int,
    val RemarksClosed: Int,
    val RemarksPending: Int
): Parcelable