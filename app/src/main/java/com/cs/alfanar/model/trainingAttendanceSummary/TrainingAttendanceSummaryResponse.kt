package com.cs.alfanar.model.trainingAttendanceSummary

data class TrainingAttendanceSummaryResponse(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: List<Data>
)