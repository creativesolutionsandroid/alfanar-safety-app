package com.cs.alfanar.model.MonthlyReportSummary

data class MonthlyReportSummary(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: List<Data>
)