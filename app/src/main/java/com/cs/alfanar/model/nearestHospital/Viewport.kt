package com.cs.alfanar.model.nearestHospital

data class Viewport(
    val northeast: Northeast,
    val southwest: Southwest
)