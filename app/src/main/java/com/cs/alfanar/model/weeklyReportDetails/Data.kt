package com.cs.alfanar.model.weeklyReportDetails

data class Data(
    val Id: Int,
    val ProjectId: Int,
    val ProjectName: String,
    val ProjectCode: String,
    val Location: String,
    val ReportId: String,
    val ReportDate: String,
    val SubmittedBy: Int,
    val ReportStatus: Int,
    val AlfanarProjectDtlsJSON: String,
    val SubCtrProjectDtlsJSON: String,
    val DailyToolBoxJSON: String,
    val TrainingsJSON: String,
    val WeeklyCheklistJSON: String,
    val OtherHealthJSON: String,
    val HSEGoodPracJSON: String,
    val VulnerableAreaJSON: String,
    val RemarksJson: String,
    val JRemarksJson: List<JRemarksJson>
)