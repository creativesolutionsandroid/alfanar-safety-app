package com.cs.alfanar.model.nearestHospital

data class NearestHospitalResponse(
    val next_page_token: String,
    val results: List<Result>,
    val status: String
)