package com.cs.alfanar.model.alertNotificationInbox

data class AlertNotificationInboxResponse(
    val Status: Boolean,
    val MessageEn: String,
    val MessageAr: String,
    val Data: List<Data>
)