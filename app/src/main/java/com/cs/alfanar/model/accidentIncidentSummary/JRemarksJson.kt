package com.cs.alfanar.model.accidentIncidentSummary

data class JRemarksJson(
    val Remarks: String,
    val EmployeeName: String,
    val RemarksDate: String
)